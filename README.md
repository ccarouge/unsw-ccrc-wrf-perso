# WRF
WRF ported on NCI machine

Installing
==========
To use at NCI, run:
```
     git clone https://<user>@bitbucket.org/dargueso/unsw-ccrc-wrf.git
```
or
```
    git clone git@bitbucket.org:dargueso/unsw-ccrc-wrf.git      
```
where user is your user name on BitBucket

Versions currently available:
=============================
* V3.6 (tag commit)
* V3.6.0.5 (in tar file)
* V3.9.1.1 (head of master branch)

Building WRF (ARW)
==================
The compilation script sources the environment from the file unsw-ccrc-wrf/build.env. Please check this file and modify any module as you'd like before starting any compilation. Also, it is a good idea to source this file in your running script in order to use the correct version of openmpi.
 
Go to the WRFV3/ subdirectory, run:
```
cd /short/$PROJECT/$USER/unsw-ccrc-wrf/WRFV3/
```
There is a *run_compile* script to help you configure and compile WRF. This script by default will compile the WRFV3 software for a real experiment, with speed optimisations for the normal and express queues and with basic nesting. There is a range of command-line arguments accepted by the script. These are the command-line arguments accepted by the *configure* script, an option to clean everything prior to compilation, the choice of compilation flags and nesting type for the *configure* script and finally the compilation option for the *compile* script. You can access a full list and description by running:
```
./run_compile -h
```
To configure and compile WRFV3 with OpenMP, you simply run:
```
./run_compile -a 8
```
The configure step will be run interactively with the output on your screen, then the compilation step will be submitted to the express queue automatically.

After a successful build, one should see the following executable files for WRF (the ARW core):
```
[abc123@raijin3 WRFV3]$ ls -l main/*.exe

-rwx------ 1 abc123 wrf 35559897 Aug 22 17:14 main/ndown.exe
-rwx------ 1 abc123 wrf 35471615 Aug 22 17:14 main/real.exe
-rwx------ 1 abc123 wrf 35081246 Aug 22 17:14 main/tc.exe
-rwx------ 1 abc123 wrf 38763058 Aug 22 17:13 main/wrf.exe
```

**Tip**: if you are only running an idealised case and you are getting tired of specifying the option on the command-line, you can:
1. change the default value of compile_case at the start of the run_compile script OR
2. create an alias for the command in your .bashrc or .cshrc file (it is a good idea to change the name of the command for the alias so it is clear it is an alias if you need help later on):
bash syntax:
```
alias my_compile='run_compile --compile_case <replace with the compile option here>'
```
csh/tcsh syntax:
```
alias my_compile 'run_compile --compile_case <replace with the compile option here>'
```
Building WPS
============
Go to the WPS/ subdirectory, run:
```
cd /short/$PROJECT/$USER/WRF/WPS/
```
There is another *run_compile* script to help with configuring and compiling WPS. You can again choose the configure input from the command-line option as well as to clean a previous compilation of WPS. The default is to compile with GRIB2 support and a distributed memory code. See the inline help for *run_compile* for a full explanation:
```
./run_compile -h
```

To configure and compile WPS with the default options, you simply run:
```
./run_compile
```
A file configure.wps should be created after configuring has completed.

After a successful build, one should see the following executable files:
```
[abc123@raijin3 WPS]$ ls -l *.exe

lrwxrwxrwx 1 abc123 wrf 23 Aug 23 10:19 geogrid.exe -> geogrid/src/geogrid.exe
lrwxrwxrwx 1 abc123 wrf 23 Aug 23 10:20 metgrid.exe -> metgrid/src/metgrid.exe
lrwxrwxrwx 1 abc123 wrf 21 Aug 23 10:20 ungrib.exe -> ungrib/src/ungrib.exe

[abc123@raijin3 WPS]$ ls -l */src/*.exe

-rwx------ 1 abc123 wrf 3412782 Aug 23 10:19 geogrid/src/geogrid.exe
-rwx------ 1 abc123 wrf 3206974 Aug 23 10:20 metgrid/src/metgrid.exe
-rwx------ 1 abc123 wrf 1209622 Aug 23 10:20 ungrib/src/g1print.exe
-rwx------ 1 abc123 wrf 1407507 Aug 23 10:20 ungrib/src/g2print.exe
-rwx------ 1 abc123 wrf 2169010 Aug 23 10:20 ungrib/src/ungrib.exe
-rwx------ 1 abc123 wrf 1184660 Aug 23 10:20 util/src/avg_tsfc.exe
-rwx------ 1 abc123 wrf 1250303 Aug 23 10:20 util/src/calc_ecmwf_p.exe
-rwx------ 1 abc123 wrf 1217842 Aug 23 10:20 util/src/height_ukmo.exe
-rwx------ 1 abc123 wrf 953821 Aug 23 10:20 util/src/int2nc.exe
-rwx------ 1 abc123 wrf 1118699 Aug 23 10:20 util/src/mod_levs.exe
-rwx------ 1 abc123 wrf 3704627 Aug 23 10:20 util/src/plotfmt.exe
-rwx------ 1 abc123 wrf 3409302 Aug 23 10:20 util/src/plotgrids.exe
-rwx------ 1 abc123 wrf 916613 Aug 23 10:20 util/src/rd_intermediate.exe
```
Note that there are some warnings in the build output like "warning: overriding commands for target '.c.o'." These are harmless and can be ignored.

