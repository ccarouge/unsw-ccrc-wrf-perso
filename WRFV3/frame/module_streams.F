!WRF:DRIVER_LAYER:IO_STREAMS
MODULE module_streams


! registry-generated switch parameters, alarms

#include "switches_and_alarms.inc"

  INTEGER, PARAMETER :: first_history     = history_only
  INTEGER, PARAMETER :: last_history      = history_only+MAX_HISTORY-1
  INTEGER, PARAMETER :: first_auxhist     = auxhist1_only
  INTEGER, PARAMETER :: last_auxhist      = last_history
  INTEGER, PARAMETER :: first_input       = input_only
  INTEGER, PARAMETER :: last_input        = input_only+MAX_HISTORY-1
  INTEGER, PARAMETER :: first_auxinput    = auxinput1_only
  INTEGER, PARAMETER :: last_auxinput     = last_input
  INTEGER, PARAMETER :: first_stream      = first_history
  INTEGER, PARAMETER :: last_stream       = last_input
  INTEGER, PARAMETER :: restart_only      = 2*(MAX_HISTORY)+1
  INTEGER, PARAMETER :: boundary_only     = 2*(MAX_HISTORY)+2

  INTEGER, PARAMETER :: RESTART_ALARM     = restart_only
  INTEGER, PARAMETER :: BOUNDARY_ALARM    = boundary_only

  INTEGER, PARAMETER :: INPUTOUT_ALARM              = 2*(MAX_HISTORY)+3       ! for outputing input (e.g. for 3dvar)
  INTEGER, PARAMETER :: ALARM_SUBTIME               = 2*(MAX_HISTORY)+4
  INTEGER, PARAMETER :: COMPUTE_VORTEX_CENTER_ALARM = 2*(MAX_HISTORY)+5

  INTEGER, PARAMETER :: MAX_WRF_ALARMS    = COMPUTE_VORTEX_CENTER_ALARM  ! WARNING:  MAX_WRF_ALARMS must be
                                                                         ! large enough to include all of
                                                                         ! the alarms declared above.

!!! dsr900: Put the buffer for MPI_Buffer_attach here for now, we can find a better
!!!         place and do it all properly later.

  INTEGER, ALLOCATABLE, DIMENSION(:) :: bsend_buff
  INTEGER, PARAMETER                 :: BYTES_IN_INT = 4    
  INTEGER, PARAMETER                 :: BSEND_BUFF_SIZE = 26214400 ! 100 or 200MB
                                                                         


  CONTAINS
    SUBROUTINE init_module_streams
    END SUBROUTINE init_module_streams
    
    SUBROUTINE reattach_mpi_buffer( attached )
      use mpi
      implicit none
      LOGICAL, INTENT(out) :: attached
      INTEGER :: ret_size
      INTEGER :: ierr
      write(0,*)'detatching MPI_Buffer'
      CALL MPI_Buffer_detach( bsend_buff, ret_size , ierr )
      write(0,*)'reattaching MPI_Buffer'
      CALL MPI_Buffer_attach( bsend_buff, BSEND_BUFF_SIZE * BYTES_IN_INT , ierr )
      write(0,*)'done.'
      attached = .true.
    END SUBROUTINE reattach_mpi_buffer

END MODULE module_streams
