#!/usr/bin/env python
## 
# python script to check time and dates in post-processed files
#
# D. Argueso, CCRC-UNSW, August 2012
#
## g.e. # ./check_timerecords.py -f /srv/ccrc/data18/z3393242/studies/NARCliMGreatSydney/postprocess/nnrp/Std_LU/1990-2010/postPROJ/ -p CCRC_NARCliM_Sydney_
# 
from Scientific.IO.NetCDF import NetCDFFile
import glob
from os.path import basename
from datetime import date
from optparse import OptionParser

### Options

parser = OptionParser()

parser.add_option("-f", "--folder", dest="folder",
                  help="folder with the files", metavar="FOLDERNAME")
parser.add_option("-p", "--pattern", dest="pattern", 
                  help="pattern of files", metavar="LABEL")

(opts, args) = parser.parse_args()

#######
path = opts.folder
patt = opts.pattern

##path="/srv/ccrc/data18/z3393242/studies/NARCliMGreatSydney/postprocess/nnrp/Std_LU/1990-2010/postPROJ/"
##patt="CCRC_NARCliM_Sydney_"

print "---------Checking MONTHLY files-------------"
timepatt="MOM"

filesm=glob.glob('%s%s%s_*.nc' % (path,patt,timepatt))

m=0
for fname in filesm:
	f=NetCDFFile(fname,'r' )
	var = f.variables['time']
	ntimes=var.shape


	syear=int(fname.split('_%s_' % (timepatt))[1][0:4])
	eyear=int(fname.split('_%s_' % (timepatt))[1][5:9])
	nmonths=(eyear-syear+1)*12


	if ntimes[0] == nmonths:
		pass
		#print 'Complete file: %s' % basename(fname)
	else:
		print 'INCOMPLETE %s' % basename(fname)
		print 'This file lacks %d records' % (nmonths-ntimes[0])
		m=m+1
		f.close()

print "There are %d incomplete monthly files in %s" % (m,path)



print "---------Checking DAILY files-------------"
timepatt="DAM"
filesd=glob.glob('%s%s%s_*.nc' % (path,patt,timepatt))

d=0
for fname in filesd:
	f=NetCDFFile(fname,'r' )
	var = f.variables['time']
	ntimes=var.shape
	
	syear=int(fname.split('_%s_' % (timepatt))[1][0:4])
	eyear=int(fname.split('_%s_' % (timepatt))[1][5:9])
	
	period=date(eyear,12,31)-date(syear,1,1)
	
	ndays=period.days+1
	
	
	if ntimes[0] == ndays:
		pass
		#print 'Complete file: %s' % basename(fname)
	else:
		print 'INCOMPLETE %s' % basename(fname)
		print 'This file lacks %d records' % (ndays-ntimes[0])
		d=d+1
		f.close()
	
print "There are %d incomplete daily files in %s" % (d,path)	
	
	
print "---------Checking 3-HOURLY files-------------"
timepatt="03H"
filesd=glob.glob('%s%s%s_*.nc' % (path,patt,timepatt))

s3=0
for fname in filesd:
	f=NetCDFFile(fname,'r' )
	var = f.variables['time']
	ntimes=var.shape
	
	syear=int(fname.split('_%s_' % (timepatt))[1][0:4])
	eyear=int(fname.split('_%s_' % (timepatt))[1][5:9])
	
	period=date(eyear,12,31)-date(syear,1,1)
	
	nsteps=(period.days+1)*8
	
	
	if ntimes[0] == nsteps:
		pass
		#print 'Complete file: %s' % basename(fname)
	else:
		print 'INCOMPLETE %s' % basename(fname)
		print 'This file lacks %d records' % (nsteps-ntimes[0])
		s3=s3+1
		f.close()
	
print "There are %d incomplete 3-hourly files in %s" % (s3,path)

print "---------Checking HOURLY files-------------"
timepatt="01H"
filesd=glob.glob('%s%s%s_*.nc' % (path,patt,timepatt))

s1=0
for fname in filesd:
	f=NetCDFFile(fname,'r' )
	var = f.variables['time']
	ntimes=var.shape
	
	syear=int(fname.split('_%s_' % (timepatt))[1][0:4])
	eyear=int(fname.split('_%s_' % (timepatt))[1][5:9])
	
	period=date(eyear,12,31)-date(syear,1,1)
	
	nsteps=(period.days+1)*24
	
	
	if ntimes[0] == nsteps:
		pass
		#print 'Complete file: %s' % basename(fname)
	else:
		print 'INCOMPLETE %s' % basename(fname)
		print 'This file lacks %d records' % (nsteps-ntimes[0])
		s1=s1+1
		f.close()
	
print "There are %d incomplete hourly files in %s" % (s1,path)	
