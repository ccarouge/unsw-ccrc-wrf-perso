import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
from scipy.stats.stats import pearsonr
import nc_var_tools as ncvar
import subprocess as sub
import datetime as dt

errormsg='check_WRF_file.py: ERROR -- error -- ERROR -- error'
## g.e. # check_WRF_file.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfhrly_d01_1997-02-01_00:00:00 -v RAINNC -t 1 -e 0.,120.

def UseFormat(val,form):
    """ Function that produces the output giving a format specification
    """
    Lform=len(form)
    fwidth=form[0:Lform-1]
    ftype=form[Lform-1:Lform]
    if form[0:1] == '0':
        ffill=0
    else:
        ffill=''

    if ftype == 'c':
        fval=str(val)
    elif ftype == 'd':
        fval=int(val)
    elif ftype == 'f':
        fval=float(val)
    else:
        print errmsg
        print '  "' + ftype + '" format specification not ready!!!'
        quit(-1)

##    print ffill, fwidth, ftype

    return '{num:{fill}{width}{type}}'.format(num=fval, fill=ffill, width=fwidth, type=ftype)

def list2string(lst, char):
    """ Function to generate a string chain from a list with a given character between values
    >>> list2string([2.3, 3.56, 1.3, 23.2, 1231.1],'--')
    2.3--3.56--1.3--23.2--1231.1
    """
    stringchain=''
    Nlst=len(lst)

    for iv in range(Nlst):
        stringchain=stringchain + str(lst[iv]) + char

    stringchain=stringchain + str(lst[Nlst-1])

    return stringchain

def WRFtimes_datetime(ncfile):
    """ Provide date/time array from a file with a series of WRF time variable
    """
    import datetime as dt

    times = ncfile.variables['Times']
    timevals = times[:]
    dimt = times.shape[0]

    realdates = np.zeros((dimt, 6), dtype=int)

    for it in range(dimt):
        timev = list2string(timevals[it],'')
        realdates[it,0] = int(timev.split('-')[0]) 
        realdates[it,1] = int(timev.split('-')[1]) 
        realdates[it,2] = int(timev.split('-')[2].split('_')[0]) 
        realdates[it,3] = int(str(timev.split('_')[1]).split(':')[0]) 
        realdates[it,4] = int(timev.split(':')[1])
        realdates[it,5] = int(timev.split(':')[2]) 

    return realdates    

class variable_inf(object):
    """ Class to provide the information from a given variable
    """

    def __init__(self, var):

        if var is None:
            self.Ndims = None
            self.dimx = None
            self.dimy = None
        else:
            varshape=var.shape
            #self.sname = var.getncattr('standard_name')
            self.lname = var.getncattr('description')
            self.coor = var.getncattr('coordinates')
            self.units = var.getncattr('units')

            self.Ndims = len(varshape)
            if self.Ndims == 1:
                self.dimx=varshape[0]
            if self.Ndims == 2:
                self.dimy=varshape[0]
                self.dimx=varshape[1]
            if self.Ndims == 3:
                self.dimy=varshape[1]
                self.dimx=varshape[2]
            if self.Ndims == 4:
                self.dimy=varshape[2]
                self.dimx=varshape[3]

class statsVal(object):
  """Statistics class providing min, max, mean mean2, median, standard deviation, non None values and percentiles of a list of values"""

  def __init__(self, vals):
    import math

    if vals is None:
      self.Nv = None
      self.minv = None
      self.maxv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.quantilesv = None
    else:
      values = vals.flat 
      self.Nv=len(values)
      self.minv=10000000000.
      self.maxv=-self.minv
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.

      sortedvalues = sorted(values)

      self.Nokvalues = 0
      for inum in range(self.Nv):
        if not values[inum] == None:
          self.Nokvalues = self.Nokvalues + 1
          if values[inum] < self.minv:
            self.minv = values[inum]
          if values[inum] > self.maxv:
            self.maxv = values[inum]

          self.meanv = self.meanv+values[inum]
          self.mean2v = self.mean2v+values[inum]*values[inum]

      self.meanv = self.meanv/float(self.Nokvalues)
      self.mean2v = self.mean2v/float(self.Nokvalues)
      self.stdv = math.sqrt(self.mean2v-self.meanv*self.meanv)
##      self.medianv = quantile(sortedvalues, 0.5, 7, issorted=True)
      self.quantilesv = []
      for iq in range(20):
        self.quantilesv.append(sortedvalues[int((self.Nv-1)*iq/20)])
#        self.quantilesv.append(quantile(sortedvalues, float(iq/20.), 7, issorted=True))

      self.quantilesv.append(sortedvalues[self.Nv-1])
      self.medianv = self.quantilesv[10]

def matrix2string(mat, chars, forms):
    """ Function to generate a string chain from a matrix with a given list of characters between each column value and another between values
    and a series of formats for each column value
    >>>matrix2string( np.array( [(2.3, 3.56, 1.3), (23.2, 1231.1, -123.1)]), ['--', '@@', ' '], ['.3f', '.9f', '06d'])
    2.300--3.560000000@@000001 23.200--1231.100000000@@-00123
    """
    stringchain=''
    dims=mat.shape
    dimr=dims[0]
    dimc=dims[1]
    if not len(chars) == dimc:
        print errormsg
        print '  number of columns: ', dimc, ' do not coincide with the number of characters ', len(chars), ' provided !'
        quit(-1)
    if not len(forms) == dimc:
        print errormsg
        print '  number of columns: ', dimc, ' do not coincide with the number of formats ', len(forms), ' provided !'
        quit(-1)

    matend=mat[dimr-1,0]
    for ir in range(dimr):
        for ic in range(dimc-1):
            stringchain=stringchain + UseFormat(mat[ir,ic], forms[ic]) + chars[ic]

        if ir == dimr-1:
            stringchain=stringchain + UseFormat(mat[ir,dimc-1], forms[dimc-1])
        else:
            stringchain=stringchain + UseFormat(mat[ir,dimc-1], forms[dimc-1]) + chars[dimc-1]

    return stringchain

def pointValue(mat, value):
    """ Function to give the point in a matrix with a given value
    """
    dy = mat.shape[0]
    dx = mat.shape[1]
    for iy in range(dy):
      for ix in range(dx):
          if mat[iy,ix] == value:
              return [iy,ix]

#######    ########
## MAIN
    #######
parser = OptionParser()

parser.add_option("-e", "--extremeValues", dest="xtrms",
                  help="minimum and maximum acceptable values for the variable (min,max)", metavar="TIMEVALUE")
parser.add_option("-f", "--file", dest="filename", 
                  help="file to check", metavar="FILENAME")
parser.add_option("-t", "--timestep", dest="tstep",
                  help="theoretical time-step (hours) between content", metavar="TIMEVALUE")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable in the file", metavar="VARNAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #

varfil=1.e20
ofilen='check_WRF_file.nc'

if not os.path.isfile(opts.filename):
    print errormsg
    print '  WRF file "' + opts.filename + '" does not exist !!'
    print errormsg
    quit(-1)    

ncf = NetCDFFile(opts.filename,'r')

if not ncf.variables.has_key(opts.varname):
    print errormsg
    print '  File does not have variable "' + opts.varname + '" !!!!'
    ncf.close()
    quit(-1)

varxtrms = np.zeros(2, dtype=float)
varxtrms[0] = float(opts.xtrms.split(',')[0])
varxtrms[1] = float(opts.xtrms.split(',')[1])

var = ncf.variables[opts.varname]
varinf = variable_inf(var)

# Checking times
##
print '  Checking times...'
timev = ncf.variables['Times']
times = timev[:]
dimt = times.shape[0]

datetimes = WRFtimes_datetime(ncf)

timest = np.zeros((dimt-1), dtype=float)
dval = np.zeros((varinf.dimy, varinf.dimx), dtype=float)
dvalt = np.zeros((dimt-1, 5), dtype=float)
quantt = np.zeros((dimt-1, 21), dtype=float)
minimumsv = []
minimumst = []
minimumsx = []
minimumsy = []
maximumsv = []
maximumst = []
maximumsx = []
maximumsy = []

for it in range(dimt-1):
    time = dt.datetime(datetimes[it,0], datetimes[it,1], datetimes[it,2], datetimes[it,3], datetimes[it,4], datetimes[it,5])
    reftime = dt.datetime(1949, 12, 01, 00, 00, 00)
    dtref = time - reftime
    timest[it] = dtref.days * 24 + dtref.seconds / 3600.

    if varinf.Ndims == 3:
        valsi = var[it,:,:]
        valsf = var[it+1,:,:]
    elif varinf.Ndims == 4:
        valsi = var[it,0,:,:]
        valsf = var[it+1,0,:,:]
    else:
        print errormsg
        print '  ', varinf.Ndims,' number of variables not ready!'
        quit(-1)

    dval = valsf - valsi
    statsdval = statsVal(dval)
    dvalt[it,0] = statsdval.minv
    dvalt[it,1] = statsdval.maxv
    dvalt[it,2] = statsdval.meanv
    dvalt[it,3] = statsdval.mean2v
    dvalt[it,4] = statsdval.stdv
    quantt[it,:] = statsdval.quantilesv

    if dvalt[it,0] < varxtrms[0]:
        pt = pointValue(dval, dvalt[it,0])
        print errormsg
        print '  Minimum too small ', dvalt[it,0],'between time-step values !'
        dates=matrix2string(datetimes[it:it+2,:], ['/', '/', ' ', ':', ':', '@'], ['4d', '02d', '02d', '02d', '02d', '02d']).split('@')
        print '  previous: ', dates[0] 
        print '  next: ', dates[1] 
        print '  point: ', pt
        minimumsv.append(dvalt[it,0])
        minimumst.append(timest[it])
        minimumsx.append(pt[1])
        minimumsy.append(pt[0])
#        quit(-1)

    if dvalt[it,1] > varxtrms[1]:
        pt = pointValue(dval, dvalt[it,1])
        print errormsg
        print '  Maximum too high ', dvalt[it,1],'between time-step values !'
        dates=matrix2string(datetimes[it:it+2,:], ['/', '/', ' ', ':', ':', '@'], ['4d', '02d', '02d', '02d', '02d', '02d']).split('@')
        print '  previous: ', dates[0] 
        print '  next: ', dates[1] 
        print '  point: ',pt
        maximumsv.append(dvalt[it,1])
        maximumst.append(timest[it])
        maximumsx.append(pt[1])
        maximumsy.append(pt[0])
#        quit(-1)

ncf.close()

ncfo = NetCDFFile(ofilen,'w')

newdim = ncfo.createDimension('time', None)
newdim = ncfo.createDimension('quant', 21)
newdim = ncfo.createDimension('stats', 5)

newvar = ncfo.createVariable('time', 'f8', ('time', ), fill_value=varfil)
newvar[:] = timest
attr = newvar.setncattr('calendar', 'standard')
attr = newvar.setncattr('units', 'hours since 1949-12-01 00:00:00')

newvar = ncfo.createVariable(opts.varname + 'stats', 'f4', ('time', 'stats',), fill_value=varfil)
newvar[:] = dvalt
attr = newvar.setncattr('statistics', 'minimum, maximum, mean, mean2, standard deviation')
attr = newvar.setncattr('standard_name', opts.varname + 'stats')
attr = newvar.setncattr('long_name', 'spatial statistics of ' + varinf.lname)
attr = newvar.setncattr('units', varinf.units)

newvar = ncfo.createVariable(opts.varname + 'quantile', 'f4', ('time', 'quant',), fill_value=varfil)
newvar[:] = quantt
attr = newvar.setncattr('quantiles', 'every 5 %')
attr = newvar.setncattr('standard_name', opts.varname + 'quant')
attr = newvar.setncattr('long_name', varinf.lname)
attr = newvar.setncattr('units', varinf.units)

dmin=len(minimumst)
if dmin > 0:
    mins = np.zeros((dmin, 4), dtype=float)
    for im in range(dmin):
        mins[im,0] = float(minimumsv[im])
        mins[im,1] = float(minimumst[im])
        mins[im,2] = float(minimumsy[im])
        mins[im,3] = float(minimumsx[im])

    newdim = ncfo.createDimension('mins', None)
    newdim = ncfo.createDimension('mvals', 4)
    newvar = ncfo.createVariable(opts.varname + 'mins', 'f4', ('mins', 'mvals',), fill_value=varfil)
    newvar[:]  = mins
    attr = newvar.setncattr('minimums', 'value, time, y-point, x-point')
    attr = newvar.setncattr('standard_name', opts.varname + 'mins')
    attr = newvar.setncattr('long_name', 'minimum values below ' + str(varxtrms[0]) + ' of ' + varinf.lname)
    attr = newvar.setncattr('units', varinf.units)

dmax=len(maximumst)
if dmax > 0:
    maxs = np.zeros((dmax, 4), dtype=float)
    for im in range(dmax):
        maxs[im,0] = float(maximumsv[im])
        maxs[im,1] = float(maximumst[im])
        maxs[im,2] = float(maximumsy[im])
        maxs[im,3] = float(maximumsx[im])

    if not ncfo.dimensions.has_key('mvals'): 
        newdim = ncfo.createDimension('mvals', 4)
    newdim = ncfo.createDimension('maxs', None)
    newvar = ncfo.createVariable(opts.varname + 'maxs', 'f4', ('maxs', 'mvals',), fill_value=varfil)
    newvar[:]  = maxs
    attr = newvar.setncattr('maximums', 'value, time, y-point, x-point')
    attr = newvar.setncattr('standard_name', opts.varname + 'maxs')
    attr = newvar.setncattr('long_name', 'maximum values above ' + str(varxtrms[1]) + ' of ' + varinf.lname)
    attr = newvar.setncattr('units', varinf.units)


ncfo.sync()
ncfo.close()

print '  file "' + ofilen + '" with all the statistics has been created'
