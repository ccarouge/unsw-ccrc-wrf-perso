import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
import nc_var_tools as ncvar
import datetime as dt

## g.e. # cell_method.py -f CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc -r  19491201000000 -v hflsmean -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table -k out -F /home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfout_d01_1997-02-17_00:00:00 -d 1 -o /home/lluis/studies/NARCliMGreatSydney2km/data/out -R /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf

errormsg='cell_method.py: ERROR -- error -- ERROR -- error'

def WRF_CFvariables(tablef):
    """ Provides the conversion of WRF variables name to a CF-convention (CFname, long_name, standard_name, units and postprocesss operation)
      one reading it from a table (wrfncxnj.table)
    """
    import csv
    WRFcfVAR = {}
    for line in csv.reader(open(tablef, "r"), delimiter=" ", skipinitialspace=True):
        if not line[0][0] == '#':
            if len(line[:]) == 6:
                WRFcfVAR[line[0]] = line[1:6]
            else:
                lval = line[1:5]
                lval.append('-')
                WRFcfVAR[line[0]] = lval

    return WRFcfVAR

def is_stats_varn(varN, wrfncxnjtab):
    """Function to check if variable name has a statistic section
    varN: variable name
    wrfncxnjtable: ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table file
    requestedfile: file with the requested variables
    >>> print is_stats_varn('tas', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    False
    >>> print is_stats_varn('pracc', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    True
    >>> print is_stats_varn('potevp', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf')
    True
    """

    statsnames=['min', 'max', 'mean', 'ac', 'mintstep', 'maxtstep', 'meantstep', 'actstep']

    Nstats = len(statsnames)

    for ist in range(Nstats):
        fstat = varN.find(statsnames[ist])
        if fstat != -1:
            return True

# particular cases
##
    specificvars=['pracc']
    for ispec in range(len(specificvars)):
        if specificvars[ispec] == varN:
            return True

# Checking deaccumulation in the 'wrfncxnj.table' file
##
    if not os.path.isfile(wrfncxnjtab):
        print errormsg
        print '  wrfncxnj.table file "' + wrfncxnjtab + '" does not exist !!'
        print errormsg
        quit(-1)    

    WRFnames = WRF_CFvariables(wrfncxnjtab)

    for WRFn in WRFnames:
        if WRFnames[WRFn][0] + '@' == varN + '@':
            if WRFnames[WRFn][4] == 'deaccumulate' or WRFnames[WRFn][4] == 'deaccumulate_flux':
                return True
        
    return False

def printing_class(classobj):
    """ Function to print all the values of a given class
    """

    valscls = vars(classobj)
    for attrcls in valscls:
        print attrcls, ':', valscls[attrcls]

class postfile_information(object):
    """ Class to provide the characteristics of a post-processed file
    pfile = post-processed file name ([header]_[freq]_[period]_[variable].nc)
    self.expname: name of the experiment
    self.frequency: frequency of the file 
    self.period: period of the file 
    self.variable: name of the variable of the file 
    >>> postfile_information('CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc')
    variable : hflsmean
    expname : CCRC_NARCliM_Sydney
    frequency : MON
    period : 1995-1999
    """
    
    def __init__(self, pfile):

        if pfile is None:
            self.expname = None
            self.frequency = None
            self.period = None
            self.variable = None

        else:
            secfile = pfile.split('_')
            Nsecs = len(secfile)

            self.expname = ''
            for isec in range(Nsecs-4):
                self.expname = self.expname + secfile[isec] + '_'

            self.expname = self.expname + secfile[Nsecs-4]

            self.frequency = secfile[Nsecs-3]
            self.period = secfile[Nsecs-2]
            self.variable = secfile[Nsecs-1].split('.')[0]

def final_stats(stn):
    """ Function to provide the final name in the file according to the statistics used
    >>> final_stats('DAM')
    ['DAY', 'mean']
    """
    if stn[0:1] == '&':
        begstn = stn[1:3]
        endstn = stn[3:4]
    else:
        begstn = stn[0:2]
        endstn = stn[2:3]

    if endstn == 'H':
        filefreq = stn
    elif endstn == 'm':
        filefreq = '{:%02dN}'.format(int(begstn))
    else:
        if begstn == 'DA':
            filefreq = 'DAY'
        elif begstn == 'MO':
            filefreq = 'MON'
        elif begstn == 'SE':
            filefreq = 'SES'
        elif begstn == 'YE':
            filefreq = 'YEA'
        elif begstn == 'Al':
            filefreq = 'All'
        else:
            print errormsg
            print '    final_stats: "' + begstn + '" statistic header not ready!!!'
            quit(-1)

    if endstn == 'N':
        varst = 'min'
    elif endstn == 'X':
        varst = 'max'
    elif endstn == 'M':
        varst = 'mean'
    elif endstn == 'S':
        varst = 'ac'
    elif endstn == 'H':
        varst = ''
    elif endstn == 'm':
        varst = ''
    elif endstn == 'l':
        varst = ''
    else:
        print errormsg
        print '  final_stats: "' + endstn + '" statistic surname not ready!!!'
        quit(-1)

    return [filefreq, varst]

def check_stats_varn(varN):
    """ Provides the statistics from a variable name
    varN= variable name
    >>> check_stats_varn('hflsmean')
    mean
    >>> check_stats_varn('pracc')
    ac
    """
    statsnames=['min', 'max', 'mean', 'ac', 'mintstep', 'maxtstep', 'meantstep', 'actstep']
    Nstats = len(statsnames)
    LvarN=len(varN)

    for ist in range(Nstats):
        Lstats=len(statsnames[ist])
        fstat = varN[LvarN-Lstats:LvarN].find(statsnames[ist])
        if fstat != -1:
            return statsnames[ist]

# specific variables
##
    specvars={}
    specvars['pracc'] = 'ac'

    for spcvar in specvars:
        if spcvar == varN:
            return specvars[spcvar]

    return None

def check_stats_frqn_varn(freqn, varN):
    """ Function to check the name of a frquency output file ('@' will be transformed to a ' ' )
    freqn= frequency name
    varN= variable name
    >>> check_stats_frqn_varn('01H','pracc')
    ['01@hour@point@value', 'accumulation@within@']
    """

    TimeWindow=freqn[0:2]
    statfrq=freqn[2:3]

    if statfrq == 'H':
        Tunit='hour'
    else:
        Tunit='month'

    if TimeWindow == 'DA':
        timeval = 'daily@values'
    elif TimeWindow == 'MO':
        timeval = 'monthly@values'
    elif TimeWindow == 'SE':
        timeval = 'seasonal@values'
    elif TimeWindow == 'YE':
        timeval = 'yearly@values'
    elif TimeWindow == '24':
        timeval = '24@' + Tunit + '@point@value'
    elif TimeWindow == '12':
        timeval = '12@' + Tunit + '@point@value'
    elif TimeWindow == '06':
        timeval = '06@' + Tunit + '@point@value'
    elif TimeWindow == '03':
        timeval = '03@' + Tunit + '@point@value'
    elif TimeWindow == '01':
        timeval = '01@' + Tunit + '@point@value'
    else:
        print errormsg
        print '  check_stats_frqn_varn: TimeWindow "' + TimeWindow + '" not ready!!!!'
        print errormsg
        quit(-1)

    statvar = check_stats_varn(varN)
### When files where: [DA/MO][M/S/N/X] this would work:
## now: statvar
    if statvar == 'mean':
        statN='mean@within@'
    elif statvar == 'ac':
        statN='accumulation@within@'
    elif statvar == 'min':
        statN='minimum@within@'
    elif statvar == 'max':
        statN='maximum@within@'
    else:
        statN='NONE'

    if statfrq == 'H':
        statN='every@'
    elif statfrq == 'm':
        statN='every@'

    return [timeval, statN]

def list2string(lst, char):
    """ Function to generate a string chain from a list with a given character between values
    >>> list2string([2.3, 3.56, 1.3, 23.2, 1231.1],'--')
    2.3--3.56--1.3--23.2--1231.1
    """
    stringchain=''
    Nlst=len(lst)

    for iv in range(Nlst):
        stringchain=stringchain + str(lst[iv]) + char

    stringchain=stringchain + str(lst[Nlst-1])

    return stringchain

def WRFtimes_datetime(ncfile):
    """ Provide date/time array from a file with a series of WRF time variable
    """
    import datetime as dt

    times = ncfile.variables['Times']
    timevals = times[:]
    dimt = times.shape[0]

    realdates = np.zeros((dimt, 6), dtype=int)

    for it in range(dimt):
        timev = list2string(timevals[it],'')
        realdates[it,0] = int(timev.split('-')[0]) 
        realdates[it,1] = int(timev.split('-')[1]) 
        realdates[it,2] = int(timev.split('-')[2].split('_')[0]) 
        realdates[it,3] = int(str(timev.split('_')[1]).split(':')[0]) 
        realdates[it,4] = int(timev.split(':')[1])
        realdates[it,5] = int(timev.split(':')[2]) 

    return realdates    

def WRFtimes_dt(postnetcdf, wrfk, domN, fout):
    """ Function to provide the regular time-step output from the WRF output according to post-processed file
    postnetcdf= postprocessed netCDF file name
    wrfk= WRF output kind
    domN= number domain of simulation
    fout= folder with all the output
    >>> WRFtimes_dt('CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc', 'out', 1, '/home/lluis/studies/NARCliMGreatSydney2km/data/out')
    10800
    """
    import subprocess as sub

    postinf = postfile_information(postnetcdf)
    varN = postinf.variable
    frqn = postinf.frequency
    per = postinf.period

##    ins=fout + '/wrf' + wrfk + '_d' + '{0:02d}'.format(domN) + '_' + per.split('-')[0] + '*'
    fileh='wrf' + wrfk + '_d' + '{0:02d}'.format(domN) + '_1997'
    Lfileh=len(fileh)

    poins=sub.Popen(["ls","-1",fout], stdout=sub.PIPE)
    oins, err = poins.communicate()

    wrffiles = oins.split('\n')
    wrffs = []
    for wrff in wrffiles:
        if fileh in wrff[0:Lfileh+1]:
            wrffs.append(wrff)

    wrffile = fout + '/' + wrffs[0]

    wrfnc = NetCDFFile(wrffile,'r')
    wrftimes = WRFtimes_datetime(wrfnc)
    wrfnc.close()

    wrftimesI=dt.datetime(wrftimes[0][0], wrftimes[0][1], wrftimes[0][2], wrftimes[0][3], wrftimes[0][4], wrftimes[0][5])
    wrftimesE=dt.datetime(wrftimes[1][0], wrftimes[1][1], wrftimes[1][2], wrftimes[1][3], wrftimes[1][4], wrftimes[1][5])

    wrfdtv = wrftimesE - wrftimesI
    wrfdt = wrfdtv.days*24*3600 + wrfdtv.seconds

    return wrfdt

def check_time_file_varfrqn(pnetcdf, varN, freqN, domN, fullnc, wrfk, foldout):
    """ Function to provide the cell_method time variable CF-compilation information for the variable. File time-steps are assumed to be
      in CF-compilant format ('time' is the name of variable time)
    pnetcdf= postprocessed netcdf file ([header]_[freq]_[period]_[variable].nc)
    varN= namr of variable
    freqN= frequency
    domN= number of the domain
    fullnc= full netCDF from raw WRF output
    wrfk= kind of output
    foldout= folder with the output
    >>> check_time_file('CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc', 1, '/home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfout_d01_1997-02-17_00:00:00', 'out', '/home/lluis/studies/NARCliMGreatSydney2km/data/out')
    point@values@10800@second
    print check_time_file('CCRC_NARCliM_Sydney_MON_1995-1999_wss1Hmaxtstep.nc', 1, '/home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfout_d01_1997-02-17_00:00:00', 'xtrm', '/home/lluis/studies/NARCliMGreatSydney2km/data/out')
    maximum@1@hour@time-window@moving@averaged@values@from@point@values@12@second
    """
    import datetime as dt
    import subprocess as sub

    postinf = postfile_information(pnetcdf)
    frqn = freqN
    per = postinf.period

    WRFdt = WRFtimes_dt(pnetcdf, wrfk, domN, foldout)

    timefileinf1='point@values@' + str(WRFdt) + '@second'

#
# extreme values use 'time_step' to compute the results clWRF modifications
#
    statn = check_stats_varn(varN)
    simTinf = ncvar.isgattrs('DT', fullnc)
    print simTinf[1]
    timefileinf0='point@values@' + str(simTinf[1]) + '@second'

    xtrmperiod=str(wrfdt / 3600)
    if statn == 'min':
        timefileinf='minimum@' + xtrmperiod + '@hour@value@from@' + timefileinf0
    elif statn == 'max':
        timefileinf='maximum@' + xtrmperiod + '@hour@value@from@' + timefileinf0
    elif statn == 'mean':
        timefileinf='mean@' + xtrmperiod + '@hour@value@from@' + timefileinf0
    elif statn == 'std':
        timefileinf='standard@deviation@' + xtrmperiod + '@hour@value@from@' + timefileinf0
    else:
        timefileinf=timefileinf1

# Specific cases
#
    if varN == 'pr5max':
        timefileinf='maximum@5@minute@values@from@' + timefileinf0
    elif varN == 'pr10max':
        timefileinf='maximum@10@minute@values@from@' + timefileinf0
    elif varN == 'pr20max':
        timefileinf='maximum@20@minute@values@from@' + timefileinf0
    elif varN == 'pr30max':
        timefileinf='maximum@30@minute@values@from@' + timefileinf0
    elif varN == 'pr1Hmax':
        timefileinf='maximum@1@hour@values@from@' + timefileinf0
    elif varN == 'wss5max':
        timefileinf='maximum@5@minute@values@from@' + timefileinf0
    elif varN == 'wss10max':
        timefileinf='maximum@10@minute@values@from@' + timefileinf0
    elif varN == 'wss20max':
        timefileinf='maximum@20@minute@values@from@' + timefileinf0
    elif varN == 'wss30max':
        timefileinf='maximum@30@minute@values@from@' + timefileinf0
    elif varN == 'wss1Hmax':
        timefileinf='maximum@1@hour@values@from@' + timefileinf0
    elif varN == 'pr5maxtstep':
        timefileinf='maximum@5@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'pr10maxtstep':
        timefileinf='maximum@10@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'pr20maxtstep':
        timefileinf='maximum@20@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'pr30maxtstep':
        timefileinf='maximum@30@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'pr1Hmaxtstep':
        timefileinf='maximum@1@hour@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'wss5maxtstep':
        timefileinf='maximum@5@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'wss10maxtstep':
        timefileinf='maximum@10@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'wss20maxtstep':
        timefileinf='maximum@20@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'wss30maxtstep':
        timefileinf='maximum@30@minute@time-window@moving@averaged@values@from@' + timefileinf0
    elif varN == 'wss1Hmaxtstep':
        timefileinf='maximum@1@hour@time-window@moving@averaged@values@from@' + timefileinf0

    return timefileinf

def set_newline_val(linev):
    """ function to provide the values of a line readed from a file without
        spaces and end-line character
    >>> print set_newline_val('12312 abc asdad       123121     131 1 11 1    1 1    \n')
    ['12312', 'abc', 'asdad', '123121', '131', '1', '11', '1', '1', '1']
    """
    linevals = filter(None, linev.split(' '))
    newlineval = []
    for val in linevals:
        newlineval.append(val.replace('\n','').replace('\r',''))

    newlineval = filter(None, newlineval)

    return newlineval

class load_requested(object):
    """ class to upload in memory a requested file
    rfile= requested file
    allflag= whether have to be uploaded all the variables ('yes'), or only that ones without the '#'
    self.vars: variables and their charactestics
    self.periods: periods of interest
    self.variables: set-up environment variables
    """

    def __init__(self, rfile, allflag):
        if rfile is None:
            self.vars=None
            self.periods=None
            self.variables=None

        else:
            requestedvars={}
            requestedperiods={}
            requestedvariables=[]

            fr = open(rfile, 'r')
            iline=0
            for line in fr:
                lineval = set_newline_val(line)
                if not len(lineval) == 0 and not lineval[0][0:1] == '#' and iline < 20:
                    if iline == 0:
                        requestedvars[lineval[0]] = lineval[1]
                    elif iline == 10:
                        requestedperiods[lineval[0].replace('@', '')] = lineval[1]
                elif iline == 20:
                    if not lineval[0] == '#':
                        requestedvariables.append(lineval[0]+'|'+lineval[1]+'|'+lineval[2]+'|'+lineval[3]+'|'+lineval[4])
                    else:
                        if allflag == 'yes':
                            requestedvariables.append(lineval[1]+'|'+lineval[2]+'|'+lineval[3]+'|'+lineval[4]+'|'+lineval[5])

                elif not len(lineval) == 0 and lineval[0] == '#Periods#':
                    iline = 10
                elif not len(lineval) == 0 and lineval[0] == '#Variables#':
                    iline = 20

            fr.close()
            self.vars=requestedvars
            self.periods=requestedperiods
            self.variables=requestedvariables

def WRF_CFvariableNames(tablef):
    """ Provides the conversion of WRF variables name to a CF-convention one reading it from a table (wrfncxnj.table)
    """
    import csv
    WRFcfVAR = {}
    for line in csv.reader(open(tablef, "r"), delimiter=" ", skipinitialspace=True):
        if not line[0][0] == '#':
            WRFcfVAR[line[0]] = line[1]

    return WRFcfVAR

def remove_stats_varn(varN):
    """ Function to remove the statistics section of a variable name
    vagrN= name of the variable
    >>> remove_stats_varn('praccmean')
    pracc
    >>> remove_stats_varn('wss1Hmaxtstepmean')
    wss1Hmaxtstep
    """
    statsnames=['min','max','mean','ac']
    Nstats = len(statsnames)
    LvarN=len(varN)

# specific variables
##
    specvars = ['pracc']

    for ist in range(Nstats):
        Lstats=len(statsnames[ist])
        fstat = varN[LvarN-Lstats:LvarN].find(statsnames[ist])
        if fstat != -1:
            return varN[0:LvarN-Lstats]
        for spvar in specvars:
            if spvar+statsnames[ist] == varN:
                return spvar

    return varN

def freq_statname(freqN):
    """ Function to give the statistic name according to the frequency
    freqN+ name of the frequency
    >>> freq_statname('MOM')
    mean
    >>> freq_statname('&MOX+DAM')
    max
    """

# Variable in requested might have the name &[FREQ1]+[FREQ2]_[HOUR]
    Nprev=freqN.find('+')
    if Nprev > 0:
        stfsec=freqN.split('+')[0].replace('&','')
    else:
        stfsec=freqN

    Nhour=freqN.find('_')
    if Nhour > 0 and not Nprev > 0:
        stfsec=freqN.split('_')[0]

    Lstfsec=len(stfsec)
    stfsecn=stfsec[Lstfsec-1:Lstfsec]

    if stfsecn == 'N':
        stname = 'min'
    elif stfsecn == 'X':
        stname = 'max'
    elif stfsecn == 'M':
        stname = 'mean'
    elif stfsecn == 'S':
        stname = 'ac'
    else:
        stname = ''

    return stname

def dictionary2entries(vals1, vals2, vals3):
    """ Function to create a dictionary with 2 entries (thanks to Jeff Exbrayat, CoECSCC-CCRC)
    >>>country = ['India', 'Germany', 'Guatemala', 'Burma', 'South Africa', 'South Africa']
    >>>person = ['Mohandas Karamchand Ghandi', 'Albert Einstein', 'Rigoberta Menchu', 'Aung San Suu Kyi', 'Nelson Mandela', 'Desmond Tutu']
    >>>fight = ['Independence', 'Nuclear bomb', 'Indigenous Rights', 'Freedom', 'anti-Apartheid', 'Reconciliation']

    >>>dict3 = dictionary3entries(country, person, fight)

    >>>print dict3['South Africa']['Nelson Mandela']
    anti-Apartheid
    >>>print dict3['South Africa']
    {'Nelson Mandela': 'anti-Apartheid', 'Desmond Tutu': 'Reconciliation'}
    """

    dicjeff={}

    for ii in range(len(vals1)):
##        print vals1[ii], vals2[ii], vals3[ii]

        if vals1[ii] not in dicjeff.keys():
            dicjeff[vals1[ii]]={}
            if vals2[ii] not in dicjeff[vals1[ii]].keys():
                dicjeff[vals1[ii]][vals2[ii]]={}
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
            else:
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
        else:
            if vals2[ii] not in dicjeff[vals1[ii]].keys():
                dicjeff[vals1[ii]][vals2[ii]]={}
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]
            else:
                dicjeff[vals1[ii]][vals2[ii]]=vals3[ii]

    return dicjeff

def allvar_requested(varN, reqfile, wncjtab):
    """ Function to provide all the requested output from a given variable
    varN= name of variable
    reqfile= requested file
    wncjtab= wrfncxnj.table file
    >>> allvar_requested('pracc','/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf','/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table')
    {'pracc:MON': '02|RAIN|hrly|SFC|&MOM+DAM_12:00|per1y07', 'pracc:All': '00|RAIN|hrly|SFC|All|per1y07', 'pracc:DAY': '01|RAIN|hrly|SFC|DAM_12:00|per1y07'}
    """

    reqval=load_requested(reqfile, 'yes')

    WRFcfn=WRF_CFvariableNames(wncjtab)

    purevarN = remove_stats_varn(varN)

    WRFnames = []
    for WRFvn in WRFcfn:
        if WRFcfn[WRFvn] == purevarN:
            WRFnames.append(WRFvn)
    
    WRFreqnames = []
    reqorder = []
    reqvals = []
    reqCFname = []

    allreqvar = {}

    ivarreq=0
    for WRFreqv in reqval.variables:
        WRFreqvrealname=WRFreqv.split('|')[0]
        for WRFn in WRFnames:
            if WRFn == WRFreqvrealname:
                if WRFn not in WRFreqnames:
                    WRFreqnames.append(WRFn)

                reqorder.append(ivarreq)
                reqvals.append(WRFreqv)
                statn = final_stats(WRFreqv.split('|')[3])
                allreqvar[varN + ":" + statn[0]] = '{0:02d}'.format(ivarreq) + "|" + WRFreqv
##                allreqvar['{0:02d}'.format(ivarreq)] = WRFreqv + "|" + statn[1] + "|" + statn[0]

                ivarreq = ivarreq + 1

    NWRFreqnames = len(WRFreqnames)
    if not NWRFreqnames > 0:
        print errormsg
        print '  allvar_requested: Variable "' + varN + '" has not been requested ?!! '
        print errormsg
        quit(-1)
   
    return allreqvar


def CF_CORDEX_allcellmethods_varfrqn(ncf, wrfk, dom, fullnc, wnctab, fout):
    """ Function to provide to the netCDF files with the variable and its all cellmethod attributes until the actual frequency following the CORDEX
      specifications
    ncf= netCDF file
    varN= variable name
    frqN= frequency name
    wrfk= kind of wrf out 'out', 'xtrm', 'hrly', 'dly'
    dom= domain
    fullnc= full netCDF from raw WRF output
    wnctab= wrfncxnj.table file
    fout= folder with out the output raw files
    >>> CF_CORDEX_cellmethods('CCRC_NARCliM_Sydney_MON_1995-1999_hflsmean.nc', 'out', 1, '/home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfout_d01_1997-02-17_00:00:00', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table','/home/lluis/studies/NARCliMGreatSydney2km/data/out')
    mean@3@hour@value@from@point@values@12.0@second
    >>> CF_CORDEX_cellmethods('CCRC_NARCliM_Sydney_03H_1995-1999_hfls.nc', 'out', 1, '/home/lluis/studies/NARCliMGreatSydney2km/data/out/wrfout_d01_1997-02-17_00:00:00', '/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table','/home/lluis/studies/NARCliMGreatSydney2km/data/out')
    point@values@10800@second
    """
    postinf = postfile_information(ncf)
    varn = varN
    freqn = frqN

# Is variable a statistic field?
# 
    celltime_method='time:@'
    if not freqn == 'All':
        statsfrqvarn = check_stats_frqn_varn(freqn, varn)
        timeName=statsfrqvarn[0]
        statName=statsfrqvarn[1]
        if not statName == 'NONE':
          celltime_method=celltime_method + statName + timeName + '@'
        else:
          celltime_method=celltime_method

    else:
        statName='NO'

# Checking if frequency output is the same as variable frequency
##
    simTinf = WRFtimes_dt(ncf, wrfk, dom, fout)

    simtHinf = simTinf / 3600.

    equalFrqSimT = False
    if statName == 'every@':
        hourfrq = freqn[0:2] + '0'
        if hourfrq == simTHinf and not freqn[2:3] == 'm':
            celltime_method = 'time:@'
            equalFrqSimT=True

# Original frequency time of the fields
##
    if freqn == 'All' or equalFrqSimT:
       filetime = check_time_file_varfrqn(ncf, varn, freqn, dom, fullnc, wrfk, fout)
       celltime_method=celltime_method + filetime

    return celltime_method

#######    #######
## MAIN
    #######
parser = OptionParser()

parser.add_option("-d", "--domain", dest="dom", 
                  help="domain of simulation", metavar="DOMAINNUM")
parser.add_option("-F", "--full_NCDfile", dest="fullnc", 
                  help="full netCDF (without removing)", metavar="FILENAME")
parser.add_option("-f", "--postprocessed_file", dest="pfile", 
                  help="post-processed file to remove time_bnds", metavar="FILENAME")
parser.add_option("-k", "--kind_WRF_file", dest="kwfile", 
                  help="kind of original WRF file", metavar="FILEKIND")
parser.add_option("-o", "--output_folder", dest="ofold", 
                  help="folder with the output", metavar="FOLDERNAME")
parser.add_option("-R", "--requested_file", dest="reqfile", 
                  help="requested file of the post-process", metavar="FILENAME")
parser.add_option("-r", "--referenceDate", dest="rdate", 
                  help="reference date [YYYY][MM][DD][HH][MI][SS]", metavar="DATE")
parser.add_option("-v", "--variable", dest="varname", 
                  help="variable of the file", metavar="VARNAME")
parser.add_option("-w", "--wrfncxnj_table", dest="wfile", 
                  help="wrfncxnj.table file", metavar="FILENAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
ncfile = opts.pfile
refdate = opts.rdate

refdateS=refdate[0:4] + '-' + refdate[4:6] + '-' + refdate[6:8] + ' ' + refdate[8:10] + ':' + refdate[10:12] + ':' + refdate[12:14]

if not os.path.isfile(ncfile):
    print errormsg
    print '  File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

if not os.path.isfile(opts.wfile):
    print errormsg
    print '  wrfncxnj.table file "' + opts.wfile + '" does not exist !!'
    print errormsg
    quit(-1)    

postinf = postfile_information(ncfile)

inc = NetCDFFile(ncfile,'a')

reqdicvar = allvar_requested(opts.varname, opts.reqfile, opts.wfile)

requestedterm = reqdicvar[opts.varname + ":" + postinf.frequency]
print requestedterm

### Make a function to give the order of the postprocess according to unique 'All', 'DAY', 'MON'

quit()



# Attributes
#
varcell=inc.variables[opts.varname]
attvarcell = varcell.ncattrs()

celltime_method=CF_CORDEX_allcellmethods(ncfile, opts.varname, postinf.frequency, opts.kwfile, int(opts.dom), opts.fullnc, opts.wfile, opts.ofold)
celltime_method=celltime_method.replace('@',' ')

if ncvar.searchInlist(attvarcell, 'cell_method'):
    attvalvarcell = varcell.getncattr('cell_method')
    if not celltime_method == attvalvarcell:
        print '  cellmethod.bash: previous cell_method: ' + attvalvarcell
        attr = varcell.delncattr('cell_method')
        attr = varcell.setncattr('cell_method', celltime_method + '  ' + attvalvarcell)
else:
    attr = varcell.setncattr('cell_method', celltime_method)

# Cell Measure
##
##if not ncvar.searchInlist(attvarcell, 'cell_measures'):
##  attr = varcell.setncattr('cell_measures', 'area: areacella')


attr = inc.setncattr('creation_date', dt.date.today().strftime("%Y-%m-%d"))

inc.sync()
inc.close()
