import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
from scipy.stats.stats import pearsonr
import nc_var_tools as ncvar
import subprocess as sub

errormsg='correct_fold.py: ERROR -- error -- ERROR -- error'
warnmsg='correct_fold.py: WARNING -- warning -- WARNING -- warning'

## g.e. # correct_fold.py -f data -r /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf.test -p /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/wrfncxnj.projection_CCRC_NARCliM_Sydney -e CCRC_NARCliM_Sydney -a attr_d01 -g Sydney_NNRP_1990-2010
## g.e. # correct_fold.py -f /srv/ccrc/data18/z3393242/studies/NARCliM/nnrp/postprocess/R2/d02/postPROJ -r /home/z3393242/studies/NARCliM/postprocess/nnrp/R2/d02/requested_variables.inf.ccrc.NARCliM11.Hamish.d02_1990-2009 -p /home/z3393242/studies/NARCliM/postprocess/nnrp/R2/d02/wrfncxnj.projection_CCRC_NARCliM -e CCRC_NARCliM -a attr_d02 -g NARCliM_NNRP_R2

def set_newline_val_vbar(linev):
    """ function to provide the values of a line readed from a file without ':'
        and end-line character
    >>> print set_newline_val_points('12312 abc asdad       123121     131| 1 11 1    1 1    \n')
    ['12312 abc asdad       123121     131', ' 1 11 1    1 1    ']
    """
    linevals = filter(None, linev.split('|'))
    newlineval = []
    for val in linevals:
        newlineval.append(val.replace('\n','').replace('\r',''))

    newlineval = filter(None, newlineval)

    return newlineval

def FilesInFolder(ifold, pfile, position):
    """ 
    Function to retrieve a list with the files in a folder [ifold] according to a pattern [pfile] in 
      [postion](beg, beginning; cent, center; end, ending)
    """
    ins=ifold 
    poins=sub.Popen(["ls","-1",ins], stdout=sub.PIPE)
    oins, err = poins.communicate()

    files=oins.split('\n')

    Lpfile=len(pfile)
    ifiles = []
    for ffile in files:
        if position == 'beg':
            if pfile in ffile[0:Lpfile]:
                ifiles.append(ffile)
        elif position == 'cent':
            if pfile in ffile:
                ifiles.append(ffile)
        elif position == 'end':
            Lffile=len(ffile)
            if pfile in ffile[Lffile-Lpfile:Lffile]:
                ifiles.append(ffile)
        else:
            print errormsg
            print '  "' + position + '" position for pattern not prepared!'
            quit(-1)
 

    if len(ifiles) == 0:
        ifiles = None

    return ifiles

def create_unlimited_dim(ncf, dimn):
    """ sets a dimension as UNLIMITED
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile
    import subprocess as sub

    onc = NetCDFFile(ncf,'r')
    if not onc.dimensions[dimn].isunlimited():
        newf = 'unlimit.nc'
        unc = NetCDFFile(newf,'w')

        dim = unc.createDimension(dimn, size=0)
    
# Dimensions
## 
        for dim in onc.dimensions.values():
            dname = dim._name
            dsize = dim.__len__()
            undim = dim.isunlimited()
            if not dname == dimn:
                dim = unc.createDimension(dname, size=dsize)

# Variables
## 
        for var in onc.variables.values():
            vname = var._name
            vkind = var.dtype
            vdims = var.dimensions

            varattrs = var.ncattrs()
            if ncvar.searchInlist(varattrs, '_FillValue'):
                fillval = var.getncattr('_FillValue')
                unvar = unc.createVariable(vname, vkind, vdims, fill_value=fillval)
            else:
                unvar = unc.createVariable(vname, vkind, vdims)

            varvals = var[:]
            unvar[:] = varvals
            for vattr in var.ncattrs():
                if not vattr == '_FillValue':
                    vattrval = var.getncattr(vattr)
                    unvar = ncvar.set_attribute(unvar, vattr, vattrval)

# Global attributes
##
        for gattr in onc.ncattrs():
            gattrval = onc.getncattr(gattr)
            unc = ncvar.set_attribute(unc, gattr, gattrval)

        unc.sync()
        unc.close()
        try:
            ins = 'cp unlimit.nc ' + ncf
            syscode = sub.call(ins, shell=True)
    
            if not syscode == 0:
                print errormsg
                print "    create_unlimited_dim: Copy to the new file failed!"
                print ins
                print syscode
                quit(-1)
            else:
                sub.call('rm unlimit.nc', shell=True)

        except OSError, e:
            print errormsg
            print "    create_unlimited_dim: Copy failed!"
            print e
            quit(-1)
    else:
        print '    create_unlimited_dim: Variable "time" is already UNLIMITED!'
    onc.close()

def ProjectName(gridvalue):
# Function to provide the name of the variable with which the projection will be known:
#   Following CF-conventions: http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/cf-conventions.html#appendix-grid-mappings
  if gridvalue == 'albers_conical_equal_area':
    GridMapName = 'Albers_Equal_Area'
  elif gridvalue == 'azimuthal_equidistant':
    GridMapName = 'Azimuthal_Equidistant'
  elif gridvalue == 'lambert_azimuthal_equal_area':
    GridMapName = 'Lambert_azimuthal_equal_area'
  elif gridvalue == 'lambert_conformal_conic':
    GridMapName = 'Lambert_Conformal'
  elif gridvalue == 'lambert_cylindrical_equal_area':
    GridMapName = 'Lambert_Cylindrical_Equal_Area'
  elif gridvalue == 'latitude_longitude':
    GridMapName = 'Latitude_Longitude'
  elif gridvalue == 'mercator':
    GridMapName = 'Mercator'
  elif gridvalue == 'orthographic':
    GridMapName = 'Orthographic'
  elif gridvalue == 'polar_stereographic':
    GridMapName = 'Polar_stereographic'
  elif gridvalue == 'rotated_latitude_longitude':
    GridMapName = 'Rotated_pole'
  elif gridvalue == 'stereographic':
    GridMapName = 'Stereographic'
  elif gridvalue == 'transverse_mercator':
    GridMapName = 'Transverse_Mercator'
  elif gridvalue == 'vertical_perspective':
    GridMapName = 'Vertical_perspective'

  return GridMapName

def add_projection_inf(mappingfile, netcdffile, varn):
    """ Adding mapping information
    """
    import time
    fname = 'add_projection_inf'

# grid mapping information
#
    if not os.path.isfile(mappingfile):
      print errormsg
      print '  ' + fname + ': File "' + mappingfile + '" does not exist !!'
      print errormsg
      quit()    

    mapf = open(mappingfile, 'r')

    if not os.path.isfile(netcdffile):
      print errormsg
      print '  ' + fname + ': File "' + netcdffile + '" does not exist !!'
      print errormsg
      quit()    

    ncf = NetCDFFile(netcdffile,'a')

    if ncf.dimensions.has_key('plev'):
      # removing pressure level from variable name
      varn = re.sub("\d+", "", varn) 

    attrvalues = []
    for fileline in mapf:
      line=fileline.replace('\n','')
      values = line.split(' ')
      if len(values) > 1:
        attrvalues.append(values)

    mapf.close()

    Nlines = len(attrvalues)

    posMapname = attrvalues[0][:].index('grid_mapping_name')
    projstdname = attrvalues[posMapname][1]

    MapProjName = ProjectName(projstdname)

    print MapProjName

# Creation of the variable with the projection information
#
    if ncf.variables.has_key(MapProjName):
      print '  ' + fname + ': File "' + netcdffile + '" already has projection variable "' + MapProjName + '" ! '
      ncf.sync()
      ncf.close()
    else:
      mapgridvar = ncf.createVariable(MapProjName, 'c')
 
      if ncf.variables.has_key(MapProjName + 'new'):
        ncf.sync()
        ncf.close()
        ncvar.varrm(netcdffile, MapProjName + 'new')
        ncf = NetCDFFile(netcdffile,'a')

# Adding projection attribute to the existing variable within the netCDF file
#
      if ncf.dimensions.has_key('plev'):
      #   removing pressure level from variable name
        varn = re.sub("\d+", "", varn) 

      var = ncf.variables[varn]

      varattributes = var.ncattrs()
      if ncvar.searchInlist(varattributes, 'grid_mapping'):
        attr = var.delncattr('grid_mapping')

      attr = var.setncattr('grid_mapping', MapProjName)

      for il in range(Nlines):
        varmapgridattrs = mapgridvar.ncattrs()
        if ncvar.searchInlist(varmapgridattrs,attrvalues[il][0]):
          attr = mapgridvar.delncattr(attrvalues[il][0])

        if attrvalues[il][2] == 'S':
          attrvals=attrvalues[il][1].split('@')
          mapgridvar = ncvar.set_attribute(mapgridvar, attrvalues[il][0], ' '.join(attrvals))
        elif attrvalues[il][2] == 'I':
          attrvals=int(attrvalues[il][1])
          mapgridvar = ncvar.set_attribute(mapgridvar, attrvalues[il][0], attrvals)
        elif attrvalues[il][2] == 'R':
          attrvals=float(attrvalues[il][1])
          mapgridvar = ncvar.set_attribute(mapgridvar, attrvalues[il][0], attrvals)
        elif attrvalues[il][2] == 'D':
          attrvals=np.float64(attrvalues[il][1])
          mapgridvar = ncvar.set_attribute(mapgridvar, attrvalues[il][0], attrvals)
        else:
          print errormsg
          print '  ' + attrvalues[il][2] + ' kind of attribute is not ready!'
          quit()

#          attr = mapgridvar.setncattr(attrvalues[il][0], attrvalues[il][1])
  
      histval = ncf.getncattr('history')
      newhist = time.ctime(time.time()) + ': adding_mapping_information.py -f ' + netcdffile + ' -m ' + mappingfile + ' -v ' + varn + '\n' + histval
      newh = ncf.setncattr('history', newhist)

      ncf.sync()
      ncf.close()

#######    ########
## MAIN
    #######
parser = OptionParser()

parser.add_option("-a", "--AttributesFold", dest="afold", 
                  help="folder with the files with the attributes to add or remove (atrribute file name structure [per]_[var]_[add-attr/rm-attr].inf)", metavar="FOLDERNAME")
parser.add_option("-e", "--experiment", dest="expn", 
                  help="experiment name (header of files)", metavar="EXPNAME")
parser.add_option("-f", "--folder", dest="ifold", 
                  help="folder with data to use", metavar="FOLDERNAME")
parser.add_option("-g", "--GlobalAttributesFiles", dest="gattrf", 
                  help="files with the attributes to add or remove (atrribute file name structure [value]_[add-attr/rm-attr].inf)", metavar="LABELNAME")
parser.add_option("-k", "--kindpost", dest="kpost", 
                  help="kind of post-processed data to use", metavar="LABEL")
parser.add_option("-r", "--requested_file", dest="rfile", 
                  help="requested file of the postprocess", metavar="FILENAME")
parser.add_option("-p", "--ProjectionFile", dest="pfile", 
                  help="name of the projection file [DIR]/wrfncxnj.projection_[EXP]", metavar="FILENAME")
parser.add_option("-v", "--variable", dest="varn",
                  help="name of the variable to include projectrion information", metavar="VARNAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #

if not opts.kpost is None:
    files = FilesInFolder(opts.ifold, opts.kpost, 'cent')
    if files is None:
        print errormsg
        print '  No files found in "' + opts.ifold + '" of kind "' + opts.kpost + '" !!!!!'
        quit(-1)
else:
    files = FilesInFolder(opts.ifold, 'nc', 'end')
    if files is None:
        print errormsg
        print '  No files found in "' + opts.ifold + '" !!!!!'
        quit(-1)

if not os.path.isfile(opts.rfile):
    print errormsg
    print '  requested file "' + opts.rfile + '" does not exist !!'
    print errormsg
    quit(-1)    

if not os.path.isfile(opts.pfile):
    print errormsg
    print '  projection file "' + opts.pfile + '" does not exist !!'
    print errormsg
    quit(-1)    

for ifile in range(len(files)):
# Unlimited time from 'correct_xytime.py'
##
    filen = opts.ifold + '/' + files[ifile]
    print filen
    if not os.path.isfile(filen):
        print errormsg
        print '  requested file "' + filen + '" does not exist !!'
        print errormsg
        quit(-1)    

    create_unlimited_dim(filen, 'time')
    inc = NetCDFFile(filen,'a')

# Correction of standard dimensions
##
    lonv = inc.variables['lon']
    Ndims=len(lonv.shape)
    dimx = lonv.shape[Ndims-1]
    dimy = lonv.shape[Ndims-2]

    lonv = ncvar.set_attribute(lonv, 'long_name', 'longitude')

    latv = inc.variables['lat']
    latv = ncvar.set_attribute(latv, 'long_name', 'latitude')

    timev = inc.variables['time']
    timev = ncvar.set_attribute(timev, 'long_name', 'time')
    timev = ncvar.set_attribute(timev, 'bounds', 'time_bnds')
    inc.dimensions['time'] = None

    if inc.variables.has_key('time_bnds'):
        timebndsv = inc.variables['time_bnds']
        tunits = timev.getncattr('units')
        timebndsv = ncvar.set_attribute(timebndsv, 'units', tunits)

# Adding variable 'x'
## 
    if not inc.variables.has_key('x'):
        print 'Creating x variable'
        xval = inc.createVariable('x', 'd', ('x'))
        xattr = xval.setncattr('axis', 'X')
        xattr = xval.setncattr('_CoordinateAxisType', 'GeoX')
    else:
        xval = inc.variables['x']

    xvalues = map(float, range(dimx))
    xval[:] = xvalues

# Adding variable 'y'
## 
    if not inc.variables.has_key('y'):
        print 'Creating y variable'
        yval = inc.createVariable('y', 'd', ('y'))
        yattr = yval.setncattr('axis', 'Y')
        yattr = yval.setncattr('_CoordinateAxisType', 'GeoY')
    else:
        yval = inc.variables['y']

    yvalues = map(float, range(dimy))
    yval[:] = yvalues

    inc.sync()
    inc.close()

    ncvar.varaddattr("_CoordinateAxisType|Time", filen, 'time')

    Nsecexpn = len(opts.expn.split('_'))
    filesecs=filen.split('_')
    filefreq = filesecs[Nsecexpn]
    fileper = filesecs[Nsecexpn+1]
    filevar = filesecs[Nsecexpn+2].split('.')[0]

    print '  filesecs: ', filefreq, fileper, filevar

    add_projection_inf(opts.pfile, filen, filevar)

# Attributes
##

    inc = NetCDFFile(filen,'a')
    varattr = inc.variables[filevar]

## Adding
    print ' Adding attributes...'
    attrk = 'add-attr.inf'

# Global
    if not os.path.isfile(opts.gattrf + '_' + attrk):
        print warnmsg
        print '  global attributes file "' + opts.gattrf + '_' + attrk + '" does not exist !!'
    else:
        print '  adding global attributes from "' + opts.gattrf + '_' + attrk +'" ...'
        fa = open(opts.gattrf + '_' + attrk, 'r')
        for line in fa:
            linevals = set_newline_val_vbar(line)
            attrn = linevals[0]
            attrv = linevals[1].replace('!', ' ')
            attrki = linevals[2]
            attr = ncvar.set_attributek(inc, attrn, attrv, attrki)
        fa.close()

    if opts.afold is None:
        print warnmsg
        print '  Folder with variable-specific attribute files is not provided !!' 
        print '  Proceed without adding any attribute to any variable'
    else:
        attrf = FilesInFolder(opts.afold, attrk, 'end')

# General
        if ncvar.searchInlist(attrf, attrk):
            print '  adding attributes from "' + opts.afold + '/' + attrk +'" ...'
            fa = open(opts.afold + '/' + attrk, 'r')
            for line in fa:
                linevals = set_newline_val_vbar(line)
                attrn = linevals[0]
                attrv = linevals[1].replace('!', ' ')
                attrki = linevals[2]
                attr = ncvar.set_attributek(varattr, attrn, attrv, attrki)
            fa.close()

# Frequency specific
        if ncvar.searchInlist(attrf, filefreq + '_' + attrk):
            print '  adding attributes from "' + opts.afold + '/' + filefreq + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filefreq + '_' + attrk, 'r')
            for line in fa:
                linevals = set_newline_val_vbar(line)
                attrn = linevals[0]
                attrv = linevals[1].replace('!', ' ')
                attrki = linevals[2]
                attr = ncvar.set_attributek(varattr, attrn, attrv, attrki)
            fa.close()
            
# Variable specific
        if ncvar.searchInlist(attrf, filevar + '_' + attrk):
            print '  adding attributes from "' + opts.afold + '/' + filevar + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filevar + '_' + attrk, 'r')
            for line in fa:
                linevals = set_newline_val_vbar(line)
                attrn = linevals[0]
                attrv = linevals[1].replace('!', ' ')
                attrki = linevals[2]
                attr = ncvar.set_attributek(varattr, attrn, attrv, attrki)
            fa.close()
            
# Frquency and Variable specific
        if ncvar.searchInlist(attrf, filefreq + '_' + filevar + '_' + attrk):
            print '  adding attributes from "' + opts.afold + '/' + filefreq + '_' + filevar + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filefreq + '_' + filevar + '_' + attrk, 'r')
            for line in fa:
                linevals = set_newline_val_vbar(line)
                attrn = linevals[0]
                attrv = linevals[1].replace('!', ' ')
                attrki = linevals[2]
                attr = ncvar.set_attributek(varattr, attrn, attrv, attrki)
            fa.close()

    inc.sync()
    inc.close()

## Removing 
    print ' Removing attributes...'

    attrk = 'rm-attr.inf'

# Global
    if not os.path.isfile(opts.gattrf + '_' + attrk):
        print warnmsg
        print '  global attributes file "' + opts.gattrf + '_' + attrk + '" does not exist !!'
    else:
        print '  removing global attributes from "' + opts.gattrf + '_' + attrk +'" ...'
        fa = open(opts.gattrf + '_' + attrk, 'r')
        for line in fa:
            attrn = line.replace('\n','').replace('\r','')
            attr = ncvar.grmattr(attrn, filen)
        fa.close()

    if opts.afold is None:
        print warnmsg
        print '  Folder with variable-specific attribute files is not provided !!' 
        print '  Proceed without removing any attribute from any variable'
    else:
        attrf = FilesInFolder(opts.afold, attrk, 'end')

# General
        if ncvar.searchInlist(attrf, attrk):
            print '  removing attributes from "' + opts.afold + '/' + attrk +'" ...'
            fa = open(opts.afold + '/' + attrk, 'r')
            for line in fa:
                attrn = line.replace('\n','').replace('\r','')
                attr = ncvar.varrmattr(attrn, filen, filevar)
            fa.close()
    
# Frequency specific
        if ncvar.searchInlist(attrf, filefreq + '_' + attrk):
            print '  removing attributes from "' + opts.afold + '/' + filefreq + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filefreq + '_' + attrk, 'r')
            for line in fa:
                attrn = line.replace('\n','').replace('\r','')
                attr = ncvar.varrmattr(attrn, filen, filevar)
            fa.close()
                
# Variable specific
        if ncvar.searchInlist(attrf, filevar + '_' + attrk):
            print '  removing attributes from "' + opts.afold + '/' + filevar + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filevar + '_' + attrk, 'r')
            for line in fa:
                attrn = line.replace('\n','').replace('\r','')
                attr = ncvar.varrmattr(attrn, filen, filevar)
            fa.close()
            
# Frquency and Variable specific
        if ncvar.searchInlist(attrf, filefreq + '_' + filevar + '_' + attrk):
            print '  removing attributes from "' + opts.afold + '/' + filefreq + '_' + filevar + '_' + attrk + '" ...'
            fa = open(opts.afold + '/' + filefreq + '_' + filevar + '_' + attrk, 'r')
            for line in fa:
                attrn = line.replace('\n','').replace('\r','')
                attr = ncvar.varrmattr(attrn, filen, filevar)
            fa.close()

# Changing file names
##
    print ' Changing file names and moving to "' + opts.ifold + '/corrected/"...'
    if ifile == 0:
        sub.call('mkdir ' + opts.ifold + '/corrected', shell=True)

    if filefreq == 'DAM':
        sub.call('mv ' + filen + ' ' + opts.ifold + '/corrected/' + opts.expn + '_DAY_' + fileper + '_' + filevar + '.nc', shell=True)
    elif filefreq == '24H':
        sub.call('mv ' + filen + ' ' + opts.ifold + '/corrected/' + opts.expn + '_DAY_' + fileper + '_' + filevar + '.nc', shell=True)
    elif filefreq == 'MOM':
        sub.call('mv ' + filen + ' ' + opts.ifold + '/corrected/' + opts.expn + '_MON_' + fileper + '_' + filevar + '.nc', shell=True)
    elif filefreq == 'SEM':
        sub.call('mv ' + filen + ' ' + opts.ifold + '/corrected/' + opts.expn + '_SES_' + fileper + '_' + filevar + '.nc', shell=True)
    else:
        sub.call('mv ' + filen + ' ' + opts.ifold + '/corrected/' + opts.expn + '_' + filefreq + '_' + fileper + '_' + filevar + '.nc', shell=True)

# Bringing back all the files
## 
sub.call('mv ' +  opts.ifold + '/corrected/*.nc ' + opts.ifold, shell=True)
sub.call('rmdir ' +  opts.ifold + '/corrected', shell=True)


