import numpy as np
from optparse import OptionParser
import os
from netCDF4 import Dataset as NetCDFFile
from scipy.stats.stats import pearsonr
import nc_var_tools as ncvar
import subprocess as sub
import csv

errormsg='check_postprocess.py: ERROR -- error -- ERROR -- error'
warnmsg='check_postprocess.py: WARNING -- warning -- WARNING -- warning'

## g.e. # check_postprocess.py -r /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table -a no
## g.e. # check_postprocess.py -r /home/z3393242/studies/NARCliM/postprocess/nnrp/R2/d02/requested_variables.inf.ccrc.NARCliM11.Hamish.d02_1970-1989 -w /home/z3393242/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table -a yes

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def set_newline_val(linev):
    """ function to provide the values of a line readed from a file without
        spaces and end-line character
    >>> print set_newline_val('12312 abc asdad       123121     131 1 11 1    1 1    \n')
    ['12312', 'abc', 'asdad', '123121', '131', '1', '11', '1', '1', '1']
    """
    linevals = filter(None, linev.split(' '))
    newlineval = []
    for val in linevals:
        newlineval.append(val.replace('\n','').replace('\r',''))

    newlineval = filter(None, newlineval)

    return newlineval

def WRF_CFvariableNames(tablef):
    """ Provides the conversion of WRF variables name to a CF-convention one reading it from a table (wrfncxnj.table)
    """
    WRFcfVAR = {}
    for line in csv.reader(open(tablef, "r"), delimiter=" ", skipinitialspace=True):
        if not line[0][0] == '#':
            WRFcfVAR[line[0]] = line[1]

    return WRFcfVAR
        
def FilesInFolder(ifold, pfile, position):
    """ 
    Function to retrieve a list with the files in a folder [ifold] according to a pattern [pfile] in 
      [postion](beg, beginning; cent, center; end, ending)
    """
    ins=ifold 
    poins=sub.Popen(["ls","-1",ins], stdout=sub.PIPE)
    oins, err = poins.communicate()

    files=oins.split('\n')

    Lpfile=len(pfile)
    ifiles = []
    for ffile in files:
        if position == 'beg':
            if pfile in ffile[0:Lpfile]:
                ifiles.append(ffile)
        elif position == 'cent':
            if pfile in ffile:
                ifiles.append(ffile)
        elif position == 'end':
            Lffile=len(ffile)
            if pfile in ffile[Lffile-Lpfile:Lffile]:
                ifiles.append(ffile)
        else:
            print errmsg
            print '    "' + position + '" position for pattern not prepared!'
            quit(-1)
 

    if len(ifiles) == 0:
        ifiles = None

    return ifiles

class fileinf(object):
    """ Class of the post-processed file 
    """
    def __init__(self, var):
        if var is None:
            self.name = None
            self.kind = None
            self.lev = None
            self.freq = None
            self.period = None
        else:
            varv = var.split('|')
            self.name=varv[0]
            self.kind=varv[1]
            self.lev=varv[2]
            self.freq=varv[3]
            self.period=varv[4]

def listdate_2_date(listdate):
  """ Conversion of a list date [Y] [Y] [Y] [Y] '-' [M] [M] '-' [D] [D] '_' [H] [H] ':' [M] [M] ':' [S] [S]

  >>> print timelist
  ['2' '0' '0' '2' '-' '0' '2' '-' '1' '7' '_' '0' '0' ':' '0' '0' ':' '0' '0']
  >>> listdate_2_date(timelist).strftime("%Y/%m/%d %H:%M:%S")
  2002/02/17 00:00:00
  """
  import datetime as dt  
  
  year=int(listdate[0])*1000 + int(listdate[1])*100 + int(listdate[2])*10 + int(listdate[3])
  month=int(listdate[5])*10 + int(listdate[6])
  day=int(listdate[8])*10 + int(listdate[9])
  hour=int(listdate[11])*10 + int(listdate[12])
  minute=int(listdate[14])*10 + int(listdate[15])
  second=int(listdate[17])*10 + int(listdate[18])

  date = dt.datetime(year, month, day, hour, minute, second)
  return date

class time_information(object):
    """ Class  to provide information about variable time
    """

    def __init__(self, ncfu, tname):
        import datetime as dt
        if ncfu is None:
            self.name = None
            self.units = None
            self.calendar = None
            self.Srefdate = None
            self.refdate = None
            self.dT = None
            self.firstT = None
            self.firstTS = None
            self.lastT = None
            self.lastTS = None
            self.dimt = None
        else:
            self.name = tname
            if not ncfu.variables.has_key(tname):
                print errormsg
                print '    time_information: netcdf does not have the variable: "' + tname + '"'
                quit(-1)

            times = ncfu.variables[tname]
            timevarVal = times[:]

            attvar = times.ncattrs()
            if not searchInlist(attvar, 'units'):
#                print warnmsg
#                print '    time_information:"' + tname + '" does not have attribute: "units"'
#                print '    imposing units=hours since 1949-12-01 00:00:00'
                self.units = 'hours since 1949-12-01 00:00:00'
            else:
                self.units = times.getncattr('units')

            txtunits = self.units.split(' ')
            tunits = txtunits[0]

# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
            timeval = self.units.find(':')

            if not timeval == -1:
                self.Srefdate = txtunits[len(txtunits) - 2] + '_' + txtunits[len(txtunits) - 1]
                self.refdate = ncvar.datetimeStr_datetime(self.Srefdate)
            else:
                self.Srefdate = txtunits[len(txtunits) - 1]
                self.refdate = ncvar.dateStr_date(self.Srefdate)

            if not searchInlist(attvar, 'calendar'):
#                print warnmsg
#                print '    time_information:"' + tname + '" does not have attribute: "calendar"'
#                print '    imposing calendar=standard'
                self.calendar = 'standard'
            else:
                self.calendar = times.getncattr('calendar')

            if len(timevarVal.shape) == 1:
                Ntimes = timevarVal.shape[0]
                self.dimt = Ntimes
                self.dT = float(timevarVal[1]-timevarVal[0])
                self.firstT = timevarVal[0]
                time0 = self.refdate + dt.timedelta(seconds=self.firstT*3600)
                self.firstTS = time0.strftime("%Y%m%d%H%M%S")
                self.lastT = timevarVal[Ntimes-1]
                time0 = self.refdate + dt.timedelta(seconds=self.lastT*3600)
                self.lastTS = time0.strftime("%Y%m%d%H%M%S")
            else:
                Ntimes = timevarVal.shape[0]
                time0 = listdate_2_date(timevarVal[0])
                time1 = listdate_2_date(timevarVal[1])
                timeN = listdate_2_date(timevarVal[Ntimes-1])
                dtime = time1 - time0
                self.dimt = Ntimes
                self.dT = int(dtime.days*24 + dtime.seconds/3600)
                DfirsT = time0 - self.refdate
                self.firstT = DfirsT.days*24 + DfirsT.seconds/3600
                self.firstTS = time0.strftime("%Y%m%d%H%M%S")
                DlastT = timeN - self.refdate
                self.lastT = DlastT.days*24 + DlastT.seconds/3600
                self.lastTS = timeN.strftime("%Y%m%d%H%M%S")

def text_dT(dT):
    """ Transform a dT in a given string format
    """
    typedT=type(dT)
    if type(dT) == type(int(1)):
        string = "{0:02d}H".format(dT)
    elif type(dT) == type(np.int(1)):
        string = "{0:02d}H".format(dT)
    elif type(dT) == type(np.int32(1)):
        string = "{0:02d}H".format(dT)
    elif type(dT) == type(np.float(1)):
        string = "{0:02d}H".format(dT)
    elif type(dT) == type(np.float32(1)):
        string = "{0:02d}H".format(dT)
    elif dT > 600 and dT < 2000:
        string = 'MON'        
    elif dT > 2000:
        string = 'SES'        
    else:
        print errormsg
        print '    text_dT: "', dT, '" is not ready !!!'
        quit(-1)

    if string=='24H': string='DAY'

    return string

def final_stats(stn):
    """ Function to provide the final name in the file according to the statistics used
    >>> final_stats('DAM')
    ['DAY', 'mean']
    """
    if stn[0:1] == '&':
        begstn = stn[1:3]
        endstn = stn[3:4]
    else:
        begstn = stn[0:2]
        endstn = stn[2:3]

    if endstn == 'H':
        filefreq = stn
    elif endstn == 'm':
        filefreq = '{:%02dN}'.format(int(begstn))
    else:
        if begstn == 'DA':
            filefreq = 'DAY'
        elif begstn == 'MO':
            filefreq = 'MON'
        elif begstn == 'SE':
            filefreq = 'SES'
        elif begstn == 'YE':
            filefreq = 'YEA'
        else:
            print errormsg
            print '    final_stats: "' + begstn + '" statistic header not ready!!!'
            quit(-1)

    if endstn == 'N':
        varst = 'min'
    elif endstn == 'X':
        varst = 'max'
    elif endstn == 'M':
        varst = 'mean'
    elif endstn == 'S':
        varst = 'ac'
    elif endstn == 'H':
        varst = ''
    elif endstn == 'm':
        varst = ''
    else:
        print errormsg
        print '  final_stats: "' + endstn + '" statistic surname not ready!!!'
        quit(-1)

    return [filefreq, varst]

def IsVarStat(varn, st):
    """ Function to give if a variable has to have statistical 'surname' to avoid that a variable name has double statistic section 
    name (the same if variable name ends with 'tstep')
    >>> print IsVarStat('pracc', 'ac')
    pracc
    >>> print IsVarStat('pracc', 'min')
    praccmin
    >>> print IsVarStat('tasmax', 'min')
    tasmax
    """ 
    stats=['min', 'max', 'mean', 'ac']
    NOstatsvar = {}
    NOstatsvar['pracc'] = 'ac'

    Nstats = len(stats)
    varst = True
    for ist in range(Nstats):
        hastat = varn.find(stats[ist])
        if not hastat == -1 and varst:
            varst = False
            stfound = stats[ist]

# Variables names with statistics surnames 'per se'
    if searchInlist(NOstatsvar.keys(), varn):
        if st == NOstatsvar[varn]:
            varst = False
        else:
            varst = True

    if varst:
        newvarn = varn + st
    else:
        newvarn = varn

    return newvarn

def Ntimesteps(period, freq):
    """ Function to give the number of time steps according to a given period [YYYYi]-[YYYYf] and frequency
    print Ntimesteps('1970-1980', 'DAY')
    4018
    """
    import datetime as dt

    yri = int(period.split('-')[0])
    yrf = int(period.split('-')[1])
    yeari=dt.datetime(yri, 1, 1, 0, 0, 0)
    yearf=dt.datetime(yrf+1, 1, 1, 0, 0, 0)

    difft = yearf - yeari
    diffhours = difft.days*24 + difft.seconds/3600

    if freq[2:3] == 'H':
        Ntimes = int(diffhours /float(freq[0:2]))
    elif freq == 'DAY':
        Ntimes = int(diffhours /24.)
    elif freq == 'MON':
        Ntimes = int((yrf - yri + 1)*12)
    elif freq == 'SES':
        Ntimes = int((yrf - yri + 1)*4)
    else:
        print errormsg
        print '    Ntimesteps: "' + freq + '" not ready!!'
        print quit(-1)

    return Ntimes

def right_period(per, reqperiods):
    """ Function to provide the right value of a period
    >>> reqper = {'per10y1': '1950-1969', 'per10y2': '1970-1979', 'per10y3': '1980-1999', 'per10y4': '1990-1999'}
    print right_period('per10y3,per10y4',reqper)
    1980-1990
    """
    iscoma = per.find(',')
    if iscoma == -1:
        newper = reqperiods[per]
    else:
        pers = per.split(',')
        peri = reqperiods[pers[0]]
        perf = reqperiods[pers[len(pers)-1]]
        yri = peri.split('-')[0]
        yrf = perf.split('-')[1]
        newper = yri + '-' + yrf

    return newper

def check_times(per, vtimes, freq, refdate):
    """ Function to check times within a netCDF file
    """
    import datetime as dt

    firstyr = int(per.split('-')[0])
    endyr = int(per.split('-')[1])
    dimtt = Ntimesteps(per, freq)

    first = dt.datetime(firstyr, 1, 1, 0, 0, 0)
    ttime = np.zeros((dimtt), dtype=float)
    if freq[2:3] == 'H':
        dtv = int(freq[0:2])
        valtimes = vtimes
        for it in range(dimtt):
            newtime = first + dt.timedelta(hours=dtv*it)
            difftime = newtime - refdate
            ttime[it] = difftime.days*24 + difftime.seconds/3600
        for it in range(len(vtimes)):
            nday = int(vtimes[it]/24)
            nsec = int((vtimes[it] - nday*24)*3600)
            ftime = refdate + dt.timedelta(nday, nsec)
            newtime = dt.datetime(ftime.year, ftime.month, ftime.day, ftime.hour, 0, 0)
            difftime = newtime - refdate
            valtimes[it] = difftime.days*24 + difftime.seconds/3600
    elif freq == 'DAY':
        valtimes = vtimes
        for it in range(dimtt):
            newtime = first + dt.timedelta(days=it)
            difftime = newtime - refdate
            ttime[it] = difftime.days*24 + difftime.seconds/3600
        for it in range(len(vtimes)):
            nday = int(vtimes[it]/24)
            nsec = int((vtimes[it] - nday*24)*3600)
            ftime = refdate + dt.timedelta(nday, nsec)
            newtime = dt.datetime(ftime.year, ftime.month, ftime.day, 0, 0, 0)
            difftime = newtime - refdate
            valtimes[it] = difftime.days*24 + difftime.seconds/3600
    elif freq == 'MON':
        imn = 0
        iyr = firstyr
        for it in range(dimtt):
            imn = imn + 1
            if imn > 12:
                iyr = iyr + 1
                imn = 1
            newtime = dt.datetime(iyr, imn, 1, 0, 0, 0)
            difftime = newtime - refdate
            ttime[it] = difftime.days*24 + difftime.seconds/3600
        valtimes = vtimes
        for it in range(len(vtimes)):
            nday = int(vtimes[it]/24)
            nsec = int((vtimes[it] - nday*24)*3600)
            ftime = refdate + dt.timedelta(nday, nsec)
            newtime = dt.datetime(ftime.year, ftime.month, 1, 0, 0, 0)
            difftime = newtime - refdate
            valtimes[it] = difftime.days*24 + difftime.seconds/3600

    elif freq == 'SES':
        imn = -2
        iyr = firstyr
        for it in range(dimtt):
            imn = imn + 3
            if imn > 12:
                iyr = iyr + 1
                imn = 1
            newtime = dt.datetime(iyr, imn, 1, 0, 0, 0)
            difftime = newtime - refdate
            ttime[it] = difftime.days*24 + difftime.seconds/3600
        valtimes = vtimes
        for it in range(len(vtimes)):
            nday = int(vtimes[it]/24)
            nsec = int((vtimes[it] - nday*24)*3600)
            ftime = refdate + dt.timedelta(nday, nsec)
            if ftime.month >= 12 and ftime.month <= 2:
                fmon = 1
            elif ftime.month >= 3 and ftime.month <= 5:
                fmon = 4
            elif ftime.month >= 6 and ftime.month <= 8:
                fmon = 7
            elif ftime.month >= 9 and ftime.month <= 11:
                fmon = 10
            newtime = dt.datetime(ftime.year, fmon, 1, 0, 0, 0)
            difftime = newtime - refdate
            valtimes[it] = difftime.days*24 + difftime.seconds/3600
    else:
        print errormsg
        print '    Ntimesteps: "' + freq + '" not ready!!'
        print quit(-1)

    nodiffvalues = sorted(list(set(ttime) - set(valtimes) ))
    yesdiffvalues = sorted(list(set(ttime) & set(valtimes) ))

    Nnodifftimes = len(nodiffvalues)
    Nyesdifftimes = len(yesdiffvalues)

    yesno = Nyesdifftimes - Nnodifftimes

    NOtimes = []
    NOtimes.append(str(yesno))
    if yesno < 0 or Nnodifftimes == 0:
        if Nnodifftimes == 0: NOtimes[0] = -yesno
        for it in range(Nyesdifftimes):
            nhours = int(yesdiffvalues[it])
            nday = int(yesdiffvalues[it]/24)
            nsec = (yesdiffvalues[it] - nday*24)*3600
            ftime = refdate + dt.timedelta(nday, nsec)
            NOtimes.append(ftime.strftime("%Y%m%d%H%M%S"))

    if yesno > 0 or Nyesdifftimes == 0:
        if Nyesdifftimes == 0: NOtimes[0] = -yesno
        for it in range(Nnodifftimes):
            nhours = int(nodiffvalues[it])
            nday = int(nodiffvalues[it]/24)
            nsec = (nodiffvalues[it] - nday*24)*3600
            ftime = refdate + dt.timedelta(nday, nsec)
            NOtimes.append(ftime.strftime("%Y%m%d%H%M%S"))
   
    return NOtimes

#######    ########
## MAIN
    #######
parser = OptionParser()

parser.add_option("-a", "--all", dest="allflag", 
                  help="do you want to check all the variables (yes) or only that ones (no) without ('#' at the begining)", metavar="FLAG")
parser.add_option("-r", "--requested_file", dest="rfile", 
                  help="requested file of the postprocess", metavar="FILENAME")
parser.add_option("-w", "--CFvarTableName", dest="cftable", 
                  help="table with CF variable names (wrfncxnj.table)", metavar="FILENAME")

(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
ofile = 'check_postprocess.inf'

if not os.path.isfile(opts.rfile):
    print errormsg
    print '  requested file "' + opts.rfile + '" does not exist !!'
    print errormsg
    quit(-1)    

if not os.path.isfile(opts.cftable):
    print errormsg
    print '  requested file "' + opts.cftable + '" does not exist !!'
    print errormsg
    quit(-1)    

WRFcfVarNames = WRF_CFvariableNames(opts.cftable)

fr = open(opts.rfile, 'r')

requestedvars={}
requestedperiods={}
requestedvariables=[]

iline=0
for line in fr:
    lineval = set_newline_val(line)
    if not len(lineval) == 0 and not lineval[0][0:1] == '#' and iline < 20:
        if iline == 0:
            requestedvars[lineval[0]] = lineval[1]
        elif iline == 10:
            requestedperiods[lineval[0].replace('@', '')] = lineval[1]
    elif iline == 20:
        if not lineval[0] == '#':
            requestedvariables.append(lineval[0]+'|'+lineval[1]+'|'+lineval[2]+'|'+lineval[3]+'|'+lineval[4])
        else:
            if opts.allflag == 'yes':
                requestedvariables.append(lineval[1]+'|'+lineval[2]+'|'+lineval[3]+'|'+lineval[4]+'|'+lineval[5])

    elif not len(lineval) == 0 and lineval[0] == '#Periods#':
        iline = 10
    elif not len(lineval) == 0 and lineval[0] == '#Variables#':
        iline = 20

fr.close()

# Variables
##
Nrequestedvariables = len(requestedvariables)
nonfexist = open(ofile,'w')
nonfexist.write('# Report from "' + opts.rfile + '" ... .. .\n' )
Nnonfexist = 0
Nnonvarinfile = 0


#####################
## ADDED PART TO TAKE INTO ACCOUNT THAT SOME VARIABLES DO NOT HAVE THE LAST TIME STEP ##
# These variables are those that refer to the previous hour and do not come from instant values (some of the max, min, mean, accum) 
acummstatvars=['tasmeantstep','tasmintstep','tasmaxtstep','pr5maxtstep','pr10maxtstep','pr20maxtstep','pr30maxtstep','pr1Hmaxtstep','pracc','wssmaxtstep','wss5maxtstep','wss10maxtstep','wss20maxtstep','wss30maxtstep','wss1Hmaxtstep'] #variables that won't have the last time step in the highest frequency files.
#Calculating the last period of 1 year, where some variables have one timestep less.
period_1year=[fileinf(requestedvariables[i]).period[-2:] for i in range(Nrequestedvariables) if fileinf(requestedvariables[i]).period[0:5] =='per1y']
last_per1y=str(np.max([ int(x) for x in period_1year ])).rjust(2,"0")
#Calculating the last period of 5 year, where some variables have one timestep less.
period_5year=[fileinf(requestedvariables[i]).period[-2:] for i in range(Nrequestedvariables) if fileinf(requestedvariables[i]).period[0:5] =='per5y']
last_per5y=str(np.max([ int(x) for x in period_5year ])).rjust(2,"0")
####################

for ivar in range(Nrequestedvariables):

    varinf = fileinf(requestedvariables[ivar])

    if not searchInlist(list(WRFcfVarNames.keys()), varinf.name):
        print errormsg
        print '  WRF variable "' + varinf.name + '" is not in the "' + opts.cftable + '" CF varnames transformation table !!!!'
        quit(-1)
    else:
        cfvarn = WRFcfVarNames[varinf.name]
        periodval = right_period(varinf.period, requestedperiods)
        print '  WRF: ',varinf.name,' post: ',cfvarn, '-- frequency "', varinf.freq,'" period (', varinf.period, '): ',periodval
        cffiles = FilesInFolder(requestedvars['POSTPROJDIR'], cfvarn, 'cent')

##        ncff = NetCDFFile(requestedvars['POSTPROJDIR'] + '/' + cffiles[0],'r')
##        cftimeinf = time_information(ncff, 'time')

        wrffiles = FilesInFolder(requestedvars['EXPDIR'], 'wrf' + varinf.kind, 'beg')

        nwrff = NetCDFFile(requestedvars['EXPDIR'] + '/' + wrffiles[0],'r')
        wrftimeinf = time_information(nwrff, 'Times')
        nwrff.close()
#        print wrftimeinf.name, wrftimeinf.dimt, wrftimeinf.units, wrftimeinf.calendar, wrftimeinf.Srefdate, wrftimeinf.refdate,           \
#          wrftimeinf.dT, wrftimeinf.firstT, wrftimeinf.firstTS, wrftimeinf.lastT, wrftimeinf.lastTS

# Frequency
##
        fromproj = varinf.freq.find('_')

        if fromproj == -1:  
            if varinf.freq == 'All':
                filefreq = text_dT(wrftimeinf.dT)
                filestat = ''
            else:
                filefreq = final_stats(varinf.freq)[0]
                filestat = final_stats(varinf.freq)[1]
        else:
            ffreq = varinf.freq.split('_')[0]
            filefreq = final_stats(varinf.freq)[0]
            filestat = final_stats(varinf.freq)[1]
            
        cfvarstat=IsVarStat(cfvarn,filestat)

        filepostn = requestedvars['@experiment@'] + '_' + filefreq + '_' +                       \
          periodval + '_' + cfvarstat + '.nc'
        print requestedvars['POSTPROJDIR']+'/'+filepostn
        postf = FilesInFolder(requestedvars['POSTPROJDIR'], filepostn, 'beg')
# Existence of file
##
        if postf is None or len(postf) < 1:
            print warnmsg
            print '  File: "' + filepostn + '" does not exist!'
            Nnonfexist = Nnonfexist + 1
            nonfexist.write('UNEXISTS: ' + filepostn + '\n')
# checking file
        else:
            ncf = NetCDFFile(requestedvars['POSTPROJDIR'] + '/' + str(postf[0]),'r')
            if not ncf.variables.has_key(cfvarstat):
                print warnmsg
                print '    File: "' + filepostn + '" does not have the variable "' + cfvarstat + '" !!'
                Nnonvarinfile = Nnonvarinfile + 1
                nonfexist.write(' ** ' + filepostn + ' does not have variable "' + cfvarstat + '"\n')
            else:
                if not ncf.variables.has_key('time'):
                    print warnmsg
                    print '    File: "' + filepostn + '" does not have the variable "time" !!'
                    Nnonvarinfile = Nnonvarinfile + 1
                    nonfexist.write(' ** ' + filepostn + ' does not have variable "time"\n')
                else:
                    ncvarv = ncf.variables['time']
                    dimt = ncvarv.shape[0]
                    theortimes = Ntimesteps(periodval, filefreq)
                    
                    # If the variable is one without the last timestep and we are in the last 1 or 5 year period, consider one timestep less
                    if ((cfvarstat in acummstatvars) & ((varinf.period == 'per5y%s' % last_per5y) | (varinf.period == 'per1y%s' % last_per1y))):
                        theortimes_def=theortimes-1
                    else:
                        theortimes_def=theortimes
                    
                    if not dimt == theortimes_def:
                        print warnmsg
                        print '    File: "' + filepostn + '" has', dimt,' time-steps and theoretically should have', theortimes_def,'!!'
                        ftinf = time_information(ncf, 'time')
                        Nnonvarinfile = Nnonvarinfile + 1
                        nonfexist.write(' tt ' + filepostn + ' has ' + str(dimt) + ' time-steps while expected ' + str(theortimes_def) + '\n')
                        timevals = ncvarv[:]
                        NOtimevals=check_times(periodval,timevals,filefreq,ftinf.refdate)
                        nNOtimevals = len(NOtimevals)
                        print 'nNOtimevals: ', nNOtimevals
                        if int(NOtimevals[0]) < 0:
                            strtime = 'ONLYEXISTS'
                        else:
                            strtime = 'UNEXISTS'
                        for it in range(nNOtimevals-1):
                            nonfexist.write(' tt.' + strtime + ' ' + NOtimevals[1+it] + '\n')
            ncf.close()

print 'FINISH -- finish -- FINISH -- finish'
print '  output file "' + ofile + '" with the ', Nnonfexist, ' non existing files has been written and ', Nnonvarinfile, ' files with issues'
