#!/bin/bash
## g.e. # ./fix_timebnds.bash /home/lluis/studies/NARCliMGreatSydney2km/data/d01/postHFRQ /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/postprocess/wrfncxnj/wrfncxnj.table
if test $1 = '-h'; then
  echo "**********************************"
  echo "*** Fxing 'time' & 'time_bnds' ***"
  echo "***   of post-processed file   ***"
  echo "***      of a given folder     ***"
  echo "**********************************"
  echo "./fix_timebnds.bash [fold] [wrfncxnj.tab](postprocess 'wrfncxnj.table' file)"
else
  module load python/2.7.2

  rootsh=`pwd`
  fold=$1
  wtab=$2

  echo "#"$0" "$* > run_removing.log
  for file in ${fold}/*.nc; do
    filen=`basename ${file}`
    echo ${filen}
    Nsecsf=`echo ${filen} | tr '_' ' ' | wc -w | awk '{print $1}'`
    varn=`echo ${filen} | tr '_' '\n' | tail -n 1`
    varn=`echo ${varn} | tr '.' ' ' | awk '{print $1}'`
    python ${rootsh}/../../../python/removing_time_bnds.py -f ${file} -v ${varn} -w ${wtab} 2>&1 >> run_removing.log

  done # end of files
fi
