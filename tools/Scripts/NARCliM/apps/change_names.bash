#!/bin/bash
if test $1 = '-h'; then
  echo "***********************"
  echo "*** Shell to change ***"
  echo "*** period names on ***"
  echo "***  post-processed ***"
  echo "***      files      ***"
  echo "***********************"
  echo "change_names.bash [fold](folder with post-processed files)"
else
  rootsh=`pwd`
  fold=$1

  cd ${fold}

# DAM
##
  for filen in *DAM*.nc; do
    secs=`echo $filen | tr '_' ' '`
    newName=''
    isec=1
    for sec in ${secs}; do
      if test ${sec} = 'DAM'; then
        newName=${newName}'_DAY'
      else
        if test ${isec} -eq 1; then
          newName=${newName}${sec}
        else
          newName=${newName}'_'${sec}
        fi
      fi 
      isec=`expr $isec + 1`
    done # end of secs
  mv ${filen} ${newName}
  done # end of daily files

# MOM
##
  for filen in *MOM*.nc; do
    secs=`echo $filen | tr '_' ' '`
    newName=''
    isec=1
    for sec in ${secs}; do
      if test ${sec} = 'MOM'; then
        newName=${newName}'_MON'
      else
        if test ${isec} -eq 1; then
          newName=${newName}${sec}
        else
          newName=${newName}'_'${sec}
        fi
      fi 
      isec=`expr $isec + 1`
    done # end of secs
  mv ${filen} ${newName}
  done # end of monthly files

# YEM
##
  for filen in *YEM*.nc; do
    secs=`echo $filen | tr '_' ' '`
    newName=''
    isec=1
    for sec in ${secs}; do
      if test ${sec} = 'YEM'; then
        newName=${newName}'_YEA'
      else
        if test ${isec} -eq 1; then
          newName=${newName}${sec}
        else
          newName=${newName}'_'${sec}
        fi
      fi 
      isec=`expr $isec + 1`
    done # end of secs
  mv ${filen} ${newName}
  done # end of yearly files

# SEM
##
  for filen in *SEM*.nc; do
    secs=`echo $filen | tr '_' ' '`
    newName=''
    isec=1
    for sec in ${secs}; do
      if test ${sec} = 'SEM'; then
        newName=${newName}'_SEA'
      else
        if test ${isec} -eq 1; then
          newName=${newName}${sec}
        else
          newName=${newName}'_'${sec}
        fi
      fi 
      isec=`expr $isec + 1`
    done # end of secs
  mv ${filen} ${newName}
  done # end of seasonal files


fi
