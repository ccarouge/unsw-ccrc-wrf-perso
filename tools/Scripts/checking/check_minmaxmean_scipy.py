#!/share/apps/python/2.7.2/bin/python
##!/usr/bin/python
# e.g. ccrc468-17 # ./check_minmaxmean.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out -v T2 -p 30 30
# e.g. cyclone# ./check_minmaxmean_scipy.py -f /srv/ccrc/data13/z3273429/NARClim/Sydney/wrf/mk3.5/out/ -v T2 -p 30 30

from optparse import OptionParser
import numpy as np
from scipy.io import netcdf
##from Scientific.IO.NetCDF import *
##from pylab import *
import subprocess as sub

### Options

parser = OptionParser()
parser.add_option("-f", "--folder", dest="foldname",
                  help="folder to check", metavar="FOLD")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check", metavar="PT")
(opts, args) = parser.parse_args()

def average(values):
    """Computes the arithmetic mean of a list of numbers.

    >>> print average([20, 30, 70])
    40.0
    """
    return sum(values, 0.0) / len(values)

##import doctest
##doctest.testmod()

#######    #######
## MAIN
    #######

### Static values

headfile='wrfxtrm'
timename='time'
dom='1'

outputfile='check_minmaxmean.inf'

####### ###### ##### #### ### ## #

ins='ls ' + '-1 ' + opts.foldname + '/' + headfile + '_d0' + dom + '_*'
cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
cmd=cmdout.communicate()[0] 
files=cmd.split('\n')
Nfiles=len(files)-1
filesOK=files[0:Nfiles]

point=np.array(opts.point)
#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

outf=open(outputfile, 'a')

for filen in filesOK:
  ncf = netcdf.netcdf_file(filen,'r')
# 
# Minimum
  stat='MIN'
  varn=opts.varname + stat
  varMinVal = ncf.variables[varn]
#  varMinVal = varMin.getValue()
  dimt = varMinVal.shape[0]
  varMinValPt=varMinVal[0:dimt,point[0],point[1]]

# 
# Maximum
  stat='MAX'
  varn=opts.varname + stat

  varMaxVal = ncf.variables[varn]
##  varMaxVal = varMax.getValue()
##  varMaxVal = varMax.getValue()
  varMaxValPt=varMaxVal[0:dimt,point[0],point[1]]

  diff=varMaxValPt - varMinValPt
  meandiff=average(diff)
  if  meandiff == 0:
    mssg=filen + '; < T2MAX == T2MIN > == ' + str(meandiff) + ' !!!!\n'
    print mssg 
    outf.write(mssg)
  
  ncf.close

