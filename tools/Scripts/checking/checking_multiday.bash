#!/bin/bash
# ccrc468-17 # e.g. # ./checking_multiday.bash /home/lluis/studies/NARCliMGreatSydney2km/data/2040-2060/out/:hrly:1 ~/studies/NARCliMGreatSydney2km/data/2040-2060/out/wrfout_d01_2039-11-01_00\:00\:00 151.23406:-33.91775:149.36139:-33.84076 T2:U10:V10:Q2:TH2:PSFC:RAINC:RAINNC Time /usr/bin/R /home/lluis/bin/gcc_cdo-1.5.3/bin/cdo /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G
# cyclone # e.g. # ./checking_multiday.bash /srv/ccrc/data13/z3393242/studies/NARCliMGreatSydney2km/mk3.5_2040-2060/out:hrly:1 /srv/ccrc/data13/z3393242/studies/NARCliMGreatSydney2km/mk3.5_2040-2060/out/wrfout_d01_2039-11-01_00\:00\:00 151.23406:-33.91775:149.36139:-33.84076 T2:U10:V10:Q2:TH2:PSFC:RAINC:RAINNC Time /share/apps/R/2.13.2/bin/R /share/apps/cdo/1.4.7/bin/cdo /home/z3393242/studies/WRF4G
if test $1 = '-h'; then
  echo "*************************************"
  echo "***    Shell function to check    ***"
  echo "*** content of a series of netCDF ***"
  echo "***  netCDF plotting daily cycles ***"
  echo "*************************************"
  echo "checking_multiday.bash [chkfoldKindDom] [chkfullfile] [points] [variables] [timename] [Rexec] [CDOexec] [WRF4Gh]"
  echo "  [chkfoldKindDom]: folder with the files, kind of files and domain number ([fold]:[kind]:[dom])"
  echo "     [kind]: out, hrly, dly, xtrm, ... "
  echo "  [chkfullfile]: file with all possible variables (e.g.: non postprocessed)"
  echo "  [points]: points to plot temporal series (lon1:lat1:lon2:lat2:...:lonN:latN) "
  echo "  [variables]: name of the variables to plot "
  echo "  [timename]: name of the variable with the 'time' information "
  echo "  [Rexec]: binary of 'R' (R.org) " 
  echo "  [CDOexec]: binary of 'CDO' (Climate Data Operators) "
  echo "  [WRF4Gh]: WRF4G home folder "
else

check_Ntimes(){
# Function to get Ntimes from a namelist.output of a given wrffile
#   [wrff]: file to check
#   [varn]: name of the variable
#   [ffrq]: file frequency of the output (seconds)
#   [fper]: covered period by the file (seconds)
#   [timen]: name of the time variable
#   [WRF4Gh]: folder with the WRF4G structure

  wrff=$1
  varn=$2
  ffrq=$3
  fper=$4
  timen=$5
  WRF4Gh=$6

  wrfk=`basename ${wrff}`

  nvalues=`expr ${fper} / ${ffrq}`

  fileNsteps=`${WRF4Gh}/util/Fortran/nc_var_inf ${wrff} ${varn} | grep -a --text DIMinf | grep ${timen} | awk '{print $4}'`

  if test ${nvalues} -ne ${fileNsteps}; then
    echo "ERROR -- errorr -- ERROR -- error"
    echo "  File '"${wrff}"' "
    echo "  must contain "${nvalues}" values, but it only has "${fileNsteps}" !!! !! !"
    echo "ERROR -- errorr -- ERROR -- error"
#    exit
  fi
}

#######    #######
## MAIN
    #######
  rootsh=`pwd`

  c1='$'
  c2=\'
  chkfoldKindDom=$1
  chkfullfile=$2
  points=$3
  varnames=$4
  timename=$5
  Rexec=$6
  CDOexec=$7
  WRF4h=$8

  hostn=`echo ${HOSTNAME}`
  if test ${hostn} = 'cyclone.ccrc.unsw.edu.au'; then
    module load hdf5/1.8.8-intel
    module load netcdf/4.1.3-intel
    module load R/2.13.2 
    module load cdo/1.4.7
  fi

#
# Writting points in 'R' format
#
  Npts=`echo $points | tr ':' ' ' | wc -w | awk '{print $1/2}'`
  N2pts=`expr ${Npts} '*' 2`
  Rpts=''
  ipt=1
  while test ${ipt} -le ${N2pts}
  do
    if test ${ipt} -eq 1
    then
      Rpts=${Rpts}$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    else    
      Rpts=${Rpts}', '$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    fi
    ipt=`expr ${ipt} + 1`
  done # end Npts 
  Rpts=${Rpts}
  
#
# Writting files in 'R' format
#
  chkfold=`echo ${chkfoldKindDom} | tr ':' ' ' | awk '{print $1}'`
  kindf=`echo ${chkfoldKindDom} | tr ':' ' ' | awk '{print $2}'`
  dom=`echo ${chkfoldKindDom} | tr ':' ' ' | awk '{print $3}'`
  
  files=`ls -1 ${chkfold}/wrf${kindf}_d0${dom}_*`
  Nfiles=`echo $files | wc -w`
  Rfiles=''
  ifile=1
  while test ${ifile} -le ${Nfiles}
  do
    file=`echo ${files} | tr ' ' '\n' | head -n ${ifile} | tail -n 1`
    if test ${ifile} -eq 1
    then
      Rfiles=${Rfiles}${c2}${file}${c2}
    else    
      Rfiles=${Rfiles}', '${c2}${file}${c2}
    fi
    ifile=`expr ${ifile} + 1`
  done # end Npts 

  mkdir -p ${chkfold}/checking_multiDay

  vars=`echo ${varnames} | tr ':' ' '`
  Nvars=`echo ${varnames} | tr ':' ' ' | wc -w`

  for varname in ${vars}; do
    echo "VARIABLE: '"${varname}"'"
    rm ${rootsh}/checking_multiDay/${varname}_*.png >& /dev/null

    cat << EOF > ${chkfold}/checking_multiDay/${varname}_checking_multiday.R
#Checking postprocess of variable ${varname}
library(ncdf)
lonlat_point<-function(varfile, lonname, latname, landmaskname='LANDMASK', points) {
# R Function to provide nearest grid point to a given longitude, latitude
  Npoints<-nrow(points)
  posmin<-rep(0,Npoints*2)
  dim(posmin)<-c(Npoints,2)
  print(points)

  ncvar<-open.ncdf(varfile)
  varinf<-ncunit_variable_info(ncvar, lonname)
  shapevar<-varinf[4]

  if (shapevar == 2) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1), count=c(-1,-1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1), count=c(-1,-1))
  }
  if (shapevar == 3) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1), count=c(-1,-1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1), count=c(-1,-1,1))
  }
  if (shapevar == 4) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1,1), count=c(-1,-1,1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1,1), count=c(-1,-1,1,1))
  }
  close.ncdf(ncvar)

  dimx<-nrow(lonmat)
  dimy<-ncol(lonmat)

  for ( i in 1:Npoints) {
    dist<-sqrt((lonmat-points[i,1])**2.+(latmat-points[i,2])**2.)
    pmindist<-which.min(dist)
##    yposmin<-as.integer(pmindist/dimx)+1
##    xposmin<-as.integer(pmindist-(yposmin-1)*dimx)
##    posmin[i,]<-c(xposmin, yposmin)
    xcoord<-as.integer(pmindist/dimx)
    posmin[i,1]<-pmindist-(xcoord)*dimx
    posmin[i,2]<-xcoord
  }
  print(posmin)
  posmin
}

ncunit_dim_info<-function(ncunit, dimension)
### R function to obtain nc variable information from a ncunit
##
{
  ndimnc<-length(ncunit${c1}dim)
  dimncnames<-matrix(0,ndimnc)
  dim(dimncnames)<-ndimnc

  for (idim in 1:ndimnc) {
    dimncnames[idim]<-ncunit${c1}dim[[idim]]${c1}name}

  pos_ncdim<-which(dimncnames==dimension)
  ncdim<-ncunit${c1}dim[[pos_ncdim]]
  ncdimname<-ncdim${c1}name
  ncdimid<-ncdim${c1}id
  ncdimsize<-ncdim${c1}len
  ncdimunits<-ncdim${c1}units
  nc_dimension_info<-c(dimension, ncdimname, ncdimid, ncdimsize, ncdimunits)

  nc_dimension_info
}
ncunit_variable_info<-function(ncunit, variable)

### R function to obtain nc variable information from a ncunit
##
{
nvarnc<-length(ncunit${c1}var)
varncnames<-matrix(0,nvarnc)
dim(varncnames)<-nvarnc

for (ivar in 1:nvarnc) {
varncnames[ivar]<-ncunit${c1}var[[ivar]]${c1}name}

pos_ncvar<-which(varncnames==variable)
ncvar<-ncunit${c1}var[[pos_ncvar]]
ncvarname<-ncvar${c1}name
ncvarid<-ncvar${c1}id
ncvardims<-ncvar${c1}ndims
ncvarunits<-ncvar${c1}units
ncvarsize<-ncvar${c1}varsize
nc_variable_info<-c(variable, ncvarname, ncvarid, ncvardims, ncvarunits, ncvarsize)

nc_variable_info
}

check_var<-function(Rvarfile, VarName){
# Function to check the characteristics of a variable according to information in an ASCII file
#   Rvarfile: 
#     1st col: variable name
#     2nd col: sign characteristics (pos; only positive values, posneg, positive and negative values)
#     3rd col: mininum absolute value (below will be coerced to NA)
#     4th col: maximum absolute value (above will be coerced to NA)
#
  Rvarvals<-read.table(Rvarfile, skip=1)
  Rvarvals<-as.matrix(Rvarvals)
  Nvars<-length(Rvarvals)/4
  dim(Rvarvals)<-c(Nvars,4)  

  posvar<-which(Rvarvals[,1] == VarName)
  Rvarvals[posvar,]
}

#
# Main
#
varname<-'${varname}'
filevarname<-'${varname}'
timename<-'${timename}'
landmask<-'LANDMASK'
geofile<-'${chkfullfile}'
longname<-'XLONG'
latname<-'XLAT'

EOF
    if test ${hostn} = 'cyclone.ccrc.unsw.edu.au'; then
      cat << EOF >> ${chkfold}/checking_multiDay/${varname}_checking_multiday.R
varRfile<-'/home/z3393242/R/variablesR.dat'

EOF
    else
      cat << EOF >> ${chkfold}/checking_multiDay/${varname}_checking_multiday.R
varRfile<-'/home/lluis/R/variablesR.dat'

EOF
    fi
      cat << EOF >> ${chkfold}/checking_multiDay/${varname}_checking_multiday.R
plot2D<-1
plot1D<-1

# Var characteristics
#
varGENinf<-check_var(varRfile, varname)
print(varGENinf)
varsign<-varGENinf[2]
varABSmin<-as.real(varGENinf[3])
varABSmax<-as.real(varGENinf[4])

colorsRGB1D<-colorRamp(c(rgb(0,0,1,1),rgb(0,1,0,1),rgb(1,0,0,1)))
if (varsign == 'pos') {
  colorsRGB2D<-colorRamp(c(rgb(0,0,1,1),rgb(0,1,0,1),rgb(1,0,0,1)))
} else {
  colorsRGB2D<-colorRamp(c(rgb(0,0,1,1),rgb(1,1,1,1),rgb(1,0,0,1)))
}

colbar2D<-rgb(colorsRGB2D(seq(0,1,length=20)),max=255)
names<-c("00UTC", "06UTC", "12UTC", "18UTC")
timevals<-c(1,7,13,19)

####### ###### ##### #### ### ## #
points<-c(${Rpts})
Npts<-${Npts}

filens<-c(${Rfiles})
Nfiles<-length(filens)

cat(varname,"Retrieving domain variables ... .. .", fill=TRUE)
nclm<-open.ncdf(geofile)
comps<-list()
Ncomps<-3
compsn<-rep(0,3)
compsn<-c(landmask, longname, latname)
for (icomp in 1:Ncomps) {
  shapevar<-ncunit_variable_info(nclm, compsn[icomp])[4]
  if (shapevar ==3 ) {
    comps[[compsn[icomp]]]<-get.var.ncdf(nclm, compsn[[icomp]], start=c(1,1,1), count=c(-1,-1,1))
  } else {
    comps[[compsn[icomp]]]<-get.var.ncdf(nclm, compsn[[icomp]], start=c(1,1), count=c(-1,-1))
  }
} # end of comps

close.ncdf(nclm)

dimx<-nrow(comps[[1]])
dimy<-ncol(comps[[1]])

lonlatpt<-rep(0, 2*Npts)
dim(lonlatpt)<-c(Npts,2)
lonlatpt[1:Npts,1]<-points[seq(1,2*Npts,2)]
lonlatpt[1:Npts,2]<-points[seq(2,2*Npts,2)]

ncvar<-open.ncdf(filens[1])
timeinf<-ncunit_dim_info(ncvar, timename)
timeunits<-timeinf[5]
timevar<-get.var.ncdf(ncvar, timename, start=c(1), count=c(-1))

varinf<-ncunit_variable_info(ncvar, varname)
shapevar<-varinf[4]
ncunits<-varinf[5]
dimx<-as.integer(varinf[6])
dimy<-as.integer(varinf[7])
close.ncdf(ncvar)

# Points 
#
pointPos<-lonlat_point(geofile, 'XLONG', 'XLAT', 'LANDMASK', lonlatpt)
ptscoord<-pointPos
ptscoord[,1]<-as.real(pointPos[,1]/dimx)
ptscoord[,2]<-as.real(pointPos[,2]/dimy)
print(ptscoord)

#
# Checking for total time steps
# 
cat(varname,"Checking total time-steps ... .. .", fill=TRUE)
ntimes<-0
for (ifl in 1:Nfiles) {
  ncvar<-open.ncdf(filens[ifl])
  timeinf<-ncunit_dim_info(ncvar, timename)
  ntimes<-ntimes+as.integer(timeinf[4])
}

mod24<-ntimes-24*as.integer(ntimes/24)
if (mod24 != 0) {
  print(paste("Non 24 h total time-steps !!!", ntimes, ":", ntimes/24, sep=" "))
  q()
}

cat(varname,"Filling up 24h object... .. .", fill=TRUE)
var24h<-list()
var24h${c1}dimx<-dimx
var24h${c1}dimy<-dimy
var24h${c1}nTOTtimes<-ntimes

for (ifl in 1:Nfiles) {
  filen<-filens[ifl]
  iflS<-sprintf("%02d",ifl)

  ncvar<-open.ncdf(filen)
  varinf<-ncunit_variable_info(ncvar, varname)

  if (shapevar == 3) {
    varval<-get.var.ncdf(ncvar, varname, start=c(1,1,1), count=c(-1,-1,-1))
    varTf<-as.integer(varinf[8])
  }

  if (shapevar == 4) {
    varval<-get.var.ncdf(ncvar, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
    varTf<-as.integer(varinf[9])
  }
  if (varABSmin != -9999.99) {varval<-ifelse(varval<varABSmin, NA, varval)}
  if (varABSmax != -9999.99) {varval<-ifelse(varval>varABSmax, NA, varval)}

  Ndays<-varTf/24
  cat('  file #',ifl,'with',varTf,'time-steps',Ndays,'days',fill=TRUE)
  varFval<-rep(0,length(varval))
  dim(varFval)<-c(dimx,dimy,Ndays,24)
  for (id in 1:Ndays) {
    Tst<-(id-1)*24+1
    Ten<-id*24
    varFval[,,id,1:24]<-varval[,,Tst:Ten]
  }
  var24h[[iflS]]${c1}filen<-filen
  var24h[[iflS]]${c1}Ndays<-Ndays
  var24h[[iflS]]${c1}dailyval<-varFval
  close.ncdf(ncvar)
} # end of files

#
# 2D plot
#
if (plot2D == 1) {
cat(varname,"Computing daily-hourly means... .. .", fill=TRUE)

meanF24<-rep(0,dimx*dimy*24*Nfiles)
dim(meanF24)<-c(Nfiles,dimx,dimy,24)

for (ifl in 1:Nfiles) {
  iflS<-sprintf("%02d",ifl)
  values<-var24h[[iflS]]${c1}dailyval
  meanF24[ifl,,,]<-apply(var24h[[iflS]]${c1}dailyval,c(1,2,4), mean, na.rm=TRUE)
  var24h[[iflS]]${c1}meandailyval<-meanF24[ifl,,,]
}
##save(var24h, file=paste("${chkfold}","/checking_multiDay/var24h_",varname,".Robj", sep=""))

meanvar24h<-rep(NA,24*dimx*dimy)
dim(meanvar24h)<-c(dimx,dimy,24)
meanvar24h<-apply(meanF24,c(2,3,4), mean, na.rm=TRUE)
cat(varname,"Plotting 2D... .. .", fill=TRUE)

for ( itimestep in 1:4) {
  timestep<-timevals[itimestep]
  graphname<-paste("${chkfold}","/checking_multiDay/",filevarname,"_2Dmean_",names[itimestep],".png", sep="")
  graphtit<-paste(filevarname,"mean at",names[itimestep], sep=" ")
  minval<-min(meanvar24h[,,timestep], na.rm=TRUE)
  maxval<-max(meanvar24h[,,timestep], na.rm=TRUE)

  if (varsign == 'posneg') {
    minval<--max(abs(minval),maxval)
    maxval<--minval
  }
  cat('  ',filevarname,'min:',minval,'max:',maxval,fill=TRUE)
  bitmap(graphname)
##  postscript(graphname, bg="white", horizontal=FALSE, paper="default")

  par(xpd=NA)
  contour(comps[[landmask]], col=gray(0.75), nlevels=1, drawlabels=FALSE, 
    axes=FALSE)
#  plot.window(xlim=c(0.,1.),ylim=c(0.,1.), asp=dimy/dimx, log="",
  plot.window(xlim=c(0.,1.),ylim=c(0.,1.), log="",
    title(main=graphtit, xlab="W-E",ylab="S-N"))
  image(meanvar24h[,,timestep], col=colbar2D, add=TRUE)
  contour(comps[[longname]], col=gray(0.75),levels=seq(-180,180,by=5),add=TRUE)
  contour(comps[[latname]], col=gray(0.75),levels=seq(-90,90,by=5),add=TRUE)
  contour(comps[[landmask]], col=gray(0.25), nlevels=1, drawlabels=FALSE, add=TRUE)
  for (ipt in 1:Npts) {
    points(ptscoord[ipt,],col="black",pch=3)
    text(ptscoord[ipt,], labels=sprintf("%02d",ipt), pos=1)
    print(ptscoord[ipt,])
  }
  stepsval<-(maxval-minval)/4
  steps21val<-(maxval-minval)/20
  Lcolvar=length(colbar2D)
  seq9colbar<-seq(1, Lcolvar, (Lcolvar-1)/8)
  seq21colbar<-seq(1, Lcolvar, (Lcolvar-1)/20)
  symbols(seq(0.1,0.9,0.8/20), matrix(-0.04,21), rectangles=cbind(matrix(0.8/20,21), 
    matrix(0.05,21)), bg=colbar2D[seq21colbar], fg="gray", inches=FALSE, add=TRUE) 
  text(seq(0.1,0.9,0.8/20)[seq(1,21,20/4)], matrix(-0.1,4), labels=sprintf("%5.3f",
    seq(minval, maxval, stepsval)))

  dev.off()
}
} # end of plotting 2D
#
# Punctual plot
# 
if (plot1D == 1) {
colbar1D<-rgb(colorsRGB1D(seq(0,1,length=ntimes/24)),max=255)

cat(varname,"Plotting 1D... .. .", fill=TRUE)
for (ipt in 1:Npts) {
  ptS<-sprintf("%02d",ipt)
  mingraph<-1000000.0
  maxgraph<--mingraph

  Ntotdays<-0
  for (ifl in 1:Nfiles) {
    iflS<-sprintf("%02d",ifl)
    vals<-var24h[[iflS]]${c1}dailyval
    varpt<-vals[pointPos[ipt,1],pointPos[ipt,2],,]

    minval<-min(varpt, na.rm=TRUE)
    maxval<-max(varpt, na.rm=TRUE)

    if (minval < mingraph) {mingraph<-minval}
    if (maxval > maxgraph) {maxgraph<-maxval}
  } # end of files
  if (varsign == 'posneg') {
    mingraph<--max(abs(mingraph),maxgraph)
    maxgraph<--mingraph
  }  
  cat('  ',filevarname,'min:',mingraph,'max:',maxgraph,fill=TRUE)
  graphname<-paste("${chkfold}","/checking_multiDay/",filevarname,"_dimx2-dimy2_timeseries_",ptS,
    ".png", sep="")
  graphtit<-paste(filevarname,"at",lonlatpt[1,1],"E ,",lonlatpt[1,2],"N", sep=" ")
  xlabel<-paste(filevarname,"(",ncunits,")", sep=" ")

  bitmap(graphname)
##  postscript(graphname, bg="white", horizontal=FALSE, paper="default")
  plot("1", type="n", xlim=c(1,24), ylim=c(mingraph, maxgraph), main=graphtit, xaxs="i", yaxs="i",
    xlab='UTC hour of the day (h)', ylab=xlabel, xaxt="n")
  for (ifl in 1:Nfiles) {
    iflS<-sprintf("%02d",ifl)
    Ndays<-var24h[[iflS]]${c1}Ndays

    for (id in 1:Ndays) {
      vals<-var24h[[iflS]]${c1}dailyval
      varpt<-vals[pointPos[ipt,1],pointPos[ipt,2],id,]
      Ntotdays<-Ntotdays+1
      lines(1:24, varpt, "l", col=colbar1D[Ntotdays], xaxt="n", add=TRUE)
    } # end of days
  } # end of files
  if (varsign == 'posneg') {lines(c(1,24), rep(0,2), col="black", lwd=2)}

  axis(1,at=seq(0,18,6), labels=c('00','06','12','18'))
  dev.off()
} # end of points
} # end of plotting 1D
EOF

    ${Rexec} -f --slave ${chkfold}/checking_multiDay/${varname}_checking_multiday.R
##    exit
  done # end of vars
fi
