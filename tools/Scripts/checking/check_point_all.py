#!/usr/bin/python
# e.g. # python check_point_all.py -v tas:0:0,huss:01H:mean,pr:01H:mean,ps:01H:mean,uas:01H:mean,vas:01H:mean,wss:01H:mean,mrso:03H:mean,prsn:03H:mean,prac:01H:mean,sst:03H:mean,evspsbl:03H:mean -p 33 33 -P 1990-1999
# e.g. # python check_point_all.py -v tas:0:0,huss:01H:mean,pr:01H:mean,ps:01H:mean,uas:01H:mean,vas:01H:mean,wss:01H:mean,mrso:03H:mean,prsn:03H:mean,prac:01H:mean,sst:03H:mean,evspsbl:03H:mean -p 39 120 -P 1990-1999

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import subprocess as sub
import datetime as dt
import matplotlib as plt

class plotValXY(object):
  """class with all the plotting attributes"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varUnits(self, vals):
    sel.varUnits = vals
  def timeUnits(self, vals):
    sel.timeUnits = vals

class plotValStatXY(object):
  """class with all the plotting attributes of a statistics variable"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varBnds(self, vals):
    self.varBnds = vals 
  def varBndsValt(self, vals):
    self.varBndsValt = vals
  def varBndsValT(self, vals):
    self.varBndsValT = vals

def get_stat_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a statistical variable in a given point
  print filename

  ncf = NetCDFFile(filename,'r')
  
  if not ncf.variables.has_key(timename):
    print 'File does not have time !!!!'
    quit()
  
  timeVar = ncf.variables[timename]
  valPlot = plotValStatXY(timeVar.getValue())
  
  var = ncf.variables[varname]
  varVal = var.getValue()
  dimt = varVal.__len__() 
  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  else:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]
  
  varbnds = ncf.variables['time_bnds']
  valPlot.varBnds = varbnds
  valPlot.varBndsVal = varbnds.getValue()
  varBndsValt = []
  varBndsValT = []
  for it in range(dimt):
    varBndsValt.append(valPlot.varBndsVal[it,0])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(valPlot.varBndsVal[it,1])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(None)
    varBndsValT.append(None)
  
  ncf.close
  valPlot.varBndsValt = varBndsValt
  valPlot.varBndsValT = varBndsValT

#  quit()

  return valPlot

def get_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a variable in a given point
  print filename

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print 'File does not have time !!!!'
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timeVar.getValue())
  valPlot.timeUnits=getattr(timeVar, 'units')

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  else:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]

  ncf.close
  return valPlot

def tas_plot(varName):

#
# All
  Varn=varName
  ncAllfile=fold + '/' + headfile + '_01H_' + opts.period + '_' + Varn + '.nc'
  valAll = get_xy(Varn, timename, ncAllfile, point)

# 
# Minimum
  stat='min'
  Varn=varName + stat
  ncMinfile=fold + '/' + headfile + '_24H_' + opts.period + '_' + Varn + '.nc'

  valMin = get_stat_xy(Varn, timename, ncMinfile, point)

#
# Maximum
  stat='max'
  Varn=varName + stat
  
  ncMaxfile=fold + '/' + headfile + '_24H_' + opts.period + '_' + Varn + '.nc'
  valMax = get_stat_xy(Varn, timename, ncMaxfile, point)

# 
# Mean
  stat='mean'
  Varn=varName + stat
  
  ncMeanfile=fold + '/' + headfile + '_24H_' + opts.period + '_' + Varn + '.nc'
  valMean = get_stat_xy(Varn, timename, ncMeanfile, point)
  
# 
# DAN
  stat='min'
  statf='DAM'
  Varn=varName
  ncDANfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAN = get_stat_xy(Varn + stat, timename, ncDANfile, point)
  
# 
# DAX
  stat='max'
  statf='DAM'
  Varn=varName
  ncDAXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAX = get_stat_xy(Varn + stat, timename, ncDAXfile, point)

# 
# DAM
  stat='mean'
  statf='DAM'
  Varn=varName
  ncDAMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAM = get_stat_xy(Varn + stat, timename, ncDAMfile, point)

# 
# MON
  stat='min'
  statf='MOM'
  Varn=varName
  ncMONfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMON = get_stat_xy(Varn + stat, timename, ncMONfile, point)
  
# 
# MOX
  stat='max'
  statf='MOM'
  Varn=varName
  ncMOXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMOX = get_stat_xy(Varn + stat, timename, ncMOXfile, point)

# 
# MOM
  stat='mean'
  statf='MOM'
  Varn=varName
  ncMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMOM = get_stat_xy(Varn + stat, timename, ncMOMfile, point)

# 
# min MOM
  stat='minmean'
  statf='MOM'
  Varn=varName
  ncminMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valminMOM = get_stat_xy(Varn + stat, timename, ncminMOMfile, point)

#
# Plotting
  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + Varn
  plt.pyplot.hold(True)

  title(Varn + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(Varn + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, 'black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(412752, 413400)
  plt.pyplot.plot(valMin.timeVarVal, valMin.varValPt, 'b1', label='min')
  plt.pyplot.plot(valMax.timeVarVal, valMax.varValPt, 'r1', label='max')
  plt.pyplot.plot(valMean.timeVarVal, valMean.varValPt, 'g1', label='mean')
  plt.pyplot.plot(valDAN.timeVarVal, valDAN.varValPt, '2', color='#32FFFF', label='DAN')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, '2', color='#FFAAAA', label='DAX')
  plt.pyplot.plot(valDAM.timeVarVal, valDAM.varValPt, '2', color='#AAFFAA', label='DAM')
  plt.pyplot.plot(valMON.timeVarVal, valMON.varValPt, '3', color='#32DDDD', label='MON')
  plt.pyplot.plot(valMOX.timeVarVal, valMOX.varValPt, '3', color='#DDAAAA', label='MOX')
  plt.pyplot.plot(valMOM.timeVarVal, valMOM.varValPt, '3', color='#AADDAA', label='MOM')
  plt.pyplot.plot(valminMOM.timeVarVal, valminMOM.varValPt, '4', color='#DDDDAA', label='minMOM')
  plt.pyplot.plot(valMin.varBndsValt, valMin.varBndsValT, 'b')
  plt.pyplot.plot(valMax.varBndsValt, valMax.varBndsValT, 'r')
  plt.pyplot.plot(valMean.varBndsValt, valMean.varBndsValT, 'g')
  plt.pyplot.plot(valDAN.varBndsValt, valDAN.varBndsValT, '#32FFFF')
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, '#FFAAAA')
  plt.pyplot.plot(valDAM.varBndsValt, valDAM.varBndsValT, '#AAFFAA')
  plt.pyplot.plot(valMON.varBndsValt, valMON.varBndsValT, '#32DDDD')
  plt.pyplot.plot(valMOX.varBndsValt, valMOX.varBndsValT, '#DDAAAA')
  plt.pyplot.plot(valMOM.varBndsValt, valMOM.varBndsValT, '#AADDAA')
  plt.pyplot.plot(valminMOM.varBndsValt, valminMOM.varBndsValT, '#DDDDAA')

  plt.pyplot.legend(ncol=5)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def pr_plot(varName):
# Function to plot precipitation for test
#
# All
  stat=''
  varn=varName + stat
  ncAllfile=fold + '/' + headfile + '_01H_' + opts.period + '_' + varName + stat + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# pr5max
  stat='5max'
  varn=varName + stat
  ncMax05file=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax05 = get_stat_xy(varn, timename, ncMax05file, point)

# 
# pr10max
  stat='10max'
  varn=varName + stat
  ncMax10file=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax10 = get_stat_xy(varn, timename, ncMax10file, point)

# 
# pr20max
  stat='20max'
  varn=varName + stat
  ncMax20file=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax20 = get_stat_xy(varn, timename, ncMax20file, point)
 
# 
# pr30max
  stat='30max'
  varn=varName + stat
  ncMax30file=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax30 = get_stat_xy(varn, timename, ncMax30file, point)

# 
# pr1Hmax
  stat='1Hmax'
  varn=varName + stat
  ncMax1Hfile=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax1H = get_stat_xy(varn, timename, ncMax1Hfile, point)

# 
# DAX
  stat='max'
  varn=varName + stat
  ncDAXfile=fold + '/' + headfile + '_DAM_' + opts.period + '_' + varName + stat + '.nc'
  valDAX = get_stat_xy(varn, timename, ncDAXfile, point)

# 
# MOM 1Hmax
  stat='1Hmaxmean'
  statf='MOM'
  Varn=varName
  ncM1Hmaxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valM1Hmax = get_stat_xy(Varn + stat, timename, ncM1Hmaxfile, point)
  
# 
# MOX
  stat='max'
  statf='MOM'
  Varn=varName
  ncMOXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMOX = get_stat_xy(Varn + stat, timename, ncMOXfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(412752, 413400)
  plt.pyplot.plot(valMax05.timeVarVal, valMax05.varValPt, linestyle='None', color=((0,0,0,1)), marker='x', label='5')
  plt.pyplot.plot(valMax10.timeVarVal, valMax10.varValPt, linestyle='None', color=((0.5,0,0,1)), marker='x', label='10')
  plt.pyplot.plot(valMax20.timeVarVal, valMax20.varValPt, linestyle='None', color=((1,0,0,1)), marker='x', label='20')
  plt.pyplot.plot(valMax30.timeVarVal, valMax30.varValPt, linestyle='None', color=((1,0.5,0,1)), marker='x', label='30')
  plt.pyplot.plot(valMax1H.timeVarVal, valMax1H.varValPt, linestyle='None', color=((1,1,0,1)), marker='x', label='1H')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
  plt.pyplot.plot(valM1Hmax.timeVarVal, valM1Hmax.varValPt, linestyle='None', color=((0.75,0.75,0,1)), marker='x', label='1H MOM')
  plt.pyplot.plot(valMOX.timeVarVal, valMOX.varValPt, linestyle='None', color=((0,0,0.75,1)), marker='x', label='MOX')
  plt.pyplot.plot(valMax05.varBndsValt, valMax05.varBndsValT, color=((0,0,0,1)))
  plt.pyplot.plot(valMax10.varBndsValt, valMax10.varBndsValT, color=((0.5,0,0,1)))
  plt.pyplot.plot(valMax20.varBndsValt, valMax20.varBndsValT, color=((1,0,0,1)))
  plt.pyplot.plot(valMax30.varBndsValt, valMax30.varBndsValT, color=((1,0.5,0,1)))
  plt.pyplot.plot(valMax1H.varBndsValt, valMax1H.varBndsValT, color=((1,1,0,1)))
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, color=((0,0,1,1)))
  plt.pyplot.plot(valM1Hmax.varBndsValt, valM1Hmax.varBndsValT, color=((0.75,0.75,0,1)))
  plt.pyplot.plot(valMOX.varBndsValt, valMOX.varBndsValT, color=((0,0,0.75,1)))

##  ylabel('log10(' + varName + ') (' + valAll.varUnits + ')')

##  plt.pyplot.plot(valAll.timeVarVal, log10(valAll.varValPt), color='black', label='All')
##  ax = plt.pyplot.gca()
##  ax.set_autoscale_on(False)
##  plt.pyplot.xlim(412752, 413400)
##  plt.pyplot.plot(valMax05.timeVarVal, log10(valMax05.varValPt), linestyle='None', color=((0,0,0,1)), marker='x', label='5')
##  plt.pyplot.plot(valMax10.timeVarVal, log10(valMax10.varValPt), linestyle='None', color=((0.5,0,0,1)), marker='x', label='10')
##  plt.pyplot.plot(valMax20.timeVarVal, log10(valMax20.varValPt), linestyle='None', color=((1,0,0,1)), marker='x', label='20')
##  plt.pyplot.plot(valMax30.timeVarVal, log10(valMax30.varValPt), linestyle='None', color=((1,0.5,0,1)), marker='x', label='30')
##  plt.pyplot.plot(valMax1H.timeVarVal, log10(valMax1H.varValPt), linestyle='None', color=((1,1,0,1)), marker='x', label='1H')
##  plt.pyplot.plot(valDAX.timeVarVal, log10(valDAX.varValPt), linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
##  plt.pyplot.plot(valMax05.varBndsValt, log10(valMax05.varBndsValT), color=((0,0,0,1)))
##  plt.pyplot.plot(valMax10.varBndsValt, log10(valMax10.varBndsValT), color=((0.5,0,0,1)))
##  plt.pyplot.plot(valMax20.varBndsValt, log10(valMax20.varBndsValT), color=((1,0,0,1)))
##  plt.pyplot.plot(valMax30.varBndsValt, log10(valMax30.varBndsValT), color=((1,0.5,0,1)))
##  plt.pyplot.plot(valMax1H.varBndsValt, log10(valMax1H.varBndsValT), color=((1,1,0,1)))
##  plt.pyplot.plot(valDAX.varBndsValt, log10(valDAX.varBndsValT), color=((0,0,1,1)))

  plt.pyplot.legend(ncol=5)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def wss_plot(varName):
# Function to plot wind speed for test
#
# All
  stat=''
  varn=varName + stat
  ncAllfile=fold + '/' + headfile + '_01H_' + opts.period + '_' + varName + stat + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# wss10max
  stat='10max'
  varn=varName + stat
  ncMax10file=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax10 = get_stat_xy(varn, timename, ncMax10file, point)

# 
# wss1Hmax
  stat='1Hmax'
  varn=varName + stat
  ncMax1Hfile=fold + '/' + headfile + '_24H_' + opts.period + '_' + varName + stat + '.nc'
  valMax1H = get_stat_xy(varn, timename, ncMax1Hfile, point)

# 
# DAX
  stat='max'
  varn=varName + stat
  ncDAXfile=fold + '/' + headfile + '_DAM_' + opts.period + '_' + varName + stat + '.nc'
  valDAX = get_stat_xy(varn, timename, ncDAXfile, point)
 
# 
# MOX
  stat='max'
  varn=varName + stat
  ncMOXfile=fold + '/' + headfile + '_MOM_' + opts.period + '_' + varName + stat + '.nc'
  valMOX = get_stat_xy(varn, timename, ncMOXfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(412752, 413400)
  plt.pyplot.plot(valMax10.timeVarVal, valMax10.varValPt, linestyle='None', color=((1,0,0,1)), marker='x', label='10')
  plt.pyplot.plot(valMax1H.timeVarVal, valMax1H.varValPt, linestyle='None', color=((0,1,0,1)), marker='x', label='1H')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
  plt.pyplot.plot(valMOX.timeVarVal, valMOX.varValPt, linestyle='None', color=((0,0,0.75,1)), marker='x', label='MOX')
  plt.pyplot.plot(valMax10.varBndsValt, valMax10.varBndsValT, color=((1,0,0,1)))
  plt.pyplot.plot(valMax1H.varBndsValt, valMax1H.varBndsValT, color=((0,1,0,1)))
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, color=((0,0,1,1)))
  plt.pyplot.plot(valMOX.varBndsValt, valMOX.varBndsValT, color=((0,0,0.75,1)))

  plt.pyplot.legend(ncol=5)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def varStat_plot(varName, frqN, stat):
# Function to plot a stat variable for test

#
# All
  varn=varName 
  ncAllfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# Stat
  frqN='DAM'
  varn=varName + stat
  ncStatfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + stat + '.nc'
  valStat = get_stat_xy(varn, timename, ncStatfile, point)

# 
# Stat
  frqN='MOM'
  varn=varName + stat
  ncMStatfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + stat + '.nc'
  valMStat = get_stat_xy(varn, timename, ncMStatfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(412752, 413400)
  if stat == 'mean':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='g', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='g')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='g', marker='x', label='MO')
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='g')
  elif stat == 'max':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='r', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='r')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='r', marker='x', label='MO')
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='r')
  elif stat == 'min':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='b', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='b')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='b', marker='x', label='MO')
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='b')

  plt.pyplot.legend()
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def prac_plot(varName):
# Function to plot prac for test

#
# All
  frqN='01H'
  varn=varName 
  ncAllfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valAllbnds = get_stat_xy(varn, timename, ncAllfile, point)
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# 24H
  frqN='24H'
  varn=varName
  nc24Hfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  val24H = get_stat_xy(varn, timename, nc24Hfile, point)

# 
# MOS
  frqN='MOM'
  varn=varName
  ncMfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valM = get_stat_xy(varn, timename, ncMfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(412752, 413400)
  plt.pyplot.plot(valAllbnds.varBndsValt, valAllbnds.varBndsValT, color='black')
  plt.pyplot.plot(val24H.timeVarVal, val24H.varValPt, linestyle='None', color='b', marker='x', label='AC')
  plt.pyplot.plot(val24H.varBndsValt, val24H.varBndsValT, color='b')
  plt.pyplot.plot(valM.timeVarVal, valM.varValPt, linestyle='None', color='#32FFFF', marker='x', label='MO')
  plt.pyplot.plot(valM.varBndsValt, valM.varBndsValT, color='#32FFFF')

  plt.pyplot.legend()
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)


### Options

parser = OptionParser()
parser.add_option("-v", "--variablesfrequencies", dest="varnamesfrq",
                  help="variables to check", metavar="VAR:FRQ:stat,[VAR:FRQ:stat,...]")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check", metavar="PT")
parser.add_option("-P", "--Period", dest="period", 
                  help="point to check", metavar="PT")
(opts, args) = parser.parse_args()

### Static values

WRF4Gfold='/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G'
fold='123'
headfile='CCRC_NARCliM_Sydney'
timename='time'

#######    #######
## MAIN
    #######

point=np.array(opts.point)
print point
#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

varnamesfrq=opts.varnamesfrq.split(',')
Nvars=len(varnamesfrq)

for ivar in range(Nvars):
  varfrqstat=varnamesfrq[ivar]

  varnames=varfrqstat.split(':')[0]
  frqnames=varfrqstat.split(':')[1]
  statnames=varfrqstat.split(':')[2]

  if varnames == 'tas':
    tas_plot('tas')
  elif varnames == 'pr':
    pr_plot('pr')
  elif varnames == 'wss':
    wss_plot('wss')
  elif varnames == 'prac':
    prac_plot('prac')
  else:
    varStat_plot(varnames, frqnames, statnames)

