###############################################################################################
##################################    NARCliMpost_regrid.py       ######################################
###############################################################################################
# 
#   Description: this script re-grid d02 post-processed NARCliM data from the original cylindrical equidistant rotated 
#                projection into a regular lat-lon (cylindrical equidistant without rotation) grid mesh.
#
#                Spatial interpolation is performed using a bi-cubic spline. The loop through all times step is 
#                parallelized in "njobs" to help the script running faster. 
#
#                After perform the spatial interpolation, definite positive variables such as precipitation and humidity 
#                are specially treated to avoid negative values simply by replacing negative values by zero.
#
#                In case the script is re-launched it first verifies if the file to be regridded is already done. If it 
#                done then just pick up the next one. So if you are not sure a given file is done correctly just deleted
#                the regular_grid folder.                
# 
# 
#   Inputs: 1) Latitude, longitude, variables and other metadata from postprocessed netcdf files to be re-gridded.
#           This is simply done by giving the absolute path of the (post-processed) simulation 
#           to be re-gridded to "post_path" variable. Several paths can be given at the same time (i.e., post_path
#           can be an array).
#
#           2) Latitude and longitude of the new grid mesh where 2D variables are going to be interpolated. 
#           This information is given by the file: 
#                     '/srv/ccrc/data23/z3444417/studies/Data/RefGridMeshes/GeoFiles/geo_em_d02_regular.nc'
#
#
#
#   Run: python NARCliMpost_regrid.py
#
#
#   Author: Alejandro Di Luca, CCRC-UNSW
#   Created: 27/08/2013
#   Last Modification: 09/09/2013
#
###############################################################################################
import numpy as np

# PATH OF THE POST-PROCESSED SIMULATION TO BE RE-GRIDDED
post_path=['/srv/ccrc/data28/z3444417/NARCliM/miroc/postprocess/R1/1990-2010/d02/']

# PATH OF THE RE-GRIDDED OUTPUT. No matter the path given here this script will create a
# "/regular_grid" folder inside so the best ius just to use post_path
regrid_path=post_path

# Number of jobs for the temporal loop interpolation parallelization
njobs=30 

# Positive definite variables (only the 4 first letters).
positive_vars=np.asarray(['pr', 'hu', 'al', 'em', 'rs'])


#**************************************************************************** 
#**************************************************************************** 
print "\n","\n","\n"
print '**************************************************************************** '
print '********************   RUNNING NARCliMpost_REGRID.PY   **********************'
print '**************************************************************************** '

# Python modules
import glob
from os.path import *
import os
from datetime import datetime,timedelta
from netCDF4 import Dataset as ncdf
from netCDF4 import num2date, date2num
import time as timem
import sys

# My modules
from interpolation_AD import *

# Check initial time
test='odn'
ctime_i=checkpoint(0)
ctime=checkpoint(0)

#***********************************************************************************************
# Read lat-lon from the reference grid mesh. Also read some attributes
file10='/srv/ccrc/data23/z3444417/studies/Data/RefGridMeshes/GeoFiles/geo_em_d02_regular.nc'
fin1=ncdf(file10,mode='r')
temp=np.squeeze(fin1.variables['XLONG_M'][:,:]) # Getting longitude
condition=np.logical_and(temp<0, temp>=-180) # conver from -180-180 to 0-361 
lon_new=np.where(condition,360+temp,temp)
del temp
lat_new=np.squeeze(fin1.variables['XLAT_M'][:,:]) # Getting latitude
dx=getattr(fin1, 'DX')
dy=getattr(fin1, 'DY')
cen_lat=getattr(fin1, 'CEN_LAT')
cen_lon=getattr(fin1, 'CEN_LON')
pole_lat=getattr(fin1, 'POLE_LAT')
pole_lon=getattr(fin1, 'POLE_LON')
fin1.close()


#***********************************************************************************************
# Loop for post-processed simulations
#***********************************************************************************************
pp=0
for path in post_path:
        print '--> Processing Simulation: ',path
	ctimesim=checkpoint(0)

	#***********************************************************************************************
	# Loop for frequency folders
	#***********************************************************************************************
	for folder in ['postPROJ']:
		print '----> Processing Folder: ',folder

	        #**************************************************************************
		# Create Output directories
		path_out=regrid_path[pp]+'regular_grid/'+folder
		make_sure_path_exists(path_out)
	        #**************************************************************************


		#***********************************************************************************************
		# Listing files to read
		files_in=sorted(glob.glob('%s%s/*.nc' % (path,folder)))

		
		print '----> Number of files to interpolate: ', len(files_in)

        	#***********************************************************************************************
	        # Loop for files inside
	        #***********************************************************************************************
		for file in files_in:
			ctimefile=checkpoint(0)
			print "\n",
			print '------> INPUT FILE: '
			print file
			

        	        #***********************************************************************************************
			# Before doing any computation check if the file already exists
			# If it does then go to the next one...
			ff=file.split('/')
			file_out='%s/%s'%(path_out,ff[len(ff)-1])
			a=os.path.exists(file_out)
			print '  --> OUTPUT FILE:'
			print file_out
			if a==True:
				print '++++++ FILE ALREADY EXISTS ++++++'
			else:
				print '++++++ FILE DOES NOT EXISTS YET ++++++'
			
				# Get variable name
				ff=file.split('_')
				varname=(ff[len(ff)-1].split('.'))[0]
				heightvar=0
				heightdim=0

				#***********************************************************************************************
				# Read input FILES
				#***********************************************************************************************
				fin = ncdf(file) #Read all files
				#print fin
				for var in fin.variables.keys():
					if var!='Rotated_pole':
						print '    --> Reading variable: ', var

					if var=='lat':
						lat=fin.variables[var][:]
					if var=='lon':
						lon=fin.variables[var][:]
					if var=='time':
						time=fin.variables[var][:]
					if var=='time_bnds':
						time_bnds=fin.variables[var][:]
					if var=='height' or var=='heightv' :
						height=fin.variables[var][:]
						heightvar=1
						heightname=var
						heightdim=height.shape[0]

				varint=fin.variables[varname][:]
				if test=='on':
					varint=varint[0:5,:,:]
					njobs=1
					file_out='%s/%s'%(path_out,ff[len(ff)-1]+'test')
				varshape=varint.shape
				time_dim=varint.shape[0]


				#****************************************************************************
				# Interpolating data to a higher resolution grid mesh
				#*****************************************************************************
				temp, lon_new, lat_new=griddata_lr2hr(lon,lat,np.squeeze(varint),lon_new,lat_new, njobs=njobs)
				print '------> Output sample after the interpolation: ',temp[0:4,10,10]


				#***************************************************************************************
				# CREATING NETCDF FILE
				#***************************************************************************************
				# Create output file
				print file_out
				fout=ncdf(file_out,mode='w', format='NETCDF4_CLASSIC')

				# ------------------------
				# Create dimensions
				for dim in fin.dimensions.keys():
					print '   Dimension ', '%s' %(dim)

					if dim=='x':
						dimValue = fin.dimensions[dim]
						fout.createDimension('%s' %(dim),lon_new.shape[1])

					if dim=='y':
						dimValue = fin.dimensions[dim]
						fout.createDimension('%s' %(dim),lon_new.shape[0])

					if dim=='time':
						fout.createDimension('time',None)

					if dim!='y' and dim!='x' and dim!='time':
						dimValue = fin.dimensions[dim]
						fout.createDimension('%s' %(dim),len(dimValue))

				# ------------------------
				# Create and assign values to variables
				print "\n"
				print 'Creating variables: ', fin.variables.keys() 
				for var in fin.variables.keys():
					if var !='Rotated_pole':
						print '  Variable:' , var

						if var=='time':
							varout=fout.createVariable(var,'d',['time'])
							varout[:]=time[0:time_dim]

						if var=='time_bnds':
							varout=fout.createVariable(var,'d',['time', 'bnds'])
							varout[:]=time_bnds[0:time_dim]

						if var=='height' or var=='heightv':
							varout=fout.createVariable(var,'f',[var])
							varout[:]=height

						if var=='lon':
							varout=fout.createVariable(var,'f',['y', 'x'])
							varout[:]=lon_new

						if var=='lat':
							varout=fout.createVariable(var,'f',['y', 'x'])
							varout[:]=lat_new

						if var==varname:
							if heightvar>0:
								varout=fout.createVariable(var,'f',['time', heightname, 'y', 'x'])
								temp.resize(temp.shape[0], heightdim, temp.shape[1], temp.shape[2])
							else:
								if varname[0:4]=='sst':
									varout=fout.createVariable(var,'f',['time', 'y', 'x'], fill_value=1.e+20)
								else:
									varout=fout.createVariable(var,'f',['time', 'y', 'x'])
							varout[:]=temp

							# Avoid negative values for definite positive variables such as 
							# precipitation, humidity, radiation variables, etc..
							aa=varname[0:2]==positive_vars
							if np.sum(aa)==1:
								condition=varout[:]<0
								varout[:]=np.where(condition,0,varout)
								print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
								print 'Number of zeros: ', np.sum(condition)
								print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'

						# ------------------------
						# Copy attributes from input variables and modify the grid_mapping attribute:
						for att in fin.variables[var].ncattrs():
							print '     Attribute: ', att
							if att!='grid_mapping':
								if att!='_FillValue':
									setattr(varout, att, getattr(fin.variables[var],att))
							else:
								setattr(varout, att, "NonRotated")
						del varout

					else:
						varout=fout.createVariable('NonRotated_pole','c',[])
						setattr(varout, 'grid_mapping_name', 'cylindrical_equidistant_non-rotated')
						setattr(varout, 'dx_m', dx)
						setattr(varout, 'dy_m', dy)
						setattr(varout, 'CEN_LAT', cen_lat)
						setattr(varout, 'CEN_LON', cen_lon)
						setattr(varout, 'POLE_LAT', pole_lat)
						setattr(varout, 'POLE_LON', pole_lon)

				# ------------------------
				# Add global attributes
				for att in fin.ncattrs():
					print '   Global Attribute: ', att
					if att!='history':
						setattr(fout, att, getattr(fin,att))
					else:
						setattr(fout, att, getattr(fin,att)+"\n"+" Created by "+os.path.abspath("regridding.py")+" on "+ timem.strftime('%x %X %z'))

				fout.close()
				fin.close()	
				print "\n"
				print 'OUTPUT FILE: ',
				print file_out 
				ctime=checkpoint(ctimefile)
				print '**************************************************************************** '
				print '**************************************************************************** '
				print '**************************************************************************** '
				print "\n","\n"
				print "\n","\n"
				print "\n","\n"
				#****************************************************************************************

	print "\n","\n"
	print '**************************************************************************** '
	print 'SIMULATION: ', path
	ctimesim=checkpoint(ctimesim)
	print '**************************************************************************** '
	print "\n","\n"
	print "\n","\n"
	pp=pp+1

print '**************************************************************************** '
print '****************   RE GRIDDING D02 DOMAIN FINISHED   ************************'
ctime=checkpoint(ctime_i)
print '**************************************************************************** '
print '**************************************************************************** '
print "\n","\n"
	
