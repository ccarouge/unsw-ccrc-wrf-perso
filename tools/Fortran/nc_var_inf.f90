PROGRAM nc_var_inf
! Program to retrieve information from a variable
! Nov. 2011
! Lluis Fita. Universidad de Cantabria,  Grupo de Meteorologia de Santander, Santander, Spain

! WRF4G by Santander Meteorology Group is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. 
! Permissions beyond the scope of this license may be available at http://www.meteo.unican.es/software/wrf4g.

! trueno
! export NETCDF_ROOT='/home/lluis/bin/netcdf-4.0.1'
! gfortran nc_var_inf.f90 -L${NETCDF_ROOT}/lib -lnetcdf -lm -I${NETCDF_ROOT}/include -o nc_var_inf

! ui01
! export NETCDF_ROOT='/software/CentOS/5.2/netcdf/4.1.3/gcc_4.1.2_NOpnetcdf' 
! gfortran nc_var_inf.f90 -g -O2 -L${NETCDF_ROOT}/lib -lnetcdf -lnetcdff -lm -I${NETCDF_ROOT}/include -o nc_var_inf

!! ccrc468-17: gfortran nc_var_inf.f90 -L/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/lib -lnetcdf -lnetcdff -lm -I/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/include -o nc_var_inf

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'
  
! Arguments
  INTEGER                                                :: iarg, narg
  CHARACTER(LEN=500), ALLOCATABLE, DIMENSION(:)          :: arguments
  CHARACTER(LEN=500)                                     :: filen, varname

! netcdf
  INTEGER                                                :: idim, rcode, ncid, ivar
  INTEGER, DIMENSION(6)                                  :: DIMshape, Vdimsid
  INTEGER                                                :: Vid, Vtype, Vndims, Vnatts
  CHARACTER(LEN=100), DIMENSION(6)                       :: DIMname
 
!!!!!!! Variables
! argument(1): file name
! argument(2): variable name
! DIMshape: shape of dimensions
! Vdimsid: Id of the dimensions of the variable
! Vid: Id of the variable within the file
! Vtype: variable type
! Vndims: number of dimensions of the variable
! Vnatts: number of attributes of the variable

!!!!!!! Subroutines/Functions
! nc_vatts: Prints all the attributes of the variable

! Arguments
  narg=COMMAND_ARGUMENT_COUNT()
  IF (ALLOCATED(arguments)) DEALLOCATE(arguments)
  ALLOCATE(arguments(narg))

  DO iarg=1, narg
    CALL GETARG(iarg,arguments(iarg))
  END DO
  IF (TRIM(arguments(1)) == '-h' ) THEN
    PRINT *,"nc_var_inf [ncf](netCDF file) [var](variable name)"
    STOP
  END IF

  filen=TRIM(arguments(1))
  varname=TRIM(arguments(2))
  
  DEALLOCATE(arguments)
 
  rcode = nf90_open(filen, 0, ncid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)

  rcode = nf90_inq_varid(ncid, varname, Vid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncid, varid=Vid, xtype=Vtype, ndims=Vndims, dimids=Vdimsid,       &
     nAtts=Vnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
  PRINT *,"Variable; ___ __ _ '",TRIM(varname)//"'"
  PRINT "(3x,A15,1x,I3,1x,A5,1x,I3)",'VARinf; var id;',Vid,'type;',Vtype
  PRINT *,'VARinf; var id;',Vid,'type;',Vtype

  PRINT "(3x,A25,1x,I3)",'VARinf; n. of dimensions;', Vndims
  PRINT "(3x,A20,1x,6(I3,1x))",'VARinf; id dims var;',(Vdimsid(idim),idim=1,Vndims)
  PRINT "(3x,A25,1x,I3)",'VARinf; n. of attributes;', Vnatts

  PRINT *,'Dimensions; ___ __ _'
  DIMname=''
  DIMshape=0
  DO idim=1, Vndims
    rcode = nf90_inquire_dimension(ncid, Vdimsid(idim), DIMname(idim), DIMshape(idim))
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    PRINT *,'  DIMinf;',Vdimsid(idim), "'"//TRIM(DIMname(idim))//"'; ", DIMshape(idim)
  END DO

  PRINT *,'Variable attributes; ___ __ _'
  CALL nc_vatts(ncid, Vid, Vnatts, 0)

  rcode = nf90_close(ncid)

END PROGRAM nc_var_inf

SUBROUTINE nc_vatts(ncid, varid, nvatts, debg) 
! Subroutine to print all attributes of a variable

  USE netcdf
  IMPLICIT NONE
  
!  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: ncid, varid, debg
! Local
  INTEGER                                                :: Vatt, j
  INTEGER                                                :: rcode, ndims, nvars, nvatts,         &
    nunlimdimid, atttype, attlen
  CHARACTER(LEN=50)                                      :: section
  CHARACTER(LEN=100)                                     :: attname
  INTEGER, DIMENSION(:), ALLOCATABLE                     :: attintvalues
  INTEGER, PARAMETER                                     :: long = SELECTED_INT_KIND(9)
  INTEGER(long), DIMENSION(:), ALLOCATABLE               :: attlongintvalues
  CHARACTER(LEN=10000)                                   :: attcharvalues
  REAL, DIMENSION(:), ALLOCATABLE                        :: attrealvalues
  DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE            :: attdoublevalues
  CHARACTER, DIMENSION(:), ALLOCATABLE                   :: attcharvalues2

!!!!!!!!!!!!!!!!! Variables
! fileinf: netCDF file with path
! varid: id of variable
! nvatts: number of attributes

  section="'nc_vatts'"
  IF (debg >= 100) PRINT *,'Section '//TRIM(section)//'... .. .'

  IF (debg >= 0) THEN
    DO Vatt=1, nvatts
      rcode = nf90_inq_attname(ncid, varid, Vatt, attname)
      rcode = nf90_inquire_attribute(ncid, varid, attname, atttype, attlen)
      SELECT CASE(atttype)
      CASE(NF90_CHAR)
        attcharvalues=''
	IF (LEN(attcharvalues) < attlen) THEN
	  PRINT *,TRIM(section)//"  Not enough space to retrieve '"//TRIM(attname)//"' attribute"
        ELSE
          rcode = nf90_get_att(ncid, varid, attname, attcharvalues)
  	  IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
          PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"' ","'"//TRIM(attcharvalues)//"'"
	END IF
      CASE(NF90_SHORT)
        IF (ALLOCATED(attintvalues)) DEALLOCATE(attintvalues)
        ALLOCATE (attintvalues(attlen))
        rcode = nf90_get_att(ncid, varid, TRIM(attname), attintvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"'",(attintvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attintvalues)
      CASE(NF90_INT)
        IF (ALLOCATED(attintvalues)) DEALLOCATE(attintvalues)
        ALLOCATE (attintvalues(attlen))
        rcode = nf90_get_att(ncid, varid, TRIM(attname), attintvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"'",(attintvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attintvalues)
!      CASE(NF90_LONGINT)
!        IF (ALLOCATED(attlongintvalues)) DEALLOCATE(attlongintvalues)
!        ALLOCATE (attlongintvalues(attlen))
!        rcode = nf90_get_att(ncid, varid, TRIM(attname), attlongintvalues)
!	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
!        PRINT *,'          * values:',(attlongintvalues(j),' ,',j=1,attlen)
!        DEALLOCATE(attlongintvalues)
      CASE(NF90_FLOAT)
        IF (ALLOCATED(attrealvalues)) DEALLOCATE(attrealvalues)
        ALLOCATE (attrealvalues(attlen))
        rcode = nf90_get_att(ncid, varid, TRIM(attname), attrealvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"'", (attrealvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attrealvalues)
      CASE(NF90_DOUBLE)
        IF (ALLOCATED(attdoublevalues)) DEALLOCATE(attdoublevalues)
        ALLOCATE (attdoublevalues(attlen))
        rcode = nf90_get_att(ncid, varid, TRIM(attname), attdoublevalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"'", (attdoublevalues(j),' ,',j=1,attlen)
        DEALLOCATE(attdoublevalues)
      CASE DEFAULT
        PRINT *,'  Vatt;',Vatt,"'"//TRIM(attname)//"' ----- Not printable data type ----- "
      END SELECT
    END DO
  ENDIF

  RETURN
END SUBROUTINE nc_vatts
