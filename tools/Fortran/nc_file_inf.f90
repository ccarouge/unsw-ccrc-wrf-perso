PROGRAM nc_file_inf
! Program to retrieve information from a file
! Nov. 2011
! Lluis Fita. Universidad de Cantabria,  Grupo de Meteorologia de Santander, Santander, Spain

! WRF4G by Santander Meteorology Group is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. 
! Permissions beyond the scope of this license may be available at http://www.meteo.unican.es/software/wrf4g.

! trueno
! export NETCDF_ROOT='/home/lluis/bin/netcdf-4.0.1'
! gfortran nc_file_inf.f90 -L${NETCDF_ROOT}/lib -lnetcdf -lm -I${NETCDF_ROOT}/include -o nc_file_inf

! ui01
! export NETCDF_ROOT='/software/CentOS/5.2/netcdf/4.1.3/gcc_4.1.2_NOpnetcdf'
! gfortran nc_file_inf.f90 -g -O2 -L${NETCDF_ROOT}/lib -lnetcdf -lnetcdff -lm -I${NETCDF_ROOT}/include -o nc_file_inf

!! ccrc468-17: gfortran nc_file_inf.f90 -L/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/lib -lnetcdf -lnetcdff -lm -I/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/include -o nc_file_inf
  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'
 
! Arguments
  INTEGER                                                :: iarg, narg
  CHARACTER(LEN=500), ALLOCATABLE, DIMENSION(:)          :: arguments
  CHARACTER(LEN=500)                                     :: filen

! netcdf
  INTEGER                                                :: idim, rcode, ncid, ivar, DIMlen, VARndim
  INTEGER                                                :: ndims, nvars, Gnatts, udim, filefmt
  INTEGER, DIMENSION(6)                                  :: VARdimsid
  CHARACTER(LEN=100)                                     :: VARname
  CHARACTER(LEN=100), DIMENSION(:), ALLOCATABLE          :: DIMname
 
!!!!!!! Variables
! argument(1): file name
! ndims: number of dimensions
! nvars: number of variables
! Gnatts: number of global attributes
! udim: unlimited dimension
! filefmt: format of the file
! DIMname: name of the dimension
! DIMlen: length of the dimension
! VARname: variable name
! VARndim: number of dimensions
! VARdimsid: if of the dimensions of variable

!!!!!!! Subroutines/Functions
! nc_gatts: Prints all the global attributes of the file

! Arguments
  narg=COMMAND_ARGUMENT_COUNT()
  IF (ALLOCATED(arguments)) DEALLOCATE(arguments)
  ALLOCATE(arguments(narg))

  DO iarg=1, narg
    CALL GETARG(iarg,arguments(iarg))
  END DO
  IF (TRIM(arguments(1)) == '-h' ) THEN
    PRINT *,"nc_file_inf [ncf](netCDF file)"
    STOP
  END IF

  filen=TRIM(arguments(1))
  
  DEALLOCATE(arguments)
 
  rcode = nf90_open(filen, 0, ncid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)

  rcode = nf90_inquire(ncid, nDimensions=ndims, nVariables=nvars, nAttributes=Gnatts,             &
    unlimitedDimId=udim, formatNum=filefmt)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)

  PRINT *,'General information; ___ __ _'
  PRINT "(3x,A24,1x,I3,1x,A20,1x,I3)",'GENinf; num. dimensions;',ndims,'number of variables;',nvars
  PRINT "(3x,A24,1x,I3,1x,A14,1x,I3)",'GENinf; num. attributes;',Gnatts,'id ulimit dim;',udim
  PRINT "(3x,A20,1x,I3)",'GENinf; file format;',filefmt

  PRINT *,'Dimensions; ___ __ _'
  IF (ALLOCATED(DIMname)) DEALLOCATE(DIMname)
  ALLOCATE(DIMname(ndims))
  DO idim=1, ndims
    rcode = nf90_inquire_dimension(ncid, idim, DIMname(idim), DIMlen)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    PRINT *,'  DIMinf;',idim, "'"//TRIM(DIMname(idim))//"'; ", DIMlen
  END DO

  PRINT *,'Variables; ___ __ _'
  DO ivar=1, nvars
    rcode = nf90_inquire_variable(ncid, varid=ivar, name=VARname, ndims=VARndim, dimids=VARdimsid)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    PRINT *,'  VARinf;',ivar, "'"//TRIM(VARname)//"'; ", '(',(TRIM(DIMname(VARdimsid(idim))),     &
      char(44), idim=1,VARndim-1),TRIM(DIMname(VARdimsid(VARndim))),')'
  END DO

  PRINT *,'Global netCDF attributes; ___ __ _'
  CALL nc_gatts(ncid, 0)
  
  rcode = nf90_close(ncid)

END PROGRAM nc_file_inf

SUBROUTINE nc_gatts(ncid, debg) 
! Subroutine to print all global attributes of a netCDF file 

  USE netcdf  
  USE typeSizes, only: TwoByteInt

  IMPLICIT NONE
  
  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: ncid, debg
! Local
  INTEGER                                                :: Gatt, j
  INTEGER                                                :: rcode, ndims, nvars, ngatts,         &
    nunlimdimid, atttype, attlen
  CHARACTER(LEN=50)                                      :: section
  CHARACTER(LEN=100)                                     :: attname
  INTEGER, DIMENSION(:), ALLOCATABLE                     :: attintvalues
  INTEGER, PARAMETER                                     :: long = SELECTED_INT_KIND(9)
  INTEGER(long), DIMENSION(:), ALLOCATABLE               :: attlongintvalues
  CHARACTER(LEN=100000)                                  :: attcharvalues
  REAL, DIMENSION(:), ALLOCATABLE                        :: attrealvalues
  DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE            :: attdoublevalues
  INTEGER(KIND=TwoByteInt)                               :: atttwobyteIntvalues

!!!!!!!!!!!!!!!!! Variables
! fileinf: netCDF file with path
! ngatts: number of global attributes

  section="'nc_gatts'"
  IF (debg >= 100) PRINT *,'Section '//TRIM(section)//'... .. .'

  rcode = nf90_inquire(ncid, ndims, nvars, ngatts, nunlimdimid)
  IF (rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
  
  IF (debg >= 0) THEN
    DO Gatt=1, ngatts
      rcode = nf90_inq_attname(ncid, NF90_GLOBAL, Gatt, attname)
      IF (rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
      rcode = nf90_inquire_attribute(ncid=ncid, varid=NF90_GLOBAL, name=attname, xtype=atttype,   &
        len=attlen)
      IF (rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)

      SELECT CASE(atttype)
      CASE(NF90_CHAR)
	IF (LEN(attcharvalues) < attlen) THEN
	  PRINT *,TRIM(section)//"  Not enough space to retrieve '"//TRIM(attname)//"' attribute"
        ELSE
          rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attcharvalues)
	  IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
          PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"' ","'"//TRIM(attcharvalues)//"'"
	END IF
      CASE(NF90_SHORT)
        IF (ALLOCATED(attintvalues)) DEALLOCATE(attintvalues)
        ALLOCATE (attintvalues(attlen))
        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attintvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"'",(attintvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attintvalues)
      CASE(NF90_INT)
        IF (ALLOCATED(attintvalues)) DEALLOCATE(attintvalues)
        ALLOCATE (attintvalues(attlen))
        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attintvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"'",(attintvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attintvalues)
!      CASE(8)
!        IF (ALLOCATED(attlongintvalues)) DEALLOCATE(attlongintvalues)
!        ALLOCATE (attlongintvalues(attlen))
!        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attlongintvalues)
!	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
!        PRINT *,'          * values:',(attlongintvalues(j),' ,',j=1,attlen)
!        DEALLOCATE(attlongintvalues)
      CASE(NF90_FLOAT)
        IF (ALLOCATED(attrealvalues)) DEALLOCATE(attrealvalues)
        ALLOCATE (attrealvalues(attlen))
        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attrealvalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"'", (attrealvalues(j),' ,',j=1,attlen)
        DEALLOCATE(attrealvalues)
      CASE(NF90_DOUBLE)
        IF (ALLOCATED(attdoublevalues)) DEALLOCATE(attdoublevalues)
        ALLOCATE (attdoublevalues(attlen))
        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), attdoublevalues)
	IF ( rcode /= 0) PRINT *,TRIM(section)//" "//nf90_strerror(rcode)
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"'", (attdoublevalues(j),' ,',j=1,attlen)
        DEALLOCATE(attdoublevalues)
      CASE(NF90_FILL_SHORT)
        rcode = nf90_get_att(ncid, NF90_GLOBAL, TRIM(attname), atttwobyteIntvalues )
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"'", atttwobyteIntvalues
      CASE DEFAULT
        PRINT *,'  Gatt;',Gatt,"'"//TRIM(attname)//"' ----- Not printable data type ",atttype,    &
	  "----- "
	STOP
      END SELECT
    END DO
  ENDIF

  RETURN
END SUBROUTINE nc_gatts
