PROGRAM nc_ASCII_lonlatval
!
! Impact of Climate Projections on Water Availability and Adaptation
! L. Fita Borrell. CCRC Feb 2012
!
! Program to provide from netCDF files asn ASCII output file as follows: 
!    lon1, lat1, value_date1, ...., dateN
!    ...
!    lonM, latM, value_date1, ...., dateN
!
!! Compilation
!
!! ccrc468-17: gfortran nc_ASCII_lonlatval.f90 -L/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/lib -lnetcdf -lnetcdff -lm -I/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/include -o nc_ASCII_lonlatval

!! cyclone: gfortran nc_ASCII_lonlatval.f90 -L/share/apps/netcdf/4.1.3/lib -lnetcdf -lnetcdff -lm -I/share/apps/netcdf/4.1.3/include -o nc_ASCII_lonlatval
!! typhoon intel: ifort nc_ASCII_lonlatval.f90 -L/share/apps/netcdf/intel/4.1.3/lib -lnetcdf -lnetcdff -lm -I/share/apps/netcdf/intel/4.1.3/include -o nc_ASCII_lonlatval
!! You need to load the modules hdf5/1.8.8 and netcdf/4.1.3 in order to run the program
!
! Examples of use
!
! e.g.: ./nc_ASCII_lonlatval 0 ../../data wrfout 00 1 7 T2 153.0 -28.0 154.0 -27.5 XLONG XLAT Times units 2000000
! e.g.: ./nc_ASCII_lonlatval 0 ~/studies/NARCliMGreatSydney2km/data/d01/postHfrq/1990-1999 CCRC_NARCliM_Sydney_All_ 1990-1999_tas.nc 1 7 tas 150.50 -33.50 150.75 -33.25  lon lat time units 299999 >& run_nc_ASCII_lonlatval.log
!
  USE netcdf
!  USE IFPORT

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'
 
! Arguments
  INTEGER                                                :: iarg, narg
  CHARACTER(LEN=250), ALLOCATABLE, DIMENSION(:)          :: arguments
  INTEGER                                                :: debug, MaxBits, firstFile, lastFile
  CHARACTER(LEN=250)                                     :: foldern, varname, lonName, latName,   &
    timeName, fileHeader, fileEnd, timeUnitsName
  REAL                                                   :: lonSW, latSW, lonNE, latNE

! netcdf
  INTEGER                                                :: idim, rcode, ncfid, istatsys, ferr
  INTEGER                                                :: ipoint, isec, ipt, itt, ippt, it
  INTEGER, DIMENSION(6)                                  :: DIMshape, Vdimsid, lonlatdimsid, coord
  INTEGER                                                :: Vid, Vtype, Vndims, Vnatts
  INTEGER                                                :: ifile, Nfiles
  INTEGER                                                :: i, j, dimx, dimy, dimz, dimt, posTdim, TOTdimt
  INTEGER                                                :: lonlattype, lonlatndims, lonlatnatts
  INTEGER                                                :: NpointsIn, NptsSec, Nsec
  INTEGER                                                :: dimTfile, timetype
  INTEGER                                                :: NwrittenPoints
  CHARACTER(LEN=250)                                     :: command, errmsg, timeunits, filen
  CHARACTER(LEN=300), ALLOCATABLE, DIMENSION(:)          :: filens
  CHARACTER(LEN=100), DIMENSION(6)                       :: DIMname
  REAL                                                   :: lonpt, latpt, valpt
  INTEGER, ALLOCATABLE, DIMENSION(:)                     :: Itime1values, Itimevalues, Isectimevalues
  REAL, ALLOCATABLE, DIMENSION(:)                        :: tvalpt, Rtime1values, Rtimevalues, Rsectimevalues
  INTEGER, ALLOCATABLE, DIMENSION(:,:)                   :: gridboxpts
  REAL, ALLOCATABLE, DIMENSION(:,:)                      :: values, gridboxlonlat
  CHARACTER(LEN=100), DIMENSION(:), ALLOCATABLE          :: Ctime1values, Ctimevalues, Csectimevalues

  CHARACTER(LEN=50)                                      :: section

  section="'nc_ASCII_lontlatval' ... .. ."

!!!!!!! Variables
! argument(1)=debug: debug level
! argument(2)=folern: folder name
! argument(3)=fileHeader: header of the files' name
! argument(4)=fileEnd: End of the files' name
! argument(5)=firstFile: first file to process
! argument(6)=lastFile: last file to process
! argument(7)=varname: variable name
! argument(8)=lonSW: longitude SW
! argument(9)=latSW: latitude SW
! argument(10)=lonNE: longitude NE
! argument(11)=latNE: latitude NE
! arguments(12)=lonName: name of the longitude variable
! arguments(13)=latName: name of the latitude variable
! arguments(14)=timeName: name of the time variable
! arguments(15)=timeUnitsName: name of the attribute with the units of the time variable
! arguments(16)=MaxBits: number maximum of bits of the matrix with all the values
! DIMshape: shape of dimensions
! Vdimsid: Id of the dimensions of the variable
! Vid: Id of the variable within the file
! Vtype: variable type
! Vndims: number of dimensions of the variable
! Vnatts: number of attributes of the variable
! gridboxpts: points of the grid within the box range
! gridboxlonlat: lon,lats of the grid within the box range
! values: Matrix with all the temporal values of the allowed maximum number of locations
! [I/C/R]timevalues: [Integer/Character/Real] time values
! [I/R/D/S/L]values: [Integer/Real/Double Precission/String/Logial] values of 
!   the variable
! foldern: folder where are all the files
! filens: vector with the name of all the files in 'foldern'
! Nfiles: number of files
! coord: coordinates of a 1point (assuming 6 dims in a netCDF file)
! [lon/lat]pt: coordinates of a given netCDF point
! NwrittenPoints: number of points already written in the output file
! Nsec: number of used sections
! timeunits: units of variable time

!!!!!!! Subroutines/Functions
! Rtime_values: Subroutine to retrieve real time values from a netCDF unit
! Ctime_values: Subroutine to retrieve character time values from a netCDF unit
! dimTfile: Function to give shape of temporal dimension of a given netCDF unit
! get_Rtval: Subroutine to get all the t-values of a variable
! get_R1val: Subroutine to get 1 value of a variable
! nc_check_variable: Subroutine to check the existence of a given variable in a given 'nc' 
!   netCDF connection

  errmsg='  ERROR -- error -- ERROR -- error'

! Arguments
  narg=COMMAND_ARGUMENT_COUNT()

  IF (narg < 16) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,'     Not enough arguments. !!! '
    PRINT *,'  ',narg,' are provided'//CHAR(44)//' but 16 are needed!!!'
    PRINT *,TRIM(errmsg)
    STOP
  END IF
  IF (ALLOCATED(arguments)) DEALLOCATE(arguments)
  ALLOCATE(arguments(narg))

  DO iarg=1, narg
    CALL GETARG(iarg,arguments(iarg))
  END DO
  IF (TRIM(arguments(1)) == '-h' ) THEN
    PRINT *,"nc_var_out [debug] [fold] [fileHeader] [fileEnd] [firstFile] [lastFile] [var] " //   &
      "[long_SW] [lat_SW] [long_NE] [lat_NE] [lonName] [latName] [timeName] [MaxBits]"
    PRINT *,"  [debug]: debug level"
    PRINT *,"  [fold]: folder with netCDF files"
    PRINT *,"  [fileHeader]: header of the files' name"
    PRINT *,"  [fileEnd]: end of the files' name"
    PRINT *,"  [firstFile]: number of the first file to process"
    PRINT *,"  [lastFile]: number of the last file to process"
    PRINT *,"  [var]: variable name"
    PRINT *,"  [long_SW]: longitude of SW corner" 
    PRINT *,"  [lat_SW]: latitude of SW corner"
    PRINT *,"  [long_NE]: longitude of NE corner" 
    PRINT *,"  [lat_NE]: latitude of NE corner"
    PRINT *,"  [lonName]: name of the longitude inside the files"
    PRINT *,"  [latName]: name of the latitude inside the files"
    PRINT *,"  [timeName]: name of the time inside the files"
    PRINT *,"  [timeUnitsName]: name of the attribute with the name of the units of the time variable"
    PRINT *,"  [MaxBits]: number maximum of bits of the matrix with the values"
    STOP
  END IF

  READ(arguments(1), 2)debug
  foldern=TRIM(arguments(2))
  fileHeader=TRIM(arguments(3))
  fileEnd=TRIM(arguments(4))
  READ(arguments(5), 2)firstFile
  READ(arguments(6), 2)lastFile
  varname=TRIM(arguments(7))
  READ(arguments(8), 4)lonSW
  READ(arguments(9), 4)latSW
  READ(arguments(10), 4)lonNE
  READ(arguments(11), 4)latNE
  lonName=TRIM(arguments(12))
  latName=TRIM(arguments(13))
  timeName=TRIM(arguments(14))
  timeUnitsName=TRIM(arguments(15))
  READ(arguments(16), 3)MaxBits

  DEALLOCATE(arguments)

  IF (debug >= 50) THEN
    PRINT '(A32)','Arguments of the program _______'
    PRINT '(2x,A12,I4)','debug level:', debug
    PRINT *," folder with netCDF files: '"//TRIM(foldern)//"'"
    PRINT *," header of files: '"//TRIM(fileHeader)//"'"
    PRINT *," end of files: '"//TRIM(fileEnd)//"'"
    PRINT '(2x,A11,I4)','First file:',firstFile
    PRINT '(2x,A10,I4)','Last file:',lastFile
    PRINT *," variable name: '"//TRIM(varname)//"'"
    PRINT '(2x,A10,F5.1,A1,F5.1)','SW corner:',lonSW,char(44),latSW
    PRINT '(2x,A10,F5.1,A1,F5.1)','NE corner:',lonNE,char(44),latNE
    PRINT *," longitude nanme '"//TRIM(lonName)//"' "//CHAR(44)//" latitude name '"//            &
      TRIM(latName)//"' time name '"//TRIM(timeName)//"' attribute name with the units: "//      &
      TRIM(timeUnitsName)
    PRINT '(2x,A23,1x,I10)','Number maximum of bits:',MaxBits
  END IF
! Files listing
!
  command='ls -1 '//TRIM(foldern)//'/'//TRIM(fileHeader)//'*'//TRIM(fileEnd)//' > data_files.inf'
  istatsys=SYSTEM(TRIM(command))
  IF (istatsys /= 0) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Instruction '"//TRIM(command)//"' ends with a system number: ",istatsys
    PRINT *,TRIM(errmsg)
    STOP
  END IF

! Number of files
!
  command="wc -l data_files.inf | awk '{print $1}' > numFiles.inf"
  istatsys=SYSTEM(TRIM(command))
  IF (istatsys /= 0) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Instruction '"//TRIM(command)//"' ends with a system number: ",istatsys
    PRINT *,TRIM(errmsg)
    STOP
  END IF

  OPEN(FILE='numFiles.inf', UNIT=17, STATUS='OLD')
  READ(17,*)Nfiles
  CLOSE(17)
  PRINT *,"Number of files in '"//TRIM(foldern)//"':",Nfiles

  IF ((firstFile > Nfiles) .OR. (lastFile > Nfiles) .OR. (firstFile > lastFile)) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Change the values of the 'firstFile'=", firstFile
    PRINT *,"    and the 'lastFile'=", lastFile, 'for ',Nfiles
    PRINT *,TRIM(errmsg)
  END IF

  IF (ALLOCATED(filens)) DEALLOCATE(filens)
  ALLOCATE(filens(Nfiles))

  OPEN(FILE='data_files.inf', UNIT=17, STATUS='OLD')

  DO ifile=1, Nfiles
    READ(17,7)filens(ifile)
    IF (debug >= 50) PRINT *,ifile," '"//TRIM(filens(ifile))//"'"
  END DO
  CLOSE(17)

!!  filen=TRIM(foldern)//'/'//TRIM(filens(1))
  filen=TRIM(filens(1))
  rcode = nf90_open(filen, nf90_NoWrite, ncfid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

! Check existence of variable, lon & lat
!!
  CALL nc_check_variable(ncfid, lonName)
  CALL nc_check_variable(ncfid, latName)
  CALL nc_check_variable(ncfid, varname)
  CALL nc_check_variable(ncfid, timeName)
  rcode = nf90_inq_varid(ncfid, lonName, Vid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncfid, varid=Vid, xtype=lonlattype, ndims=lonlatndims,           &
     dimids=lonlatdimsid, nAtts=lonlatnatts)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

! Space information
!!
  rcode = nf90_inq_varid(ncfid, varname, Vid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncfid, varid=Vid, xtype=Vtype, ndims=Vndims, dimids=Vdimsid,      &
     nAtts=Vnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  IF (debug >= 50) THEN
    PRINT *,'var id;',Vid,"name; '"//TRIM(varname)//"' type;",Vtype,' n. of dimensions; ',       &
      Vndims, ' n. of attributes; ', Vnatts

  PRINT *,'Dimensions; ___ __ _'
  END IF
  DIMname=''
  DIMshape=0
  DO idim=1, Vndims
    rcode = nf90_inquire_dimension(ncfid, Vdimsid(idim), DIMname(idim), DIMshape(idim))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (debug >= 50) PRINT *,'   ',idim, Vdimsid(idim), TRIM(DIMname(idim)),'; ', DIMshape(idim)
  END DO

  dimx=DIMshape(1)
  dimy=DIMshape(2)
  IF (Vndims > 3) THEN
    dimz=DIMshape(3)
    dimt=DIMshape(4)
    posTdim=4
  ELSE
    dimt=DIMshape(3)
    posTdim=3
  END IF

! Time information
!
  timeUnits=''
  rcode = nf90_inq_varid(ncfid, timeName, Vid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncfid, varid=Vid, xtype=timetype, ndims=Vndims, dimids=Vdimsid,  &
     nAtts=Vnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

  IF (timetype == NF90_CHAR) THEN
    IF (ALLOCATED(Ctime1values)) DEALLOCATE(Ctime1values)
    ALLOCATE(Ctime1values(dimt))
    Ctime1values=''
    CALL Ctime_values(debug, ncfid, timeName, dimt, Ctime1values)
  ELSE IF (timetype == NF90_INT) THEN
    IF (ALLOCATED(Itime1values)) DEALLOCATE(Itime1values)
    ALLOCATE(Itime1values(dimt))
    CALL Itime_values(debug, ncfid, timeName, dimt, Itime1values)
    rcode = nf90_get_att(ncfid, Vid, timeUnitsName, timeUnits)
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    PRINT *,"  Units of variable '"//TRIM(timeName)//"': "//TRIM(timeUnits)
  ELSE IF (timetype == NF90_FLOAT) THEN
    IF (ALLOCATED(Rtime1values)) DEALLOCATE(Rtime1values)
    ALLOCATE(Rtime1values(dimt))
    CALL Rtime_values(debug, ncfid, timeName, dimt, Rtime1values)
    rcode = nf90_get_att(ncfid, Vid, timeUnitsName, timeUnits)
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    PRINT *,"  Units of variable '"//TRIM(timeName)//"': "//TRIM(timeUnits)
  ELSE IF (timetype == NF90_DOUBLE) THEN
    IF (ALLOCATED(Rtime1values)) DEALLOCATE(Rtime1values)
    ALLOCATE(Rtime1values(dimt))
    CALL Dtime_values(debug, ncfid, timeName, dimt, Rtime1values)
    rcode = nf90_get_att(ncfid, Vid, timeUnitsName, timeUnits)
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    PRINT *,"  Units of variable '"//TRIM(timeName)//"': "//TRIM(timeUnits)
  END IF
  
! Data summary
!

  PRINT *,"  Varname: '"//TRIM(varname)//"' hor. dimension:", dimx, CHAR(44), dimy,            &
    'time-steps:', dimt

  IF (debug >= 50) THEN
    PRINT *,'  Time steps in the first file'
    IF (timetype == NF90_CHAR) THEN
      DO i=1,dimt-3,3
        PRINT '(4x,3(A25,1x))',(("'"//TRIM(Ctime1values(i+j))//"'"), j=0,2)
      END DO
    ELSE IF ((timetype == NF90_INT)) THEN
      DO i=1,dimt-1,3
        PRINT '(4x,3(I10,1x))',((Itime1values(i+j)), j=0,2)
      END DO
    ELSE IF ((timetype == NF90_REAL) .OR. (timetype == NF90_DOUBLE)) THEN
      DO i=1,dimt-1,3
        PRINT '(4x,3(F20.10,1x))',((Rtime1values(i+j)), j=0,2)
      END DO
    END IF
  END IF

! Grid points within the lon/lat box
!!
  coord=1
  NpointsIn=1
  alldomain: DO i=1, dimx
    DO j=1, dimy
      coord(1)=i
      coord(2)=j
      IF (lonlattype == NF90_FLOAT) THEN
        CALL get_R1val(debug, ncfid, lonName, coord, lonpt)
        CALL get_R1val(debug, ncfid, latName, coord, latpt)
      ELSE IF (lonlattype == NF90_DOUBLE) THEN
        CALL get_D1val(debug, ncfid, lonName, coord, lonpt)
        CALL get_D1val(debug, ncfid, latName, coord, latpt)
      END IF
      IF (debug >= 100) THEN
        PRINT '(4x,F7.3,A1,1x,F7.3,2x,A1,F5.1,A1,F5.1,1x,A1,1x,F5.1,A1,F5.1,A1)',lonpt,CHAR(44)  &
          ,latpt,'(',lonSW,',',latSW,'x',lonNE,',',latNE,')'
      END IF
      within: IF ( ((lonpt>=lonSW).AND.(lonpt<=lonNE)) .AND. ((latpt>=latSW).AND.                &
        (latpt<=latNE))) THEN
          NpointsIn=NpointsIn+1
      END IF within
    END DO
  END DO alldomain

  IF (NpointsIn <= 1) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,'    There are not enough points inside this lon/lat box !!'
    PRINT '(5x,A1,F5.1,A1,F5.1,1x,A1,1x,F5.1,A1,F5.1,A2,1x,I6,1x,A6)','(',lonSW,',',latSW,'x',   &
      lonNE,',',latNE,'):', NpointsIn,'points'
    PRINT *,'    Please'//CHAR(44)//' redefine lon/lat box'
    PRINT *,TRIM(errmsg)
    STOP
  ELSE
    PRINT *,'Number of gridpoints within the lon/lat grid box:', NpointsIn
  END IF 

  IF (ALLOCATED(gridboxpts)) DEALLOCATE(gridboxpts)
  ALLOCATE(gridboxpts(NpointsIn,2))
  IF (ALLOCATED(gridboxlonlat)) DEALLOCATE(gridboxlonlat)
  ALLOCATE(gridboxlonlat(NpointsIn,2))

! Grid points within the lon/lat box
!!
  coord=1
  ipoint=1
  OPEN(UNIT=19, FILE='coords.inf', STATUS='unknown')
  DO i=1, dimx
    DO j=1, dimy
      coord(1)=i
      coord(2)=j
      IF (lonlattype == NF90_FLOAT) THEN
        CALL get_R1val(debug, ncfid, lonName, coord, lonpt)
        CALL get_R1val(debug, ncfid, latName, coord, latpt)
      ELSE IF (lonlattype == NF90_DOUBLE) THEN
        CALL get_D1val(debug, ncfid, lonName, coord, lonpt)
        CALL get_D1val(debug, ncfid, latName, coord, latpt)
      END IF
      IF ( ((lonpt>=lonSW).AND.(lonpt<=lonNE)) .AND. ((latpt>=latSW).AND.(latpt<=latNE))) THEN
          gridboxpts(ipoint,:)=RESHAPE((/i,j/),(/2/))
          gridboxlonlat(ipoint,:)=RESHAPE((/lonpt,latpt/),(/2/))
          WRITE(19,195)ipoint,gridboxpts(ipoint,:),gridboxlonlat(ipoint,:)
          ipoint=ipoint+1
      END IF
    END DO
  END DO

  rcode = nf90_close(ncfid)
  CLOSE(UNIT=19)

  IF (debug >= 100) THEN
    PRINT *,'  Grid box values ... .. .'
    DO i=1, NpointsIn
      PRINT '(4x,I5,1x,A1,F10.5,A1,1x,F10.5)',i,':',gridboxlonlat(i,1),CHAR(44),gridboxlonlat(i,2)
    END DO
  END IF

  TOTdimt=0
! Accounting for the total of time-steps
!!
!!  DO ifile=1, Nfiles
  DO ifile=firstFile, lastFile

!!    filen=TRIM(foldern)//'/'//TRIM(filens(ifile))
    filen=TRIM(filens(ifile))
    rcode = nf90_open(filen, nf90_NoWrite, ncfid)
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

    dimt=dimTfile(debug, ncfid, varname)
    PRINT *,ifile," File: '"//TRIM(filen)//"' dimt:", dimt
    TOTdimt=TOTdimt+dimt
  END DO

  PRINT *,'Total number of time-steps: ',TOTdimt
!
! Matrix of values will be filled as follows:
!    - All files will be open consecutively (it is assumed that the file names are right)
!    - According to 'MaxBits', only all temporal values of 'MaxBits/(TOTdimt)' stations 
!        will be memorized
!    - At the end of file this values will be written on the ASCII file
!    - Keep going with the following 'MaxBits/(TOTdimt)' stations

!  Definition of filling matrix (up to 'MaxBits' B)
!    Matrix filled with all the temporal values at a given number of locations up to 'MaxBits'
!!
  NptsSec=MaxBits/(TOTdimt)
  IF (NptsSec == 0) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Not enough memory!!! Increase the value of 'MaxBits'=",MaxBits
    PRINT *,'    at least to: ',TOTdimt
    PRINT *,'    or use another machine...'
    PRINT *,TRIM(errmsg)
  END IF

! Are we able to keep much more points that points within the grid box?
!
  IF (NptsSec >= NpointsIN) NptsSec=NpointsIN

  IF (ALLOCATED(values)) DEALLOCATE(values)
  ALLOCATE(values(NptsSec,TOTdimt), STAT=ferr)
  IF (ferr /= 0) THEN
    PRINT *,errmsg
    PRINT '(4x,A22,I4,A1,I4,A32)','Allocation of matrix (',NptsSec,CHAR(44),TOTdimt,         &
      ') with all the values has failed'
    PRINT *,errmsg
    STOP
  END IF
  PRINT *, 'Dimensions of the values matrix: ',UBOUND(values)

  PRINT *, 'There are ',NpointsIn,' points inside the LON/LAT box that will be splitted '//  &
   'in pieces of ', NptsSec,' points'
  PRINT *, 'Starting temporal loop ... .. .'

  coord=1
  isec=1
  IF (timetype == NF90_CHAR) THEN
    IF (ALLOCATED(Ctimevalues)) DEALLOCATE(Ctimevalues)
    ALLOCATE(Ctimevalues(TOTdimt))
  ELSE IF ( (timetype == NF90_INT)) THEN
    IF (ALLOCATED(Itimevalues)) DEALLOCATE(Itimevalues)
    ALLOCATE(Itimevalues(TOTdimt))
  ELSE IF ( (timetype == NF90_FLOAT) .OR. (timetype == NF90_DOUBLE)) THEN
    IF (ALLOCATED(Rtimevalues)) DEALLOCATE(Rtimevalues)
    ALLOCATE(Rtimevalues(TOTdimt))
  END IF

  OPEN(UNIT=17, FILE='nc_ASCII_lonlatval_'//TRIM(varname)//'.dat', STATUS='UNKNOWN')

  NwrittenPoints=0
  Nsec=0
  allpoints: DO ipoint=1, NpointsIn, NptsSec
    itt=0

    IF (ALLOCATED(values)) DEALLOCATE(values)
    ALLOCATE(values(NptsSec,TOTdimt), STAT=ferr)
    IF (ferr /= 0) THEN
      PRINT *,errmsg
      PRINT '(4x,A22,I4,A1,I4,A32)','Allocation of matrix (',NptsSec,CHAR(44),TOTdimt,         &
        ') with all the values has failed'
      PRINT *,errmsg
      STOP
    END IF
    values=0.
!!    allfiles: DO ifile=1, Nfiles
    allfiles: DO ifile=firstFile, lastFile

      filen=TRIM(filens(ifile))
      rcode = nf90_open(filen, nf90_NoWrite, ncfid)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

      OPEN(UNIT=19, FILE='coords.inf', STATUS='OLD')
      dimt=dimTfile(debug, ncfid, varname)
      IF (debug >= 10) PRINT *,ifile," File: '"//TRIM(filen)//"' dimt:", dimt, 'ipoint: ', ipoint, 'itt: ',itt
      IF (timetype == NF90_CHAR) THEN
        IF (ALLOCATED(Csectimevalues)) DEALLOCATE(Csectimevalues)
        ALLOCATE(Csectimevalues(dimt))
        CALL Ctime_values(debug, ncfid, timeName, dimt, Csectimevalues)
        Ctimevalues(itt+1:dimt)=Csectimevalues
      ELSE IF (timetype == NF90_INT) THEN
        IF (ALLOCATED(Isectimevalues)) DEALLOCATE(Isectimevalues)
        ALLOCATE(Isectimevalues(dimt))
        CALL Itime_values(debug, ncfid, timeName, dimt, Isectimevalues)
        Itimevalues(itt+1:dimt)=Isectimevalues
      ELSE IF (timetype == NF90_FLOAT) THEN
        IF (ALLOCATED(Rsectimevalues)) DEALLOCATE(Rsectimevalues)
        ALLOCATE(Rsectimevalues(dimt))
        CALL Rtime_values(debug, ncfid, timeName, dimt, Rsectimevalues)
        Rtimevalues(itt+1:dimt)=Rsectimevalues
      ELSE IF (timetype == NF90_DOUBLE) THEN
        IF (ALLOCATED(Rsectimevalues)) DEALLOCATE(Rsectimevalues)
        ALLOCATE(Rsectimevalues(dimt))
        CALL Dtime_values(debug, ncfid, timeName, dimt, Rsectimevalues)
        Rtimevalues(itt+1:dimt)=Rsectimevalues
      END IF
      IF (ALLOCATED(tvalpt)) DEALLOCATE(tvalpt)
      ALLOCATE(tvalpt(dimt))

!      PRINT *,'    Loop of points... .. .', NptsSec, 'ifile: ',ifile
      secpts: DO ipt=1,NptsSec
        ippt=ipoint+ipt-1
        IF (ippt < NpointsIN) THEN
!          coord(1)=gridboxpts(ippt,1)
!          coord(2)=gridboxpts(ippt,2)
          READ(19,198),j,coord(1),coord(2)
          IF (ALLOCATED(tvalpt)) DEALLOCATE(tvalpt)
          ALLOCATE(tvalpt(dimt), STAT=ferr)
          IF (ferr /= 0) THEN
            PRINT *,errmsg
            PRINT '(4x,A22,I4,A50)','Allocation of matrix (',dimt,                               &
              ') with all temporal values at one point has failed'
            PRINT *,errmsg
            STOP
          END IF

          IF (Vtype == NF90_FLOAT) THEN
            CALL get_Rtval(debug, ncfid, varname, coord, posTdim, dimt, tvalpt)
          ELSE
            CALL get_Dtval(debug, ncfid, varname, coord, posTdim, dimt, tvalpt)
          END IF
          DO j=1,dimt
            values(ipt,itt+j)=tvalpt(j)
          ENDDO
          IF ((debug >= 75).AND.(ipt==1)) THEN
            PRINT *,ifile," File: '"//TRIM(filen)//"' dimt:", dimt
            PRINT '(6x,A3,1x,I3,A1,1x,I3,1x,A8,1x,f5.1,A1,1x,f5.1,1x,A6,1x,E8.2)','ij:',coord(1),&
              CHAR(44),coord(2),'lon/lat:',gridboxlonlat(ippt,1),CHAR(44),gridboxlonlat(ippt,2)
            DO i=1,dimt,3
              PRINT '(6x,3(I4,1x,A4,1x,F10.5))',(i+j,'val:',tvalpt(i+j),j=0,2)
            END DO
          END IF
!!          STOP
        ELSE
          EXIT
        END IF
      END DO secpts

      rcode = nf90_close(ncfid)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
      CLOSE(UNIT=19)
      itt=itt+dimt
    END DO allfiles

    IF (isec == 1) THEN
      IF (timetype == NF90_CHAR) THEN
        WRITE(17,52)'#longitude[1]',CHAR(44),'latitude[2]',                                     &
          (CHAR(44),TRIM(Ctimevalues(it)), it=1,itt)
      ELSE IF (timetype == NF90_INT) THEN
        WRITE(17,55)'#longitude[1]',CHAR(44),'latitude[2]',CHAR(44),timeUnits,':',              &
          (Itimevalues(it), CHAR(44), it=1,itt)
      ELSE IF ((timetype == NF90_FLOAT) .OR. (timetype == NF90_DOUBLE)) THEN
        WRITE(17,57)'#longitude[1]',CHAR(44),'latitude[2]',CHAR(44),timeUnits,':',              &
          (Rtimevalues(it), CHAR(44), it=1,itt)
      END IF
    END IF

    DO ipt=1,NptsSec
      NwrittenPoints=NwrittenPoints+1
      IF (NwrittenPoints < NpointsIn) THEN
        WRITE(17,50)gridboxlonlat(ipt,1),CHAR(44),gridboxlonlat(ipt,2),                         &
          (CHAR(44),values(ipt,j), j=1,TOTdimt)
      ELSE
        EXIT
      END IF
    END DO
    isec=isec+1

!!    IF (isec >= 3) STOP
  END DO allpoints

  CLOSE(UNIT=17)
  DEALLOCATE(values)
  DEALLOCATE(gridboxlonlat)
  DEALLOCATE(tvalpt)

  rcode = nf90_close(ncfid)

! removing temporal files
!
  command='rm data_files.inf'
  istatsys=SYSTEM(TRIM(command))
  IF (istatsys /= 0) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Instruction '"//TRIM(command)//"' ends with a system number: ",istatsys
    PRINT *,TRIM(errmsg)
    STOP
  END IF

  command='rm numFiles.inf'
  istatsys=SYSTEM(TRIM(command))
  IF (istatsys /= 0) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    Instruction '"//TRIM(command)//"' ends with a system number: ",istatsys
    PRINT *,TRIM(errmsg)
    STOP
  END IF

  PRINT *,'******* ****** ***** **** *** ** *'
  PRINT *,'**  Successful completition of the program !!!'
  PRINT *,"**     Output written in 'nc_ASCII_lonlatval_"//TRIM(varname)//".dat'"
  PRINT *,'******* ****** ***** **** *** ** *'

 2 FORMAT(I4)
 3 FORMAT(I10)
 4 FORMAT(F5.2)
 6 FORMAT(A500)
 7 FORMAT(A300)
50 FORMAT(F25.5,A1,1x,F25.5,50000(A1,1x,F25.5))
52 FORMAT(50000(A25,A1,1x))
55 FORMAT(2(A25,A1,1x),A50,A1,50000(I25,A1,1x))
57 FORMAT(2(A25,A1,1x),A50,A1,50000(F25.5,A1,1x))
195 FORMAT(3(I5,1x),2(F25.5,1x))
198 FORMAT(3(I5,1x))

END PROGRAM nc_ASCII_lonlatval

SUBROUTINE Dtime_values(dbg, ncIDnum, timen, dimt, timevals)
! Subroutine to retrieve real time values from a netCDF unit

  USE netcdf

  IMPLICIT NONE 

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, dimt
  CHARACTER(LEN=250), INTENT(IN)                         :: timen
  REAL, DIMENSION(dimt), INTENT(OUT)                     :: timevals

! Local 
  INTEGER                                                :: id, it
  INTEGER                                                :: rcode, Tid, Ttype, Tndims, Tnatts
  INTEGER, ALLOCATABLE, DIMENSION(:)                     :: DIMsh
  INTEGER, DIMENSION(6)                                  :: startv, countv, Tdimsid
  INTEGER, PARAMETER                                     :: DoubleReal = SELECTED_REAL_KIND(8)
  CHARACTER(LEN=100), ALLOCATABLE, DIMENSION(:)          :: DIMn
  REAL(DoubleReal), ALLOCATABLE, DIMENSION(:,:,:,:,:,:)  :: Dval
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: netCDF id connection
! timen: name of the time variable
! dimt: dimension of the temporal variable
! timevals: values of time

  section="'Dtime_values'... .. ."

  startv=1
  countv=1
  rcode = nf90_inq_varid(ncIDnum, timen, Tid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncIDnum, varid=Tid, xtype=Ttype, ndims=Tndims, dimids=Tdimsid,   &
     nAtts=Tnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  
  IF (ALLOCATED(DIMn)) DEALLOCATE(DIMn)
  ALLOCATE(DIMn(Tndims))
  IF (ALLOCATED(DIMsh)) DEALLOCATE(DIMsh)
  ALLOCATE(DIMsh(Tndims))

  IF (dbg >= 50) THEN
    PRINT *,'Time id;',Tid,"name; '"//TRIM(timen)//"' type;",Ttype,' n. of dimensions;',         &
      Tndims,' n. of attributes; ', Tnatts
    PRINT *,' Dimensions; ___ __ _'
  END IF
  DIMn=''
  DIMsh=0  
  DO id=1, Tndims
    rcode = nf90_inquire_dimension(ncIDnum, Tdimsid(id), DIMn(id), DIMsh(id))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (dbg >= 50) PRINT *,'   ',id, Tdimsid(id), TRIM(DIMn(id)),'; ', DIMsh(id)
  END DO  
  IF (ALLOCATED(Dval)) DEALLOCATE(Dval)
  IF (Tndims==3) THEN
    ALLOCATE(Dval(DIMsh(1),DIMsh(2),DIMsh(3),1,1,1))
    countv=RESHAPE((/DIMSh(1),DIMsh(2),DIMsh(3),1,1,1/),(/6/))
  ELSE IF (Tndims==2) THEN
    ALLOCATE(Dval(DIMsh(1),DIMsh(2),1,1,1,1))
    countv=RESHAPE((/DIMsh(1),DIMsh(2),1,1,1,1/),(/6/))
  ELSE IF (Tndims==1) THEN
    ALLOCATE(Dval(dimt,1,1,1,1,1))
    countv=RESHAPE((/dimt,1,1,1,1,1/),(/6/))
  END IF

  rcode = nf90_get_var(ncIDnum, Tid, Dval, startv, countv)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

  IF (Tndims==3) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Dval(it,0,0,1,1,1)
    END DO
  ELSE IF (Tndims==2) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Dval(it,0,1,1,1,1)
    END DO
  ELSE IF (Tndims==1) THEN
    DO it=1, DIMsh(1)
      timevals(it)=Dval(it,1,1,1,1,1)
    END DO
  END IF

  IF (dbg >= 75) THEN
    PRINT *,'Time values ___ __ _', dimt, ':', DIMsh(1)
    DO it=1, DIMsh(1)
      PRINT *,it,": ",timevals(it)
    END DO
  END IF

!  DEALLOCATE(Rval)
  RETURN

END SUBROUTINE Dtime_values

SUBROUTINE Rtime_values(dbg, ncIDnum, timen, dimt, timevals)
! Subroutine to retrieve real time values from a netCDF unit

  USE netcdf

  IMPLICIT NONE 

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, dimt
  CHARACTER(LEN=250), INTENT(IN)                         :: timen
  REAL, DIMENSION(dimt), INTENT(OUT)                     :: timevals

! Local 
  INTEGER                                                :: id, it
  INTEGER                                                :: rcode, Tid, Ttype, Tndims, Tnatts
  INTEGER, ALLOCATABLE, DIMENSION(:)                     :: DIMsh
  INTEGER, DIMENSION(6)                                  :: startv, countv, Tdimsid
  CHARACTER(LEN=100), ALLOCATABLE, DIMENSION(:)          :: DIMn
  REAL, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)              :: Rval
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: netCDF id connection
! timen: name of the time variable
! dimt: dimension of the temporal variable
! timevals: values of time

  section="'Rtime_values'... .. ."

  startv=1
  countv=1
  rcode = nf90_inq_varid(ncIDnum, timen, Tid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncIDnum, varid=Tid, xtype=Ttype, ndims=Tndims, dimids=Tdimsid,   &
     nAtts=Tnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  
  IF (ALLOCATED(DIMn)) DEALLOCATE(DIMn)
  ALLOCATE(DIMn(Tndims))
  IF (ALLOCATED(DIMsh)) DEALLOCATE(DIMsh)
  ALLOCATE(DIMsh(Tndims))

  IF (dbg >= 50) THEN
    PRINT *,'Time id;',Tid,"name; '"//TRIM(timen)//"' type;",Ttype,' n. of dimensions;',         &
      Tndims,' n. of attributes; ', Tnatts
    PRINT *,' Dimensions; ___ __ _'
  END IF
  DIMn=''
  DIMsh=0  
  DO id=1, Tndims
    rcode = nf90_inquire_dimension(ncIDnum, Tdimsid(id), DIMn(id), DIMsh(id))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (dbg >= 50) PRINT *,'   ',id, Tdimsid(id), TRIM(DIMn(id)),'; ', DIMsh(id)
  END DO  
  IF (ALLOCATED(Rval)) DEALLOCATE(Rval)
  IF (Tndims==3) THEN
    ALLOCATE(Rval(DIMsh(1),DIMsh(2),DIMsh(3),1,1,1))
    countv=RESHAPE((/DIMSh(1),DIMsh(2),DIMsh(3),1,1,1/),(/6/))
  ELSE IF (Tndims==2) THEN
    ALLOCATE(Rval(DIMsh(1),DIMsh(2),1,1,1,1))
    countv=RESHAPE((/DIMsh(1),DIMsh(2),1,1,1,1/),(/6/))
  ELSE IF (Tndims==1) THEN
    ALLOCATE(Rval(DIMsh(1),1,1,1,1,1))
    countv=RESHAPE((/DIMsh(1),1,1,1,1,1/),(/6/))
  END IF
  rcode = nf90_get_var(ncIDnum, Tid, Rval, startv, countv)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

  IF (Tndims==3) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Rval(it,0,0,1,1,1)
    END DO
  ELSE IF (Tndims==2) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Rval(it,0,1,1,1,1)
    END DO
  ELSE IF (Tndims==1) THEN
    DO it=1, DIMsh(1)
      timevals(it)=Rval(it,1,1,1,1,1)
    END DO
  END IF

  IF (dbg >= 75) THEN
    PRINT *,'Time values ___ __ _'
    DO it=1, DIMsh(1)
      PRINT *,it,": ",timevals(it)
    END DO
  END IF

!  DEALLOCATE(Rval)
  RETURN

END SUBROUTINE Rtime_values

SUBROUTINE Itime_values(dbg, ncIDnum, timen, dimt, timevals)
! Subroutine to retrieve integer time values from a netCDF unit

  USE netcdf

  IMPLICIT NONE 

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, dimt
  CHARACTER(LEN=250), INTENT(IN)                         :: timen
  INTEGER, DIMENSION(dimt), INTENT(OUT)                  :: timevals

! Local 
  INTEGER                                                :: id, it
  INTEGER                                                :: rcode, Tid, Ttype, Tndims, Tnatts
  INTEGER, ALLOCATABLE, DIMENSION(:)                     :: DIMsh
  INTEGER, DIMENSION(6)                                  :: startv, countv, Tdimsid
  CHARACTER(LEN=100), ALLOCATABLE, DIMENSION(:)          :: DIMn
  INTEGER, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)           :: Ival
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: netCDF id connection
! timen: name of the time variable
! dimt: dimension of the temporal variable
! timevals: values of time

  section="'Rtime_values'... .. ."

  startv=1
  countv=1
  rcode = nf90_inq_varid(ncIDnum, timen, Tid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncIDnum, varid=Tid, xtype=Ttype, ndims=Tndims, dimids=Tdimsid,   &
     nAtts=Tnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  
  IF (ALLOCATED(DIMn)) DEALLOCATE(DIMn)
  ALLOCATE(DIMn(Tndims))
  IF (ALLOCATED(DIMsh)) DEALLOCATE(DIMsh)
  ALLOCATE(DIMsh(Tndims))

  IF (dbg >= 50) THEN
    PRINT *,'Time id;',Tid,"name; '"//TRIM(timen)//"' type;",Ttype,' n. of dimensions;',         &
      Tndims,' n. of attributes; ', Tnatts
    PRINT *,' Dimensions; ___ __ _'
  END IF
  DIMn=''
  DIMsh=0  
  DO id=1, Tndims
    rcode = nf90_inquire_dimension(ncIDnum, Tdimsid(id), DIMn(id), DIMsh(id))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (dbg >= 50) PRINT *,'   ',id, Tdimsid(id), TRIM(DIMn(id)),'; ', DIMsh(id)
  END DO  
  IF (ALLOCATED(Ival)) DEALLOCATE(Ival)
  IF (Tndims==3) THEN
    ALLOCATE(Ival(DIMsh(1),DIMsh(2),DIMsh(3),1,1,1))
    countv=RESHAPE((/DIMSh(1),DIMsh(2),DIMsh(3),1,1,1/),(/6/))
  ELSE IF (Tndims==2) THEN
    ALLOCATE(Ival(DIMsh(1),DIMsh(2),1,1,1,1))
    countv=RESHAPE((/DIMsh(1),DIMsh(2),1,1,1,1/),(/6/))
  ELSE IF (Tndims==1) THEN
    ALLOCATE(Ival(DIMsh(1),1,1,1,1,1))
    countv=RESHAPE((/DIMsh(1),1,1,1,1,1/),(/6/))
  END IF
  rcode = nf90_get_var(ncIDnum, Tid, Ival, startv, countv)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

  IF (Tndims==3) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Ival(it,0,0,1,1,1)
    END DO
  ELSE IF (Tndims==2) THEN
    DO it=1, DIMsh(2)
      timevals(it)=Ival(it,0,1,1,1,1)
    END DO
  ELSE IF (Tndims==1) THEN
    DO it=1, DIMsh(1)
      timevals(it)=Ival(it,1,1,1,1,1)
    END DO
  END IF

  IF (dbg >= 75) THEN
    PRINT *,'Time values ___ __ _'
    DO it=1, DIMsh(1)
      PRINT *,it,": ",timevals(it)
    END DO
  END IF

!  DEALLOCATE(Ival)
  RETURN

END SUBROUTINE Itime_values

SUBROUTINE Ctime_values(dbg, ncIDnum, timen, dimt, timevals)
! Subroutine to retrieve character time values from a netCDF unit

  USE netcdf

  IMPLICIT NONE 

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, dimt
  CHARACTER(LEN=250), INTENT(IN)                         :: timen
  CHARACTER(LEN=100), DIMENSION(dimt), INTENT(OUT)       :: timevals

! Local 
  INTEGER                                                :: id, it, ic
  INTEGER                                                :: rcode, Tid, Ttype, Tndims, Tnatts
  INTEGER, ALLOCATABLE, DIMENSION(:)                     :: DIMsh
  INTEGER, DIMENSION(6)                                  :: startv, countv, Tdimsid
  CHARACTER(LEN=100), ALLOCATABLE, DIMENSION(:)          :: DIMn
  CHARACTER(LEN=1), ALLOCATABLE, DIMENSION(:,:,:,:,:,:)  :: Cval
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: netCDF id connection
! timen: name of the time variable
! dimt: dimension of the temporal variable
! timevals: values of time  

  section="'Ctime_values' ... .. ."

  startv=1
  countv=1
  rcode = nf90_inq_varid(ncIDnum, timen, Tid)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncIDnum, varid=Tid, xtype=Ttype, ndims=Tndims, dimids=Tdimsid,   &
     nAtts=Tnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  
  IF (ALLOCATED(DIMn)) DEALLOCATE(DIMn)
  ALLOCATE(DIMn(Tndims))
  IF (ALLOCATED(DIMsh)) DEALLOCATE(DIMsh)
  ALLOCATE(DIMsh(Tndims))

  IF (dbg >= 50) THEN
    PRINT *,'Time id;',Tid,"name; '"//TRIM(timen)//"' type;",Ttype,' n. of dimensions;',         &
      Tndims,' n. of attributes; ', Tnatts
    PRINT *,' Dimensions; ___ __ _'
  END IF
  DIMn=''
  DIMsh=0  
  DO id=1, Tndims
    rcode = nf90_inquire_dimension(ncIDnum, Tdimsid(id), DIMn(id), DIMsh(id))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (dbg >= 50) PRINT *,'   ',id, Tdimsid(id), TRIM(DIMn(id)),'; ', DIMsh(id)
  END DO  
  IF (ALLOCATED(Cval)) DEALLOCATE(Cval)
  IF (Tndims==3) THEN
    ALLOCATE(Cval(DIMsh(1),DIMsh(2),DIMsh(3),1,1,1))
    countv=RESHAPE((/DIMSh(1),DIMsh(2),DIMsh(3),1,1,1/),(/6/))
  ELSE IF (Tndims==2) THEN
    ALLOCATE(Cval(DIMsh(1),DIMsh(2),1,1,1,1))
    countv=RESHAPE((/DIMsh(1),DIMsh(2),1,1,1,1/),(/6/))
  ELSE IF (Tndims==1) THEN
    ALLOCATE(Cval(DIMsh(1),1,1,1,1,1))
    countv=RESHAPE((/DIMsh(1),1,1,1,1,1/),(/6/))
  END IF
  rcode = nf90_get_var(ncIDnum, Tid, Cval, startv, countv)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)

  timevals=''
  DO it=1, DIMsh(2)
    DO ic=1, DIMsh(1)
      timevals(it)(ic:ic)=Cval(ic,it,1,1,1,1)
    END DO
  END DO

  IF (dbg >= 75) THEN
    PRINT *,'Time values ___ __ _'
    DO it=1, DIMsh(1)
      PRINT *,it,": '"//TRIM(timevals(it))//"'"
    END DO
  END IF

  DEALLOCATE(Cval)
  RETURN

END SUBROUTINE Ctime_values

INTEGER FUNCTION dimTfile(dbg, ncIDnum, varn)
! Function to give shape of temporal dimension of a given netCDF unit

  USE netcdf

  IMPLICIT NONE 

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum
  CHARACTER(LEN=250), INTENT(IN)                         :: varn

!Local
  INTEGER                                                :: rcode, VARi, VNDIM, id
  INTEGER, DIMENSION(6)                                  :: VDIMSi, DIMsh
  CHARACTER(LEN=50), DIMENSION(6)                        :: DIMn
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: netCDF id connection
! varn: name of the variable

  section="'dimTfile' ... .. ."

  DIMn=''
  DIMsh=0
  rcode = nf90_inq_varid(ncIDnum, name=varn, varid=VARi)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncIDnum, varid=VARi, ndims=VNDIM, dimids=VDIMSi)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
  IF (dbg >= 75) PRINT '(4x,A21,1x,I4)','Number of dimensions:',VNDIM
  DO id=1, VNDIM
    rcode = nf90_inquire_dimension(ncIDnum, VDIMSi(id), DIMn(id), DIMsh(id))
    IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode)
    IF (dbg >= 75) PRINT *,'   ',id, VDIMSi(id), TRIM(DIMn(id)),'; ', DIMsh(id)
  END DO

  IF (VNDIM > 3) THEN
    dimTfile=DIMsh(4)
  ELSE
    dimTfile=DIMsh(3)
  END IF

END FUNCTION dimTfile

SUBROUTINE get_Dtval(dbg, ncIDnum, variablename, coordinates, tdim, dt, value)
! Subroutine to get all the t-values of a variable

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, tdim, dt
  CHARACTER(LEN=250), INTENT(IN)                         :: variablename
  INTEGER, DIMENSION(6), INTENT(IN)                      :: coordinates
  REAL, DIMENSION(dt), INTENT(OUT)                       :: value

!Local
  INTEGER                                                :: rcode, VarIDnum  
  INTEGER, DIMENSION(6)                                  :: ones
  REAL, DIMENSION(1,1,dt,1,1,1)                          :: ncvalue3
  REAL, DIMENSION(1,1,1,dt,1,1)                          :: ncvalue4
  REAL, DIMENSION(1,1,1,1,dt,1)                          :: ncvalue5
  CHARACTER(LEN=50)                                      :: section

  section="'get_Dtval' ... .. ."

!!!!!! Variables
! ncIDnum: netCDF link number
! variablename: name of the variable
! coordinates: Starting point of the values
! tdim: number of the temporal dimension
! dt: number of temporal time-steps
! value: vector with the values

  ones=1
  ones(tdim)=dt

  IF (dbg >= 75) THEN
    PRINT *,'    '//TRIM(section)
    PRINT *,'coordinates: ',coordinates
  END IF

  rcode = nf90_inq_varid(ncid=ncIDnum, name=TRIM(variablename), varid=varIDnum)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
  SELECT CASE (tdim)
    CASE (3)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue3, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue3(1,1,1:dt,1,1,1)
    CASE (4)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue4, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue4(1,1,1,1:dt,1,1)
    CASE (5)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue5, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue5(1,1,1,1,1:dt,1)
    CASE DEFAULT
      PRINT *,'Variable has a temporal dimension not programmed!!'
      STOP
  END SELECT
!    PRINT *,coordinates,' value:', value

  RETURN

END SUBROUTINE get_Dtval

SUBROUTINE get_Rtval(dbg, ncIDnum, variablename, coordinates, tdim, dt, value)
! Subroutine to get all the t-values of a variable

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum, tdim, dt
  CHARACTER(LEN=250), INTENT(IN)                         :: variablename
  INTEGER, DIMENSION(6), INTENT(IN)                      :: coordinates
  REAL, DIMENSION(dt), INTENT(OUT)                       :: value

!Local
  INTEGER                                                :: rcode, VarIDnum  
  INTEGER, DIMENSION(6)                                  :: ones
  REAL, DIMENSION(1,1,dt,1,1,1)                          :: ncvalue3
  REAL, DIMENSION(1,1,1,dt,1,1)                          :: ncvalue4
  REAL, DIMENSION(1,1,1,1,dt,1)                          :: ncvalue5
  CHARACTER(LEN=50)                                      :: section

  section="'get_Rtval' ... .. ."

!!!!!! Variables
! ncIDnum: netCDF link number
! variablename: name of the variable
! coordinates: Starting point of the values
! tdim: number of the temporal dimension
! dt: number of temporal time-steps
! value: vector with the values

  ones=1
  ones(tdim)=dt

  IF (dbg >= 75) THEN
    PRINT *,'    '//TRIM(section)
    PRINT *,'coordinates: ',coordinates
  END IF

  rcode = nf90_inq_varid(ncid=ncIDnum, name=TRIM(variablename), varid=varIDnum)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
  SELECT CASE (tdim)
    CASE (3)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue3, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue3(1,1,1:dt,1,1,1)
    CASE (4)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue4, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue4(1,1,1,1:dt,1,1)
    CASE (5)
      rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue5, coordinates, ones)
      IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
      value=ncvalue5(1,1,1,1,1:dt,1)
    CASE DEFAULT
      PRINT *,'Variable has a temporal dimension not programmed!!'
      STOP
  END SELECT
!    PRINT *,coordinates,' value:', value

  RETURN

END SUBROUTINE get_Rtval

SUBROUTINE get_D1val(dbg, ncIDnum, variablename, coordinates, value)
! Subroutine to get 1 double real value of a variable

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum
  CHARACTER(LEN=250), INTENT(IN)                         :: variablename
  INTEGER, DIMENSION(6), INTENT(IN)                      :: coordinates
  REAL, INTENT(OUT)                                      :: value

!Local
  INTEGER                                                :: rcode, VarIDnum  
  INTEGER, DIMENSION(6)                                  :: ones
  INTEGER, PARAMETER                                     :: DoubleReal = SELECTED_REAL_KIND(8)
  REAL(DoubleReal), DIMENSION(1,1,1,1,1,1)               :: ncvalue
  CHARACTER(LEN=50)                                      :: section

!!!!!! Variables
! ncIDnum: netCDF link number
! variablename: name of the variable
! coordinates: Starting point of the values
! value: one point value

  section="'get_D1val' ... .. ."

  ones=1

  If (dbg >= 100) THEN
    PRINT '(4x,A16,1x,6(I4,1x))','1 point value at:',coordinates
  END IF
  rcode = nf90_inq_varid(ncIDnum, variablename, VarIDnum)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
  rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue, coordinates, ones)
  IF (rcode /= nf90_NoErr) THEN
    PRINT *,TRIM(section)//nf90_strerror(rcode) 
  ELSE
    value=ncvalue(1,1,1,1,1,1)
    RETURN
  END IF
  PRINT '(6x,A6,1x,F20.10)','value:', value

END SUBROUTINE get_D1val

SUBROUTINE get_R1val(dbg, ncIDnum, variablename, coordinates, value)
! Subroutine to get 1 value of a variable

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: dbg, ncIDnum
  CHARACTER(LEN=250), INTENT(IN)                         :: variablename
  INTEGER, DIMENSION(6), INTENT(IN)                      :: coordinates
  REAL, INTENT(OUT)                                      :: value

!Local
  INTEGER                                                :: rcode, VarIDnum  
  INTEGER, DIMENSION(6)                                  :: ones
  REAL, DIMENSION(1,1,1,1,1,1)                           :: ncvalue
  CHARACTER(LEN=50)                                      :: section

!!!!!! Variables
! ncIDnum: netCDF link number
! variablename: name of the variable
! coordinates: Starting point of the values
! value: one point value

  section="'get_R1val' ... .. ."

  ones=1

  If (dbg >= 100) THEN
    PRINT '(4x,A16,1x,6(I4,1x))','1 point value at:',coordinates
  END IF
  rcode = nf90_inq_varid(ncIDnum, variablename, VarIDnum)
  IF (rcode /= nf90_NoErr) PRINT *,TRIM(section)//nf90_strerror(rcode) 
  rcode = nf90_get_var(ncIDnum, VarIDnum, ncvalue, coordinates, ones)
  IF (rcode /= nf90_NoErr) THEN
    PRINT *,TRIM(section)//nf90_strerror(rcode) 
  ELSE
    value=ncvalue(1,1,1,1,1,1)
    RETURN
  END IF
  PRINT '(6x,A6,1x,F20.10)','value:', value

END SUBROUTINE get_R1val

SUBROUTINE nc_check_variable(ncIDnum, variablename)
! Subroutine to check the existence of a given variable in a given 'nc' netCDF connection

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'

  INTEGER, INTENT(IN)                                    :: ncIDnum
  CHARACTER(LEN=250), INTENT(IN)                         :: variablename

!Local
  INTEGER                                                :: rcode, VarIDnum
  CHARACTER(LEN=250)                                     :: errmsg
  CHARACTER(LEN=50)                                      :: section

!!!!!!! Variables
! ncIDnum: number of the netCDF connection
! variablename: name of the variable

  section="'nc_check_variable' ... .. ."

  errmsg='  ERROR -- error -- ERROR -- error'

  rcode = nf90_inq_varid(ncIDnum, variablename, VarIDnum)
  IF (rcode /= nf90_NoErr) THEN
    PRINT *,TRIM(errmsg)
    PRINT *,"    '"//TRIM(variablename)//"'"
    PRINT *,TRIM(section)//nf90_strerror(rcode) 
    PRINT *,TRIM(errmsg)
    STOP
  ELSE
    RETURN
  END IF

END SUBROUTINE nc_check_variable
