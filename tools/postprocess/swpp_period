#! /bin/bash -x
if test $1 = '-h'
then
  echo " "
  echo "### ## # J. Fernandez & others. Grupo Meteorologia Santander # ## ###"
  echo "                 Universidad de Cantabria Oct. 2010"
  echo " "
  echo "****************************"
  echo "*** Shell to postprocess ***"
  echo "***     WRF outputs      ***"
  echo "***  obtained with WRF4G ***"
  echo "****************************"
  echo "postprocess_vars_climate.sh 'FILE' (absolute path/file)"
  echo "   'FILE' with different sections: "
  echo "       'MainFrame', 'FrameWork', 'GeneralNames', 'Periods', 'Variables'"
  echo "       "
  echo "        #MainFrame#"
  echo "        BASHsource [absolute path/file] file of any desired source"
  echo "        WRF4Ghome [WRF4Gpath] path to WRF4G root framework"
  echo "        p_interp [absolute path/p_interp] executable 'p_interp'"
  echo "        DomainNetCDF [absoulte path/netCDF of domain] netCDF of Domain"
  echo "        EXPDIR [absolute path] folder of experiment"
  echo "        POSTDIR [absolute path] folder for first step of postprocess"
  echo "        POST2DIR [absolute path] folder for second step of postprocess"
  echo "        POST3HDIR [absolute path] folder for 3H yearmon variable output"
  echo "        DIAGDIR [relative path] folder for diagnostics table ?"
  echo "        FIGSDIR [absolute path] folder for figures ?"
  echo "        OBSDIR [absolute path] folder for observations ?"
  echo "        postprocessor [name] WRF4G postprocessor name to be used"
  echo "        FULLnc netCDF file with all variables (without any postprocess)"
  echo "        FirstEXPy [YYYY] first year of experiment for avoiding inexistent 'prevfile'"
  echo "        LastEXPy [YYYY] Last year of experiment for avoiding inexistent 'nexfile'"
  echo "        overlap_yearmon [YYYYMM]:[YYYYMM]:... up to which [YYYY][MM] has to be taken from [YYYY]f and complete the year with [YYYY]i. This is usefull on overlaping years coming from different threads "
  echo "        checking [1/0] whether checking has to be done"
  echo "        VarnameSecFile [n] which number of section ('_varname_') of file gives variable name" 
  echo "        ptstime 2 pair of values to draw monthly time-series for checking"
  echo "        Rhome path to R executable"
  echo "       "
  echo "        #GeneralNames#"
  echo "        @experiment@ [Name] (label to appear in output files)"
  echo "        @simulations@ [Name1]:[Name2]:... (experiment labels; ':' sep.)"
  echo "       "
  echo "        #Periods#"
  echo "        @[periodname]@ [values]"
  echo "        ..."
  echo "        #Variables#"
  echo "        [varname] [level] [temp_postprocess] [periodname]"
  echo "        ..."
  echo "             NOTE: if '#' appears previously to [varname],"
  echo "                   variable is not computed"
  echo "       "
else
function replace_substring() {
# Shell to replace a substring for another one within a string
  string=$1
  ssubstring=$2
  rsubstring=$3
  
  wheresub=`echo ${string}" "${ssubstring} | awk '{print index($1,$2)}'`
  wheresub1=`expr ${wheresub} - 1`
  Lssubstring=`expr length ${ssubstring}`
  Lrsubstring=`expr length ${rsubstring}`
  Lstring=`expr length ${string}`
  Lendssubstring=`expr ${wheresub} + ${Lssubstring}`
  Lendnewstring=`expr $Lstring - ${Lendssubstring} + 1`
  begnewstring=`expr substr ${string} 1 ${wheresub1}`
  endnewstring=`expr substr ${string} ${Lendssubstring} ${Lendnewstring}`
  newstring=${begnewstring}${rsubstring}${endnewstring}
  
  echo ${newstring}
}

function checking_file() {
# Checking of single variable postprocessed file
  c1='$'
  chkfile=$1
  chkfullfile=$2
  points=$3
  varfilesec=$4
  Rexec=$5

  Npts=`echo $points | tr ':' ' ' | wc -w | awk '{print $1/2}'`
  N2pts=`expr ${Npts} '*' 2`
  Rpts=''
  ipt=1
  while test ${ipt} -le ${N2pts}
  do
    if test ${ipt} -eq 1
    then
      Rpts=${Rpts}$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    else    
      Rpts=${Rpts}', '$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    fi
    ipt=`expr ${ipt} + 1`
  done # end Npts 
  Rpts=${Rpts}
  
  chkfold=`dirname ${chkfile}`  
  mkdir -p ${chkfold}/checking
  ncfvarname=`cdo showname ${chkfile} | awk '{print $1}'` 
  fvarname=`basename ${chkfile}`
  fvarname=`echo ${fvarname}" _ "${varfilesec} | awk '{split($1,words,$2); print words[$3]}'` 
  fvarname=`echo $fvarname" . 1" | awk '{split($1,words,$2); print words[$3]}'` 
  cdo yseasmean ${chkfile} ${chkfold}/checking/${fvarname}_yseasmean.nc
  cdo monmean ${chkfile} ${chkfold}/checking/${fvarname}_monmean.nc
  cdo monstd ${chkfile} ${chkfold}/checking/${fvarname}_monstd.nc
  rm ${chkfold}/checking/${fvarname}_*.eps
  cat << EOF > ${chkfold}/checking/checking_postprocess.R
library(ncdf)
ncunit_variable_info<-function(ncunit, variable)
### R function to obtain nc variable information from a ncunit
##
{
nvarnc<-length(ncunit${c1}var)
varncnames<-matrix(0,nvarnc)
dim(varncnames)<-nvarnc

for (ivar in 1:nvarnc) {
varncnames[ivar]<-ncunit${c1}var[[ivar]]${c1}name}

pos_ncvar<-which(varncnames==variable)
ncvar<-ncunit${c1}var[[pos_ncvar]]
ncvarname<-ncvar${c1}name
ncvarid<-ncvar${c1}id
ncvardims<-ncvar${c1}ndims
ncvarunits<-ncvar${c1}units
ncvarsize<-ncvar${c1}varsize
nc_variable_info<-c(variable, ncvarname, ncvarid, ncvardims, ncvarunits, ncvarsize)

nc_variable_info
}
#
# Main
#
geofile<-'${chkfullfile}'
varfile<-'${chkfold}/checking/${fvarname}_yseasmean.nc'
landmask<-'LANDMASK'
longname<-'XLONG'
latname<-'XLAT'
varname<-'${ncfvarname}'
filevarname<-'${fvarname}'

nclm<-open.ncdf(geofile)
linecoast<-get.var.ncdf(nclm, landmask, start=c(1,1,1), count=c(-1,-1,1))
longs<-get.var.ncdf(nclm, longname, start=c(1,1,1), count=c(-1,-1,1))
latits<-get.var.ncdf(nclm, latname, start=c(1,1,1), count=c(-1,-1,1))
close.ncdf(nclm)

ncvar<-open.ncdf(varfile)
shapevar<-ncunit_variable_info(ncvar, varname)[4]

if (shapevar == 3) {
  variable<-get.var.ncdf(ncvar, varname, start=c(1,1,1), count=c(-1,-1,-1))
}

if (shapevar == 4) {
  variable<-get.var.ncdf(ncvar, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
}
close.ncdf(ncvar)

dimx<-nrow(linecoast)
dimy<-ncol(linecoast)
variable<-ifelse(variable<=-9.e+32, NA, variable)

colorsRGB<-colorRamp(c(rgb(0,0,1,1),rgb(0,1,0,1),rgb(1,0,0,1)))
colbar<-rgb(colorsRGB(seq(0,1,length=20)),max=255)

seas<-c("DJF", "MAM", "JJA", "SON")
timestep<-1
for ( timestep in 1:4) {
  graphname<-paste("${chkfold}","/checking/",filevarname,"_yseasmean_",seas[timestep],".png", sep="")
  graphtit<-paste(filevarname,"${ncstat}",seas[timestep], sep=" ")
  minval<-min(variable[,,timestep], na.rm=TRUE)
  maxval<-max(variable[,,timestep], na.rm=TRUE)

  bitmap(graphname)
##  postscript(graphname, bg="white", horizontal=FALSE, paper="default")

  par(xpd=NA)
  contour(linecoast, col=gray(0.75), nlevels=1, drawlabels=FALSE, asp=dimy/dimx,
    axes=FALSE)
  plot.window(xlim=c(0,1),ylim=c(0,1), asp=dimy/dimx, log="",
    title(main=graphtit, xlab="W-E",ylab="S-N"))
  image(variable[,,timestep],col=rgb(colorsRGB(seq(0,1,length=20)),max=255),
    add=TRUE)
  contour(longs,col=gray(0.75),levels=seq(-60,60,by=5),add=TRUE)
  contour(latits,col=gray(0.75),levels=seq(10,90,by=5),add=TRUE)
  contour(linecoast, col=gray(0.25), nlevels=1, drawlabels=FALSE, add=TRUE)
  stepsval<-(maxval-minval)/4
  steps21val<-(maxval-minval)/20
  Lcolvar=length(colbar)
  seq9colbar<-seq(1, Lcolvar, (Lcolvar-1)/8)
  seq21colbar<-seq(1, Lcolvar, (Lcolvar-1)/20)
  symbols(seq(0.1,0.9,0.8/20), matrix(-0.04,21), rectangles=cbind(matrix(0.8/20,21), 
    matrix(0.05,21)), bg=colbar[seq21colbar], fg="gray", inches=FALSE, add=TRUE) 
  text(seq(0.1,0.9,0.8/20)[seq(1,21,20/4)], matrix(-0.1,4), labels=sprintf("%4.2f",
    seq(minval, maxval, stepsval)))

  dev.off()

} 
EOF
  ${Rexec} -f --slave ${chkfold}/checking/checking_postprocess.R

cat << EOF > ${chkfold}/checking/checking_1D_postprocess.R
#Checking postprocess of variable ${fvarname}
library(ncdf)
lonlat_point<-function(varfile, lonname, latname, landmaskname='LANDMASK', points) {
# R Function to provide nearest grid point to a given longitude, latitude
  Npoints<-nrow(points)
  posmin<-rep(0,Npoints*2)
  dim(posmin)<-c(Npoints,2)

  ncvar<-open.ncdf(varfile)
  varinf<-ncunit_variable_info(ncvar, lonname)
  shapevar<-varinf[4]

  if (shapevar == 2) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1), count=c(-1,-1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1), count=c(-1,-1))
  }
  if (shapevar == 3) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1), count=c(-1,-1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1), count=c(-1,-1,1))
  }
  if (shapevar == 4) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1,1), count=c(-1,-1,1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1,1), count=c(-1,-1,1,1))
  }
  close.ncdf(ncvar)

  dimx<-nrow(lonmat)
  dimy<-ncol(lonmat)

  for ( i in 1:Npoints) {
    dist<-sqrt((lonmat-points[i,1])**2.+(latmat-points[i,2])**2.)
    pmindist<-which.min(dist)
    yposmin<-as.integer(pmindist/dimx)+1
    xposmin<-as.integer(pmindist-(yposmin-1)*dimx)
    posmin[i,]<-c(xposmin, yposmin)
  }
  posmin
}

ncunit_dim_info<-function(ncunit, dimension)
### R function to obtain nc variable information from a ncunit
##
{
  ndimnc<-length(ncunit${c1}dim)
  dimncnames<-matrix(0,ndimnc)
  dim(dimncnames)<-ndimnc

  for (idim in 1:ndimnc) {
    dimncnames[idim]<-ncunit${c1}dim[[idim]]${c1}name}

  pos_ncdim<-which(dimncnames==dimension)
  ncdim<-ncunit${c1}dim[[pos_ncdim]]
  ncdimname<-ncdim${c1}name
  ncdimid<-ncdim${c1}id
  ncdimsize<-ncdim${c1}len
  ncdimunits<-ncdim${c1}units
  nc_dimension_info<-c(dimension, ncdimname, ncdimid, ncdimsize, ncdimunits)

  nc_dimension_info
}
ncunit_variable_info<-function(ncunit, variable)

### R function to obtain nc variable information from a ncunit
##
{
nvarnc<-length(ncunit${c1}var)
varncnames<-matrix(0,nvarnc)
dim(varncnames)<-nvarnc

for (ivar in 1:nvarnc) {
varncnames[ivar]<-ncunit${c1}var[[ivar]]${c1}name}

pos_ncvar<-which(varncnames==variable)
ncvar<-ncunit${c1}var[[pos_ncvar]]
ncvarname<-ncvar${c1}name
ncvarid<-ncvar${c1}id
ncvardims<-ncvar${c1}ndims
ncvarunits<-ncvar${c1}units
ncvarsize<-ncvar${c1}varsize
nc_variable_info<-c(variable, ncvarname, ncvarid, ncvardims, ncvarunits, ncvarsize)

nc_variable_info
}
#
# Main
#
varmeanfile<-'${chkfold}/checking/${fvarname}_monmean.nc'
varstdfile<-'${chkfold}/checking/${fvarname}_monstd.nc'
varname<-'${ncfvarname}'
filevarname<-'${fvarname}'
timename<-'time'
landmask<-'LANDMASK'
geofile<-'${chkfullfile}'
points<-c(${Rpts})
lonlatpt<-rep(0, 2*${Npts})
dim(lonlatpt)<-c(${Npts},2)
lonlatpt[1:${Npts},1]<-points[seq(1,${N2pts},2)]
lonlatpt[1:${Npts},2]<-points[seq(2,${N2pts},2)]

ncvarmean<-open.ncdf(varmeanfile)
ncvarstd<-open.ncdf(varstdfile)

varinf<-ncunit_variable_info(ncvarmean, varname)
shapevar<-varinf[4]
ncunits<-varinf[5]

if (shapevar == 3) {
  meanvariable<-get.var.ncdf(ncvarmean, varname, start=c(1,1,1), count=c(-1,-1,-1))
  stdvariable<-get.var.ncdf(ncvarstd, varname, start=c(1,1,1), count=c(-1,-1,-1))
}
if (shapevar == 4) {
  meanvariable<-get.var.ncdf(ncvarmean, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
  stdvariable<-get.var.ncdf(ncvarstd, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
}

timeinf<-ncunit_dim_info(ncvarmean, timename)
timeunits<-timeinf[5]
timevar<-get.var.ncdf(ncvarmean, timename, start=c(1), count=c(-1))
close.ncdf(ncvarmean)
close.ncdf(ncvarstd)

pointPos<-lonlat_point(geofile, 'XLONG', 'XLAT', 'LANDMASK', lonlatpt)

meanvariable<-ifelse(meanvariable<=-9.e+32, NA, meanvariable)
stdvariable<-ifelse(stdvariable<=-9.e+32, NA, stdvariable)
meanval1<-meanvariable[pointPos[1,1],pointPos[1,2],]
meanval2<-meanvariable[pointPos[2,1],pointPos[2,2],]
stdval1<-stdvariable[pointPos[1,1],pointPos[1,2],]
stdval2<-stdvariable[pointPos[2,1],pointPos[2,2],]

mingraph1<-min(meanval1-stdval1, na.rm=TRUE)
maxgraph1<-max(meanval1+stdval1, na.rm=TRUE)
mingraph2<-min(meanval2-stdval2, na.rm=TRUE)
maxgraph2<-max(meanval2+stdval2, na.rm=TRUE)
mingraph<-min(mingraph1, mingraph2, maxgraph1, maxgraph2)
maxgraph<-max(mingraph1, mingraph2, maxgraph1, maxgraph2)

graphname<-paste("${chkfold}","/checking/",filevarname,"_dimx2-dimy2_timeserie.png", sep="")
graphtit<-paste(filevarname,"at",lonlatpt[1,1],"E ,",lonlatpt[1,2],"N &",lonlatpt[2,1],"E ,",
  lonlatpt[2,2],"N", sep=" ")
xlabel<-paste(filevarname,"(",ncunits,")", sep=" ")

bitmap(graphname)
##postscript(graphname, bg="white", horizontal=FALSE, paper="default")
plot (timevar, meanval1, "l", col="red", ylim=c(mingraph, maxgraph), main=graphtit, xaxs="i", 
  yaxs="i", xlab=timeunits, ylab=xlabel, lwd="2")
polygon(c(timevar, rev(timevar)), c(meanval1-stdval1, rev(meanval1+stdval1)), col=rgb(1., 0.5,
  0.5, 1.), border=NA)
polygon(c(timevar, rev(timevar)), c(meanval2-stdval2, rev(meanval2+stdval2)), col=rgb(0.5, 0.5,
  1., 1.), border=NA)
lines (timevar, meanval1, col="red", lwd="2")
lines (timevar, meanval2, col="blue", lwd="2")
points (timevar, meanval1, col="red", lwd="2")
points(timevar, meanval2, col="blue", lwd="2")

dev.off()
EOF

  ${Rexec} -f --slave ${chkfold}/checking/checking_1D_postprocess.R
}

function sub_list() {
# Function that creates a new ':' list from a section (from sval to eval) of the values of
#    the components of the list 
   list=$1
   sval=$2
   eval=$3

   Nvalues=`echo ${list} | tr ':' ' ' | wc -w | awk '{print $1}'`
   iv=1

   while test ${iv} -le ${Nvalues}
   do
     val=`echo ${list}" "${iv}" "$sval" "$eval | awk '{split($1,words,":"); \
       print substr(words[$2],$3,$4-$3+1)}'`
     if test ${iv} -eq 1
     then
       newlist=${val}
     else
       newlist=${newlist}':'${val}
     fi
     iv=`expr ${iv} + 1`
   done # end Nvalues

   echo ${newlist}
}

function isin_list() {
# Function to check whether a value is in a list
  list=$1
  value=$2
  
  is=`echo ${list} | tr ':' '\n' | awk '{print "@"$1"@"}' | grep '@'${value}'@' | wc -w`
  if test ${is} -eq 1
  then
    true
  else
    false
  fi
}

function leave_firstN() {
# Function to leave first N characters
  string=$1
  N=$2

  N1=`expr $N + 1`
  Lstring=`expr length $string`

  Estring=`expr $Lstring - $N`
  newstring=`expr substr ${string} $N1 ${Estring}`
  echo $newstring
}

function leave_lastN() {
# Function to leave last N characters
  string=$1
  N=$2
  Lstring=`expr length $string`
  
  Estring=`expr $Lstring - $N`
  newstring=`expr substr ${string} 1 ${Estring}`
  echo $newstring
}

function rewrite_attributes() {
# Function to re-write netCDF attributes given wrfncxnj table file
  ncfile=$1
  wrfncxnjtable=$2
  varname=$3
  newvarname=$4
  c1='#'
  c2=\"

  nlinevar=`cat -n ${wrfncxnjtable} | awk '{print " "$3" "$1}' | grep ' '${newvarname}' ' | awk '{print $2}' | head -n 1`
  varline=`head -n ${nlinevar} ${wrfncxnjtable} | tail -n 1`
  varline_tmp1=`echo ${varline} | tr ' ' ':' | sed s/${c2}/${c1}/g`
  ilongname=`expr index ${varline_tmp1} ${c1}`
  varline_tmp2=`leave_firstN $varline_tmp1 $ilongname`
  elongname=`expr index ${varline_tmp2} ${c1}`
  elname1=`expr ${elongname} - 1`
  longname=`expr substr ${varline_tmp2} 1 ${elname1}`
  elname1=`expr ${elongname} + 1`
  varline_tmp3=`leave_firstN $varline_tmp2 $elname1`
  istdname=`expr index ${varline_tmp3} ${c1}`
  varline_tmp4=`leave_firstN $varline_tmp3 $istdname`
  estdname=`expr index ${varline_tmp4} ${c1}`
  estdname1=`expr ${estdname} - 1`
  stdname=`expr substr ${varline_tmp4} 1 ${estdname1}`
  estdname1=`expr ${estdname} + 1`
  varline_tmp5=`leave_firstN $varline_tmp4 $estdname1`
  units=`echo ${varline_tmp5} | tr '#' ' ' | awk '{print $1}'`
  ncatted -a long_name,${varname},o,c,"$(echo ${longname} | tr ':' ' ')" ${ncfile}
  ncatted -a standard_name,${varname},o,c,"$(echo ${stdname} | tr ':' ' ')" ${ncfile}
  ncatted -a units,${varname},o,c,"$(echo ${units} | tr ':' ' ')" ${ncfile}

  cdo setname,${newvarname} ${ncfile} tmpA.nc
  mv tmpA.nc ${ncfile}
}

function last_word() {
# Function that gives the last word of a line selected by a key-word (from Ncol) from a 
#    given file
  wfile=$1
  keyword=$2
  Ncol=$3

  case $Ncol in
    1)
      nlinevar=`cat -n ${wfile} | awk '{print " "$2" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
    2)
      nlinevar=`cat -n ${wfile} | awk '{print " "$3" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
    3)
      nlinevar=`cat -n ${wfile} | awk '{print " "$4" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
  esac
  fline=`head -n ${nlinevar} ${wfile} | tail -n 1`
  nword=`echo ${fline} | wc -w | awk '{print $1}'`
  fline=`echo ${fline} | tr ' ' ':'`
  word=`echo $fline" "$nword | awk '{split($1,words,":"); print words[$2]}'`
  echo $word
}

function next_yearmon() {
# Function to obtain next month from a [yyyy][mm] structure
  ymon=$1
  year=`expr substr ${ymon} 1 4`
  month=`expr substr ${ymon} 5 2`

  nextmon=`expr $month + 1`
  nextmonS=$nextmon
  if test $nextmon -lt 10
  then
    nextmonS='0'${nextmon}
  fi

  if test ${nextmon} -gt 12
  then
    nextmonS='01'
    nextyear=`expr ${year} + 1`
    echo ${nextyear}${nextmonS}" year_changed"
  else
    echo ${year}${nextmonS}
  fi
}

function prev_yearmon() {
# Function to obtain previous month from a [yyyy][mm] structure
  ymon=$1
  year=`expr substr ${ymon} 1 4`
  month=`expr substr ${ymon} 5 2`

  prevmon=`expr $month - 1`
  prevmonS=$prevmon
  if test $prevmon -lt 10
  then
    prevmonS='0'${prevmon}
  fi

  if test ${prevmon} -lt 1
  then
    prevmonS='12'
    prevyear=`expr ${year} - 1`
    echo ${prevyear}${prevmonS}" year_changed"
  else
    echo ${year}${prevmonS}
  fi
}

function check_accumulate() {
# Function to check whether a variable should be deaccumualted
  varname=$1
  wrfncxnjtable=$2

  linvalue=`cat -n ${wrfncxnjtable} | awk '{print " "$2" "$1}' | grep ' '${varname}' ' | awk '{print $2}'`
  deaccum=`head -n ${linvalue} ${wrfncxnjtable} | tail -n 1 | grep deaccumulate | wc -m`
  if test ${deaccum} -gt 0
  then
    true
  else
    false
  fi
}

function export_MainFrame() {
# Function to export all variables of 'MainFrame' 
  mfile=$1
  export POSTFILE=${mfile}

  Nframe=`cat -n ${mfile} | grep '#MainFrame#' | awk '{print $1}'`
  Nnames=`cat -n ${mfile} | grep '#GeneralNames#' | awk '{print $1}'`

  iframe=`expr $Nframe + 1`
  lastframe=`expr $Nnames - 1`

  while test $iframe -lt $lastframe
  do
    varname=`head -n ${iframe} ${mfile} | tail -n 1 | awk '{print $1}'`
    varvalue=`head -n ${iframe} ${mfile} | tail -n 1 | awk '{print $2}'`
    export ${varname}=${varvalue}

    iframe=`expr ${iframe} + 1`
### End of variables of MainFrame
  done
}

##
export_MainFrame $1
##
overlap_years=`sub_list ${overlap_yearmon} 1 4`

function get_yearmons(){
  idir=$1
  okind=$2
  find ${idir}/output/*/ -name wrf${okind}_d01_\*.nc \
    | sed -e 's/^.*wrf'${okind}'_d01_\(.*\)..T......Z.nc$/\1/' \
    | sort -n | uniq
}

function get_filelist_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    if test ${yearmon} -le $(echo ${overlap_yearmon} | tr ':' '\n' | grep ${YMy})
    then
      overlap='f'
    else
      overlap='i'
    fi
  fi
  find ${exppath}/output/*${overlap}/ -name wrf${filekind}_d01_${ymon}\*.nc | sort
}

function get_filefirst_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    overlap='i'
  fi
  find ${exppath}/output/*${overlap}/ -name wrf${filekind}_d01_${ymon}\*.nc | sort | head -n 1
}

function get_filelast_yearmon() {
  exppath=$1
  ymon=$2
  filekind=$3
  YMy=`expr substr ${ymon} 1 4`
  overlap=''
  if $(isin_list ${overlap_years} ${YMy})
  then
    overlap='f'
  fi
  find ${exppath}/output/*${overlap}/ -name wrf${filekind}_d01_${ymon}\*.nc | sort | tail -n 1
}

function simulation_path(){
  sname=$1
   simpath=$EXPDIR'/'$(cat ${POSTFILE} | grep ${sname} | awk '{print $3}')
   echo ${simpath}
}

function filelist_is_short(){
  flfile=$1
  ymon=$2
  s1=$(date --utc '+%s' -d "${ymon:0:4}-${ymon:4:2}-01 1 month")
  s2=$(date --utc '+%s' -d "${ymon:0:4}-${ymon:4:2}-01")
  daysinmonth=$(python -c "print ($s1-$s2)/60/60/24")
  test "${daysinmonth}" -ne $(cat ${flfile} | wc -l)
}

function get_3h_data(){
  expname=$1
  exppath=$(simulation_path ${expname})
  variable=$2
  outkind=$3
  syearmon=$4
  eyearmon=$5

  makelevs=''
  if test ${outkind} = 'out'
  then
    makelevs='-p -s'
  fi

  for yearmon in $(get_yearmons ${exppath} ${outkind}); do
    echo "Are we?"
    if test ${yearmon} -ge ${syearmon} && test ${yearmon} -le ${eyearmon}
    then
      echo "Let's go!"
      outfile="${POSTDIR}/${expname}_${yearmon}.nc"
      if test -f ${outfile}; then
        echo "I won't overwrite ${outfile}."
        continue
      fi
      prevyearmon=`prev_yearmon ${yearmon}`
      nextyearmon=`next_yearmon ${yearmon}`
#
# Checking overlapping period
#
      YMy=`expr substr ${yearmon} 1 4`
      if $(isin_list ${overlap_years} ${YMy})
      then
        echo "Desired year: "${YMy}" is an overlapping year!"
      fi 
      
      rm -f filelist.txt
      if test ! ${outkind} = 'out'
      then
        get_filelast_yearmon ${exppath} $(echo ${prevyearmon} | awk '{print $1}') ${outkind} > filelist.txt  
      fi
      get_filelist_yearmon ${exppath} ${yearmon} ${outkind} \
        | while read f; do
        if ! ncdump -h ${f} | grep -q num_metgrid_levels && test ${outkind} = 'out' ; then
          echo "$f was not filtered at running time"
          echo "Filtering now!"
          indir=$(dirname $f)
          infile=$(basename $f)
          if test -d ${indir}/not_filtered
          then
            rm ${indir}/not_filtered/*
          else
            mkdir ${indir}/not_filtered/  
          fi
          cp $f ${indir}/not_filtered
          ${WRF4Ghome}/ui/scripts/wrf4g_run_postprocessor.sh ${WRF4Ghome} ${postprocessor} \
            ${indir}/not_filtered
          mv ${indir}/not_filtered/${infile} ${indir}
          echo ${f} >> filelist.txt
        else
          echo ${f} >> filelist.txt
        fi
      done
      if test ! ${outkind} = 'out'
      then
        get_filefirst_yearmon ${exppath} $(echo ${nextyearmon} | awk '{print $1}') ${outkind} >> filelist.txt
      fi
      if filelist_is_short filelist.txt ${yearmon} && test ${outkind} = 'out' ; then
        echo "Too few files for month ${yearmon:4:2}: $(wc -l filelist.txt)"
        continue
      fi
      if $(check_accumulate ${variable} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table)
      then
        noprevyear=0
        nwords=`echo $prevyearmon | wc -w`
        if test ${nwords} -gt 1
        then
          yearprev=`echo ${prevyearmon} | awk '{print substr($1,1,4)}'`
          if test ${yearprev} -ge ${FirstEXPy}
          then
            indir=`dirname $(tail -n 1 filelist.txt)`
            prevfile=${indir}/../${yearprev}/wrfout_d01_${yearprev}1231T000000Z.nc
          else
            noprevyear=1
          fi
        else
          prevfile=`tail -n 1 filelist_${prevyearmon}.txt` 
          if $(isin_list ${overlap_yearmon} ${prevyearmon})
          then
            prevyear=`expr substr 1 4 ${prevyearmon}`
            prevfile=`replace_substring ${prevfile} ${prevyear}'f' ${prevyear}'i'`
          fi
        fi
        
        if test ${noprevyear} -eq 0
        then
          ${wxajcmd} \
            --from-file filelist.txt \
            -o ${outfile} \
            -v ${variable} \
            ${makelevs} \
            --previous-file ${prevfile}
            >& $(basename ${outfile}.log)
        else
          ${wxajcmd} \
            --from-file filelist.txt \
            -o ${outfile} \
            -v ${variable} \
            ${makelevs} \
            >& $(basename ${outfile}.log)
        fi
      else
        ${wxajcmd} \
          --from-file filelist.txt \
          -o ${outfile} \
          -v ${variable} \
          ${makelevs} \
          >& $(basename ${outfile}.log)
      fi
      if test ! ${outkind} = 'out'
      then
        idate=`date -u +%Y-%m-%d"T00:00:00" -d"${yearmon}02"`
        fdate=`date -u +%Y-%m-%d"T00:00:00" -d"${yearmon}01 1 month"`
        cdo seldate,${idate},${fdate} ${outfile} tmpA.nc
        mv tmpA.nc ${outfile}
      fi
      
      mv filelist.txt filelist_${yearmon}.txt
   fi # end between syearmon eyearmon
  done
}

function procesa(){
  expname=$1
  var=$2
  thelevels="$3"
  thetimes="$4"   # e.g: 12H_00:00,12:00
  theperiods="$5" # e.g: 1989-1993,1994-1997
  do_nothing=$6 # .true. it is a 'do_nothing' variable flag
  thisdir=`pwd`
  mkdir -p ${POST3HDIR}/procesa_files
  cd ${POST3HDIR}/procesa_files
  read freqname thetimes <<< ${thetimes/_/ }
  test "${thelevels}" != "SFC" && splitlevel="splitlevel" || splitlevel=""
  test "${thetimes}" != "All" && seltime="-seltime,${thetimes}" || seltime=""
  case ${freqname} in
    DAM) ovar=${var}    ; freqname=DAM ; seltime="-settime,${thetimes} -daymean" ;;
    DAS) ovar=${var}    ; freqname=DAM ; seltime="-settime,${thetimes} -daysum" ;;
    DAX) ovar=${var}    ; freqname=DAM ; seltime="-settime,${thetimes} -daymax" ; 
      if ${do_nothing}; then
        ovar=${var}max; fi  ;;
    DAN) ovar=${var}    ; freqname=DAM ; seltime="-settime,${thetimes} -daymin" ;
      if ${do_nothing}; then
        ovar=${var}min; fi  ;;
    MOM) ovar=${var}    ; freqname=DAM ; seltime="-settime,${thetimes} -monmean" ;;
    MOS) ovar=${var}    ; freqname=MOM ; seltime="-settime,${thetimes} -monsum" ;;
    MOX) ovar=${var}    ; freqname=MOM ; seltime="-settime,${thetimes} -monmax" ;
      if ${do_nothing}; then
        ovar=${var}max; fi  ;;
    MON) ovar=${var}    ; freqname=MOM ; seltime="-settime,${thetimes} -monmin" ;
      if ${do_nothing}; then
        ovar=${var}min; fi  ;;
    SEM) ovar=${var}    ; freqname=SEM ; seltime="-settime,${thetimes} -seasmean" ;;
    SES) ovar=${var}    ; freqname=SEM ; seltime="-settime,${thetimes} -seassum" ;;
    SEX) ovar=${var}    ; freqname=SEM ; seltime="-settime,${thetimes} -seasmax" ;
      if ${do_nothing}; then
        ovar=${var}max; fi  ;;
    SEN) ovar=${var}    ; freqname=SEM ; seltime="-settime,${thetimes} -seasmin" ;
      if ${do_nothing}; then
        ovar=${var}min; fi  ;;
    YEM) ovar=${var}    ; freqname=YEM ; seltime="-settime,${thetimes} -yearmean" ;;
    YES) ovar=${var}    ; freqname=YEM ; seltime="-settime,${thetimes} -yearsum" ;;
    YEX) ovar=${var}    ; freqname=YEM ; seltime="-settime,${thetimes} -yearmax" ;
      if ${do_nothing}; then
        ovar=${var}max; fi  ;;
    YEN) ovar=${var}    ; freqname=YEM ; seltime="-settime,${thetimes} -yearmin" ;
      if ${do_nothing}; then
        ovar=${var}min; fi  ;;

    DM)  ovar=$var      ; freqname=DM  ; seltime="-settime,${thetimes} -daymean" ;;
    DX)  ovar=${var}max ; freqname=DM  ; seltime="-settime,${thetimes} -chname,${var},${ovar} -daymax" ;;
    DN)  ovar=${var}min ; freqname=DM  ; seltime="-settime,${thetimes} -chname,${var},${ovar} -daymin" ;;
    DA)  ovar=$var      ; freqname=DM  ; seltime="-settime,${thetimes} -daysum" ;;
     *)  ovar=$var
       ;;
  esac
  echo "Processing $ovar"
  for per in ${theperiods//,/ }; do
    rm -f procesa_??????_*
    echo -n "  Period $per:"
    for yr in $(seq ${per/-/ }); do
      echo -n " $yr"
      for mon in $(seq 1 12); do
        pmon=$(printf "%02d" ${mon})
##        cdo -s ${splitlevel} ${seltime} \
##          -selvar,${var} ${POSTDIR}/${expname}_${yr}${pmon}.nc \
##          procesa_${yr}${pmon}_${var}
        if test "${thelevels}" = "SFC" 
        then
          cp ${POST3HDIR}/${expname}_${yr}${pmon}_03H_${var}.nc.gz ./
          gunzip ${expname}_${yr}${pmon}_03H_${var}.nc.gz
          mv ${expname}_${yr}${pmon}_03H_${var}.nc \
            procesa_${yr}${pmon}_${var}
##          cdo -s ${splitlevel} -selvar,${var} ${POSTDIR}/${expname}_${yr}${pmon}.nc \
##            ${POST3HDIR}/${expname}_${yr}${pmon}_03H_${var}.nc
        else
##          cdo -s ${splitlevel} -selvar,${var} ${POSTDIR}/${expname}_${yr}${pmon}.nc \
##            ${POST3HDIR}/${expname}_${yr}${pmon}_03H_${var}
          for lev in ${thelevels//,/ }; do
            level=`printf "%04d" ${lev}`
            cp ${POST3HDIR}/${expname}_${yr}${pmon}_03H_${var}${level}.nc.gz ./
            gunzip ${expname}_${yr}${pmon}_03H_${var}${level}.nc.gz
            mv ${expname}_${yr}${pmon}_03H_${var}${level}.nc \
              procesa_${yr}${pmon}_${var}${level}.nc
          done # end levels
        fi
      done # mon
    done # yr
    echo
    if test "${thelevels}" = "SFC"; then
      echo "  Processing only the surface value (or all levels together...)"
      ofile="${POST2DIR}/${expname}_${freqname}_${per}_${ovar}.nc"
      rm -f ${ofile}.gz ${ofile} >& /dev/null
      cdo -s cat procesa_??????_${var} ${ofile}
      if ${do_nothing}
      then
        rewrite_attributes ${ofile} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${var} ${ovar} 
      fi
      if test ${checking} -eq 1
      then
        checking_file ${ofile} ${FULLnc} ${ptstime} ${VarnameSecFile} ${Rhome}
      fi
      gzip ${ofile}
    else
      echo -n "  Levels:"
      for level in ${thelevels//,/ }; do
        echo -n " $level"
        plevel=`printf "%04d" ${level}`
        ofile="${POST2DIR}/${expname}_${freqname}_${per}_${ovar}${level}.nc"
        rm -f ${ofile}.gz ${ofile} >& /dev/null
        cdo -s cat procesa_??????_${var}${plevel}.nc ${ofile}
        if ${do_nothing}
        then
          rewrite_attributes ${ofile} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${var} ${ovar}   
        fi
        if test ${checking} -eq 1
        then
          checking_file ${ofile} ${FULLnc} ${ptstime} ${VarnameSecFile} ${Rhome} 
        fi
        gzip ${ofile}
      done # level
    fi
    rm -f procesa_??????_*
  done # per
  cd ${thisdir}
}

function read_postprocess_variables() {
  # Function to postprocess variables from a given file (1st arg) with different sections: 
  # 'MainFrame', 'FrameWork', 'GeneralNames', 'Periods' and 'Variables'
  #
  # #MainFrame#
  # (...)
  #
  # #GeneralNames#
  # @experiment@ [Name] (label to appear in output files)
  #
  # #Periods#
  # @[periodname]@=[values]
  # ...
  #
  # #Variables#
  # [varname] [level] [temp_postprocess] [periodname]
  # ...
  # NOTE: if previously to [varname] is there a '#', variable is not computed
  ##
  # 
  file=$1

  Nvariables=`cat -n ${file} | grep '#Variables#' | awk '{print $1}'`
  experimentName=`cat ${file} | grep '@experiment@' | awk '{print $2}'`
  Nlines=`wc -l ${file} | awk '{print $1}'`

  ivar=`expr $Nvariables + 1`
  while test ${ivar} -le ${Nlines}
  do
    varname=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $1}'`
    if test ! ${varname} = '#'
    then
      do_nothing=false
      lastword=`last_word ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${varname} 1`
      if test ${lastword} = 'do_nothing'
      then
        is_do_nothing=true
        varname=`leave_lastN ${varname} 4`
      fi
      nlinevar=`cat -n ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | awk '{print " "$2" "$1}' | grep ' '${varname}' ' | awk '{print $2}'`
      CFvarname=`head -n ${nlinevar} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | tail -n 1 | awk '{print $2}'`

      outkind=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $2}'`
      varlev=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $3}'`
      vartime=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $4}'`
      varperiod=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $5}'`
      periodval=`cat ${file} | grep '@'${varperiod}'@' | awk '{print $2}'` 
      Lperiodval=`expr length ${periodval}`
      Lperiodval=`expr ${Lperiodval} + 0`
      if test ${Lperiodval} -lt 1
      then
        echo "ERROR -- error -- ERROR -- error: Period '"${varperiod}"' does not exists!"
        exit
      fi
      speriodval=`echo ${periodval} | tr '-' ' ' | awk '{print $1}'`
      eperiodval=`echo ${periodval} | tr '-' ' ' | awk '{print $2}'`
     
      speriodval=${speriodval}'01'
      eperiodval=${eperiodval}'12'
##      get_3h_data ${experimentName} ${varname} ${outkind} ${speriodval} ${eperiodval}
      procesa ${experimentName} ${CFvarname} ${varlev} ${vartime} ${periodval} ${is_do_nothing}
## removing intermediate data file (from get_3h_data)
      rm ${POSTDIR}/${experimentName}_*.nc
# renaming intermediate data file (from get_3h_data)
      varplev=$(printf "%04d" ${varlev})
#      if test "${varlev}" = "SFC"
#      then
#        rm ${POST3HDIR}/*_${CFvarname}.nc.gz 
#      else
#        rm ${POST3HDIR}/*_${CFvarname}${varplev}.nc.gz
#      fi

#      for ncfile in $(ls -1 ${POST3HDIR}/${experimentName}_*.nc)
#      do
#        if test "${varlev}" = "SFC"
#        then
#          gzip ${ncfile} 
#        else
#          nncfile=`basename ${ncfile}`
#          dirncfile=`dirname ${ncfile}`
#          nncfile=`leave_lastN ${nncfile} 3`
#          nlevncfile=`leave_lastN ${nncfile} 6`
#          Lnncfile=`expr length ${nncfile}`
#          Nlevfile=`expr $Lnncfile - 5`
#          levfile=`expr substr ${nncfile} ${Nlevfile} 6`
#          if test ${levfile} -eq ${varlev}'00'
#          then
#            mv ${ncfile} ${dirncfile}/${nlevncfile}${varplev}.nc 
#            gzip ${dirncfile}/${nlevncfile}${varplev}.nc
#          else
#            rm ${ncfile}
#          fi
#        fi
#      done # end ncfiles
      rm ${POST3HDIR}/${experimentName}_*.nc >& /dev/null

    fi
    ivar=`expr ${ivar} + 1`
  done # varname
}

#################################################
# Main program
################################################

#tag3h=minimal
variablesfile=$1

scriptdir=$( (cd `dirname $0` && echo $PWD) )
thisdir=$(pwd)

source $BASHsource
#
# update WRF4G
#
cd ${WRF4Ghome}
svn up
cd ${thisdir}
#
#  Get a private space to run
#
tmpdir=${thisdir}/tmpdir.`date +%Y%m%d%H%M%S%n`
mkdir -p ${tmpdir} && cd ${tmpdir} || exit

cp ${DomainNetCDF} .

mkdir -p $POSTDIR
mkdir -p $POST2DIR
mkdir -p $POST3HDIR

wxajcmd="python ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.py -a ${thisdir}/wrfncxnj.globalattr_ESCENA -g ${tmpdir}/geo_em.d01.nc -r 1950-01-01_00:00:00 --fullfile ${FULLnc}" 

tag=$(date +'%Y%m%d%H%M%S')

# Postprocessing variables from '${variablesfile}'
read_postprocess_variables ${variablesfile}

#rm ${tmpdir}/*
#cd ${thisdir}
#rmdir ${tmpdir}
fi
