#!/usr/bin/python
# e.g. # ./check_point_pr.py -v pr -p 33 33 -P 1990-1999

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import subprocess as sub

### Options

parser = OptionParser()
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check", metavar="PT")
parser.add_option("-P", "--Period", dest="period", 
                  help="point to check", metavar="PT")
(opts, args) = parser.parse_args()

### Static values

WRF4Gfold='/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G'
fold='123'
headfile='CCRC_NARCliM_Sydney'
timename='time'

#######    #######
## MAIN
    #######

point=np.array(opts.point)
print point
#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

#
# All
stat=''
varn=opts.varname + stat
ncAllfile=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncAll = NetCDFFile(ncAllfile,'r')

if not ncAll.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarAll = ncAll.variables[timename]
timeVarAllVal = timeVarAll.getValue()
timeUnits=getattr(timeVarAll, 'units')

varAll = ncAll.variables[varn]
varAllVal = varAll.getValue()
dimt = varAllVal.__len__() 
varAllValPt=varAllVal[0:dimt,point[0],point[1]]
varUnits=getattr(varAll, 'units')

ncAll.close
# 
# pr5max
stat='5max'
varn=opts.varname + stat
ncMax05file=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncMax05 = NetCDFFile(ncMax05file,'r')

if not ncMax05.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarMax05 = ncMax05.variables[timename]
timeVarMax05Val = timeVarMax05.getValue()
timeUnits=getattr(timeVarMax05, 'units')

varMax05 = ncMax05.variables[varn]
varMax05Val = varMax05.getValue()
dimt = varMax05Val.__len__() 
varMax05ValPt=varMax05Val[0:dimt,point[0],point[1]]

ncMax05.close

# 
# pr10max
stat='10max'
varn=opts.varname + stat
ncMax10file=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncMax10 = NetCDFFile(ncMax10file,'r')

if not ncMax10.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarMax10 = ncMax10.variables[timename]
timeVarMax10Val = timeVarMax10.getValue()
timeUnits=getattr(timeVarMax10, 'units')

varMax10 = ncMax10.variables[varn]
varMax10Val = varMax10.getValue()
dimt = varMax10Val.__len__() 
varMax10ValPt=varMax10Val[0:dimt,point[0],point[1]]

ncMax10.close

# 
# pr20max
stat='20max'
varn=opts.varname + stat
ncMax20file=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncMax20 = NetCDFFile(ncMax20file,'r')

if not ncMax20.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarMax20 = ncMax20.variables[timename]
timeVarMax20Val = timeVarMax20.getValue()
timeUnits=getattr(timeVarMax20, 'units')

varMax20 = ncMax20.variables[varn]
varMax20Val = varMax20.getValue()
dimt = varMax20Val.__len__() 
varMax20ValPt=varMax20Val[0:dimt,point[0],point[1]]

ncMax20.close

# 
# pr30max
stat='30max'
varn=opts.varname + stat
ncMax30file=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncMax30 = NetCDFFile(ncMax30file,'r')

if not ncMax30.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarMax30 = ncMax30.variables[timename]
timeVarMax30Val = timeVarMax30.getValue()
timeUnits=getattr(timeVarMax30, 'units')

varMax30 = ncMax30.variables[varn]
varMax30Val = varMax30.getValue()
dimt = varMax30Val.__len__() 
varMax30ValPt=varMax30Val[0:dimt,point[0],point[1]]

ncMax30.close

# 
# pr1Hmax
stat='1Hmax'
varn=opts.varname + stat
ncMax1Hfile=fold + '/' + headfile + '_All_' + opts.period + '_' + opts.varname + stat + '.nc'
ncMax1H = NetCDFFile(ncMax1Hfile,'r')

if not ncMax1H.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarMax1H = ncMax1H.variables[timename]
timeVarMax1HVal = timeVarMax1H.getValue()
timeUnits=getattr(timeVarMax1H, 'units')

varMax1H = ncMax1H.variables[varn]
varMax1HVal = varMax1H.getValue()
dimt = varMax1HVal.__len__() 
varMax1HValPt=varMax1HVal[0:dimt,point[0],point[1]]

ncMax1H.close

# 
# DAX
stat='max'
varn=opts.varname + stat
ncDAXfile=fold + '/' + headfile + '_DAM_' + opts.period + '_' + opts.varname + stat + '.nc'
ncDAX = NetCDFFile(ncDAXfile,'r')

if not ncDAX.variables.has_key(timename):
  print 'File does not have time !!!!'
  quit()

timeVarDAX = ncDAX.variables[timename]
timeVarDAXVal = timeVarDAX.getValue()
timeUnits=getattr(timeVarDAX, 'units')

varDAX = ncDAX.variables[varn]
varDAXVal = varDAX.getValue()
dimt = varDAXVal.__len__() 
varDAXValPt=varDAXVal[0:dimt,point[0],point[1]]

ncDAX.close


#
# Plotting

graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + opts.varname
hold(True)

title(opts.varname + ' at ' + ptS[0] + ', ' + ptS[1])
xlabel(timename + ' (' + timeUnits + ')')
ylabel('log10(' + opts.varname + ') (' + varUnits + ')')

plot(timeVarAllVal, log10(varAllValPt), color='black', label='All')
plot(timeVarMax05Val, log10(varMax05ValPt), linestyle='None', color=((0,0,0,1)), marker='x', label='5')
plot(timeVarMax10Val, log10(varMax10ValPt), linestyle='None', color=((0.5,0,0,1)), marker='x', label='10')
plot(timeVarMax20Val, log10(varMax20ValPt), linestyle='None', color=((1,0,0,1)), marker='x', label='20')
plot(timeVarMax30Val, log10(varMax30ValPt), linestyle='None', color=((1,0.5,0,1)), marker='x', label='30')
plot(timeVarMax1HVal, log10(varMax1HValPt), linestyle='None', color=((1,1,0,1)), marker='x', label='1H')
plot(timeVarDAXVal, log10(varDAXValPt), linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
##plot(timeVarMeanVal, varDAMValPt, 'g2', label='DAM')

legend()
savefig(graphname)

sub.call('display ' + graphname + '.png &', shell=True)
