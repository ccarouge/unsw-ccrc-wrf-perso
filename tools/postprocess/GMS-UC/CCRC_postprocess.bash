#!/bin/bash
## L. Fita, February 2013, CCRC-ARC CoECSS, UNSW, Sydney
## g.e. # ./CCRC_postprocess.bash miroc_1990-2020_R2.deck
if test $1 = '-h'; then
  echo "*******************************"
  echo "***     Shell to run the    ***"
  echo "*** CCRC's WRF post-process ***"
  echo "*******************************"
  echo "CCRC_postprocess.bash [postprocess.deck](File with the configuration of the post-process)"
  echo "[postprocess.deck]________________"
  echo "BASHsource [value] # Path to bash script which sets up post-process system configuration"
  echo "WRF4Ghome [value] # Path to different utilities (assuming a given folder structure)"
  echo "p_interp [value] # Path to Fortran executable to pressure-level interpolation of WRF outputs"
  echo "DomainNetCDF [value] # Path to domain file of the simulations (from geogrid.exe)"
  echo "EXPDIR [value] # Path to experiment outputs"
  echo "POSTFOLD [value] # Path to the post-process output"
  echo "postprocessor [value] # Postprocessor to use for the 'p_interp' Fortran program"
  echo "FULLnc [value] # Path to a WRF raw file "
  echo "FirstEXPy [value] # Beginning of the period of WRF outputs ([YYYY], format)"
  echo "LastEXPy [value] # End of period of WRF outputs ([YYYY], format)"
  echo "overlap_yearmon [value] # Overlaping year and month (for experiments ran with different overlaping periods, [YYYY][MM] format)"
  echo "checking [value] # Flag to indicate if an internal checking is needed using R at two grid points (1, yes; 0, no)"
  echo "VarnameSecFile [value] # Section on the post-processed files with the name of the variable ([expName]_[freqlabel]_[periodlabel]_[varname+statistics])"
  echo "ptstime [value] # 2 lon/lat grid points to perform the checking with R ([lon1]:[lat1]:[lon2]:[lat2])"
  echo "Rhome [value] # path to the R executable"
  echo "CDOhome [value] # path to cdo executable"
  echo "ncHOME [value] # path to the netCDF library root folder"
  echo "DOMAINsim [value] # domain of simulation of the WRF namelist"
  echo "envFile [value] # ASCII file with different values used to configure the environment for each independent script"
  echo "NAMELISToutput [value] # path to a 'namelist.output' of the simulations"
  echo "RefDate [value] # [YYYY]-[MM]-[DD]_[HH]-[MI]-[SS]: reference date used to compute hours since this date (following CF-conventions)"
  echo "PROCESSout [value] # sum of: 1, compress; 2, verify; 4, internal netcdf4 compress for the processed files (if it is not activated, files are transformed to netcdf classic)"
  echo "HFRQout [value] # sum of: 1, zip compress; 2, internal netCDF compress (if it is not activated, files are transformed to netcdf classic); 4, introduce cell_methods, on files in postHFRQ"
  echo "PROJout [value] # sum of: 1, zip compress; 2, internal netCDF compress (if it is not activated, files are transformed to netcdf classic); 4, introduce cell_methods, on files in postPROJ"
  echo "cleanAll [value] # !!!!!!! remove all the existing files of the post-processed outptus (1, yes: 0, no) !!!!!!!"
  echo "cleanINDIV [value] # !!!!!!! remove previous existing file of that variable and statistic to be post-processed (1, yes: 0, no) !!!!!!!"
  echo "experimentName [value] # Name to give to the post-processed files"
  echo "Period [value] # Period to post-process ([YYYYi]-[YYYYf], format)"
  echo "PerFrequencies [value] # number of years in which the output should be grouped ([freq1]:...:[freqn], format)"
  echo "VarlistFile [value] # full path to file with the list of variables to post-process"
  echo "UserMail [value] # mail direction to which a message will be send when post-processed will finish"
else
  rootsh=`pwd`
  deckfile=$1
  ./write_requested.bash ${deckfile}
  ./postprocess_vars_climate.CCRC.sh ${rootsh}/requested_variables.inf
fi
