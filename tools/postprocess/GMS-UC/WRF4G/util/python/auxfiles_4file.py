# e.g. # python auxfiles_4file.py -f wrfout_d01_1997-02-17_00:00:00 -d /home/lluis/studies/NARCliMGreatSydney2km/data/out -k 'hrly' -D 1 -r 1949120101000000 -w /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/python
from optparse import OptionParser
import numpy as np
import subprocess as sub
import datetime as dt
from netCDF4 import Dataset as NetCDFFile
import pdb

def checkIns(instruction):
  try:
    cmdout=sub.Popen(instruction, stdout=sub.PIPE, stderr=sub.PIPE ,shell=True)
    return cmdout.communicate()[0]
  except OSError as e:
    return None


### Options
parser = OptionParser()
parser.add_option("-D", "--Domain", dest="domN",
                  help="domain number to check", metavar="DOM")
parser.add_option("-d", "--directory", dest="dir",
                  help="directory with the files", metavar="FOLDER")
parser.add_option("-f", "--file", dest="file",
                  help="file to find the auxiliar files", metavar="FILE")
parser.add_option("-k", "--kind", dest="kind",
                  help="kind of file of  auxiliar files (out, hrly, dly, xtrm)", metavar="KIND")
parser.add_option("-r", "--referencedate", dest="refdate",
                  help="reference date of variable time [YYYY][MM][DD][HH][MI][SS]", metavar="DATE")
parser.add_option("-w", "--working_directory", dest="wdir",
                  help="directory with the python script 'nc_time_tools.py'", metavar="FOLDER")
(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######
folderpath=opts.dir
auxkind=opts.kind
dom=opts.domN
filen=opts.file
refd=opts.refdate

filein=folderpath + '/' + filen
ins='python ' + opts.wdir + '/nc_time_tools.py -f ' + filein + ' -o timeINF -r ' + refd +        \
  ' -t Times'
cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
cmd=cmdout.communicate()[0].split(' ')
# initial and final time/date in input file

inidate = cmd[3]
infdate = cmd[5]

# Only the auxiliar files from the previous and the next month will be checked
##
iniyr = int(inidate[0:4])
inimon = '{0:02d}'.format(int(inidate[4:6]))
iniyr1 = infyr1 = iniyr

inimon1 = int(inidate[4:6])-1
if inimon1 < 1:
  inimon1 = 12
  iniyr1 = iniyr - 1
infyr = int(infdate[0:4])
infmon1 = int(infdate[4:6])+1
if infmon1 > 12:
  infmon1 = 1
  infyr1 = infyr + 1

inimon1 = '{0:02d}'.format(inimon1)
infmon1 = '{0:02d}'.format(infmon1)


ins1='ls -1 ' + folderpath + '/wrf' + auxkind + '_d0' + str(dom) + '_' + str(iniyr1) +'-' +      \
  inimon1 + '-*'
ins0='ls -1 ' + folderpath + '/wrf' + auxkind + '_d0' + str(dom) + '_' + str(iniyr) +'-' +       \
  inimon + '-*'
ins_1='ls -1 ' + folderpath + '/wrf' + auxkind + '_d0' + str(dom) + '_' + str(infyr1) +'-' +     \
  infmon1 + '-*'

#
# Files within the range moni-1, monf+1
auxfilesrg=[]

#cmdout=sub.Popen(ins1, stdout=sub.PIPE, shell=True)
cmdout=checkIns(ins1)
if not cmdout is None:
  cmdvals=cmdout.split('\n')
  for ifv in range(len(cmdvals)):
    if len(cmdvals[ifv]) > 0:
      auxfilesrg.append(cmdvals[ifv])

#cmdout=sub.Popen(ins0, stdout=sub.PIPE, shell=True)
cmdout=checkIns(ins0)
if not cmdout is None:
  cmdvals=cmdout.split('\n')
  for ifv in range(len(cmdvals)):
    if len(cmdvals[ifv]) > 0:
      auxfilesrg.append(cmdvals[ifv])

#cmdout=sub.Popen(ins_1, stdout=sub.PIPE, shell=True)
cmdout=checkIns(ins_1)
if not cmdout is None:
  cmdvals=cmdout.split('\n')
  for ifv in range(len(cmdvals)):
    if len(cmdvals[ifv]) > 0:
      auxfilesrg.append(cmdvals[ifv])

validfiles=[]
for ifile in range(len(auxfilesrg)):
  auxfile=auxfilesrg[ifile]
  ins='python ' + opts.wdir + '/nc_time_tools.py -f ' + auxfile + ' -o timeINF -r ' + refd +     \
    ' -t Times'
  cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
  cmd=cmdout.communicate()[0].split(' ')
  auxidate = cmd[3]
  auxfdate = cmd[5]

  if auxidate <= inidate and auxfdate >= infdate:
    validfiles.append(auxfilesrg[ifile])
  elif auxidate <= inidate and auxfdate >= inidate and auxfdate <= infdate:
    validfiles.append(auxfilesrg[ifile])
  elif auxidate >= inidate and auxfdate <= infdate:
    validfiles.append(auxfilesrg[ifile])
  elif auxidate <= infdate and auxfdate >= infdate:
    validfiles.append(auxfilesrg[ifile])
    
for ifile in range(len(validfiles)):
  print validfiles[ifile]
