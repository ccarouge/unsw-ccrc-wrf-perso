##!/usr/bin/python
# e.g. ccrc468-17 # python ./nc_var_attr.py -v prac -f abc/CCRC_NARCliM_Sydney_MOM_1990-1999_prac.nc -o r -a standard_name

from optparse import OptionParser
import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os

####### ###### ##### #### ### ## #

def searchInlist(listname, nameFind):
  for x in listname:
    if x == nameFind:
      return True
  return False

### Options

parser = OptionParser()
parser.add_option("-a", "--attribute", dest="attribute",
                  help="attribute to operate", metavar="VAR")
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
parser.add_option("-o", "--operation", dest="operation", 
                  help="operation: a (add), d (delete), m (modify), r (read) ", metavar="FILE")
parser.add_option("-s", "--stringValue", dest="Svalue",
                  help="value to give (when applies)", metavar="VAR")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")
(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
errormsg='ERROR -- error -- ERROR -- error'

varn=opts.varname
oper=opts.operation
attr=opts.attribute

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errormsg
  quit()

if oper == 'a' or oper == 'm' or oper == 'd':
  ncf = NetCDFFile(opts.ncfile,'a')
elif oper == 'r':
  ncf = NetCDFFile(opts.ncfile,'r')
else:
  print errormsg
  print '  Operation ' + oper + ' does not exist !!!'
  print errormsg
  quit()

if not ncf.variables.has_key(varn):
  print errormsg
  print '  File does not have variable ' + varn + ' !!!!'
  print errormsg
  quit()

var = ncf.variables[varn]
var_attrs = var.ncattrs()

attrPos = searchInlist(var_attrs, attr)

if oper == 'a': 
  if attrPos:
    print errormsg
    print '  Variable ' + varn + ' already have attribute ' + attr + ' !!!!'
    print errormsg
    quit()
else:
  if not attrPos:
    print errormsg
    print '  Variable ' + varn + ' does not have attribute ' + attr + ' !!!!'
    print errormsg
    quit()

if oper == 'a':
  Attr = var.setncattr( attr, options.Svalue)
elif oper == 'd':
  Attr = var.delncattr( attr )
elif oper == 'm':
  Attr = var.delncattr( attr )
  Attr = var.setncattr( attr, options.Svalue)
elif oper == 'r':
  print var.getncattr(attr)

ncf.sync()
ncf.close
