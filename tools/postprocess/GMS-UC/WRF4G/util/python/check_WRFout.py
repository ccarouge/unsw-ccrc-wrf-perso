#!/usr/bin/python
# e.g. # python check_WRFout.py -f /home/lluis/studies/NARCliMGreatSydney2km/data/out -n /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/namelist.output -d 1 -v UST -k out -p 33 33 -P 19970201000000 19970228235959 -t 'Times' -r 19491201000000
# e.g. # python check_WFRout.py -f -n namelist.output -d 1 -v all -p 39 120 -P 1990 1999

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import subprocess as sub
import datetime as dt
import matplotlib as plt
import os

####### Classes
# plotValXY: class with all the plotting attributes
# plotValStatXY: class with all the plotting attributes of a statistics variable
# statsVal: Statistics class providing min, max, mean, mean2, median, standard deviation and percentiles of a list of values
# ncFileStats: Statistics class for a netCDF file providing: format, name of the dimensions, number of dimensions, name of the variables, 
#    number of variables, name of attributes and number of attributes, time difference between time-steps, first time-step
# dates_period: Class to provide the dates for a given period for a given frequency

####### Functions
# get_stat_xy: Function to get necessary information to plot a statistical variable in a given point
# get_xy: Function to get necessary information to plot a variable in a given point
# get_2D: Function to get necessary information to plot a variable for the whole domain
# SecondsBetween: Gives the number of seconds between two dates [date1], [date2]  ([YYYY][MM][DD][HH][MI][SS], format)
# IsinPeriod: Gives a true/false if a given [valdate] format is within a period [iniper], [endper] ([YYYY][MM][DD][HH][MI][SS], format)

# seconds_dateRef: Function to provide the number of seconds since a reference date

class plotValXY(object):
  """class with all the plotting attributes"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varUnits(self, vals):
    sel.varUnits = vals
  def timeUnits(self, vals):
    sel.timeUnits = vals

class plotValStatXY(object):
  """class with all the plotting attributes of a statistics variable"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varBnds(self, vals):
    self.varBnds = vals 
  def varBndsValt(self, vals):
    self.varBndsValt = vals
  def varBndsValT(self, vals):
    self.varBndsValT = vals

def get_stat_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a statistical variable in a given point
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(filename):
    print errormsg
    print '  File ' + filename + ' does not exist !!'
    print errormsg
    quit()  
 
  print filename

  ncf = NetCDFFile(filename,'r')
  
  if not ncf.variables.has_key(timename):
    print 'File does not have time !!!!'
    quit()
  
  timeVar = ncf.variables[timename]
  valPlot = plotValStatXY(timeVar.getValue())
  
  var = ncf.variables[varname]
  varVal = var.getValue()
  dimt = varVal.__len__() 
  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  else:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]
  
  varbnds = ncf.variables['time_bnds']
  valPlot.varBnds = varbnds
  valPlot.varBndsVal = varbnds.getValue()
  varBndsValt = []
  varBndsValT = []
  for it in range(dimt):
    varBndsValt.append(valPlot.varBndsVal[it,0])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(valPlot.varBndsVal[it,1])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(None)
    varBndsValT.append(None)
  
  ncf.close
  valPlot.varBndsValt = varBndsValt
  valPlot.varBndsValT = varBndsValT

  return valPlot

def get_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a variable in a given point
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(filename):
    print errormsg
    print '  File ' + filename + ' does not exist !!'
    print errormsg
    quit()  
 
#  print filename

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print errormsg
    print 'File does not have time !!!!'
    print errormsg
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timeVar.getValue())
  if not timename == 'Times':
    valPlot.timeUnits=getattr(timeVar, 'units')
  else:
    valPlot.timeUnits=''
 
  if not ncf.variables.has_key(varname):
    print errormsg
    print 'File does not have variable ' + varname + ' !!!!'
    print errormsg
    quit()  

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  elif varVal.ndim == 3:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]
#  elif varVal.ndim == 2:
#    valPlot.varValPt=varVal[0:dimt,pt[0]]
#  elif varVal.ndim == 1:
#    valPlot.varValPt=varVal[0:dimt]
  else:
    print errormsg
    print '   Variable ' + varname + ' has ', varVal.ndim, ' dimensions !!'
    print errormsg
    quit()

  ncf.close
  return valPlot

def get_2D(varname, timename, filename):
# Function to get necessary information to plot a variable for the whole domain
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(filename):
    print errormsg
    print '  File ' + filename + ' does not exist !!'
    print errormsg
    quit()  

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print errormsg
    print 'File does not have time !!!!'
    print errormsg
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timeVar.getValue())
  if not timename == 'Times':
    valPlot.timeUnits=getattr(timeVar, 'units')
  else:
    valPlot.timeUnits=''
 
  if not ncf.variables.has_key(varname):
    print errormsg
    print 'File does not have variable ' + varname + ' !!!!'
    print errormsg
    quit()  

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,:,:]
  elif varVal.ndim == 3:
    valPlot.varValPt=varVal[0:dimt,:,:]
#  elif varVal.ndim == 2:
#    valPlot.varValPt=varVal[0:dimt,pt[0]]
#  elif varVal.ndim == 1:
#    valPlot.varValPt=varVal[0:dimt]
  else:
    print errormsg
    print '   Variable ' + varname + ' has ', varVal.ndim, ' dimensions !!'
    print errormsg
    quit()

  ncf.close
  return valPlot

def SecondsBetween(date1, date2):
  """ Gives the number of seconds between two dates [date1], [date2]  ([YYYY][MM][DD][HH][MI][SS], format)
  >>> print SecondsBetweeen('19760101000000', '19761231235959')
  31622399
  """
  import datetime as dt
  import numpy as np

  dt1=dt.datetime(int(date1[0:4]), int(date1[4:6]), int(date1[6:8]), int(date1[8:10]), int(date1[10:12]), int(date1[12:14]))
  dt2=dt.datetime(int(date2[0:4]), int(date2[4:6]), int(date2[6:8]), int(date2[8:10]), int(date2[10:12]), int(date2[12:14]))

  difft = dt2-dt1

  return difft.days*24*3600 + difft.seconds


def IsinPeriod(valdate, iniper, endper):
  """ Gives a true/false if a given [valdate] format is within a period [iniper], [endper] ([YYYY][MM][DD][HH][MI][SS], format)
  >>> print IsinPeriod('19760217083000', '19760101000000', '19761231235959')
  """
  import datetime as dt
  import numpy as np

  vdate=dt.datetime(int(valdate[0:4]), int(valdate[4:6]), int(valdate[6:8]), int(valdate[8:10]), int(valdate[10:12]), int(valdate[12:14]))
  idate=dt.datetime(int(iniper[0:4]), int(iniper[4:6]), int(iniper[6:8]), int(iniper[8:10]), int(iniper[10:12]), int(iniper[12:14]))
  edate=dt.datetime(int(endper[0:4]), int(endper[4:6]), int(endper[6:8]), int(endper[8:10]), int(endper[10:12]), int(endper[12:14]))

  if vdate >= idate and vdate <= edate:
    return True
  else:
    return False

class statsVal(object):
  """Statistics class providing min, max, mean mean2, median, standard deviation and percentiles of a list of values"""

  def __init__(self, values):
    import math

    self.Nv=len(values)
    self.minv=10000000000.
    self.maxv=-self.minv
    self.meanv=0.
    self.mean2v=0.
    self.stdv=0.

    sortedvalues = sorted(values)

    for inum in range(self.Nv):
      if values[inum] < self.minv:
        self.minv = values[inum]
      if values[inum] > self.maxv:
        self.maxv = values[inum]

      self.meanv = self.meanv+values[inum]
      self.mean2v = self.mean2v+values[inum]*values[inum]

    self.meanv = self.meanv/float(self.Nv)
    self.mean2v = self.mean2v/float(self.Nv)
    self.stdv = math.sqrt(self.mean2v-self.meanv*self.meanv)
    self.medianv = sortedvalues[(self.Nv -1)/2]
    self.quantilesv = []
    for iq in range(20): 
      self.quantilesv.append(sortedvalues[int((self.Nv-1)*iq/20)])

    self.quantilesv.append(sortedvalues[self.Nv-1])

def listdate_2_date(listdate):
  """ Conversion of a list date [Y] [Y] [Y] [Y] '-' [M] [M] '-' [D] [D] '_' [H] [H] ':' [M] [M] ':' [S] [S]

  >>> print timelist
  ['2' '0' '0' '2' '-' '0' '2' '-' '1' '7' '_' '0' '0' ':' '0' '0' ':' '0' '0']
  >>> listdate_2_date(timelist).strftime("%Y/%m/%d %H:%M:%S")
  2002/02/17 00:00:00
  """
  import datetime as dt  
  
  year=int(listdate[0])*1000 + int(listdate[1])*100 + int(listdate[2])*10 + int(listdate[3])
  month=int(listdate[5])*10 + int(listdate[6])
  day=int(listdate[8])*10 + int(listdate[9])
  hour=int(listdate[11])*10 + int(listdate[12])
  minute=int(listdate[14])*10 + int(listdate[15])
  second=int(listdate[17])*10 + int(listdate[18])

  date = dt.datetime(year, month, day, hour, minute, second)
  return date

class ncFileStats(object):
  """Statistics class for a netCDF file providing: format, name of the dimensions, number of dimensions, name of the variables, 
     number of variables, name of attributes and number of attributes, number of times, time difference between time-steps, first time-step """
  import numpy as np
  from netCDF4 import Dataset as NetCDFFile
  import datetime as dt
  import time

  def __init__(self, ncfile, timename, refdatestr):
    print ncfile
    ncf = NetCDFFile(ncfile,'r')
#    self.format = ncf.file_format
    self.dims = ncf.dimensions
    self.ndims = len(self.dims)
    self.vars = ncf.variables
    self.nvars = len(self.vars)
#    self.attrs = ncf.ncattrs()
#    self.nattrs = len(self.attrs)

    vartime = ncf.variables[timename]
    timeval = vartime.getValue()
    self.ntimes = len(timeval)

    refdate = dt.datetime(int(refdatestr[0:4]), int(refdatestr[4:6]), int(refdatestr[6:8]), 
      int(refdatestr[8:10]), int(refdatestr[10:12]), int(refdatestr[12:14]) )
    if len(timeval.shape) == 1:
      dT = timeval[1]-timevarVal[0]
      firstT = timeval[0]
      time0 = refdate + dt.timedelta(seconds=firstT*3600)
      firstTS = time0.strftime("%Y%m%d%H%M%S")
    else:
      time0 = listdate_2_date(timeval[0])
      time1 = listdate_2_date(timeval[1])
      dtime = time1 - time0
      dT = dtime.days*24 + dtime.seconds/3600
      DfirsT = time0 - refdate
      firstT = DfirsT.days*24 + DfirsT.seconds/3600
      firstTS = time0.strftime("%Y%m%d%H%M%S")

    self.dT = dT*3600
    self.firstTS = firstTS

    ncf.close()

class dates_period(object):
  """ Class to provide the dates for a given period for a given frequency
    self.firstTs: first date in seconds since reference date
    self.lastTs: last date in seconds since reference date
    self.speriod: total number of seconds covered by the period
    self.Ntimes: Number of time steps in the period
    self.stimes: list of times for the period for a given frequency as seconds from referencedate
    self.dtimes: list of times for the period for a given frequency in [YYYY][MM][DD][HH][MI][SS] format
  >>> datesPer = dates_period('19760201000000', '19760203000000', '19491201000000', 10800)
  >>> print datesPer.stimes
  [825811200, 825822000, 825832800, 825843600, 825854400, 825865200, 825876000, 825886800, 825897600, 825908400, 825919200, 825930000, 825940800, 825951600, 825962400, 825973200, 825984000]
  >>> print datesPer.dtimes
  ['19760201000000', '19760201030000', '19760201060000', '19760201090000', '19760201120000', '19760201150000', '19760201180000', '19760201210000', '19760202000000', '19760202030000', '19760202060000', '19760202090000', '19760202120000', '19760202150000', '19760202180000', '19760202210000', '19760203000000']
  >>> print datesPer.Ntimes
  17
  """
  import datetime as dt
  import numpy as np
  
  def __init__(self, begPeriod, endPeriod, refdate, freq):

    init = dt.datetime(int(begPeriod[0:4]), int(begPeriod[4:6]), int(begPeriod[6:8]), \
      int(begPeriod[8:10]), int(begPeriod[10:12]), int(begPeriod[12:14]))
    endt = dt.datetime(int(endPeriod[0:4]), int(endPeriod[4:6]), int(endPeriod[6:8]), \
      int(endPeriod[8:10]), int(endPeriod[10:12]), int(endPeriod[12:14]))
    reft = dt.datetime(int(refdate[0:4]), int(refdate[4:6]), int(refdate[6:8]), \
      int(refdate[8:10]), int(refdate[10:12]), int(refdate[12:14]))
    
    difftper = endt - init
    difftini = init - reft
    difftend = endt - reft

    self.firstTs = difftini.days*24*3600 + difftini.seconds
    self.lastTs = difftend.days*24*3600 + difftend.seconds
    self.speriod = difftper.days*24*3600 + difftper.seconds

    self.Ntimes = self.speriod / freq + 1
    timestepsS = []
    timestepsD = []

    for it in range(self.Ntimes):
      newdate = init + dt.timedelta(seconds = freq*it)
      diffnew = newdate - reft
      timestepsS.append(diffnew.days*24*3600 + diffnew.seconds)
      timestepsD.append(newdate.strftime("%Y%m%d%H%M%S"))

    self.stimes = timestepsS
    self.dtimes = timestepsD

def seconds_dateRef(date, refdate):
  """ Function to provide the number of seconds since a reference date
  print seconds_dateRef('19760217083000', '19491201000000')
  827224200
  """
  datt = dt.datetime(int(date[0:4]), int(date[4:6]), int(date[6:8]), \
    int(date[8:10]), int(date[10:12]), int(date[12:14]))
  reft = dt.datetime(int(refdate[0:4]), int(refdate[4:6]), int(refdate[6:8]), \
    int(refdate[8:10]), int(refdate[10:12]), int(refdate[12:14]))
  diffdate = datt - reft
  return diffdate.days*24*3600 + diffdate.seconds

print seconds_dateRef('19491201100000', '19491201000000')

### Options

parser = OptionParser()
parser.add_option("-d", "--domain", dest="domN",
                  help="domain number to check", metavar="DOM")
parser.add_option("-f", "--folder", dest="folder",
                  help="folder with the files", metavar="FOLD")
parser.add_option("-k", "--kindFile", dest="kind",
                  help="kind of files", metavar="KIND")
parser.add_option("-n", "--namelistoutput", dest="namoutput",
                  help="'namelist.output' to use as reference", metavar="DOM")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check (2 values); -1 -1 all values", metavar="PT")
parser.add_option("-P", "--Period", dest="period", type="int",
                  nargs=2, help="period to check (2 values, [YYYY][MM][DD][HH][MI][SS])", metavar="PER")
parser.add_option("-r", "--referencedate", dest="refdate",
                  help="reference date of variable time [YYYY][MM][DD][HH][MI][SS]", metavar="DATE")
parser.add_option("-t", "--timename", dest="timename",
                  help="name of variable time in files", metavar="NAME")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check ('all': all variables; [VAR1],[[VAR2],...])", metavar="VAR")
(opts, args) = parser.parse_args()

### Static values
WRFout='ACGRDFLX,ACHFX,ACLHF,ACSNOM,ACSNOW,ALBBCK,ALBEDO,AVG_FUEL_FRAC,CANWAT,CF1,CF2,CF3,CFN,CFN1,CLDFRA,DN,DNW,DZS,EL_MYJ,EMISS,FNM,FNP,GLW,GRAUPELNC,GRDFLX,GSW,HAILNC,HFX,HFX_FORCE,HFX_FORCE_TEND,H_DIABATIC,LAI,LH,LH_FORCE,LH_FORCE_TEND,LWCF,MAVAIL,MAX_MSTFX,MAX_MSTFY,MU,NEST_POS,NOAHRES,OLR,P,P00,PB,PBLH,PH,PHB,POTEVP,P_HYD,P_TOP,QCG,QCLOUD,QFX,QGRAUP,QICE,QNICE,QNRAIN,QRAIN,QSNOW,QVAPOR,QVG,RAINCV,RAINNCV,RAINSH,RDN,RDNW,RDX,RDY,REGIME,RESM,SEAICE,SFCEVP,SFCEXC,SFROFF,SH2O,SMCREL,SMOIS,SMSTOT,SNOPCX,SNOW,SNOWC,SNOWH,SNOWNC,SOILTB,SR,SST,SSTSK,SWCF,SWDOWN,SWNORM,T,T00,TISO,TKESFCF,TKE_MYJ,TLP,TMN,TSK,TSK_FORCE,TSK_FORCE_TEND,TSLB,U,UAH,UDROFF,UST,V,VAH,VEGFRA,W,XICEM,XTIME,ZETATOP,ZNU,ZNW,ZS,COSALPHA,E,F,HGT,LANDMASK,LU_INDEX,MAPFAC_M,MAPFAC_MX,MAPFAC_MY,MAPFAC_U,MAPFAC_UX,MAPFAC_UY,MAPFAC_V,MAPFAC_VX,MAPFAC_VY,MF_VX_INV,MUB,SINALPHA,XLAND,XLAT,XLAT_U,XLAT_V,XLONG,XLONG_U,XLONG_V'
WRFxtrm='T2MIN,T2MAX,TT2MIN,TT2MAX,T2MEAN,T2STD,Q2MIN,Q2MAX,TQ2MIN,TQ2MAX,Q2MEAN,Q2STD,SKINTEMPMIN,SKINTEMPMAX,TSKINTEMPMIN,TSKINTEMPMAX,SKINTEMPMEAN,SKINTEMPSTD,U10MAX,V10MAX,SPDUV10MAX,TSPDUV10MAX,U10MEAN,V10MEAN,SPDUV10MEAN,U10STD,V10STD,SPDUV10STD,RAINCVMAX,RAINNCVMAX,TRAINCVMAX,TRAINNCVMAX,RAINCVMEAN,RAINNCVMEAN,RAINCVSTD,RAINNCVSTD,XLAT,XLONG'
WRFdly='SUNSHINE,TSUNSHINE,PRMAX5,TPRMAX5,UV10MAX5,TUV10MAX5,PRMAX10,TPRMAX10,UV10MAX10,TUV10MAX10,PRMAX20,TPRMAX20,UV10MAX20,TUV10MAX20,PRMAX30,TPRMAX30,UV10MAX30,TUV10MAX30,PRMAX1H,TPRMAX1H,UV10MAX1H,TUV10MAX1H,XLAT,XLONG'
WRFhrly='Q2,T2,TH2,PSFC,U10,V10,RAINC,RAINNC,XLAT,XLONG'

#######    #######
## MAIN
    #######
errmsg = 'ERROR -- error -- ERROR -- error '
warnmsg = 'warning *** WARNING '

sub.call('killall display ', shell=True)

#
# Checking point
point=np.array(opts.point)
print point
if point[0] == -1 and point[1] == -1:
  ptall = 1
else:
  ptall = 0

if ptall == 0:
  ptS=["%03d" % number for number in point]

#
# Checking period
period=np.array(opts.period)
print period
perS=["%04d" % number for number in period]

#
# variables
if opts.kind == 'out':
  thvariables=WRFout.split(',')
elif opts.kind == 'xtrm':
  thvariables=WRFxtrm.split(',')
elif opts.kind == 'hrly':
  thvariables=WRFhrly.split(',')
elif opts.kind == 'dly':
  thvariables=WRFdly.split(',')
else:
  print errmsg
  print '  WRF kind ' + opts.kind + ' not available !!'
  print errmsg
  quit()

Ntheorvar=len(thvariables)

if opts.varname == 'all':
  variables=thvariables
else:
  variables=opts.varname.split(',')

Nvars=len(variables)

dateIniPer = perS[0]
dateEndPer = perS[1]

#
# files
ins='ls ' + '-1 ' + opts.folder + '/wrf' + opts.kind + '_d0' + opts.domN + '_*'
cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
cmd=cmdout.communicate()[0] 
files=cmd.split('\n')
#print files
Nfiles=len(files)-1
valfiles = []
datefiles = []
sdatefiles = []
for ifile in range(Nfiles):
  datefile=files[ifile].split('_')
  Nsecs = len(datefile)
  datef=''.join(datefile[Nsecs-2].split('-')) + ''.join(datefile[Nsecs-1].split(':'))
  if IsinPeriod(datef, dateIniPer, dateEndPer):
    valfiles.append(ifile)
    datefiles.append(datef)
    sdatefiles.append(seconds_dateRef(datef,opts.refdate))

Nvalfiles=len(valfiles)
if Nvalfiles < 1:
  print errmsg
  print '  Has not found any coincident file !!!!'
  print errmsg
  quit()

#
# Providing all theoretical dates using values from first and second file
date1stfile=files[0].split('_')
Nsecs = len(date1stfile)
date1st=''.join(date1stfile[Nsecs-2].split('-')) + ''.join(date1stfile[Nsecs-1].split(':'))
date2ndfile=files[1].split('_')
date2nd=''.join(date2ndfile[Nsecs-2].split('-')) + ''.join(date2ndfile[Nsecs-1].split(':'))
secbetween1st2nd = SecondsBetween(date1st, date2nd) 
PeriodVals = dates_period(dateIniPer, dateEndPer, opts.refdate, secbetween1st2nd)

diffdates = sorted(list(set(PeriodVals.dtimes) - set(datefiles) ))
if not len(diffdates) == 0:
  difffile = open('missing_dates.dat' , 'w')
  print 'There are not the following dates in the files !!!'
  print diffdates
  for it in range(len(diffdates)):
    difffile.write(diffdates[it] + '\n') 
    print PeriodVals.dtimes[it], datefiles[it]

  difffile.close()
  print '  List of missed dates in file missing_dates.dat '
  print 'seconds between files: ',secbetween1st2nd
  print '   ',len(diffdates),' dates are missing!'
#  quit()

for ivar in range(Nvars):
  varn=variables[ivar]
  print '  variable: ' + varn 

  timesTOT=0
  varmins = []
  varmaxs = []
  varmeans = []
  varmean2s = []
  varmedians = []
  varstds = []

  for ifile in range(Nvalfiles):
    filen=files[valfiles[ifile]]
    filestats = ncFileStats(filen, opts.timename, opts.refdate)
    timesTOT = timesTOT + filestats.ntimes
    print ifile,':',timesTOT,filestats.ntimes
#
# number of variables
    if filestats.nvars == Ntheorvar: 
      print errmsg
      print '  File has ', filestats.nvars, ' and should have ', Ntheorvar, ' !!!'
      print errmsg
      quit()
#
# difference between time-steps
    if ifile == 0:
      dTprev = filestats.dT
    else:
      if not filestats.dT == dTprev: 
        print errmsg
        print '  time-step frequency in file has change from previous ', dTprev, ' to ', filestats.dT, ' !!!'
        print errmsg
        quit()
      else:
        dTprev = filestats.dT

    if ptall == 0:
      FileValpt=get_xy(varn, opts.timename, filen, point)
      Stats=statsVal(FileValpt.varValPt)
    else:
      FileValpt=get_2D(varn, opts.timename, filen)
      values2D = np.arange(FileValpt.varValPt.size)*1.
      values2D = np.reshape(FileValpt.varValPt, FileValpt.varValPt.size)
      Stats=statsVal(values2D)

    if Stats.minv < Stats.meanv - 2.5*(10000/Stats.Nv)*Stats.stdv:
      print warnmsg
      print '  minimum value: ',Stats.minv, ' is less than ', Stats.meanv - 2.5*(10000/Stats.Nv)*Stats.stdv, '(mean - 2.5*(10000/Nvalues)*stdv) !!!'
      print warnmsg

    if Stats.maxv > Stats.meanv + 2.5*(10000/Stats.Nv)*Stats.stdv:
      print warnmsg
      print '  maximum value: ',Stats.maxv, ' is more than ', Stats.meanv + 2.5*(10000/Stats.Nv)*Stats.stdv, '(mean - 2.5*(10000/Nvalues)*stdv) !!!'
      print warnmsg


    varmins.append(Stats.minv)
    varmaxs.append(Stats.maxv)
    varmeans.append(Stats.meanv)
    varmean2s.append(Stats.mean2v)
    varmedians.append(Stats.medianv)
    varstds.append(Stats.stdv)
    
  secsPeriod = SecondsBetween(dateIniPer, dateEndPer)
  timesTOTtheor = secsPeriod / dTprev

  if not timesTOTtheor == timesTOT:
    print errmsg
    print '  Total theoretical time steps of the period (' + dateIniPer + '-' + dateEndPer + ')', timesTOTtheor, ' does not coincide with the total number of time-steps ', timesTOT , ' from the files !!!'
    print errmsg
#    quit()

# Plotting
#
  print 'plotting ... .. .'

  boxplotvalues = np.reshape(np.arange(5*Nvalfiles),(Nvalfiles,5))*1.

  boxplotvalues[:,0]=varmins
  boxplotvalues[:,1]=varmaxs
  boxplotvalues[:,2]=varmeans
  boxplotvalues[:,3]=varmedians
  boxplotvalues[:,4]=varstds

  if ptall == 0:
    graphname = 'check_WRFout_' + ptS[0] + '-' + ptS[1] + '_' + varn
    titletxt = 'Statistics Boxplots for ' + varn + ' at ' + ptS[0] + ', ' + ptS[1] + '\n period ' + perS[0] + '-' + perS[1]
  else:
    graphname = 'check_WRFout_All_' + varn
    titletxt = 'Statistics Boxplots for ' + varn + ' all domain ' + '\n period ' + perS[0] + '-' + perS[1]

  plt.pyplot.hold(True)
  title(titletxt)
  xlabel('statistics from each file')
  ylabel(varn + ' (' + FileValpt.varUnits + ')')

  plt.pyplot.boxplot(boxplotvalues)
  plt.pyplot.xticks(arange(6), ('', 'min', 'max', 'mean', 'median', 'std'))
  savefig(graphname)
  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)


