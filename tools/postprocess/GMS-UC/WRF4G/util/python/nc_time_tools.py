##!/usr/bin/python
# e.g. # python WRF4G/util/python/nc_time_tools.py -f 123/CCRC_NARCliM_Sydney_01H_1990-1999_tas.nc -t time -r 19500101000000 -F 1 -o cdoF

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
import subprocess as sub
import datetime as dt
import time

def month_end(RefDate, hours):
    """ Gives de end date of a month ([YYYY]-[MM]-01, [YYYY]-[MM+1]-01)/2 from a given number of hours according to a referebce date [RefDate]

    >>> print month_end('19500101000000', 229032)
    [229344]
    """
    import datetime as dt
    import numpy as np

    refdate = dt.datetime(int(RefDate[0:4]), int(RefDate[4:6]), int(RefDate[6:8]), int(RefDate[8:10]), int(RefDate[10:12]), int(RefDate[12:14]))
    newdate = refdate + dt.timedelta(seconds=hours*3600)
    newdateval = np.array([int(newdate.strftime("%Y")), int(newdate.strftime("%m")), int(newdate.strftime("%d")), int(newdate.strftime("%H")), 
      int(newdate.strftime("%M")), int(newdate.strftime("%S"))])
    hoursmonthref = hours - (newdateval[2]-1)*24 - (newdateval[3]) - newdateval[4]/60 - newdateval[5]/3600
    mon1dateval=newdateval
    mon1dateval[1]=mon1dateval[1]+1
    mon1dateval[2]=1
    mon1dateval[3]=0
    mon1dateval[4]=0
    mon1dateval[5]=0
    refmon1date = dt.datetime(mon1dateval[0], mon1dateval[1], mon1dateval[2], mon1dateval[3], mon1dateval[4], mon1dateval[5])
    diffmon1 = refmon1date - refdate
    hoursmon1ref=diffmon1.days*24 + diffmon1.seconds/3600
    return int(hoursmon1ref)

def listdate_2_date(listdate):
  """ Conversion of a list date [Y] [Y] [Y] [Y] '-' [M] [M] '-' [D] [D] '_' [H] [H] ':' [M] [M] ':' [S] [S]

  >>> print timelist
  ['2' '0' '0' '2' '-' '0' '2' '-' '1' '7' '_' '0' '0' ':' '0' '0' ':' '0' '0']
  >>> listdate_2_date(timelist).strftime("%Y/%m/%d %H:%M:%S")
  2002/02/17 00:00:00
  """
  import datetime as dt  
  
  year=int(listdate[0])*1000 + int(listdate[1])*100 + int(listdate[2])*10 + int(listdate[3])
  month=int(listdate[5])*10 + int(listdate[6])
  day=int(listdate[8])*10 + int(listdate[9])
  hour=int(listdate[11])*10 + int(listdate[12])
  minute=int(listdate[14])*10 + int(listdate[15])
  second=int(listdate[17])*10 + int(listdate[18])

  date = dt.datetime(year, month, day, hour, minute, second)
  return date

### Options

parser = OptionParser()
parser.add_option("-F", "--frequency", dest="freq",
                  help="frequency (according to needs)", metavar="FREQ")
parser.add_option("-u", "--unit", dest="unit",
                  help="unit of the frequency (according to needs. H: hour, M: month)", metavar="UNIT")
parser.add_option("-f", "--ncfile", dest="ncfile",
                  help="file to check", metavar="FILE")
parser.add_option("-o", "--operation", dest="oper",
                  help="operation to do:\n " + 
  "cdoF; provide times for 'cdo timesel' beginning on first date in file every 'freq'\n" + 
  "timeINF: provides information about number of time-steps, time distance between time-steps (hours), first time & first date [YYYY][MM][DD][HH][MI][SS] and last time & last date [YYYY][MM][DD][HH][MI][SS]", metavar="FILE")
parser.add_option("-r", "--reftime", dest="reftime",
                  help="time reference [YYYY][MM][DD][HH][MM][SS]", metavar="FILE")
parser.add_option("-t", "--timename", dest="timen",
                  help="name of the variable time", metavar="FILE")
(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######
errormsg='ERROR -- error -- ERROR -- error'

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errormsg
  quit()

ncf = NetCDFFile(opts.ncfile,'r')

timevar = ncf.variables[opts.timen]
timevarVal = timevar.getValue()
Ntimes = len(timevarVal)

refdate=dt.datetime(int(opts.reftime[0:4]), int(opts.reftime[4:6]), int(opts.reftime[6:8]), int(opts.reftime[8:10]), 
  int(opts.reftime[10:12]), int(opts.reftime[12:14]))

if len(timevarVal.shape) == 1:
  dT = timevarVal[1]-timevarVal[0]
  firstT = timevarVal[0]
  time0 = refdate + dt.timedelta(seconds=firstT*3600)
  firstTS = time0.strftime("%Y%m%d%H%M%S")
  lasttT = timevarVal[Ntimes-1]
  time0 = refdate + dt.timedelta(seconds=lasttT*3600)
  lastTS = time0.strftime("%Y%m%d%H%M%S")
else:
  time0 = listdate_2_date(timevarVal[0])
  time1 = listdate_2_date(timevarVal[1])
  timeN = listdate_2_date(timevarVal[Ntimes-1])
  dtime = time1 - time0
  dT = dtime.days*24 + dtime.seconds/3600
  DfirsT = time0 - refdate
  firstT = DfirsT.days*24 + DfirsT.seconds/3600
  firstTS = time0.strftime("%Y%m%d%H%M%S")
  DlastT = timeN - refdate
  lasttT = DlastT.days*24 + DlastT.seconds/3600
  lastTS = timeN.strftime("%Y%m%d%H%M%S")

# cdoF
#
if opts.oper == 'cdoF':
  if not opts.reftime: 
    print errormsg
    print '  operation requires a reference date !!!'
    quit()
  if not opts.freq: 
    print errormsg
    print '  operation requires a frequency !!!'
    quit()
  if not opts.unit: 
    print errormsg
    print '  operation requires a time unit !!!'
    quit()

  cdodates=''
  Tdt=int(opts.freq)

  if opts.unit == 'H':
    NTdt = 24/Tdt
    for it in range(NTdt):
      newdate = refdate + dt.timedelta(seconds=firstT*3600+Tdt*3600*it)
      cdodates=cdodates + ',' + newdate.strftime("%H:%M:%S")
  else:
    Ndt = 12
    firsTy = int(firsT.strftime("%Y"))
    firsTm = int(firsT.strftime("%m"))
    firsTd = int(firsT.strftime("%s"))
    firsTH = int(firsT.strftime("%H"))
    firsTM = int(firsT.strftime("%M"))
    firsTS = int(firsT.strftime("%S"))
    for it in range(Ndt):
      newmonth = firsTm + 1
      if newmonth > 12: 
        newmonth = 1
        firsTy = firsTy + 1

      newdate = dt.timedate(firsTy, firsTm, firsTd, firsTH, firsTM, firsTS)
      cdodates=cdodates + ',' + newdate.strftime("%Y-%m-%d")

  print cdodates
# timeINF
# 
elif opts.oper == 'timeINF':
  print Ntimes, dT, firstT, firstTS, lasttT, lastTS
##  print np.array([Ntimes, dT, firstT])
