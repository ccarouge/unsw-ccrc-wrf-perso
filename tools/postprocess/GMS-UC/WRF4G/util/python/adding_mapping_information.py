## Writting mapping information in the netCDF file as an empty variable
##
## e.g. # python WRF4G/util/python/adding_maping_information.py -f 123/CCRC_NARCliM_Sydney_DAM_1990-1999_tasmin.nc -m wrfncxnj.projection_CCRC_NARCliM_Sydney -v tasmin 

import numpy as np
from netCDF4 import Dataset as NetCDFFile
from optparse import OptionParser
import time
import os
import re

def ProjectName(gridvalue):
# Function to provide the name of the variable with which the projection will be known:
#   Following CF-conventions: http://cf-pcmdi.llnl.gov/documents/cf-conventions/1.6/cf-conventions.html#appendix-grid-mappings
  if gridvalue == 'albers_conical_equal_area':
    GridMapName = 'Albers_Equal_Area'
  elif gridvalue == 'azimuthal_equidistant':
    GridMapName = 'Azimuthal_Equidistant'
  elif gridvalue == 'lambert_azimuthal_equal_area':
    GridMapName = 'Lambert_azimuthal_equal_area'
  elif gridvalue == 'lambert_conformal_conic':
    GridMapName = 'Lambert_Conformal'
  elif gridvalue == 'lambert_cylindrical_equal_area':
    GridMapName = 'Lambert_Cylindrical_Equal_Area'
  elif gridvalue == 'latitude_longitude':
    GridMapName = 'Latitude_Longitude'
  elif gridvalue == 'mercator':
    GridMapName = 'Mercator'
  elif gridvalue == 'orthographic':
    GridMapName = 'Orthographic'
  elif gridvalue == 'polar_stereographic':
    GridMapName = 'Polar_stereographic'
  elif gridvalue == 'rotated_latitude_longitude':
    GridMapName = 'Rotated_pole'
  elif gridvalue == 'stereographic':
    GridMapName = 'Stereographic'
  elif gridvalue == 'transverse_mercator':
    GridMapName = 'Transverse_Mercator'
  elif gridvalue == 'vertical_perspective':
    GridMapName = 'Vertical_perspective'

  return GridMapName

def searchInlist(listname, nameFind):
  for x in listname:
    if x == nameFind:
      return True
  return False

def set_attribute(ncvar, attrname, attrvalue):
    """ Sets a value of an attribute of a netCDF variable
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)

    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

### Options

parser = OptionParser()
parser.add_option("-f", "--file", dest="netcdffile",
                  help="netcdf file to add mapping information", metavar="FILE")
parser.add_option("-m", "--mapping_file", dest="mappingfile", 
                  help="ASCII file with the mapping attributes (at least with grid_mapping_name", metavar="FILE")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to add mapping information", metavar="VARIABLE")
(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######
errormsg='ERROR -- error -- ERROR -- error'
varn=opts.varname

# grid mapping information
#
if not os.path.isfile(opts.mappingfile):
  print errormsg
  print '  File ' + opts.mappingfile + ' does not exist !!'
  print errormsg
  quit()    

mapf = open(opts.mappingfile, 'r')

if not os.path.isfile(opts.netcdffile):
  print errormsg
  print '  File ' + opts.netcdffile + ' does not exist !!'
  print errormsg
  quit()    

ncf = NetCDFFile(opts.netcdffile,'a')

if ncf.dimensions.has_key('plev'):
  # removing pressure level from variable name
  varn = re.sub("\d+", "", varn) 

attrvalues = []
for fileline in mapf:
  line=fileline.replace('\n','')
  values = line.split(' ')
  if len(values) > 1:
    attrvalues.append(values)

Nlines = len(attrvalues)

posMapname = attrvalues[0][:].index('grid_mapping_name')
projstdname = attrvalues[posMapname][1]

MapProjName = ProjectName(projstdname)

# Creation of the variable with the porjection information
#
if not ncf.variables.has_key(MapProjName):
  mapgridvar = ncf.createVariable(MapProjName, 'c')
else:
  mapgridvar = ncf.variables[MapProjName]

# Adding projection attribute to the existing variable within the netCDF file
#
if ncf.dimensions.has_key('plev'):
  # removing pressure level from variable name
  varn = re.sub("\d+", "", varn) 

var = ncf.variables[varn]

varattributes = var.ncattrs()
if searchInlist(varattributes, 'grid_mapping'):
  attr = var.delncattr('grid_mapping')

attr = var.setncattr('grid_mapping', MapProjName)

for il in range(Nlines):
  varmapgridattrs = mapgridvar.ncattrs()
  if searchInlist(varmapgridattrs,attrvalues[il][0]):
    attr = mapgridvar.delncattr(attrvalues[il][0])

  if attrvalues[il][2] == 'S':
    attrvals=attrvalues[il][1].split('@')
    mapgridvar = set_attribute(mapgridvar, attrvalues[il][0], ' '.join(attrvals))
  elif attrvalues[il][2] == 'I':
    attrvals=int(attrvalues[il][1])
    mapgridvar = set_attribute(mapgridvar, attrvalues[il][0], attrvals)
  elif attrvalues[il][2] == 'R':
    attrvals=float(attrvalues[il][1])
    mapgridvar = set_attribute(mapgridvar, attrvalues[il][0], attrvals)
  elif attrvalues[il][2] == 'D':
    attrvals=np.float64(attrvalues[il][1])
    mapgridvar = set_attribute(mapgridvar, attrvalues[il][0], attrvals)
  else:
    print errormsg
    print '  ' + attrvalues[il][2] + ' kind of attribute is not ready!'
    quit()

#    attr = mapgridvar.setncattr(attrvalues[il][0], attrvalues[il][1])
  

histval = ncf.getncattr('history')
newhist = time.ctime(time.time()) + ': adding_mapping_information.py -f ' + opts.netcdffile + ' -m ' + opts.mappingfile + ' -v ' + opts.varname + '\n' + histval
newh = ncf.setncattr('history', newhist)

ncf.sync()
ncf.close()
