EL_MYJ       mxln      "Mixing length in MYJ scheme"                      "mixing_length_in_myj"                                   "m"   
GLW          rlds      "Downward LW surface radiation"                    "surface_downwelling_longwave_flux_in_air"               "W m-2"       
GRDFLX       hfso      "Upward soil heat flux"                            "upward_heat_flux_at_ground_level_in_soil"               "W m-2" 
HFX          hfss      "Upward sensible heat flux at surface"             "surface_upward_sensible_heat_flux"                      "W m-2" 
HGT          orog      "Surface altitude"                                 "surface_altitude"                                       "m"       
LH           hfls      "Upward latent heat flux at surface"               "surface_upward_latent_heat_flux"                        "W m-2" 
LWUPT        rlnt      "Net upward longwave flux at TOA"                  "toa_net_upward_longwave_flux"                           "W m-2" 
OLR          rlut      "Outgoing LW radiation at top of atmosphere"       "toa_outgoing_longwave_flux"                             "W m-2" 
PBLH         zmla      "Atmosphere_boundary_layer_thickness"              "atmosphere_boundary_layer_thickness"                    "m"     
POTEVP       potevp    "Acummulated potential evaporation"                "water_potential_evaporation_amount"                     "W m-2"   
PSFC         ps        "surface pressure"                                 "surface_air_pressure"                                   "Pa"      
Q2           huss      "Surface specific humidity"                        "specific_humidity"                                      1         screenvar_at_2m
QCLOUD       clwmr     "Cloud water content"                              "cloud_liquid_water_mixing_ratio"                        1         
QFX          hufs      "Upward moisture flux at the surface"              "surface_upward_water_vapor_flux_in_air"                 "kg m-2 s-1" 
QVAPOR       hus       "Specific Humidity"                                "specific_humidity"                                      1         
RAINNC       prls      "Precipitation large scale"                        "large_scale_precipitation_amount"                       "Kg m-2" deaccumulate 
RAINC        prc       "Precipitation convective"                         "convective_precipitation_amount"                        "Kg m-2" deaccumulate 
REGIME       regime    "Stability regime (1=Night/Stable, 2=Mech. Turb., 3=Forced Conv, 4=Free Conv)"  "stability_regime"                       1        
SFCEVP       evspsbl   "Evaporation"                                      "water_evaporation_flux"                                 "kg m-2 s-1" 
SFROFF       mrros     "Surface runoff"                                   "surface_runoff_flux"                                    "kg m-2 s-1" 
SMOIS        mrsos     "Moisture of soil layer"                           "moisture_content_of_soil_layer"                         "kg m-2"   
SMSTOT       mrso      "Total soil moisture content"                      "soil_moisture_content"                                  "kg m-2"  SMSTOT*1000. 
SNOWH        snd       "Snow depth"                                       "surface_snow_thickness"                                 "m"
SST          sst       "sea surface temperature"                          "sea_surface_temperature"                                "K"
SWDOWN       rsds      "Downward SW surface radiation"                    "surface_downwelling_shortwave_flux_in_air"              "W m-2" 
SWUPT        rsnt      "Net upward shortwave flux at TOA"                 "toa_net_upward_shortwave_flux"                          "W m-2" 
T2           tas       "Surface air temperature"                          "air_temperature"                                        "K"       screenvar_at_2m
TSLB         tsos      "Soil temperature"                                 "soil_temperature"                                       "K"
TSK          ts        "Surface skin temperature"                         "surface_temperature"                                    "K"
TKE_MYJ      tkemyj    "Specific turbulent kinetic Energy in MYJ scheme"  "specific_turbulent_kinetic_energy_in_myj"               "m 2 s-2" 
U10          uascurv   "X surface wind"                                   "x_wind"                                                 "m s-1"   
UDROFF       mrfso     "Subsurface runoff"                                "subsurface_runoff"                                      "kg m-2 s-1"
V10          vascurv   "Y surface wind"                                   "y_wind"                                                 "m s-1"  
UST          ustar     "Frictional velocity in similarity theory"         "frictional_velocity_in_similarity_theory"               "m s-1"   
W            wa        "Upward air velocity"                              "upward_air_velocity"                                    "m s-1"     
Z0           z0        "Surface roughness length"                         "surface_roughness_length"                               "m"       
#
# From geo_em file
#
HGT_M        orog      "Surface altitude"                                 "surface_altitude"                                       "m"       
LANDMASK     sftlf     "Land mask"                                        "land_binary_mask"                                       1         
LU_INDEX     lco       "Land use"                                         "land_use"                                               1
#
# From p_interp
#
CLT          clt       "Total cloud area(fraction)"                       "cloud_area_fraction"                                    1        
GHT          zg        "Geopotential height"                              "geopotential_height"                                    "m"       
MSLP         psl       "Mean sea level pressure"                          "air_pressure_at_sea_level"                              "Pa"      
RAINTOT      pr        "Precipitation"                                    "precipitation_amount"                                   "Kg m-2"  
RH           hur       "Relative humidity"                                "relative_humidity"                                      1         RH*0.01 
VIM          prw       "Column water vapour content"                      "atmosphere_water_vapor_content"                         "kg m-2"
VIQC         cllwvi    "Column liquid water content"                      "atmosphere_cloud_liquid_water_content"                  "kg m-2"
VIQI         clivi     "Column ice water content"                         "atmosphere_cloud_ice_content"                           "kg m-2"
#
# From CLWRF
#
Q2MAX        hursmax   "Max. surface relative humidity"                   "relative_humidity"                                      1         screenvar_at_2m 
Q2MIN        hursmin   "Min. surface relative humidity"                   "relative_humidity"                                      1         screenvar_at_2m 
RAINMAX1H    prhmax    "Max. hourly precipitation rate"                   "precipitation_flux"                                     "kg m-2 s-1"
SKINTEMPMAX  tsosmin   "Max. surface skin temperature"                    "soil_skin_emperature"                                   "K"
SKINTEMPMIN  tsosmin   "Min. surface skin temperature"                    "soil_skin_temperature"                                  "K"
SPDUV10MAX   wssmax    "Max. surface wind speed"                          "air_velocity"                                           "m s-1"   screenvar_at_10m
SUNSHINE     sund      "Sunshine duration"                                "sunshine_duration"                                      "minutes"
T2MAX        tasmax    "Max. surface air temperature"                     "air_temperature"                                        "K"       screenvar_at_2m
T2MEAN       tasmean   "Mean surface air temperature"                     "air_temperature"                                        "K"       screenvar_at_2m
T2MIN        tasmin    "Min. surface air temperature"                     "air_temperature"                                        "K"       screenvar_at_2m
WINDGUSTMAX  wgsmax    "Max. wind gust"                                   "wind_speed_of_gust"                                     "m s-1" 
#
# Derived variables
#
ACLHF        ahfls     "Latent heat flux at surface"                      "surface_upward_latent_heat_flux"                        "W m-2"  deaccumulate_flux
ACHFX        ahfss     "Heat flux at the surface"                         "surface_upward_sensible_heat_flux"                      "W m-2"  deaccumulate_flux
ACRLS        rls       "Net LW surface radiation "                        "surface_net_downward_longwave_flux"                     "W m-2"  deaccumulate_flux
ACLWUPB      rlus      "Upwelling surface LW radiation"                   "surface_upwelling_longwave_flux_in_air"                 "W m-2"  deaccumulate_flux
ACSNOM       snm       "Snow melt"                                        "surface_snow_melt_flux"                                 "kg m-2 s-1" deaccumulate_flux
ACSNOW       snw       "Accumulated snow"                                 "surface_snow_amount"                                    "kg m-2" deaccumulate
ACSWDNT      rsdt      "incoming/downward SW radiation at top of atmosphere" "toa_incoming_shortwave_flux"                         "W m-2"  deaccumulate_flux
ACSWUPB      rsus      "Upwelling surface SW radiation"                   "surface_upwelling_shortwave_flux_in_air"                "W m-2"  deaccumulate_flux
ACSWUPT      rsut      "Outgoing SW radiation at top of atmosphere"       "toa_outgoing_shortwave_flux"                            "W m-2"  deaccumulate_flux
CLD          clwi      "Total cloud water and ice"                        "cloud_water_and_ice_content"                            "kg m-2"  
CLISCCP      clisccp   "clouds (1: p<440 hPa, 2: 660<p<440, 3: p>660)     "cloud"                                                  1  
CLWVI        clwvi     "Column condensed water content"                   "atmosphere_cloud_condensed_water_content"               "kg m-2"
GEOP         geop      "Geopotential"                                     "geopotential"                                           "m 2 s-2"  
MRFSO        mrfso     "Soil frozen water content"                        "soil_frozen_water_content"                              "kg m-2"
MRSO         mrso      "Total soil moisture content"                      "soil_moisture_content"                                  "kg m-2"   
PRES         pres      "Air pressure"                                     "air_pressure"                                           "Pa"      
PRW          prw       "Column water vapour content"                      "atmosphere_water_vapor_content"                         "kg m-2"     
RH2          hurs      "Relative humidty at 2M"                           "relative_humidity"                                      1         screenvar_at_2m
RAINF        pr        "Precipitation flux"                               "precipitation_flux"                                     "Kg m-2 s-1"
RAIN         pr        "Accumulated precipitation"                        "precipitation_amount"                                   "Kg m-2"
#TODO: RAINM1H      prhmax    "Max. hourly precipitation rate"                   "precipitation_flux"                              "kg m-2 s-1"
RAINFORWARD  pr        "Precipitation"                                    "precipitation_amount"                                   "Kg m-2"  
#TODO: RLS          rls       "Net LW surface radiation "                        "surface_net_downward_longwave_flux"                     "W m-2"   "EMISS*GLW - 5.67e-8*EMISS*SKINTEMP**4"
RLT          rlt       "Net downward flux of LW radiation at top of atmosphere"  "toa_net_downward_longwave_flux"                  "W m-2"
RSS          rss       "Net SW surface radiation"                         "surface_net_downward_shortwave_flux"                    "W m-2"   
RST          rst       "Net downward flux of SW radiation at top of atmosphere"  "toa_net_downward_longwave_flux"                  "W m-2"
SNW          prsn      "Snowfall"                                         "snowfall_flux"                                          "kg m-2 s-1" 
SMOIS1       mrsos1    "Moisture of soil layer 1"                         "moisture_content_of_soil_layer"                         "kg m-2"   
TEMP         ta        "Air temperature"                                  "air_temperature"                                        "K"       
TDPS         tdps      "Dew point temperature at 2M"                      "dew_point_temperature"                                  "K"       screenvar_at_2m
U10ER        uas       "Eastward surface wind"                            "eastward_wind"                                          "m s-1"    rotate_uas 
U10MAXER     uasmax    "Max. eastward surface wind"                       "eastward_wind"                                          "m s-1"    rotate_uas 
U10MEANER    uasmean   "Mean eastward surface wind"                       "eastward_wind"                                          "m s-1"    rotate_uas 
UER          ua        "Eastward wind"                                    "eastward_wind"                                          "m s-1"   
V10ER        vas       "Northward surface wind"                           "northward_wind"                                         "m s-1"    rotate_vas
V10MAXER     vasmax    "Max. northward surface wind"                      "northward_wind"                                         "m s-1"    rotate_vas 
V10MEANER    vasmean   "Mean northward surface wind"                      "northward_wind"                                         "m s-1"    rotate_vas 
VER          va        "Northward wind"                                   "northward_wind"                                         "m s-1"   
WIND         wspd      "wind speed"                                       "wind_speed"                                             "m s-1"   
