PROGRAM nc_wrt_attr
! Program to add an attribute to an existing netCDF file
! Nov. 2011
! Lluis Fita. Universidad de Cantabria,  Grupo de Meteorologia de Santander, Santander, Spain

! WRF4G by Santander Meteorology Group is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. 
! Permissions beyond the scope of this license may be available at http://www.meteo.unican.es/software/wrf4g.

! trueno
! export NETCDF_ROOT='/home/lluis/bin/netcdf-4.0.1'
! gfortran nc_wrt_attr.f90 -L${NETCDF_ROOT}/lib -lnetcdf -lm -I${NETCDF_ROOT}/include -o nc_wrt_attr

! ui01
! export NETCDF_ROOT='/software/CentOS/5.2/netcdf/4.1.3/gcc_4.1.2_NOpnetcdf' 
! gfortran nc_wrt_attr.f90 -g -O2 -L${NETCDF_ROOT}/lib -lnetcdf -lnetcdff -lm -I${NETCDF_ROOT}/include -o nc_wrt_attr

! ccrc468-17
! export NETCDF_ROOT='/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/'
! gfortran nc_wrt_attr.f90 -g -O2 -L${NETCDF_ROOT}/lib -lnetcdf -lnetcdff -lm -I${NETCDF_ROOT}/include -o nc_wrt_attr

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'
  
! Arguments
  INTEGER                                                :: iarg, narg
  CHARACTER(LEN=500), ALLOCATABLE, DIMENSION(:)          :: arguments
  CHARACTER(LEN=500)                                     :: file, attrname, attrvalue, varname
  CHARACTER(LEN=2)                                       :: attrtype, attrkind

! netcdf
  INTEGER                                                :: idim, rcode, ncid, ivar
  INTEGER                                                :: Vid, Vnatts, Gnatts
  CHARACTER(LEN=100), DIMENSION(6)                       :: DIMname

! Values
  INTEGER                                                :: attrvalueI
  REAL                                                   :: attrvalueR
  DOUBLE PRECISION                                       :: attrvalueD
  CHARACTER(LEN=500)                                     :: attrvalueS

 
!!!!!!! Variables
! argument(1): file name
! argument(2): kind of attribute (-v; variable, -g; global)
! argument(3): Attribute name
! argument(4): Kind (I: integer, R: real, D: double, S: string) of the attribute
! argument(5): Attribute value
! argument(6): Variable name (if argument(2)='-v')
! Vid: Id of the variable
! Vnatts: number of attributes of the variable
! Gnatts: number of attributes of the netcdf file

! Arguments
  narg=COMMAND_ARGUMENT_COUNT()
  IF (ALLOCATED(arguments)) DEALLOCATE(arguments)
  ALLOCATE(arguments(narg))

  DO iarg=1, narg
    CALL GETARG(iarg,arguments(iarg))
  END DO
  IF (TRIM(arguments(1)) == '-h' ) THEN
    PRINT *,"nc_wrt_attr [ncf](netCDF file) [kind](-v, variable; -g, global) [attrname](name of"//&
      " attribute) [kind](I: integer, R: real, D: double, S: string) [value] [var](variable "//   &
      "name if '-v')"
    STOP
  END IF

  file=TRIM(arguments(1))
  attrtype=TRIM(arguments(2))
  attrname=TRIM(arguments(3))
  attrkind=TRIM(arguments(4))
!
! kind of attribute
  SELECT CASE (attrkind)
    CASE ('I')
      READ((arguments(5)), "(I20)")attrvalueI
    CASE ('R')
      READ((arguments(5)), "(F20.10)")attrvalueR
    CASE ('D')
      READ((arguments(5)), "(D20.10)")attrvalueD
    CASE ('S')
      attrvalueS=arguments(5)
  END SELECT
  IF (attrtype == '-v') THEN
    IF (narg == 6) THEN
      varname=TRIM(arguments(6))
    ELSE
      PRINT *,"A variable name is required for '-v'"
      STOP
    END IF
  END IF
  
  DEALLOCATE(arguments)
 
  rcode = nf90_open(file, NF90_WRITE, ncid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
  rcode = nf90_redef(ncid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
!
! Global attribute
  IF (attrtype == '-g') THEN
    rcode = nf90_inquire(ncid, nAttributes=Gnatts)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    SELECT CASE (attrkind)
      CASE ('I')
        rcode = nf90_put_att(ncid, NF90_GLOBAL, TRIM(attrname), attrvalueI)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('R')
        rcode = nf90_put_att(ncid, NF90_GLOBAL, TRIM(attrname), attrvalueR)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('D')
        rcode = nf90_put_att(ncid, NF90_GLOBAL, TRIM(attrname), attrvalueD)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('S')
        rcode = nf90_put_att(ncid, NF90_GLOBAL, TRIM(attrname), attrvalueS)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE DEFAULT
        PRINT *,"attrkind= '"//TRIM(attrkind)//"' not allowed"
	STOP
    END SELECT 
!
! variable attribute
  ELSE IF (attrtype == '-v' ) THEN
    rcode = nf90_inq_varid(ncid, varname, Vid)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    rcode = nf90_inquire_variable(ncid, varid=Vid, nAtts=Vnatts)    
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    SELECT CASE (attrkind)
      CASE ('I')
        rcode = nf90_put_att(ncid, Vid, TRIM(attrname), attrvalueI)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('R')
        rcode = nf90_put_att(ncid, Vid, TRIM(attrname), attrvalueR)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('D')
        rcode = nf90_put_att(ncid, Vid, TRIM(attrname), attrvalueD)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE ('S')
        rcode = nf90_put_att(ncid, Vid, TRIM(attrname), attrvalueS)
        IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)  
      CASE DEFAULT
        PRINT *,"attrkind= '"//TRIM(attrkind)//"' not allowed"
	STOP
    END SELECT 
  ELSE
    PRINT *,"Operation with '"//attrtype//"' is not ready"
    STOP
  END IF
  
  rcode = nf90_close(ncid)
 5 FORMAT(I4)

END PROGRAM nc_wrt_attr
