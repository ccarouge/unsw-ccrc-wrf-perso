#!/bin/bash
if test $1 = '-h'; then
  echo "************************"
  echo "*** Shell to process ***"
  echo "***  A series of wrf ***"
  echo "***   output files   ***"
  echo "************************"
  echo "process.bash [environfile] [expname] [var] [thelevels] [theperiods] [do_nothing] [oper]"
  echo "  [environfile]: file with the definition of the environment of the postprocess"
  echo "  [expname]: Name of the experiment to be written on output files "
  echo "  [var]: WRF name of the variable "
  echo "  [thelevels]: Required vertical levels ('SFC' for surface)"
  echo "  [theperiods]: Period between has to be run the postprocess "
  echo "  [do_nothing]: flag that indicates 'do nothing' "
  echo "  [oper]: different operations to be done on files (sum of values)"
  echo "     compress: 1"
  echo "     verify: 2"
  echo "     internal netcdf4 compress: 4"
else

####### Functions
# rewrite_attributes_py: Function to re-write netCDF attributes given wrfncxnj table file using python
# rewrite_attributes: Function to re-write netCDF attributes given wrfncxnj table file
# decimal2binary: Transforms a number to its binary in base 2
# check_execution: Checking if the execution of a program has worked

function rewrite_attributes_py() {
# Function to re-write netCDF attributes given wrfncxnj table file using python
  ncfile=$1
  wrfncxnjtable=$2
  varname=$3
  newvarname=$4
  c1='#'
  c2=\"

  nlinevar=`cat -n ${wrfncxnjtable} | awk '{print " "$3" "$1}' | grep ' '${newvarname}' ' | awk '{print $2}' | head -n 1`
  varline=`head -n ${nlinevar} ${wrfncxnjtable} | tail -n 1`
  varline_tmp1=`echo ${varline} | tr ' ' ':' | sed s/${c2}/${c1}/g`
  ilongname=`expr index ${varline_tmp1} ${c1}`
  varline_tmp2=`leave_firstN $varline_tmp1 $ilongname`
  elongname=`expr index ${varline_tmp2} ${c1}`
  elname1=`expr ${elongname} - 1`
  longname=`expr substr ${varline_tmp2} 1 ${elname1}`
  elname1=`expr ${elongname} + 1`
  varline_tmp3=`leave_firstN $varline_tmp2 $elname1`
  istdname=`expr index ${varline_tmp3} ${c1}`
  varline_tmp4=`leave_firstN $varline_tmp3 $istdname`
  estdname=`expr index ${varline_tmp4} ${c1}`
  estdname1=`expr ${estdname} - 1`
  stdname=`expr substr ${varline_tmp4} 1 ${estdname1}`
  estdname1=`expr ${estdname} + 1`
  varline_tmp5=`leave_firstN $varline_tmp4 $estdname1`
  units=`echo ${varline_tmp5} | tr '#' ' ' | awk '{print $1}'`
  python ${WRF4Ghome}/util/python/nc_var.py -f ${ncfile} -o addvattrk -v ${varname} -S long_name|$(echo ${longname} | tr ':' '!')|S
  python ${WRF4Ghome}/util/python/nc_var.py -f ${ncfile} -o addvattrk -v ${varname} -S standard_name|$(echo ${stdname} | tr ':' '!')|S
  python ${WRF4Ghome}/util/python/nc_var.py -f ${ncfile} -o addvattrk -v ${varname} -S units|$(echo ${units} | tr ':' '!')|S
  python ${WRF4Ghome}/util/python/nc_var.py -f ${ncfile} -o mname -v ${varname} -S ${newvarname}
}

function rewrite_attributes() {
# Function to re-write netCDF attributes given wrfncxnj table file
  ncfile=$1
  wrfncxnjtable=$2
  varname=$3
  newvarname=$4
  c1='#'
  c2=\"

  nlinevar=`cat -n ${wrfncxnjtable} | awk '{print " "$3" "$1}' | grep ' '${newvarname}' ' | awk '{print $2}' | head -n 1`
  varline=`head -n ${nlinevar} ${wrfncxnjtable} | tail -n 1`
  varline_tmp1=`echo ${varline} | tr ' ' ':' | sed s/${c2}/${c1}/g`
  ilongname=`expr index ${varline_tmp1} ${c1}`
  varline_tmp2=`leave_firstN $varline_tmp1 $ilongname`
  elongname=`expr index ${varline_tmp2} ${c1}`
  elname1=`expr ${elongname} - 1`
  longname=`expr substr ${varline_tmp2} 1 ${elname1}`
  elname1=`expr ${elongname} + 1`
  varline_tmp3=`leave_firstN $varline_tmp2 $elname1`
  istdname=`expr index ${varline_tmp3} ${c1}`
  varline_tmp4=`leave_firstN $varline_tmp3 $istdname`
  estdname=`expr index ${varline_tmp4} ${c1}`
  estdname1=`expr ${estdname} - 1`
  stdname=`expr substr ${varline_tmp4} 1 ${estdname1}`
  estdname1=`expr ${estdname} + 1`
  varline_tmp5=`leave_firstN $varline_tmp4 $estdname1`
  units=`echo ${varline_tmp5} | tr '#' ' ' | awk '{print $1}'`
  ncatted -a long_name,${varname},o,c,"$(echo ${longname} | tr ':' ' ')" ${ncfile}
  ncatted -a standard_name,${varname},o,c,"$(echo ${stdname} | tr ':' ' ')" ${ncfile}
  ncatted -a units,${varname},o,c,"$(echo ${units} | tr ':' ' ')" ${ncfile}

  cdo setname,${newvarname} ${ncfile} tmpA.nc
  mv tmpA.nc ${ncfile}
}

function decimal2binary() {
# Transforms a number to its binary in base 2
#
# output formats:
#   base2: String with bits from 2^[nbits-1] to 1
#   pot2: decomposition of number in 2 power series: 2^[nbits-1] 2^[nbits-2] ...
#      2^[0] 

  decimal=$1
  nbits=$2
  oformat=$3

# Checking range
##
  nbit0=`expr $nbits`
  maximbit=`echo "2 "$nbit0 | awk '{print $1^$2}'`
  if test $maximbit -le $1
  then
    echo "ERROR -- error -- ERROR: Number "$decimal" is too big for "$nbits\
      " nbits!!!!!!"
    exit
  fi

# Giving bits
##
  diff=$decimal
  base2=''
  pot2=''
  ibit=`expr $nbits - 1`
  while test $ibit -ge 0
  do
    bit=`echo "2 "${ibit} | awk '{print $1^$2}'`
    if test $bit -le $diff
    then
      diff=`(expr $diff - $bit)`
#      echo "bit "$ibit": "$bit" dif= "$diff
      base2=$base2"1"
      pot2=$pot2" "$bit
    else
      base2=$base2"0"
    fi
    ibit=`(expr $ibit - 1)`
  done
  if test $diff -le 0
  then

#    echo "Decomposition ended"
##
    case $oformat in
      'pot2')
        echo $pot2
      ;;
      'base2')
        echo $base2
      ;;
      esac
#    exit

  else
    echo "Error !!!!"
    echo "Decomposition not ended!"$diferencia
    exit
  fi
}

function check_execution() {
# Checking if the execution of a program has worked
  oexec=$1
  program=$2

  errmsg='process.bash: ERROR -- error -- ERROR -- error'

  if test ! $oexec -eq 0; then
    echo ${errmsg}" "${oexec}
    echo "  execution failed!"
    echo "  "${program}
    exit
  fi
}

#######    #######
## MAIN
    #######

  environfile=$1
  expname=$2
  var=$3
  thelevels="$4"
  theperiods="$5" # e.g: 1989-1993,1994-1997
  namelst=$6
  do_nothing=$7 # .true. it is a 'do_nothing' variable flag
  oper=$8

  source ${environfile}
  thetimes='All'
  freqname='All'

  thisdir=`pwd`
  mkdir -p ${POST2DIR}/procesa_files
  cd ${POST2DIR}/procesa_files
  test "${thelevels}" != "SFC" && splitlevel="splitlevel" || splitlevel=""

  operbits=`decimal2binary ${oper} 3 base2`
  compress=${operbits:2:1}
  checking=${operbits:1:1}
  netcdfcomp=${operbits:0:1}

  ovar=$var
 
  echo "Processing periods '"${theperiods}"' file of '"$ovar"'"
  for per in ${theperiods//,/ }; do
    rm -f procesa_??????_*
    echo -n "  Period $per:"
    for yr in $(seq ${per/-/ }); do
      echo -n " $yr"
      for mon in $(seq 1 12); do
        pmon=$(printf "%02d" ${mon})
        if test "${thelevels}" = "SFC" 
        then
          cp ${POSTDIR}/${expname}_${yr}${pmon}.nc ./
          mv ${expname}_${yr}${pmon}.nc \
            procesa_${yr}${pmon}_${var}
        else
          ${CDOhome} -s ${splitlevel} -selvar,${var} ${POSTDIR}/${expname}_${yr}${pmon}.nc \
            ${POSTDIR}/${expname}_${yr}${pmon}_Hfrq_${var}
            for lev in ${thelevels/,/ }; do
              level=`printf "%04d" ${lev}`
              mv ${POSTDIR}/${expname}_${yr}${pmon}_Hfrq_${var}${level}'00'.nc \
                ${POSTDIR}/procesa_${yr}${pmon}_${var}${level}.nc
            done
        fi
      done # mon
    done # yr
    pwd
    if test "${thelevels}" = "SFC"; then
      echo "  Processing only the surface value (or all levels together...)"
      ofile="${POST2DIR}/${expname}_${freqname}_${per}_${ovar}.nc"
      rm -f ${ofile}.gz ${ofile} >& /dev/null
      ${CDOhome} -s cat procesa_??????_${var} ${ofile}
      if ${do_nothing}
      then
        rewrite_attributes_py ${ofile} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${var} ${ovar} 
      fi
      if test $netcdfcomp -eq 1; then 
        ${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc
        check_execution $? "$*"
        mv tmp.nc ${ofile}
      else
        ${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc
        check_execution $? "$*"
        mv tmp.nc ${ofile}
      fi
# Writting projection information
#
      python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile} -m ${projectionFILE} -v ${ovar}
# Re-writting time_bounds
#
##      python ${WRF4Ghome}/util/python/rename_dimension.py -d gsize,bnds -f ${ofile} -v ${ovar}
      python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds 
      python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds 

      if test ${checking} -eq 1; then
        ${ROOTpost}/checking_file.bash ${ofile} ${FULLnc} ${ptstime} ${VarnameSecFile} ${namelst} time ${Rhome} ${CDOhome} ${WRF4Ghome}
      fi
      if test ${compress} -eq 1; then 
        rm ${ofile}.gz
        gzip ${ofile}
      fi
    else
      echo -n "  Levels:"
      for level in ${thelevels//,/ }; do
        echo -n " $level"
        plevel=`printf "%04d" ${level}`
        ofile="${POST2DIR}/${expname}_${freqname}_${per}_${ovar}${plevel}.nc"
        rm -f ${ofile}.gz ${ofile} >& /dev/null
        ${CDOhome} -s cat ${POSTDIR}/procesa_??????_${var}${plevel}.nc ${ofile}
        if ${do_nothing}
        then
          rewrite_attributes_py ${ofile} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${var} ${ovar}   
        fi
# Writting projection information
#
        python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile} -m ${projectionFILE} -v ${ovar}
# Re-writting time_bounds
#
##        python ${WRF4Ghome}/util/python/rename_dimension.py -d gsize,bnds -f ${ofile} -v ${ovar}
        python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds 
        python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds 
        if test $netcdfcomp -eq 1; then 
          ${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc
          check_execution $? "$*"
          mv tmp.nc ${ofile}
        else
          ${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc
          check_execution $? "$*"
          mv tmp.nc ${ofile}
        fi
        if test ${checking} -eq 1; then
          ${ROOTpost}/checking_file.bash ${ofile} ${FULLnc} ${ptstime} ${VarnameSecFile} ${namelst} time ${Rhome} ${CDOhome} ${WRF4Ghome}
        fi
        if test ${compress} -eq 1; then 
          rm ${ofile}.gz
          gzip ${ofile}
        fi
      done # level
    fi
    rm -f procesa_??????_*
  done # per
  cd ${thisdir}
fi
