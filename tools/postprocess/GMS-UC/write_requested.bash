#!/bin/bash 
## g.e. # ./write_requested.bash Sydney_test.pre"
if test $1 = '-h'; then
  echo "**************************"
  echo "*** Shell to downwrite ***"
  echo "***   requested file   ***"
  echo "**************************"
  echo "write_requested.bash [fileInf](file with information)"
  echo "[fileInf]___________________"
  echo "  BASHsource [value](full path)"
  echo "  WRF4Ghome [value](full path)"
  echo "  p_interp [value](full path)"
  echo "  DomainNetCDF [value](full path)"
  echo "  EXPDIR [value](full path)"
  echo "  EXPinterpDIR [value](full path)"
  echo "  POSTFOLD [value](folder where will be make:)"
  echo "     EXPinterpDIR ([POSTFOLD]/interp)"
  echo "     POSTDIR ([POSTFOLD]/postA)"
  echo "     POST2DIR ([POST2DIR]/postHFRQ)"
  echo "     POSTPROJDIR ([POSTPROJDIR]/postPROJ)"
  echo "  postprocessor [value](name)"
  echo "  FULLnc [value](full path)"
  echo "  FirstEXPy [value]([YYYY] format)"
  echo "  LastEXPy [value]([YYYY] format)"
  echo "  overlap_yearmon [value]([YYYY][MM] format)"
  echo "  checking [value](0: No, 1: yes)"
  echo "  VarnameSecFile [value](section with the name of the variable)"
  echo "  ptstime [value](2 points where to check [lon1]:[lat1]:[lon2]:[lat2])"
  echo "  Rhome [value](location of executable R)"
  echo "  CDOhome [value](location of executable cdo)"
  echo "  ncHOME [value](root of netCDF libraries)"
  echo "  DOMAINsim [value]"
  echo "  envFile [value](it will be automatically created)"
  echo "  NAMELISToutput [value](full path)"
  echo "  RefDate [value]([YYYY]-[MM]-[DD]_[HH]:[MI]:[SS] format)" 
  echo "  PROCESSout [value] (sum of: 1, compress; 2, verify; 4, internal netcdf4 compress)"
  echo "  HFRQout [value] (sum of: 1, zip compress; 2, internal netCDF compress)"
  echo "  PROJout [value] (sum of: 1, zip compress; 2, internal netCDF compress)"
  echo "  cleanAll [value] (0: do nothing; 1: remove all previous files)"
  echo "  cleanINDIV [value] (0: do nothing; 1: remove variable specific previous files)"
  echo "  experimentName [value]"
  echo "  Period [value]([YYYYi]-[YYYYf], format)"
  echo "  PeriodFrequencies [value]([freq1]:[freq2]:[freq3]:[freq4])"
  echo "  VarlistFile [value](full path to file with the list of variables to post-process)"
  echo "  UserMail [value] mail direction to which a message will be send when post-processed will finish"
else

function periods() {
#  write periods of a requested file
  iniyr=$1
  endyr=$2
  yrfreqsl=$3
  perf=$4

  yrfreqs=`echo ${yrfreqsl} | tr ':' ' '`

  iper=1
  for yrfreq in ${yrfreqs}; do
    iyr=$iniyr
    if test ${yrfreq} -eq 1; then
      nyrfreq=0
    else
      nyrfreq=1
    fi
    while test ${iyr} -le ${endyr}; do
      iyrfrq=`expr ${iyr} + ${yrfreq} - 1`
      if test ${yrfreq} -lt 10; then
        nyrfreqS=`echo ${nyrfreq} | awk '{printf("%02d",$1)}'`
      else
        nyrfreqS=${nyrfreq}
      fi

      echo "@per"${yrfreq}"y"${nyrfreqS}"@ "${iyr}"-"${iyrfrq} >> ${perf}
      iyr=`expr ${iyr} + ${yrfreq}`
      iper=`expr ${iper} + 1`
      nyrfreq=`expr ${nyrfreq} + 1`
    done ## end of years of the period
  done ## end of year frequencies
}

function remove_car() {
# Function to remove a number of characters from a word
  word=$1
  ncar=$2
  where=$3

  Ncar=`echo ${word} | wc -c | awk '{print $1}'`
  Nrest=`expr ${Ncar} - ${ncar} - 1`
  if test ${where} = 'beg'; then
    echo ${word:${ncar}:${Nrest}}
  else
    echo ${word:0:${Nrest}}
  fi
}

#######    #######
## MAIN
    #######
  rootsh=`pwd`
  ifile=$1

  ofile=${rootsh}/'requested_variables.inf'

# Direct variables
##  
  varfilesdirectl='BASHsource:WRF4Ghome:p_interp:DomainNetCDF:EXPDIR:postprocessor:WRFconf:'
  varfilesdirectl=${varfilesdirectl}'FirstEXPy:LastEXPy:overlap_yearmon:checking:VarnameSecFile:ptstime:'
  varfilesdirectl=${varfilesdirectl}'Rhome:CDOhome:ncHOME:DOMAINsim:envFile:NAMELISToutput:RefDate:Calendar:'
  varfilesdirectl=${varfilesdirectl}'PROCESSout:HFRQout:PROJout:cleanAll:cleanINDIV:UserMail'

  varfilesdirect=`echo ${varfilesdirectl} | tr ':' ' '`

  echo "#MainFrame#" > ${ofile}
  for varf in ${varfilesdirect}; do
    valf=`cat ${ifile} | grep ${varf} | awk '{print $2}'`
    echo ${varf}" "${valf} >> ${ofile}
  done # end of direct files

# Full file
##
# If in deck file there is a file, this will file will be used. Otherwise, the first wrfout file in the EXPDIR
  isfull=`cat ${ifile} | grep FULLnc | awk '{print $1}'`
  isfull=${isfull}'0'
  Lisfull=`expr length ${isfull}`
  if test ${Lisfull} -gt 1; then
    fullfile=`cat ${ifile} | grep FULLnc | awk '{print $2}'`
    echo "  A [FULLnc] is provided. Let's use it !"
  else
    expdir=`cat ${ifile} | grep EXPDIR | awk '{print $2}'`
    domn=`cat ${ifile} | grep DOMAINsim | awk '{printf ("%02d",$2)}'`
    filename=`ls -1 ${expdir} | grep wrfout_d${domn}_ | head -n 1 | tail -n 1`
    fullfile=${expdir}"/"${filename}
  fi
  echo "FULLnc "${fullfile} >> ${ofile}

# Folders
##
  valf=`cat ${ifile} | grep POSTFOLD | awk '{print$2}'`
  echo "EXPinterpDIR "${valf}"/interp" >> ${ofile}
  echo "POSTDIR "${valf}"/postA" >> ${ofile}
  echo "POST2DIR "${valf}"/postHFRQ" >> ${ofile}
  echo "POSTPROJDIR "${valf}"/postPROJ" >> ${ofile}
  echo " " >> ${ofile}

# Experiment name
##
  echo "#GeneralNames#" >> ${ofile}
  valf=`cat ${ifile} | grep experimentName | awk '{print$2}'`
  echo "@experiment@ "${valf} >> ${ofile}
  echo " " >> ${ofile}

# period
##
  echo "#Periods#" >> ${ofile}
  iyr=`cat ${ifile} | grep Period | awk '{print$2}' | tr '-' ' ' | awk '{print $1}'`
  eyr=`cat ${ifile} | grep Period | awk '{print$2}' | tr '-' ' ' | awk '{print $2}'`
  yrfreq=`cat ${ifile} | grep PerFrequencies | awk '{print$2}'`
  echo "periods ${iyr} ${eyr} ${yrfreq} ${ofile}"
  periods ${iyr} ${eyr} ${yrfreq} ${ofile}
  echo " " >> ${ofile}

# variables
##
  echo "#Variables#" >> ${ofile}
  valf=`cat ${ifile} | grep VarlistFile | awk '{print$2}'`
  if test ! -f ${valf}; then
    echo ${errmsg}
    echo "  "${valf}" does not exist!"
    exit
  fi

  nlines=`wc -l ${valf} | awk '{print $1}'`
  iline=1
  while test $iline -le $nlines; do
    line=`head -n ${iline} ${valf} | tail -n 1`
    varn=`echo ${line} | tr '!' ' ' | awk '{printf ("%-18s",$1)}'`
    wrfk=`echo ${line} | tr '!' ' ' | awk '{printf ("%-11s",$2)}'`
    levl=`echo ${line} | tr '!' ' ' | awk '{printf ("%-12s",$3)}'`
    stat=`echo ${line} | tr '!' ' ' | awk '{printf ("%-16s",$4)}'`
    perd=`echo ${line} | tr '!' ' ' | awk '{print $5}' | tr '@' ' '`
    echo ${varn} ${wrfk} ${levl} ${stat} ${perd}
    multiper=`expr index ${perd} 'm'`
    uniqper=`expr index ${perd} 'u'`
    compute=`expr ${varn:0:1}`
    first=' '
    if test ${compute} = '#'; then 
      first='# '
      varn=`remove_car ${varn} 1 beg`
      varn=`echo ${varn} | awk '{printf ("%-18s",$1)}'`
    fi
    
    if test ${multiper} -eq 0 && test ${uniqper} -eq 0; then
# Period as single period
      periods=`cat ${ofile} | grep '@per'$(echo ${perd})'y' | awk '{print $1}' | tr '@' ' '`
      for per in ${periods}; do
          cat << EOF >> ${ofile}
${first}${varn}${wrfk}${levl}${stat}${per}
EOF
      done # end of periods
    elif test ${multiper} -ne 0; then
# Period as combination of different consecutive periods
      perdl=`echo ${perd} | tr 'm' ' ' | awk '{print $1}'`
      perdn=`echo ${perd} | tr 'm' ' ' | awk '{print $2}'`
      periods=`cat ${ofile} | grep '@per'$(echo ${perdl})'y' | awk '{print $1}' | tr '@' ' '`
      iper=1
      per=''
      for pers in ${periods}; do
        if test ${iper} -le ${perdn}; then
           if test ${iper} -eq 1; then 
             per=${pers}
           else
             per=${per}','${pers}
           fi
           iper=`expr ${iper} + 1`
        else
          cat << EOF >> ${ofile}
${first}${varn}${wrfk}${levl}${stat}${per}
EOF
          per=${pers}
          iper=2
        fi
      done
      cat << EOF >> ${ofile}
${first}${varn}${wrfk}${levl}${stat}${per}
EOF
    elif test ${uniqper} -ne 0; then
# Unique period of all the possible ones
      perdl=`echo ${perd} | tr 'u' ' ' | awk '{print $1}'`
      perdn=`echo ${perd} | tr 'u' ' ' | awk '{printf("%02d", $2)}'`
      per=`cat ${ofile} | grep '@per'$(echo ${perdl})'y'${perdn} | awk '{print $1}' | tr '@' ' '`
      cat << EOF >> ${ofile}
${first}${varn}${wrfk}${levl}${stat}${per}
EOF
    fi

    iline=`expr ${iline} + 1`
  done # end of file

##  cat ${valf} >> ${ofile}

  echo "SUCCESSFULL -- successfull -- SUCCESSFULL -- successfull"
  echo "  Requested file created! "
  echo "    "${ofile}
  echo "SUCCESSFULL -- successfull -- SUCCESSFULL -- successfull"
fi
