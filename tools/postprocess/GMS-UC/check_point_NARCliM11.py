#!/usr/bin/python
# e.g. # python check_point_NARCliM11.py -v tas:0:0,pr:01H:mean,huss:01H:mean,ps:01H:mean,uas:01H:mean,vas:01H:mean,wss:01H:mean,mrso:03H:mean,prsn:03H:mean,pracc:01H:mean,sst:03H:mean,evspsbl:03H:mean -p 33 33 -P 1990-1999
# e.g. # python check_point_NARCliM11.py -v tas:0:0,pr:01H:mean,huss:01H:mean,ps:01H:mean,uas:01H:mean,vas:01H:mean,wss:01H:mean,mrso:03H:mean,prsn:03H:mean,pracc:01H:mean,sst:03H:mean,evspsbl:03H:mean -p 39 120 -P 1990-1999

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import subprocess as sub
import datetime as dt
import matplotlib as plt
import os

class plotValXY(object):
  """class with all the plotting attributes"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varUnits(self, vals):
    sel.varUnits = vals
  def timeUnits(self, vals):
    sel.timeUnits = vals

class plotValStatXY(object):
  """class with all the plotting attributes of a statistics variable"""
  def __init__(self, vals):
    self.timeVarVal = vals
  def __len__(self):
    return 0
  def varValPT(self, vals):
    self.varValPT = vals
  def varBnds(self, vals):
    self.varBnds = vals 
  def varBndsValt(self, vals):
    self.varBndsValt = vals
  def varBndsValT(self, vals):
    self.varBndsValT = vals

def get_stat_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a statistical variable in a given point
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(filename):
    print errormsg
    print '  File ' + filename + ' does not exist !!'
    print errormsg
    quit()  
 
  print filename

  ncf = NetCDFFile(filename,'r')
  
  if not ncf.variables.has_key(timename):
    print 'File does not have time !!!!'
    quit()
  
  timeVar = ncf.variables[timename]
  valPlot = plotValStatXY(timeVar.getValue())
  
  var = ncf.variables[varname]
  varVal = var.getValue()
  dimt = varVal.__len__() 
  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  else:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]
  
  varbnds = ncf.variables['time_bnds']
  valPlot.varBnds = varbnds
  valPlot.varBndsVal = varbnds.getValue()
  varBndsValt = []
  varBndsValT = []
  for it in range(dimt):
    varBndsValt.append(valPlot.varBndsVal[it,0])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(valPlot.varBndsVal[it,1])
    varBndsValT.append(valPlot.varValPt[it])
    varBndsValt.append(None)
    varBndsValT.append(None)
  
  ncf.close
  valPlot.varBndsValt = varBndsValt
  valPlot.varBndsValT = varBndsValT

#  quit()

  return valPlot

def get_xy(varname, timename, filename, pt):
# Function to get necessary information to plot a variable in a given point
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(filename):
    print errormsg
    print '  File ' + filename + ' does not exist !!'
    print errormsg
    quit()  
 
  print filename

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print 'File does not have time !!!!'
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timeVar.getValue())
  valPlot.timeUnits=getattr(timeVar, 'units')

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if varVal.ndim == 4:
    valPlot.varValPt=varVal[0:dimt,0,pt[0],pt[1]]
  else:
    valPlot.varValPt=varVal[0:dimt,pt[0],pt[1]]

  ncf.close
  return valPlot

def tas_plot(varName):

#
# All
  Varn=varName
  statf = '01H'
  ncAllfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valAll = get_xy(Varn, timename, ncAllfile, point)

# 
# Minimum
  stat='min'
  statf = '24H'
  Varn=varName + stat + 'tstep'
  ncMinfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valMin = get_stat_xy(Varn, timename, ncMinfile, point)

#
# Maximum
  stat='max'
  statf = '24H'
  Varn=varName + stat + 'tstep'
  ncMaxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valMax = get_stat_xy(Varn, timename, ncMaxfile, point)

# 
# Mean
  stat='mean'
  statf = '24H'
  Varn=varName + stat + 'tstep'
  ncMeanfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valMean = get_stat_xy(Varn, timename, ncMeanfile, point)
  
# 
# DAN
  stat='min'
  statf='DAM'
  Varn=varName
  ncDANfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAN = get_stat_xy(Varn + stat, timename, ncDANfile, point)
  
# 
# DAX
  stat='max'
  statf='DAM'
  Varn=varName
  ncDAXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAX = get_stat_xy(Varn + stat, timename, ncDAXfile, point)

# 
# DAM
  stat='mean'
  statf='DAM'
  Varn=varName
  ncDAMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valDAM = get_stat_xy(Varn + stat, timename, ncDAMfile, point)

# 
# MON
  stat='min'
  statf='MOM'
  Varn=varName
  ncMONfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMON = get_stat_xy(Varn + stat, timename, ncMONfile, point)
  
# 
# MOX
  stat='max'
  statf='MOM'
  Varn=varName
  ncMOXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMOX = get_stat_xy(Varn + stat, timename, ncMOXfile, point)

# 
# MOM
  stat='mean'
  statf='MOM'
  Varn=varName
  ncMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + stat + '.nc'
  valMOM = get_stat_xy(Varn + stat, timename, ncMOMfile, point)

# 
# mintstep MOM
  stat='min'
  statf='MOM'
  Varn=varName + stat + 'tstep'
  ncminMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valminMOM = get_stat_xy(Varn, timename, ncminMOMfile, point)

# 
# maxtstep MON
  stat='max'
  statf='MOM'
  Varn=varName + stat + 'tstep'
  ncmaxMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valmaxMOM = get_stat_xy(Varn, timename, ncmaxMOMfile, point)

# 
# meantstep MON
  stat='mean'
  statf='MOM'
  Varn=varName + stat + 'tstep'
  ncmeanMOMfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + Varn + '.nc'
  valmeanMOM = get_stat_xy(Varn, timename, ncmeanMOMfile, point)

#
# Plotting
  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + Varn
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, 'black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  plt.pyplot.plot(valMin.timeVarVal, valMin.varValPt, linestyle='None', marker='1', color='b', label='min')
  plt.pyplot.plot(valMax.timeVarVal, valMax.varValPt, linestyle='None', marker='1', color='r', label='max')
  plt.pyplot.plot(valMean.timeVarVal, valMean.varValPt, linestyle='None', marker='1', color='g', label='mean')
  plt.pyplot.plot(valDAN.timeVarVal, valDAN.varValPt, linestyle='None', marker='2', color='#32FFFF', label='DAN')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', marker='2', color='#FFAAAA', label='DAX')
  plt.pyplot.plot(valDAM.timeVarVal, valDAM.varValPt, linestyle='None', marker='2', color='#AAFFAA', label='DAM')
  plt.pyplot.plot(valMON.timeVarVal, valMON.varValPt, linestyle='None', marker='3', color='#AAAAFF')
  plt.pyplot.plot(valMOX.timeVarVal, valMOX.varValPt, linestyle='None', marker='3', color='#DDAAAA')
  plt.pyplot.plot(valMOM.timeVarVal, valMOM.varValPt, linestyle='None', marker='3', color='#AADDAA')
  plt.pyplot.plot(valminMOM.timeVarVal, valminMOM.varValPt, linestyle='None', marker='4', color='#3232FF')
  plt.pyplot.plot(valmaxMOM.timeVarVal, valmaxMOM.varValPt, linestyle='None', marker='4', color='#643232')
  plt.pyplot.plot(valmeanMOM.timeVarVal, valmeanMOM.varValPt, linestyle='None', marker='4', color='#326432')
  plt.pyplot.plot(valMin.varBndsValt, valMin.varBndsValT, color='b')
  plt.pyplot.plot(valMax.varBndsValt, valMax.varBndsValT, color='r')
  plt.pyplot.plot(valMean.varBndsValt, valMean.varBndsValT, color='g')
  plt.pyplot.plot(valDAN.varBndsValt, valDAN.varBndsValT, color='#32FFFF')
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, color='#FFAAAA')
  plt.pyplot.plot(valDAM.varBndsValt, valDAM.varBndsValT, color='#AAFFAA')
  plt.pyplot.plot(valMON.varBndsValt, valMON.varBndsValT, linestyle=':', color='#AAAAFF', label='MON')
  plt.pyplot.plot(valMOX.varBndsValt, valMOX.varBndsValT, linestyle=':', color='#DDAAAA', label='MOX')
  plt.pyplot.plot(valMOM.varBndsValt, valMOM.varBndsValT, linestyle=':', color='#AADDAA', label='MOM')
  plt.pyplot.plot(valminMOM.varBndsValt, valminMOM.varBndsValT, linestyle='--', color='#3232FF', label='MOM mintstep')
  plt.pyplot.plot(valmaxMOM.varBndsValt, valmaxMOM.varBndsValT, linestyle='--', color='#643232', label='MOM maxtstep')
  plt.pyplot.plot(valmeanMOM.varBndsValt, valmeanMOM.varBndsValT, linestyle='--', color='#326432', label='MOM meantstep')

  plt.pyplot.legend(ncol=4)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def pr_plot(varName):
# Function to plot precipitation for test
#
# All
  stat=''
  varn=varName + stat
  statf = '01H'
  ncAllfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# pr5max
  stat='5max'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax05file=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valMax05 = get_stat_xy(varn, timename, ncMax05file, point)

# 
# pr10max
  stat='10max'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax10file=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valMax10 = get_stat_xy(varn, timename, ncMax10file, point)

# 
# pr20max
  stat='20max'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax20file=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valMax20 = get_stat_xy(varn, timename, ncMax20file, point)
 
# 
# pr30max
  stat='30max'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax30file=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valMax30 = get_stat_xy(varn, timename, ncMax30file, point)

# 
# pr1Hmax
  stat='1Hmax'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax1Hfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valMax1H = get_stat_xy(varn, timename, ncMax1Hfile, point)

# 
# DAX
##  stat='max'
##  varn=varName + stat
##  statf = 'DAM'
##  ncDAXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
##  valDAX = get_stat_xy(varn, timename, ncDAXfile, point)

# 
# MOM
##  stat='max'
##  varn=varName + stat
##  statf = 'MOM'
##  ncMDAXfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
##  valMDAX = get_stat_xy(varn, timename, ncMDAXfile, point)

# 
# MOM 5max
  stat='5max'
  statf='MOM'
  varn=varName + stat + 'tstep'
  ncM5maxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valM5max = get_stat_xy(varn, timename, ncM5maxfile, point)
# 
# MOM 10max
  stat='10max'
  statf='MOM'
  varn=varName + stat + 'tstep'
  ncM10maxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valM10max = get_stat_xy(varn, timename, ncM10maxfile, point)
# 
# MOM 20max
  stat='20max'
  statf='MOM'
  varn=varName + stat + 'tstep'
  ncM20maxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valM20max = get_stat_xy(varn, timename, ncM20maxfile, point)
# 
# MOM 30max
  stat='30max'
  statf='MOM'
  varn=varName + stat + 'tstep'
  ncM30maxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valM30max = get_stat_xy(varn, timename, ncM30maxfile, point)

# 
# MOM 1Hmax
  stat='1Hmax'
  statf='MOM'
  varn=varName + stat + 'tstep'
  ncM1Hmaxfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valM1Hmax = get_stat_xy(varn, timename, ncM1Hmaxfile, point)
  
#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  plt.pyplot.plot(valMax05.timeVarVal, valMax05.varValPt, linestyle='None', color=((0,0,0,1)), marker='x', label='5')
  plt.pyplot.plot(valMax10.timeVarVal, valMax10.varValPt, linestyle='None', color=((0.5,0,0,1)), marker='x', label='10')
  plt.pyplot.plot(valMax20.timeVarVal, valMax20.varValPt, linestyle='None', color=((1,0,0,1)), marker='x', label='20')
  plt.pyplot.plot(valMax30.timeVarVal, valMax30.varValPt, linestyle='None', color=((1,0.5,0,1)), marker='x', label='30')
  plt.pyplot.plot(valMax1H.timeVarVal, valMax1H.varValPt, linestyle='None', color=((1,1,0,1)), marker='x', label='1H')
#  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
#  plt.pyplot.plot(valMDAX.timeVarVal, valMDAX.varValPt, linestyle='None', color=((0,0,1,1)), marker='1', label='MOM from DAX')
  plt.pyplot.plot(valM5max.timeVarVal, valM5max.varValPt, linestyle='None', color=((0,0,0,1)), marker='1', label='5 MOM')
  plt.pyplot.plot(valM10max.timeVarVal, valM10max.varValPt, linestyle='None', color=((0.5,0,0,1)), marker='1', label='10 MOM')
  plt.pyplot.plot(valM20max.timeVarVal, valM20max.varValPt, linestyle='None', color=((1,0,0,1)), marker='1', label='20 MOM')
  plt.pyplot.plot(valM30max.timeVarVal, valM30max.varValPt, linestyle='None', color=((1,0.5,0,1)), marker='1', label='30 MOM')
  plt.pyplot.plot(valM1Hmax.timeVarVal, valM1Hmax.varValPt, linestyle='None', color=((1,1,0,1)), marker='1', label='1H MOM')
  plt.pyplot.plot(valMax05.varBndsValt, valMax05.varBndsValT, color=((0,0,0,1)))
  plt.pyplot.plot(valMax10.varBndsValt, valMax10.varBndsValT, color=((0.5,0,0,1)))
  plt.pyplot.plot(valMax20.varBndsValt, valMax20.varBndsValT, color=((1,0,0,1)))
  plt.pyplot.plot(valMax30.varBndsValt, valMax30.varBndsValT, color=((1,0.5,0,1)))
  plt.pyplot.plot(valMax1H.varBndsValt, valMax1H.varBndsValT, color=((1,1,0,1)))
#  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, color=((0,0,1,1)))
#  plt.pyplot.plot(valMDAX.varBndsValt, valMDAX.varBndsValT, linestyle=':', color=((0,0,1,1)))
  plt.pyplot.plot(valM5max.varBndsValt, valM5max.varBndsValT, linestyle='--', color=((0,0,0,1)))
  plt.pyplot.plot(valM10max.varBndsValt, valM10max.varBndsValT, linestyle='--', color=((0.5,0,0,1)))
  plt.pyplot.plot(valM20max.varBndsValt, valM20max.varBndsValT, linestyle='--', color=((1,0,0,1)))
  plt.pyplot.plot(valM30max.varBndsValt, valM30max.varBndsValT, linestyle='--', color=((1,0.5,0,1)))
  plt.pyplot.plot(valM1Hmax.varBndsValt, valM1Hmax.varBndsValT, linestyle='--', color=((1,1,0,1)))
##  ylabel('log10(' + varName + ') (' + valAll.varUnits + ')')

##  plt.pyplot.plot(valAll.timeVarVal, log10(valAll.varValPt), color='black', label='All')
##  ax = plt.pyplot.gca()
##  ax.set_autoscale_on(False)
##  plt.pyplot.xlim(init, endt)
##  plt.pyplot.plot(valMax05.timeVarVal, log10(valMax05.varValPt), linestyle='None', color=((0,0,0,1)), marker='x', label='5')
##  plt.pyplot.plot(valMax10.timeVarVal, log10(valMax10.varValPt), linestyle='None', color=((0.5,0,0,1)), marker='x', label='10')
##  plt.pyplot.plot(valMax20.timeVarVal, log10(valMax20.varValPt), linestyle='None', color=((1,0,0,1)), marker='x', label='20')
##  plt.pyplot.plot(valMax30.timeVarVal, log10(valMax30.varValPt), linestyle='None', color=((1,0.5,0,1)), marker='x', label='30')
##  plt.pyplot.plot(valMax1H.timeVarVal, log10(valMax1H.varValPt), linestyle='None', color=((1,1,0,1)), marker='x', label='1H')
##  plt.pyplot.plot(valDAX.timeVarVal, log10(valDAX.varValPt), linestyle='None', color=((0,0,1,1)), marker='x', label='DAX')
##  plt.pyplot.plot(valMax05.varBndsValt, log10(valMax05.varBndsValT), color=((0,0,0,1)))
##  plt.pyplot.plot(valMax10.varBndsValt, log10(valMax10.varBndsValT), color=((0.5,0,0,1)))
##  plt.pyplot.plot(valMax20.varBndsValt, log10(valMax20.varBndsValT), color=((1,0,0,1)))
##  plt.pyplot.plot(valMax30.varBndsValt, log10(valMax30.varBndsValT), color=((1,0.5,0,1)))
##  plt.pyplot.plot(valMax1H.varBndsValt, log10(valMax1H.varBndsValT), color=((1,1,0,1)))
##  plt.pyplot.plot(valDAX.varBndsValt, log10(valDAX.varBndsValT), color=((0,0,1,1)))

  plt.pyplot.legend(ncol=4)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def wss_plot(varName):
# Function to plot wind speed for test
#
# All
  stat=''
  varn=varName + stat
  statf = '01H'
  ncAllfile=fold + '/' + headfile + '_' + statf + '_' + opts.period + '_' + varn + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# wss10max
  stat='10max'
  varn=varName + stat + 'tstep'
  statf = '24H'
  ncMax10file=fold + '/' + headfile + '_' +  statf + '_' + opts.period + '_' + varn + '.nc'
  valMax10 = get_stat_xy(varn, timename, ncMax10file, point)

### 
### wss1Hmax
##  stat='1Hmax'
##  varn=varName + stat + 'tstep'
##  statf = '24H'
##  ncMax1Hfile=fold + '/' + headfile + '_' + statf +'_' + opts.period + '_' + varn + '.nc'
##  valMax1H = get_stat_xy(varn, timename, ncMax1Hfile, point)

# 
# DAX
  stat='max'
  varn=varName + stat
  statf = 'DAM'
  ncDAXfile=fold + '/' + headfile + '_DAM_' + opts.period + '_' + varName + stat + '.nc'
  valDAX = get_stat_xy(varn, timename, ncDAXfile, point)
 
# 
# DAM
  stat='mean'
  varn=varName + stat
  statf = 'DAM'
  ncDAMfile=fold + '/' + headfile + '_DAM_' + opts.period + '_' + varName + stat + '.nc'
  valDAM = get_stat_xy(varn, timename, ncDAMfile, point)

# 
# MOM max
  stat='max'
  varn=varName + stat
  ncMOXfile=fold + '/' + headfile + '_MOM_' + opts.period + '_' + varName + stat + '.nc'
  valMOX = get_stat_xy(varn, timename, ncMOXfile, point)

# 
# MOM wss10max
  stat='10max'
  varn=varName + stat + 'tstep'
  statf = 'MOM'
  ncM10maxfile=fold + '/' + headfile + '_' +  statf + '_' + opts.period + '_' + varn + '.nc'
  valM10max = get_stat_xy(varn, timename, ncM10maxfile, point)

### 
### MOM wss1Hmax
##  stat='1Hmax'
##  varn=varName + stat + 'tstep'
##  statf = 'MOM'
##  ncM1Hmaxfile=fold + '/' + headfile + '_' + statf +'_' + opts.period + '_' + varn + '.nc'
##  valM1Hmax = get_stat_xy(varn, timename, ncM1Hmaxfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  plt.pyplot.plot(valMax10.timeVarVal, valMax10.varValPt, linestyle='None', color=((1,0,0,1)), marker='x', label='max10')
##  plt.pyplot.plot(valMax1H.timeVarVal, valMax1H.varValPt, linestyle='None', color=((0,1,0,1)), marker='x', label='max1H')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', color=((0,0,1,1)), marker='1', label='DAX')
  plt.pyplot.plot(valDAM.timeVarVal, valDAM.varValPt, linestyle='None', color=((0,1,0,1)), marker='1', label='DAM')
  plt.pyplot.plot(valMOX.timeVarVal, valMOX.varValPt, linestyle='None', color=((0,0,0.75,1)), marker='1', label='MOM from DAX')
  plt.pyplot.plot(valM10max.timeVarVal, valM10max.varValPt, linestyle='None', color=((1,0,0,1)), marker='2', label='MOM max10')
##  plt.pyplot.plot(valM1Hmax.timeVarVal, valM1Hmax.varValPt, linestyle='None', color=((0,1,0,1)), marker='2', label='MOM max1H')
  plt.pyplot.plot(valMax10.varBndsValt, valMax10.varBndsValT, color=((1,0,0,1)))
##  plt.pyplot.plot(valMax1H.varBndsValt, valMax1H.varBndsValT, color=((0,1,0,1)))
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, color=((0,0,1,1)))
  plt.pyplot.plot(valDAM.varBndsValt, valDAM.varBndsValT, color=((0,1,0,1)))
  plt.pyplot.plot(valMOX.varBndsValt, valMOX.varBndsValT, linestyle=':', color=((0,0,0.75,1)))
  plt.pyplot.plot(valM10max.varBndsValt, valM10max.varBndsValT, linestyle='--', color=((1,0,0,1)))
##  plt.pyplot.plot(valM1Hmax.varBndsValt, valM1Hmax.varBndsValT, linestyle='--', color=((0,1,0,1)))
  plt.pyplot.legend(ncol=4)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def varStat_plot(varName, frqN, stat):
# Function to plot a stat variable for test

#
# All
  varn=varName 
  ncAllfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# Stat
  frqN='DAM'
  varn=varName + stat
  ncStatfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + stat + '.nc'
  valStat = get_stat_xy(varn, timename, ncStatfile, point)

# 
# Stat
  frqN='MOM'
  varn=varName + stat
  ncMStatfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + stat + '.nc'
  valMStat = get_stat_xy(varn, timename, ncMStatfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  if stat == 'mean':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='g', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='g')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='g', marker='x', label='MOM ' + stat)
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='g')
  elif stat == 'max':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='r', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='r')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='r', marker='x', label='MOM ' + stat)
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='r')
  elif stat == 'min':
    plt.pyplot.plot(valStat.timeVarVal, valStat.varValPt, linestyle='None', color='b', marker='x', label=stat)
    plt.pyplot.plot(valStat.varBndsValt, valStat.varBndsValT, color='b')
    plt.pyplot.plot(valMStat.timeVarVal, valMStat.varValPt, linestyle='None', color='b', marker='x', label='MOM ' + stat)
    plt.pyplot.plot(valMStat.varBndsValt, valMStat.varBndsValT, color='b')

  plt.pyplot.legend()
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def prac_plot(varName):
# Function to plot prac for test

#
# All
  frqN='01H'
  varn=varName 
  ncAllfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valAllbnds = get_stat_xy(varn, timename, ncAllfile, point)
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# 24H
  frqN='24H'
  varn=varName
  nc24Hfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  val24H = get_stat_xy(varn, timename, nc24Hfile, point)

# 
# 01M
  frqN='01M'
  varn=varName
  ncMfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varName + '.nc'
  valM = get_stat_xy(varn, timename, ncMfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  plt.pyplot.plot(valAllbnds.varBndsValt, valAllbnds.varBndsValT, color='black')
  plt.pyplot.plot(val24H.timeVarVal, val24H.varValPt, linestyle='None', color='b', marker='x', label='AC')
  plt.pyplot.plot(valM.timeVarVal, valM.varValPt, linestyle='None', color='g', marker='x', label='01M AC')
  plt.pyplot.plot(val24H.varBndsValt, val24H.varBndsValT, color='b')
  plt.pyplot.plot(valM.varBndsValt, valM.varBndsValT, linestyle='--', color='g')

  plt.pyplot.legend(ncol=4)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

def pracc_plot(varName):
# Function to plot prac for test

#
# All
  frqN='01H'
  stname=''
  varn=varName + stname
  ncAllfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varn + '.nc'
  valAllbnds = get_stat_xy(varn, timename, ncAllfile, point)
  valAll = get_xy(varn, timename, ncAllfile, point)

# 
# DAS
  frqN='DAM'
  stname=''
  varn=varName + stname
  ncDASfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varn + '.nc'
  valDAS = get_stat_xy(varn, timename, ncDASfile, point)

# 
# DAX
  frqN='DAM'
  stname='max'
  varn=varName + stname
  ncDAXfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varn + '.nc'
  valDAX = get_stat_xy(varn, timename, ncDAXfile, point)

# 
# MOS
  frqN='MOM'
  stname=''
  varn=varName + stname
  ncMOSfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varn + '.nc'
  valMOS = get_stat_xy(varn, timename, ncMOSfile, point)

# 
# MOM
  frqN='MOM'
  stname='mean'
  varn=varName + stname
  ncMOMfile=fold + '/' + headfile + '_' + frqN + '_' + opts.period + '_' + varn + '.nc'
  valMOM = get_stat_xy(varn, timename, ncMOMfile, point)

#
# Plotting

  graphname='checking_pt_' + ptS[0] + '-' + ptS[1] + '_' + varName
  plt.pyplot.hold(True)

  title(varName + ' at ' + ptS[0] + ', ' + ptS[1])
  xlabel(timename + ' (' + valAll.timeUnits + ')')
  ylabel(varName + ' (' + valAll.varUnits + ')')

  plt.pyplot.plot(valMOS.timeVarVal, valMOS.varValPt, linestyle='None', color='r', marker='x', label='MOS')
  plt.pyplot.plot(valAll.timeVarVal, valAll.varValPt, color='black', label='All')
  ax = plt.pyplot.gca()
  ax.set_autoscale_on(False)
  plt.pyplot.xlim(init, endt)
  plt.pyplot.plot(valAllbnds.varBndsValt, valAllbnds.varBndsValT, color='black')
  plt.pyplot.plot(valDAS.timeVarVal, valDAS.varValPt, linestyle='None', color='b', marker='x', label='DAS')
  plt.pyplot.plot(valDAX.timeVarVal, valDAX.varValPt, linestyle='None', color='g', marker='x', label='DAX')
#  plt.pyplot.plot(valMOS.timeVarVal, valMOS.varValPt, linestyle='None', color='r', marker='x', label='MOS')
  plt.pyplot.plot(valMOM.timeVarVal, valMOM.varValPt, linestyle='None', color=((0.75,0.25,0.25,1)), marker='x', label='MOM')
  plt.pyplot.plot(valDAS.varBndsValt, valDAS.varBndsValT, color='b')
  plt.pyplot.plot(valDAX.varBndsValt, valDAX.varBndsValT, linestyle='--', color='g')
  plt.pyplot.plot(valMOS.varBndsValt, valMOS.varBndsValT, color='r')
  plt.pyplot.plot(valMOM.varBndsValt, valMOM.varBndsValT, linestyle='--', color=((0.75,0.25,0.25,1)))

  plt.pyplot.legend(ncol=4)
  plt.pyplot.savefig(graphname)

  plt.pyplot.hold(False)
  plt.pyplot.close('all')

  sub.call('display ' + graphname + '.png &', shell=True)

### Options

parser = OptionParser()
parser.add_option("-v", "--variablesfrequencies", dest="varnamesfrq",
                  help="variables to check", metavar="VAR:FRQ:stat,[VAR:FRQ:stat,...]")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check", metavar="PT")
parser.add_option("-P", "--Period", dest="period", 
                  help="point to check", metavar="PT")
(opts, args) = parser.parse_args()

### Static values

WRF4Gfold='/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G'
fold='NARCliM11'
headfile='CCRC_NARCliM_Sydney'
timename='time'
init=413496
endt=414168

#######    #######
## MAIN
    #######

sub.call('killall display ', shell=True)

point=np.array(opts.point)
print point
#ptS=(str(point[0]), str(point[1]))
ptS=["%03d" % number for number in point]

varnamesfrq=opts.varnamesfrq.split(',')
Nvars=len(varnamesfrq)

for ivar in range(Nvars):
  varfrqstat=varnamesfrq[ivar]

  varnames=varfrqstat.split(':')[0]
  frqnames=varfrqstat.split(':')[1]
  statnames=varfrqstat.split(':')[2]

  if varnames == 'tas':
    tas_plot('tas')
  elif varnames == 'pr':
    pr_plot('pr')
  elif varnames == 'wss':
    wss_plot('wss')
  elif varnames == 'pracc':
    pracc_plot('pracc')
  else:
    varStat_plot(varnames, frqnames, statnames)

##  quit()
