#!/bin/bash
if test $1 = '-h'; then
  echo "***************************"
  echo "*** Script to create an ***"
  echo "***  envirionment file  ***"
  echo "***************************"
  echo "engironment_creation.bash [RequestedFile]"
else
  rootsh=`pwd`
  reqf=$1

  varnamesl='BASHsource:WRF4Ghome:p_interp:DomainNetCDF:EXPDIR:POSTDIR:POST2DIR:POSTPROJDIR:postprocessor:FULLnc'
  varnamesl=${varnamesl}':FirstEXPy:LastEXPy:overlap_yearmon:checking:VarnameSecFile:ptstime:DOMAINsim:NAMELISToutput:RefDate:Rhome:CDOhome:ncHOME'

  envfile=`cat ${reqf} | grep envFile | awk '{print $2}'`

  varnames=`echo ${varnamesl} | tr ":" " "`
  ivar=1
  for varname in ${varnames}; do
    value=`cat ${reqf} | grep ${varname} | awk '{print $2}'`
    if test ${ivar} -eq 1; then
      echo ${varname}"="${value} > ${envfile}
    else
      echo ${varname}"="${value} >> ${envfile}
    fi
    ivar=`expr ${ivar} + 1`
  done
  cat ${envfile}
fi

