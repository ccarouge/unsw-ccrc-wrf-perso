#! /bin/bash 
# e.g. # ./process_from_proj.bash 
if test $1 = '-h'
then
  echo " "
  echo "### ## # J. Fernandez & others. Grupo Meteorologia Santander # ## ###"
  echo "                 Universidad de Cantabria Oct. 2010"
  echo " "
  echo "****************************"
  echo "*** Shell to postprocess ***"
  echo "***     WRF outputs      ***"
  echo "***  obtained with WRF4G ***"
  echo "***  postprocessing from ***"
  echo "***     project files    ***"
  echo "****************************"
  echo "process_from_proj.bash [environ] [expname] [var] [thelevels] [thetimes] [theperiods] [do_nothing] [wrfkind] [addons]:[prevaddons]"
  echo "  [environ]: environmental file "
  echo "  [expname]: name of the experiment "
  echo "  [var]: name of the variable "
  echo "  [thelevels]: desired level "
  echo "  [thetimes]: statistic required "
  echo "  [theperiods]: period required "
  echo "  [do_nothing]: 'do nothing' flag "
  echo "  [wrfkind]: kind of wrf output "
  echo "  [addons]:[prevaddons]: which different end processed has to be done on the files (sum of values)"
  echo "     [addons]: To be done on output "
  echo "       1: zip compress "
  echo "       2: internal netCDF compress "
  echo "       4: introduce cell_methods "
  echo "     [prevaddons]: done on previous step "
  echo "       compress: 1"
  echo "       verify: 2"
  echo "       internal netcdf4 compress: 3"
else
####### Functions
# fl_periods: Function to provide start/ending total period from a list of periods [YY1f]-[YY1l],[YY2f]-[YY2l],... --> Nperiods, [YY1f]-[YYnl]
# decimal2binary: Transforms a number to its binary in base 2
# times_selH_nc: Times for output at a given frequency
# check_Dstats_varn: Function to check if variable name has to have a double statistic section name
# check_NODstats_varn: Function to avoid that a variable name has double statistic section name (the same if variable name ends with 'tstep')
# new_statname: Function to provide a new stat-variable name if the variable has not a stat section
# file_freq_name: Function to provide frequency name for the file from the real used frequency
# new_statname: Function to provide a new stat-variable name if the variable has not a stat section
# TimeStepsMonth: Function to give number of time-steps for a given month
# ntimes_period_flux: Function to provide the number of time-steps required from a given file to reach a given period
# NON_accVar: Function to remove 'stats' surname from the name of such variables that are computed as statistical value, but it is their 
#   standard method to be computed
# NON_accVarNames: Function to provide 'stats' surname from the name of such variables that are computed as a statistical value, but it is their
#   standard method to be computed
# check_execution: Checking if the execution of a program has worked

function fl_periods() {
# Function to provide start/ending total period from a list of periods [YY1f]-[YY1l],[YY2f]-[YY2l],... --> Nperiods, [YY1f]-[YYnl]
  pers=$1
  perns=`echo ${pers} | tr ',' ' '`
  Npers=`echo ${perns} | wc -w`
  fyr=`echo ${perns} | tr ' ' '\n' | head -n 1 | tr '-' ' ' | awk '{print $1}'`
  lyr=`echo ${perns} | tr ' ' '\n' | tail -n 1 | tr '-' ' ' | awk '{print $2}'`
  echo ${Npers} ${fyr}-${lyr}
}

function decimal2binary() {
# Transforms a number to its binary in base 2
#
# output formats:
#   base2: String with bits from 2^[nbits-1] to 1
#   pot2: decomposition of number in 2 power series: 2^[nbits-1] 2^[nbits-2] ...
#      2^[0] 

  decimal=$1
  nbits=$2
  oformat=$3

# Checking range
##
  nbit0=`expr $nbits`
  maximbit=`echo "2 "$nbit0 | awk '{print $1^$2}'`
  if test $maximbit -le $1
  then
    echo "ERROR -- error -- ERROR: Number "$decimal" is too big for "$nbits\
      " nbits!!!!!!"
    exit
  fi

# Giving bits
##
  diff=$decimal
  base2=''
  pot2=''
  ibit=`expr $nbits - 1`
  while test $ibit -ge 0
  do
    bit=`echo "2 "${ibit} | awk '{print $1^$2}'`
    if test $bit -le $diff
    then
      diff=`(expr $diff - $bit)`
#      echo "bit "$ibit": "$bit" dif= "$diff
      base2=$base2"1"
      pot2=$pot2" "$bit
    else
      base2=$base2"0"
    fi
    ibit=`(expr $ibit - 1)`
  done
  if test $diff -le 0
  then

#    echo "Decomposition ended"
##
    case $oformat in
      'pot2')
        echo $pot2
      ;;
      'base2')
        echo $base2
      ;;
      esac
#    exit

  else
    echo "Error !!!!"
    echo "Decomposition not ended!"$diferencia
    exit
  fi
}

function times_selH_nc() {
# Times for output at a given frequency
#
### Variables
# ncf: netCDF file
# freqsel: frequency of output

  ncf=$1
  freqsel=$2

}

function check_Dstats_varn() {
# Function to check if variable name has to have a double statistic section name (if both statistics are different)
  varN=$1
  statV=$2

  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'
  frqN=${statV:2:1}

  case ${frqN} in 
  'N')   stat2='min'   ;;
  'X')   stat2='max'   ;;
  'M')   stat2='mean'  ;;
  'S')   stat2='ac'    ;;
  esac

  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
    varsec=${varN:${Lvarsec}:${Lstatsn}}
# When variable name has 'tstep' it has to take into account !
# 
    tstep=''
    if test ${Lstatsn} -gt 4; then tstepsec='tstep'; fi
    if test ${Lvarsec} -gt 1; then
      if test ${varsec} = ${statsn}; then
        if test ! ${varsec} = ${stat2}${tstepsec}; then
          statsvar=0
        else
          statsvar=1
        fi
      fi
    fi
  done # end of statsnames
  echo ${statsvar}
}

function check_NODstats_varn() {
# Function to avoid that a variable name has double statistic section name (the same if variable name ends with 'tstep')
  varN=$1
  statV=$2

  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'
  frqN=${statV:2:1}

  case ${frqN} in 
  'N')   stat2='min'   ;;
  'X')   stat2='max'   ;;
  'M')   stat2='mean'  ;;
  'S')   stat2='ac'    ;;
  esac

  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
# When variable name has 'tstep' it has to take into account !
# 
    tstep=''
    if test ${Lstatsn} -gt 4; then tstepsec='tstep'; fi
    varsec=${varN:${Lvarsec}:${Lstatsn}}
    if test ${Lvarsec} -gt 1; then
      if test ${varsec} = ${statsn}; then
        statsvar=1
      fi
    fi
  done # end of statsnames
  echo ${statsvar}
}

function new_statname() {
# Function to provide a new stat-variable name if the variable has not a stat section
  varName=$1
  statn=$2

  statN=`check_stats_varn ${varName}`
  if test ${statN} -eq 0; then
    varNamestat=${varName}${stat}
  else
    varNamestat=${varName}
  fi

  echo ${varNamestat}
}

function file_freq_name() {
# Function to provide frequency name for the file from the real used frequency
  freqN=$1
    
  freq2=${freqN:0:2}

  case ${freq2} in
    'DA')     freqNfile='DAY'     ;;
    'MO')     freqNfile='MON'     ;;
    'SE')     freqNfile='SES'     ;;
    'YE')     freqNfile='YER'     ;;
    *)        freqNfile=${freqN}  ;;
  esac

  freq3=${freqN:2:1}
  case ${freq3} in
    'N')      varStat='min'     ;;
    'X')      varStat='max'     ;;
    'M')      varStat='mean'    ;;
    'S')      varStat='ac'      ;;
  esac

  echo ${freqNfile} ${varStat}
}

function new_statname() {
# Function to provide a new stat-variable name if the variable has not a stat section
  varName=$1
  statn=$2

  statN=`check_stats_varn ${varName}`
  if test ${statN} -eq 0; then
    varNamestat=${varName}${stat}
  else
    varNamestat=${varName}
  fi

  echo ${varNamestat}
}

function TimeStepsMonth() {
# Function to give number of time-steps for a given month
#   $1: year month of interest )[YYYYMM] format)
#   $2: unit of time (s: second, m: minute, h: hour, d: day)
#   $3: Interval of time between time-steps
  yearmonth=$1
  TimeUnit=$2
  Interval=$3

  datei=`date --utc '+%s' -d"${yearmonth:0:4}-${yearmonth:4:2}-01"`
  datef=`date --utc '+%s' -d"${yearmonth:0:4}-${yearmonth:4:2}-01 1 month"`
  secondsmonth=`expr ${datef} - ${datei}`
  case $TimeUnit in
    's')
      TOTtimeunits=${secondsmonth}
    ;;
    'm')
      TOTtimeunits=`expr ${secondsmonth} / 60`
    ;;
    'h')
      TOTtimeunits=`expr ${secondsmonth} / 3600`
    ;;
    'd')
      TOTtimeunits=`expr ${secondsmonth} / 3600 / 24`
    ;;
  esac
  Nullmod=`expr ${TOTtimeunits} % ${Interval}`
  if test ${Nullmod} -eq 0; then
    TimeSteps=`expr ${TOTtimeunits} / ${Interval}`
  else
    TimeSteps=`echo ${TOTtimeunits}" "${Interval} | awk '{print $1/$2}'`
  fi
  echo ${TimeSteps} 
}

function ntimes_period_flux() {
# Function to provide the number of time-steps required from a given file to reach a given period
  period=$1
  file=$2
  fDT=$3

  case ${period} in 
    'DA')
      Nhper=`echo ${fdT}" 24" | awk '{printf("%d",$2/$1)}'`
      cdo divc,${Nhper} ${file} ${POSTPROJDIR}/process_files/tmp.nc
      cdoc=$?
      if test ${cdoc} -ne 0; then
        echo ${errmsg}
        echo "  cdo divc,${Nhper} ${file} ${POSTPROJDIR}/process_files/tmp.nc"
        echo "  Does not work !!! " $cdoc
        exit
      fi
      mv ${POSTPROJDIR}/process_files/tmp.nc ${file}
    ;;
    'MO')
      if=1
      fyears=`cdo showyear ${file}`
      rm ${POSTPROJDIR}/process_files/tmpyr*.nc >& /dev/null
      rm ${file}.final.nc >& /dev/null
      for yr in ${fyears}; do
        cdo selyear,${yr} ${file} ${POSTPROJDIR}/process_files/tmpyr.nc
        fmonths=`cdo showmon ${POSTPROJDIR}/process_files/tmpyr.nc`
        for mon in ${fmonths}; do
          monS=`printf "%02d" ${mon}`
          ifS=`printf "%02d" ${if}`
          cdo selmon,${mon} ${POSTPROJDIR}/process_files/tmpyr.nc ${POSTPROJDIR}/process_files/tmpyrmon.nc
          Nhper=`TimeStepsMonth ${yr}${monS} h ${fDT}`
          cdo divc,${Nhper} ${POSTPROJDIR}/process_files/tmpyrmon.nc ${POSTPROJDIR}/process_files/tmpyrmonper_${ifS}.nc
          cdoc=$?
          if test ${cdoc} -ne 0; then
            echo ${errmsg}
            echo "  cdo divc,${Nhper} ${POSTPROJDIR}/process_files/tmpyrmon.nc ${POSTPROJDIR}/process_files/tmpyrmonper_${ifS}.nc"
            echo "  Does not work !!! " $cdoc
            exit
          fi
          if=`expr ${if} + 1`
        done # end of months
      done # end of years
      cdo cat ${POSTPROJDIR}/process_files/tmpyrmonper_*.nc ${file}.final.nc
      mv ${file}.final.nc ${file}
      rm ${POSTPROJDIR}/process_files/tmpyr*.nc >& /dev/null
    ;;
    'SE')
      Nhper='24'
    ;;
  esac
}

function NON_accVar() {
# Function to remove 'stats' surname from the name of such variables that are computed as statistical value, but it is their standard method to 
#   be computed
#
####### variables
# filen: name of the file
# varn: variable name
# CFvarn: netCDF CF varname
#
####
# nostvars: variables that should not contain a statistic 'surname' in their name [old variable name]:[CF varname]@[correct variable name]
  filen=$1
  varn=$2
  CFvarn=$3

  nostvars='prac:precipitation_flux@pr praccac:precipitation_amount@pracc'

  for nostvar in ${nostvars}; do
    nostvarold=`echo ${nostvar} | tr '@' ' ' | awk '{print $1}'`
    nostvarn=`echo ${nostvar} | tr '@' ' ' | awk '{print $2}'`
    if test ${varn}':'${CFvarn} = ${nostvarold}; then
      fileN=`basename ${filen}`
      foldN=`dirname ${filen}`
      fileNnc=`echo ${fileN} | tr '.' ' ' | awk '{print $1}'`
      LfileNnc=`expr length ${fileNnc}`
      LfileNnc2=`expr ${LfileNnc} - 2`
      fileNncnew=${fileNnc:0:${LfileNnc2}}
      python ${WRF4Ghome}/util/python/nc_var.py -v ${varn} -f ${filen} -o mname -S ${nostvarn}
      mv ${filen} ${foldN}/${fileNncnew}.nc
      ofile=${foldN}/${fileNncnew}.nc
    fi
  done
}

function NON_accVarNames() {
# Function to provide 'stats' surname from the name of such variables that are computed as a statistical value, but it is their standard method
#   to be computed
#
####### variables
# filen: name of the file
# varn: variable name
# CFvarn: netCDF CF varname
#
####
# nostvars: variables that should not contain a statistic 'surname' in their name [old variable name]:[CF varname]@[correct variable name]
  filen=$1
  varn=$2
  CFvarn=$3

  nostvars='prac:precipitation_flux@pr praccac:precipitation_amount@pracc'

  for nostvar in ${nostvars}; do
    nostvarold=`echo ${nostvar} | tr '@' ' ' | awk '{print $1}'`
    nostvarn=`echo ${nostvar} | tr '@' ' ' | awk '{print $2}'`
    if test ${varn}':'${CFvarn} = ${nostvarold}; then
      fileN=`basename ${filen}`
      foldN=`dirname ${filen}`
      fileNnc=`echo ${fileN} | tr '.' ' ' | awk '{print $1}'`
      LfileNnc=`expr length ${fileNnc}`
      LfileNnc2=`expr ${LfileNnc} - 2`
      fileNncnew=${fileNnc:0:${LfileNnc2}}
      echo ${nopstvarn}" "${foldN}"/"${fileNncnew}
      break
    fi
  done
  echo ${varn}" "${filen}
}

function check_execution() {
# Checking if the execution of a program has worked
  oexec=$1
  program=$2

  errmsg='process_from_proj.bash: ERROR -- error -- ERROR -- error'

  if test ! $oexec -eq 0; then
    echo ${errmsg}" "${oexec}
    echo "  execution failed!"
    echo "  "${program}
    exit
  fi
}

#######    #######
## MAIN
    #######
  environ=$1
  expname=$2
  var=$3
  thelevels="$4"
  thetimes="$5"   # e.g: &MOM+12H_00:00; monthly means from 12H values
  theperiods="$6" # e.g: 1989-1993,1994-1997
  do_nothing=$7 # .true. it is a 'do_nothing' variable flag
  wrfkind=$8
  adds=$9

  errmsg='ERROR -- error -- ERROR -- error '

  addons=`echo ${adds} | tr ':' ' ' | awk '{print $1}'`
  prevaddons=`echo ${adds} | tr ':' ' ' | awk '{print $2}'`

  addonsbits=`decimal2binary ${addons} 3 base2`

  abits1=${addonsbits:2:1}
  abits2=${addonsbits:1:1}
  abits3=${addonsbits:0:1}

  prevaddonsbits=`decimal2binary ${prevaddons} 3 base2`

  pabits1=${prevaddonsbits:2:1}
  pabits2=${prevaddonsbits:1:1}
  pabits3=${prevaddonsbits:0:1}

  source $environ

  refdateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

  periodname=`fl_periods ${theperiods} | awk '{print $2}'`

  thisdir=`pwd`
  mkdir -p ${POSTPROJDIR}/process_files
  cd ${POSTPROJDIR}/process_files
  read freqname thetimes <<< ${thetimes/_/ }
  freqnames=`echo ${freqname} | tr '&' ' ' | tr '+' ' '`
  freqend=`echo ${freqnames} | awk '{print $1}'`
  freqprj=`echo ${freqnames} | awk '{print $2}'`

  freqp=`file_freq_name ${freqprj} | awk '{print $1}'`
  firstperiodval=`echo ${theperiods} | tr ',' ' ' | awk '{print $1}'`

#
# Checking existence of already done project output files
#
  pers=`echo ${theperiods} | tr ',' ' '`
  for per in ${pers}; do
    if test "${thelevels}" = "SFC"; then
      file=${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}.nc
    else
      lev=`echo ${thelevels} | tr ',' ' ' | awk '{print $1}'`
      plev=`printf "%04d" ${lev}`
      file=${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}${plev}.nc
    fi
      if test ! -f ${file}.gz && test ! -f ${file}; then
        echo ${errmsg}
        echo "  Already run project related files are not done!!"
        echo "  '"${file}"[.gz] does not exist "
        echo "  It must exist to be able to produce the desired '"${freqend}"' with '"${freqprj}"'"
        exit
    fi
  done # End of periods

  freqn=${freqname}
  test "${thelevels}" != "SFC" && splitlevel="splitlevel" || splitlevel=""
  test "${thetimes}" != "All" && seltime="-seltime,${thetimes}" || seltime=""

#
# Problems (segmentation faults) on cdo concatanation of instructions, impose split of the 'seltime' instruction
  Lthetimes=`expr length ${thetimes}'A'`

  if test ! ${freqend} = 'All'; then statN=`check_NODstats_varn ${var} ${freqend}`; fi
  rm -f procesa_*

  case ${freqend} in
    DAM) if test ${statN} -eq 0; then
        ovar=${var}'mean'   ; freqname=DAM ; seltime="-daymean -chname,${var},${var}mean" 
      else
        ovar=${var}         ; freqname=DAM ; seltime="-daymean" 
      fi ;;
    DAS) if test ${statN} -eq 0; then 
        ovar=${var}'ac'     ; freqname=DAM ; seltime="-daysum -chname,${var},${var}ac" 
      else
        ovar=${var}         ; freqname=DAM ; seltime="-daysum" 
      fi ;;
    DAX) if test ${statN} -eq 0; then 
        ovar=${var}'max'    ; freqname=DAM ; seltime="-daymax -chname,${var},${var}max" 
      else
        ovar=${var}         ; freqname=DAM ; seltime="-daymax" 
      fi 
      if ${do_nothing}; then
        ovar=${var}'max'; fi  ;;
    DAN) if test ${statN} -eq 0; then 
        ovar=${var}'min'    ; freqname=DAM ; seltime="-daymin -chname,${var},${var}min" 
      else
        ovar=${var}         ; freqname=DAM ; seltime="-daymin" 
      fi
      if ${do_nothing}; then
        ovar=${var}'min'; fi  ;;
    MOM) if test ${statN} -eq 0; then
        ovar=${var}'mean'   ; freqname=MOM ; seltime="-monmean -chname,${var},${var}mean" 
      else
        ovar=${var}         ; freqname=MOM ; seltime="-monmean" 
      fi ;;
    MOS) if test ${statN} -eq 0; then 
        ovar=${var}'ac'     ; freqname=MOM ; seltime="-monsum -chname,${var},${var}ac" 
      else
        ovar=${var}         ; freqname=MOM ; seltime="-monsum" 
      fi ;;
    MOX) if test ${statN} -eq 0; then 
        ovar=${var}'max'    ; freqname=MOM ; seltime="-monmax -chname,${var},${var}max" 
      else
        ovar=${var}         ; freqname=MOM ; seltime="-monmax" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'max'`; fi  ;;
    MON) if test ${statN} -eq 0; then 
        ovar=${var}'min'    ; freqname=MOM ; seltime="-monmin -chname,${var},${var}min" 
      else
        ovar=${var}         ; freqname=MOM ; seltime="-monmin" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'min'`; fi  ;;
    SEM) if test ${statN} -eq 0; then 
        ovar=${var}'mean'   ; freqname=SEM ; seltime="-seasmean -chname,${var},${var}mean" 
      else
        ovar=${var}         ; freqname=SEM ; seltime="-seasmean" 
      fi ;;
    SES) if test ${statN} -eq 0; then 
        ovar=${var}'ac'     ; freqname=SEM ; seltime="-seassum -chname,${var},${var}ac" 
      else
        ovar=${var}         ; freqname=SEM ; seltime="-seassum" 
      fi ;;
    SEX) if test ${statN} -eq 0; then 
        ovar=${var}'max'    ; freqname=SEM ; seltime="-seasmax -chname,${var},${var}max" 
      else
        ovar=${var}         ; freqname=SEM ; seltime="-seasmax" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'max'`; fi  ;;
    SEN) if test ${statN} -eq 0; then 
        ovar=${var}'min'    ; freqname=SEM ; seltime="-seasmin -chname,${var},${var}min" 
      else
        ovar=${var}         ; freqname=SEM ; seltime="-seasmin" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'min'`; fi  ;;
    YEM) if test ${statN} -eq 0; then 
        ovar=${var}'mean'   ; freqname=YEM ; seltime="-yearmean -chname,${var},${var}mean" 
      else
        ovar=${var}         ; freqname=YEM ; seltime="-yearmean" 
      fi ;;
    YES) if test ${statN} -eq 0; then 
        ovar=${var}'ac'     ; freqname=YEM ; seltime="-yearsum -chname,${var},${var}ac" 
      else
        ovar=${var}         ; freqname=YEM ; seltime="-yearsum" 
      fi ;;
    YEX) if test ${statN} -eq 0; then 
        ovar=${var}'max'    ; freqname=YEM ; seltime="-eyarmax -chname,${var},${var}max" 
      else
        ovar=${var}         ; freqname=YEM ; seltime="-eyarmax" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'max'`; fi  ;;
    YEN) if test ${statN} -eq 0; then 
        ovar=${var}'min'    ; freqname=YEM ; seltime="-yearmin -chname,${var},${var}min" 
      else
        ovar=${var}         ; freqname=YEM ; seltime="-yearmin" 
      fi
      if ${do_nothing}; then
        ovar=`new_statname ${var} 'min'`; fi  ;;
     *)  ovar=$var 
       ;;
  esac

  echo "Processing '"$var"' frequency: '"${freqend}"' time in file :"${thetimes}" from '"${freqprj}"' project files"
# POSTPROJDIR files have already concatenated as a unique file
  rm -f procesa_*
  for per in ${theperiods//,/ }; do
    echo -n "  Period $per:"
    if test "${thelevels}" = "SFC" 
    then
      rm ${POSTPROJDIR}/process_files/${expname}_${freqp}_${periodname}_${var}.nc.gz >& /dev/null
      rm ${POSTPROJDIR}/process_files/${expname}_${freqp}_${periodname}_${var}.nc >& /dev/null
      if test ${pabits1} -eq 1; then
        cp ${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}.nc.gz ./ >& /dev/null
        gunzip ${expname}_${freqp}_${per}_${var}.nc.gz
      else
        cp ${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}.nc ./
      fi
      if test ${freqend} = 'All'; then
        mv ${POSTPROJDIR}/process_files/${expname}_All_${per}_${var}.nc \
          ${POSTPROJDIR}/process_files/procesa_${per}_${var}      
      else
        if test ${freqend:2:1} = 'H'; then
          Hfreq=${freqend:0:2} 
# cdo times to select 
#
          cdotimes=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f                                    \
            ${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}.nc -t time -r ${refdateYmdHMS}  \
            -F ${Hfreq} -o cdoF -u ${Hfreq}`
          check_execution $? "${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}.nc -t time -r ${refdateYmdHMS} -F ${Hfreq} -o cdoF -u ${Hfreq}"
          seltime="-seltime${cdotimes}"
        fi
        ${CDOhome} ${seltime} ${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}.nc            \
          ${POSTPROJDIR}/process_files/procesa_${per}_${var}
        if test ${Lthetimes} -gt 1; then
          ${CDOhome} settime,${thetimes}                                                                   \
            ${POSTPROJDIR}/process_files/procesa_${per}_${var} ${POSTPROJDIR}/process_files/tmp.nc
          mv ${POSTPROJDIR}/process_files/tmp.nc ${POSTPROJDIR}/process_files/procesa_${per}_${var}
        fi
      fi
    else
      echo -n "  Levels:"
      for level in ${thelevels//,/ }; do
        echo -n " $level"
        plevel=`printf "%04d" ${level}`
        rm ${POSTPROJDIR}/process_files/${expname}_${freqp}_${periodname}_${var}${plevel}.nc.gz >& /dev/null
        rm ${POSTPROJDIR}/process_files/${expname}_${freqp}_${periodname}_${var}${plevel}.nc >& /dev/null
        if test ${pabits1} -eq 1; then
          cp ${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}${plevel}.nc.gz ./ >& /dev/null
          gunzip ${expname}_${freqp}_${per}_${var}${plevel}.nc.gz
        else
          cp ${POSTPROJDIR}/${expname}_${freqp}_${per}_${var}${plevel}.nc ./
        fi
        if test ${freqend} = 'All'; then
          mv ${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}${plevel}.nc          \
            ${POSTPROJDIR}/process_files/procesa_${per}_${var}${plevel}.nc
        else
          if test ${freqend:2:1} == 'H'; then
            Hfreq=${freqend:0:2} 
# cdo times to select 
#
            cdotimes=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f                        \
              ${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}${plevel}.nc -t time \
              -r ${refdateYmdHMS} -F ${Hfreq} -o cdoF -u ${Hfreq}`
            check_execution $? "${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}${plevel}.nc -t time -r ${refdateYmdHMS} -F ${Hfreq} -o cdoF -u ${Hfreq}"
            seltime="seltime${cdotimes}"
          fi
          ${CDOhome} ${seltime}                                                                  \
            ${POSTPROJDIR}/process_files/${expname}_${freqp}_${per}_${var}${plevel}.nc           \
            ${POSTPROJDIR}/process_files/procesa_${per}_${var}${plevel}.nc
          if test ${Lthetimes} -gt 1; then
            ${CDOhome} settime,${thetimes}                                                       \
              ${POSTPROJDIR}/process_files/procesa_${per}_${var}${plevel}.nc                     \ 
              ${POSTPROJDIR}/process_files/tmp.nc
            mv ${POSTPROJDIR}/process_files/tmp.nc                                               \
              ${POSTPROJDIR}/process_files/procesa_${per}_${var}${plevel}.nc
          fi
        fi
      done # end of levels
    fi
  done # per
  Nfiles=`ls -1 ${POSTPROJDIR}/process_files/procesa_*_${var}* | wc -l`
#
# Writting output file
#
#
# deaccumulated flux-variables (precipitation_flux) have to be divided by the number of 
#   output-steps in order to provide the flux value for the entire period
#
  if test "${thelevels}" = "SFC"; then
##    CFattrname=`python ${WRF4Ghome}/util/python/nc_var_attr.py -v ${var} -f                      \
##      ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}.nc -o r -a standard_name`
    CFattrname=`python ${WRF4Ghome}/util/python/nc_var.py -v ${var} -f                           \
      ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}.nc -o rvattr -S standard_name`
    if test ${freqn:2:1} = 'S' && test ${CFattrname} = 'precipitation_flux'; then
      fdT=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f                                   \
        ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}.nc -t time                   \
        -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{print $2}'`
      for file in ${POSTPROJDIR}/process_files/procesa_*_${var}; do  
        ntimes_period_flux ${freqn:0:2} ${file} ${fdT}
      done
    else
      freqName=${freqname}
    fi
  else
    echo -n "  Levels:"
    for level in ${thelevels//,/ }; do
      echo -n " $level"
      plevel=`printf "%04d" ${level}`
##      CFattrname=`python ${WRF4Ghome}/util/python/nc_var_attr.py -v ${var} -f                    \
##        ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}${plevel}.nc -o r             \
##        -a standard_name`
      CFattrname=`python ${WRF4Ghome}/util/python/nc_var.py -v ${var} -f                         \
        ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}${plevel}.nc -o rvattr -S     \
        standard_name`
      if test ${freqn:2:1} = 'S' && test ${CFattrname} = 'precipitation_flux'; then
        fdT=`python ${WRF4Ghome}/util/python/nc_time_tools.py -f                                 \
          ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}${plevel}.nc -t time        \
          -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{print $2}'`
        check_execution $? "python ${WRF4Ghome}/util/python/nc_time_tools.py -f ${POSTPROJDIR}/${expname}_${freqp}_${firstperiodval}_${var}${plevel}.nc -t time -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{print $2}'"
        for file in ${POSTPROJDIR}/process_files/procesa_*_${var}${plevel}; do  
          ntimes_period_flux ${freqn:0:2} ${file} ${fdT}
        done
      else
        freqName=${freqname}
      fi
    done # end levels
  fi

  if test ${freqname} = 'All'; then
    firstper=`echo ${theperiods} | tr ',' ' ' | awk '{print $1}'`
    if test "${thelevels}" = "SFC"; then
      freqName=`python ${WRF4Ghome}/util/python/nc_time_tools.py                                 \
       -f ${POSTPROJDIR}/process_files/procesa_${firstper}_${var} -t time                        \
       -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{printf ("%02dH",$2)}'`
    else
      firstlev=`echo ${thelevels//,/ } | awk '{print $1}'`
      fSlev=`printf "%04d" ${firstlev}`
      freqName=`python ${WRF4Ghome}/util/python/nc_time_tools.py                                 \
       -f ${POSTPROJDIR}/process_files/procesa_${firstper}_${var}${fSlev}.nc -t time             \
       -r ${refdateYmdHMS} -F 1 -o timeINF | awk '{printf ("%02dH",$2)}'`
    fi
  else
    if test ${freqname:2:1} = 'm'; then 
      freqName=${freqname:0:2}'M'
      if test ${freqName} = '12M'; then freqName='YEA'; fi
    else
      case ${freqname:0:2} in
        'DA') freqName='DAY' ;;
        'MO') freqName='MON' ;;
        'SE') freqName='SES' ;;
        'YE') freqName='YEA' ;;
      esac       
##      freqName=${freqname}
    fi
  fi
  if test ${freqName} = '24H'; then freqName='DAY'; fi

  if test "${thelevels}" = "SFC"; then
    echo "  Processing only the surface value (or all levels together...)"
    ofile="${POSTPROJDIR}/${expname}_${freqName}_${periodname}_${ovar}.nc"
    rm -f ${ofile}.gz ${ofile} >& /dev/null
    if test $Nfiles -gt 1; then
      ${CDOhome} -s cat ${POSTPROJDIR}/process_files/procesa_*_${var} ${ofile}
    else
      cp ${POSTPROJDIR}/process_files/procesa_${theperiods}_${var} ${ofile}
    fi
##    if test $abits3 -eq 1 && test ! ${freqname} = 'All'; then
    if test ! ${freqname} = 'All'; then
      ${ROOTpost}/cellmethods.bash ${ofile} ${ovar} ${wrfkind} ${freqend} ${NAMELISToutput}      \
        ${DOMAINsim} ${RefDate} ${WRF4Ghome}
    fi
# Writting projection information
#
    python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile}                    \
      -m ${projectionFILE} -v ${ovar}
    check_execution $? "python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile} -m ${projectionFILE} -v ${ovar}"
# Re-writting time_bounds
#
##    python ${WRF4Ghome}/util/python/rename_dimension.py -d gsize,bnds -f ${ofile} -v ${ovar}
    python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds 
    check_execution $? "python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds"
    python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds 
    check_execution $? "python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds"

# Removing statistical 'surnames' to that variables that by definition are statistically obtained
#
    NON_accVar ${ofile} ${ovar} ${CFattrname}

    if test $abits2 -eq 1; then 
      ${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc
      check_execution $? "${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc"
      mv tmp.nc ${ofile}
    else
      ${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc
      check_execution $? "${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc"
      mv tmp.nc ${ofile}
    fi
    if test $abits1 -eq 1; then gzip ${ofile}; fi
  else
    echo -n "  Levels:"
    for level in ${thelevels//,/ }; do
      echo -n " $level"
      plevel=`printf "%04d" ${level}`
      ofile="${POSTPROJDIR}/${expname}_${freqName}_${periodname}_${ovar}${plevel}.nc"
      rm -f ${ofile}.gz ${ofile} >& /dev/null
      if test $Nfiles -gt 1; then
        ${CDOhome} -s cat ${POSTPROJDIR}/process_files/procesa_*_${var}${plevel}.nc ${ofile}
      else
        cp ${POSTPROJDIR}/process_files/procesa_${theperiods}_${var}${plevel}.nc ${ofile}
      fi
#      if test ${checking} -eq 1
#      then
#        checking_file ${ofile} ${FULLnc} ${ptstime} ${VarnameSecFile} ${Rhome} 
#      fi
##      if test $abits3 -eq 1 && test ! ${freqname} = 'All'; then
      if test ! ${freqname} = 'All'; then
        ${ROOTpost}/cellmethods.bash ${ofile} ${ovar} ${wrfkind} ${freqend} ${NAMELISToutput} \
          ${DOMAINsim} ${RefDate} ${WRF4Ghome}
      fi
# Writting projection information
#
      python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile} -m            \
        ${projectionFILE} -v ${ovar}
      check_execution $? "python ${WRF4Ghome}/util/python/adding_mapping_information.py -f ${ofile} -m ${projectionFILE} -v ${ovar}"
# Re-writting time_bounds
#
##      python ${WRF4Ghome}/util/python/rename_dimension.py -d gsize,bnds -f ${ofile} -v ${ovar}
      python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds
      check_execution $? "python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S gsize:bnds"
      python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds 
      check_execution $? "python ${WRF4Ghome}/util/python/nc_var.py -f ${ofile} -o mdname -v ${ovar} -S nb2:bnds "

# Removing statistical 'surnames' to that variables that by definition are statistically obtained
#
      NON_accVar ${ofile} ${ovar} ${CFattrname}

      if test $abits2 -eq 1; then 
        ${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc
        check_execution $? "${ncHOME}/bin/nccopy -d 9 ${ofile} tmp.nc"
        mv tmp.nc ${ofile}
      else
        ${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc
        check_execution $? "${ncHOME}/bin/nccopy -k classic ${ofile} tmp.nc"
        mv tmp.nc ${ofile}
      fi
      if test $abits1 -eq 1; then gzip ${ofile}; fi
    done # level
  fi
  rm -rf ${POSTPROJDIR}/process_files
  cd ${thisdir}
fi
