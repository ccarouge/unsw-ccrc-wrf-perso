#!/bin/bash -x
# g.e. # checking_file.bash CCRC_NARCliM_GreatSydney__2000-2009_tas.nc@1440@2678400 ~/studies/NARCliMGreatSydney2km/data/compressout/wrfout_d01_2003-02-17_00\:00\:00 151.23406:-33.91775:151.36139:-33.84076@mon 6 ../namelist.output time /usr/bin/R /home/lluis/bin/gcc_cdo-1.5.3/bin/cdo /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G
if test $1 = '-h'; then
  echo "*******************************"
  echo "*** Shell function to check ***"
  echo "***   content of a netCDF   ***"
  echo "***   plotting mean values  ***"
  echo "*******************************"
  echo "checking_file.bash [chkfile] [chkfullfile] [pointsKind] [varfilesec] [namelist] [timename] [Rexec] [CDOexec] [WRF4Gh]"
  echo "  [chkfile]: name of the file followed by frequency of values (seconds) and the period that covers (seconds) '@' separated ([file]@[freq]@[per])"
  echo "  [chkfullfile]: file with all possible variables (e.g.: non postprocessed)"
  echo "  [pointsKind]: points to plot temporal series and kind of statistics (lon1:lat1:lon2:lat2:...:lonN:latN@kind) "
  echo "    kind: "
  echo "      mon: monthly mean/std & yearly season means "
  echo "      day: daily mean/std & yearly hourly means "
  echo "  [varfilesec]: section of the file with the variable name inside netcdf (assuming '_') "
  echo "  [namelist]: file with the 'namelist.output' of the simulations"
  echo "  [timename]: name of the variable with the 'time' information "
  echo "  [Rexec]: binary of 'R' (R.org) " 
  echo "  [CDOexec]: binary of 'CDO' (Climate Data Operators) "
  echo "  [WRF4Gh]: WRF4G home folder "
else

get_Ntimes_namelist(){
# Function to get Ntimes from a namelist.output of a given wrffile
  wrff=$1
  fk=$2
  fper=$3
  namlst=$4
  timen=$5
  WRF4Gh=$6

  wrfk=`basename ${wrff}`
  wrfdom=`echo ${wrfk} | tr '_' ' ' | awk '{print substr($2,3,1)}'`

  auxhist=`cat ${namlst} | grep ${fk} | awk '{print $1}' | tr '_' ' ' | awk '{print $1}'`
  histfrq=`cat ${namlst} | grep ${auxhist}_ITNERVAL`
# interval
#
  nvalues=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
  if test ${nvalues} = $wrfdom; then
    histfrq=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  else
    histfrq=`echo $histfrq | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  fi
# frames x file
# 
  nvalues=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
  if test  ${nvalues} = $wrfdom; then
    framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  else
    framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  fi

  echo $histfrq" "$nvalues
}

check_Ntimes(){
# Function to get Ntimes from a namelist.output of a given wrffile
#   [wrff]: file to check
#   [varn]: name of the variable
#   [ffrq]: file frequency of the output (seconds)
#   [fper]: covered period by the file (seconds)
#   [timen]: name of the time variable
#   [WRF4Gh]: folder with the WRF4G structure

  wrff=$1
  varn=$2
  ffrq=$3
  fper=$4
  timen=$5
  WRF4Gh=$6

  wrfk=`basename ${wrff}`

  nvalues=`expr ${fper} / ${ffrq}`

  refDateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

  fileNsteps=`python ${WRF4Gh}/util/python/nc_time_tools.py -f ${wrff} -t time -r ${refDateYmdHMS} -F 1 -o timeINF | awk '{print $1}'`

  if test ${nvalues} -ne ${fileNsteps}; then
    echo "ERROR -- errorr -- ERROR -- error"
    echo "  File '"${wrff}"' "
    echo "  must contain "${nvalues}" values, but it only has "${fileNsteps}" !!! !! !"
    echo "ERROR -- errorr -- ERROR -- error"
#    exit
  fi
}

#######    #######
## MAIN
    #######

# Checking of single variable postprocessed file
  c1='$'
  chkfileFrqPer=$1
  chkfullfile=$2
  pointsKind=$3
  varfilesec=$4
  namelst=$5
  timename=$6
  Rexec=$7
  CDOexec=$8
  WRF4h=$9

  chkfile=`echo ${chkfileFrqPer} | tr '@' ' ' | awk '{print $1}'`
  filefrq=`echo ${chkfileFrqPer} | tr '@' ' ' | awk '{print $2}'`
  fileper=`echo ${chkfileFrqPer} | tr '@' ' ' | awk '{print $3}'`
  points=`echo ${pointsKind} | tr '@' ' ' | awk '{print $1}'`
  kind=`echo ${pointsKind} | tr '@' ' ' | awk '{print $2}'`

  Npts=`echo $points | tr ':' ' ' | wc -w | awk '{print $1/2}'`
  N2pts=`expr ${Npts} '*' 2`
  Rpts=''
  ipt=1
  while test ${ipt} -le ${N2pts}
  do
    if test ${ipt} -eq 1
    then
      Rpts=${Rpts}$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    else    
      Rpts=${Rpts}', '$(echo $points | tr ':' '\n' | head -n ${ipt} | tail -n 1) 
    fi
    ipt=`expr ${ipt} + 1`
  done # end Npts 
  Rpts=${Rpts}
  
  chkfold=`dirname ${chkfile}`  
  mkdir -p ${chkfold}/checking
  ncfvarname=`${CDOexec} showname ${chkfile} | awk '{print $1}'` 
  fvarname=`basename ${chkfile}`
  fvarname=`echo ${fvarname}" _ "${varfilesec} | awk '{split($1,words,$2); print words[$3]}'` 
  fvarname=`echo $fvarname" . 1" | awk '{split($1,words,$2); print words[$3]}'` 

  check_Ntimes ${chkfile} ${fvarname} ${filefrq} ${fileper} ${timename} ${WRF4h}

  if test ${kind} = 'mon'; then
    ${CDOexec} yseasmean ${chkfile} ${chkfold}/checking/${fvarname}_2Dmean.nc
    ${CDOexec} monmean ${chkfile} ${chkfold}/checking/${fvarname}_1Dmean.nc
    ${CDOexec} monstd ${chkfile} ${chkfold}/checking/${fvarname}_1Dstd.nc
  else
    ${CDOexec} yhourmean ${chkfile} ${chkfold}/checking/${fvarname}_2Dmean.nc
    ${CDOexec} daymean ${chkfile} ${chkfold}/checking/${fvarname}_1Dmean.nc
    ${CDOexec} daystd ${chkfile} ${chkfold}/checking/${fvarname}_1Dstd.nc
  fi

  rm ${chkfold}/checking/${fvarname}_*.png

  cat << EOF > ${chkfold}/checking/checking_postprocess.R
library(ncdf)
ncunit_variable_info<-function(ncunit, variable)
### R function to obtain nc variable information from a ncunit
##
{
nvarnc<-length(ncunit${c1}var)
varncnames<-matrix(0,nvarnc)
dim(varncnames)<-nvarnc

for (ivar in 1:nvarnc) {
varncnames[ivar]<-ncunit${c1}var[[ivar]]${c1}name}

pos_ncvar<-which(varncnames==variable)
ncvar<-ncunit${c1}var[[pos_ncvar]]
ncvarname<-ncvar${c1}name
ncvarid<-ncvar${c1}id
ncvardims<-ncvar${c1}ndims
ncvarunits<-ncvar${c1}units
ncvarsize<-ncvar${c1}varsize
nc_variable_info<-c(variable, ncvarname, ncvarid, ncvardims, ncvarunits, ncvarsize)

nc_variable_info
}
#
# Main
#
geofile<-'${chkfullfile}'
varfile<-'${chkfold}/checking/${fvarname}_2Dmean.nc'
landmask<-'LANDMASK'
longname<-'XLONG'
latname<-'XLAT'
varname<-'${ncfvarname}'
filevarname<-'${fvarname}'

nclm<-open.ncdf(geofile)
comps<-list()
Ncomps<-3
compsn<-rep(0,3)
compsn<-c(landmask, longname, latname)
for (icomp in 1:Ncomps) {
  shapevar<-ncunit_variable_info(nclm, compsn[icomp])[4]
  if (shapevar ==3 ) {
    comps[[compsn[icomp]]]<-get.var.ncdf(nclm, compsn[[icomp]], start=c(1,1,1), count=c(-1,-1,1))
  } else {
    comps[[compsn[icomp]]]<-get.var.ncdf(nclm, compsn[[icomp]], start=c(1,1), count=c(-1,-1))
  }
} # end of comps

close.ncdf(nclm)

ncvar<-open.ncdf(varfile)
shapevar<-ncunit_variable_info(ncvar, varname)[4]

if (shapevar == 3) {
  variable<-get.var.ncdf(ncvar, varname, start=c(1,1,1), count=c(-1,-1,-1))
}

if (shapevar == 4) {
  variable<-get.var.ncdf(ncvar, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
}
close.ncdf(ncvar)

dimx<-nrow(comps[[1]])
dimy<-ncol(comps[[1]])
variable<-ifelse(variable<=-9.e+32, NA, variable)

colorsRGB<-colorRamp(c(rgb(0,0,1,1),rgb(0,1,0,1),rgb(1,0,0,1)))
colbar<-rgb(colorsRGB(seq(0,1,length=20)),max=255)
EOF

  if test ${kind} = 'mon'; then 
    cat << EOF >> ${chkfold}/checking/checking_postprocess.R
names<-c("DJF", "MAM", "JJA", "SON")
timevals<-c(1,2,3,4)
EOF
  else
    cat << EOF >> ${chkfold}/checking/checking_postprocess.R
names<-c("00UTC", "06UTC", "12UTC", "18UTC")
timevals<-c(1,7,13,19)
EOF
  fi
  cat << EOF >> ${chkfold}/checking/checking_postprocess.R
for ( itimestep in 1:4) {
  timestep<-timevals[itimestep]
  graphname<-paste("${chkfold}","/checking/",filevarname,"_2Dmean_",names[itimestep],".png", sep="")
  graphtit<-paste(filevarname,"${ncstat}",names[itimestep], sep=" ")
  minval<-min(variable[,,timestep], na.rm=TRUE)
  maxval<-max(variable[,,timestep], na.rm=TRUE)

  bitmap(graphname)
##  postscript(graphname, bg="white", horizontal=FALSE, paper="default")

  par(xpd=NA)
  contour(comps[[landmask]], col=gray(0.75), nlevels=1, drawlabels=FALSE, asp=dimy/dimx,
    axes=FALSE)
  plot.window(xlim=c(0,1),ylim=c(0,1), asp=dimy/dimx, log="",
    title(main=graphtit, xlab="W-E",ylab="S-N"))
  image(variable[,,timestep],col=rgb(colorsRGB(seq(0,1,length=20)),max=255),
    add=TRUE)
  contour(comps[[longname]],col=gray(0.75),levels=seq(-180,180,by=5),add=TRUE)
  contour(comps[[latname]],col=gray(0.75),levels=seq(-90,90,by=5),add=TRUE)
  contour(comps[[landmask]], col=gray(0.25), nlevels=1, drawlabels=FALSE, add=TRUE)
  stepsval<-(maxval-minval)/4
  steps21val<-(maxval-minval)/20
  Lcolvar=length(colbar)
  seq9colbar<-seq(1, Lcolvar, (Lcolvar-1)/8)
  seq21colbar<-seq(1, Lcolvar, (Lcolvar-1)/20)
  symbols(seq(0.1,0.9,0.8/20), matrix(-0.04,21), rectangles=cbind(matrix(0.8/20,21), 
    matrix(0.05,21)), bg=colbar[seq21colbar], fg="gray", inches=FALSE, add=TRUE) 
  text(seq(0.1,0.9,0.8/20)[seq(1,21,20/4)], matrix(-0.1,4), labels=sprintf("%4.2f",
    seq(minval, maxval, stepsval)))

  dev.off()

} 
EOF
  ${Rexec} -f --slave ${chkfold}/checking/checking_postprocess.R

cat << EOF > ${chkfold}/checking/checking_1D_postprocess.R
#Checking postprocess of variable ${fvarname}
library(ncdf)
lonlat_point<-function(varfile, lonname, latname, landmaskname='LANDMASK', points) {
# R Function to provide nearest grid point to a given longitude, latitude
  Npoints<-nrow(points)
  posmin<-rep(0,Npoints*2)
  dim(posmin)<-c(Npoints,2)

  ncvar<-open.ncdf(varfile)
  varinf<-ncunit_variable_info(ncvar, lonname)
  shapevar<-varinf[4]

  if (shapevar == 2) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1), count=c(-1,-1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1), count=c(-1,-1))
  }
  if (shapevar == 3) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1), count=c(-1,-1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1), count=c(-1,-1,1))
  }
  if (shapevar == 4) {
    lonmat<-get.var.ncdf(ncvar, lonname, start=c(1,1,1,1), count=c(-1,-1,1,1))
    latmat<-get.var.ncdf(ncvar, latname, start=c(1,1,1,1), count=c(-1,-1,1,1))
  }
  close.ncdf(ncvar)

  dimx<-nrow(lonmat)
  dimy<-ncol(lonmat)

  for ( i in 1:Npoints) {
    dist<-sqrt((lonmat-points[i,1])**2.+(latmat-points[i,2])**2.)
    pmindist<-which.min(dist)
    yposmin<-as.integer(pmindist/dimx)+1
    xposmin<-as.integer(pmindist-(yposmin-1)*dimx)
    posmin[i,]<-c(xposmin, yposmin)
  }
  posmin
}

ncunit_dim_info<-function(ncunit, dimension)
### R function to obtain nc variable information from a ncunit
##
{
  ndimnc<-length(ncunit${c1}dim)
  dimncnames<-matrix(0,ndimnc)
  dim(dimncnames)<-ndimnc

  for (idim in 1:ndimnc) {
    dimncnames[idim]<-ncunit${c1}dim[[idim]]${c1}name}

  pos_ncdim<-which(dimncnames==dimension)
  ncdim<-ncunit${c1}dim[[pos_ncdim]]
  ncdimname<-ncdim${c1}name
  ncdimid<-ncdim${c1}id
  ncdimsize<-ncdim${c1}len
  ncdimunits<-ncdim${c1}units
  nc_dimension_info<-c(dimension, ncdimname, ncdimid, ncdimsize, ncdimunits)

  nc_dimension_info
}
ncunit_variable_info<-function(ncunit, variable)

### R function to obtain nc variable information from a ncunit
##
{
nvarnc<-length(ncunit${c1}var)
varncnames<-matrix(0,nvarnc)
dim(varncnames)<-nvarnc

for (ivar in 1:nvarnc) {
varncnames[ivar]<-ncunit${c1}var[[ivar]]${c1}name}

pos_ncvar<-which(varncnames==variable)
ncvar<-ncunit${c1}var[[pos_ncvar]]
ncvarname<-ncvar${c1}name
ncvarid<-ncvar${c1}id
ncvardims<-ncvar${c1}ndims
ncvarunits<-ncvar${c1}units
ncvarsize<-ncvar${c1}varsize
nc_variable_info<-c(variable, ncvarname, ncvarid, ncvardims, ncvarunits, ncvarsize)

nc_variable_info
}
#
# Main
#
varmeanfile<-'${chkfold}/checking/${fvarname}_1Dmean.nc'
varstdfile<-'${chkfold}/checking/${fvarname}_1Dstd.nc'
varname<-'${ncfvarname}'
filevarname<-'${fvarname}'
timename<-'time'
landmask<-'LANDMASK'
geofile<-'${chkfullfile}'
points<-c(${Rpts})
lonlatpt<-rep(0, 2*${Npts})
dim(lonlatpt)<-c(${Npts},2)
lonlatpt[1:${Npts},1]<-points[seq(1,${N2pts},2)]
lonlatpt[1:${Npts},2]<-points[seq(2,${N2pts},2)]

ncvarmean<-open.ncdf(varmeanfile)
ncvarstd<-open.ncdf(varstdfile)

varinf<-ncunit_variable_info(ncvarmean, varname)
shapevar<-varinf[4]
ncunits<-varinf[5]

if (shapevar == 3) {
  meanvariable<-get.var.ncdf(ncvarmean, varname, start=c(1,1,1), count=c(-1,-1,-1))
  stdvariable<-get.var.ncdf(ncvarstd, varname, start=c(1,1,1), count=c(-1,-1,-1))
}
if (shapevar == 4) {
  meanvariable<-get.var.ncdf(ncvarmean, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
  stdvariable<-get.var.ncdf(ncvarstd, varname, start=c(1,1,1,1), count=c(-1,-1,1,-1))
}

timeinf<-ncunit_dim_info(ncvarmean, timename)
timeunits<-timeinf[5]
timevar<-get.var.ncdf(ncvarmean, timename, start=c(1), count=c(-1))
close.ncdf(ncvarmean)
close.ncdf(ncvarstd)

pointPos<-lonlat_point(geofile, 'XLONG', 'XLAT', 'LANDMASK', lonlatpt)

meanvariable<-ifelse(meanvariable<=-9.e+32, NA, meanvariable)
stdvariable<-ifelse(stdvariable<=-9.e+32, NA, stdvariable)
meanval1<-meanvariable[pointPos[1,1],pointPos[1,2],]
meanval2<-meanvariable[pointPos[2,1],pointPos[2,2],]
stdval1<-stdvariable[pointPos[1,1],pointPos[1,2],]
stdval2<-stdvariable[pointPos[2,1],pointPos[2,2],]

mingraph1<-min(meanval1-stdval1, na.rm=TRUE)
maxgraph1<-max(meanval1+stdval1, na.rm=TRUE)
mingraph2<-min(meanval2-stdval2, na.rm=TRUE)
maxgraph2<-max(meanval2+stdval2, na.rm=TRUE)
mingraph<-min(mingraph1, mingraph2, maxgraph1, maxgraph2)
maxgraph<-max(mingraph1, mingraph2, maxgraph1, maxgraph2)

graphname<-paste("${chkfold}","/checking/",filevarname,"_dimx2-dimy2_timeserie.png", sep="")
graphtit<-paste(filevarname,"at",lonlatpt[1,1],"E ,",lonlatpt[1,2],"N &",lonlatpt[2,1],"E ,",
  lonlatpt[2,2],"N", sep=" ")
xlabel<-paste(filevarname,"(",ncunits,")", sep=" ")

bitmap(graphname)
##postscript(graphname, bg="white", horizontal=FALSE, paper="default")
plot (timevar, meanval1, "l", col="red", ylim=c(mingraph, maxgraph), main=graphtit, xaxs="i", 
  yaxs="i", xlab=timeunits, ylab=xlabel, lwd="2")
polygon(c(timevar, rev(timevar)), c(meanval1-stdval1, rev(meanval1+stdval1)), col=rgb(1., 0.5,
  0.5, 1.), border=NA)
polygon(c(timevar, rev(timevar)), c(meanval2-stdval2, rev(meanval2+stdval2)), col=rgb(0.5, 0.5,
  1., 1.), border=NA)
lines (timevar, meanval1, col="red", lwd="2")
lines (timevar, meanval2, col="blue", lwd="2")
points (timevar, meanval1, col="red", lwd="2")
points(timevar, meanval2, col="blue", lwd="2")

dev.off()
EOF

  ${Rexec} -f --slave ${chkfold}/checking/checking_1D_postprocess.R
  rm times0.inf times.inf

fi
