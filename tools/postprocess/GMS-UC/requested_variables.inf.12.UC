#MainFrame#
BASHsource /software/ScientificLinux/4.6/etc/bashrc
WRF4Ghome /oceano/gmeteo/users/lluis/WRF4G_viejo
p_interp /oceano/gmeteo/users/lluis/WRF4G_viejo/util/postprocess/Fortran/p_interp
DomainNetCDF /oceano/gmeteo/WORK/ASNA/WRF/domains/ESCENA-UCLM2/geo_em.d01.nc
EXPDIR /oceano/gmeteo/WORK/ASNA/projects/escena/12_ECHAM_A2_phA/data/raw
POSTDIR /oceano/gmeteo/WORK/ASNA/projects/escena/12_ECHAM_A2_phA/data/raw/postUC/2041-2050
POST2DIR /oceano/gmeteo/WORK/ASNA/projects/escena/12_ECHAM_A2_phA/data/postUC/2041-2050
POST3HDIR /oceano/gmeteo/WORK/ASNA/projects/escena/12_ECHAM_A2_phA/data/postUC_3h/2041-2050
DIAGDIR diag
FIGSDIR $(pwd)/figs
OBSDIR /oceano/gmeteo/WORK/ASNA/projects/escena/123/data/obs
postprocessor CLIMATICBASIC
FULLnc /oceano/gmeteo/WORK/ASNA/projects/escena/_multiexperiments/scripts/postprocess/ESCENA_full.nc
FirstEXPy 2001
LastEXPy 2050
overlap_yearmon 201211:202511:203711
checking 0
VarnameSecFile 7
ptstime 5:40:10:50
Rhome /software/CentOS/5.2/R/2.11.1/bin/R

#GeneralNames#
@experiment@ UCAN_WRA_EC5R2_A2 scne5/scne5

#Periods#
@per10y1@ 2001-2010
@per10y2@ 2011-2020
@per10y3@ 2021-2030
@per10y4@ 2031-2040
@per10y5@ 2041-2050
@per10y6@ 2000-2001

#Variables#
# T2               out        SFC         24H_00:00       per10y5
# U10ER            out        SFC         24H_00:00       per10y5
# V10ER            out        SFC         24H_00:00       per10y5
# Q2               out        SFC         24H_00:00       per10y5
# RH2              out        SFC         24H_00:00       per10y5
# TDPS             out        SFC         24H_00:00       per10y5
#  MSLP             out        SFC         24H_00:00       per10y5
# PSFC             out        SFC         24H_00:00       per10y5
# SFCEVP           out        SFC         24H_00:00       per10y5
# ACHFX            out        SFC         24H_00:00       per10y5
# ACLHF            out        SFC         24H_00:00       per10y5
# SKINTEMP         out        SFC         24H_00:00       per10y5
# SST              out        SFC         24H_00:00       per10y5
# SPDUV10          out        SFC         24H_00:00       per10y5
# RH2              out        SFC         24H_00:00       per10y5
# RAINF            out        SFC         24H_00:00       per10y5
# RAINNC           out        SFC         24H_00:00       per10y5
# RAINC            out        SFC         24H_00:00       per10y5
# VIM              out        SFC         24H_00:00       per10y5
# SNOW             out        SFC         24H_00:00       per10y5
# CLTFR            out        SFC         24H_00:00       per10y5
# GRDFLX           out        SFC         24H_00:00       per10y5
# RSS              out        SFC         24H_00:00       per10y5
# ACSWDNB          out        SFC         24H_00:00       per10y5
# ACRLS            out        SFC         24H_00:00       per10y5
# RLSKIN           out        SFC         24H_00:00       per10y5
# GLW              out        SFC         24H_00:00       per10y5
# RST              out        SFC         24H_00:00       per10y5
# ACSWDNT          out        SFC         24H_00:00       per10y5
# OLR              out        SFC         24H_00:00       per10y5
# SMOIS1           out        SFC         24H_00:00       per10y5
# SMSTOT           out        SFC         24H_00:00       per10y5
# ACSNOW           out        SFC         24H_00:00       per10y5
# SFROFF           out        SFC         24H_00:00       per10y5
# GHT              out       1000         24H_00:00       per10y5
# GHT              out        975         24H_00:00       per10y5
# GHT              out        925         24H_00:00       per10y5
# GHT              out        850         24H_00:00       per10y5
# GHT              out        700         24H_00:00       per10y5
# GHT              out        600         24H_00:00       per10y5
# GHT              out        500         24H_00:00       per10y5
# GHT              out        400         24H_00:00       per10y5
# GHT              out        300         24H_00:00       per10y5
# GHT              out        250         24H_00:00       per10y5
# GHT              out        200         24H_00:00       per10y5
# GHT              out        100         24H_00:00       per10y5
# RH               out       1000         24H_00:00       per10y5
# RH               out        975         24H_00:00       per10y5
# RH               out        925         24H_00:00       per10y5
# RH               out        850         24H_00:00       per10y5
# RH               out        700         24H_00:00       per10y5
# RH               out        600         24H_00:00       per10y5
# RH               out        500         24H_00:00       per10y5
# RH               out        400         24H_00:00       per10y5
# RH               out        300         24H_00:00       per10y5
# RH               out        250         24H_00:00       per10y5
# RH               out        200         24H_00:00       per10y5
# RH               out        100         24H_00:00       per10y5
# UER              out       1000         24H_00:00       per10y5
# UER              out        975         24H_00:00       per10y5
# UER              out        925         24H_00:00       per10y5
# UER              out        850         24H_00:00       per10y5
# UER              out        700         24H_00:00       per10y5
# UER              out        600         24H_00:00       per10y5
# UER              out        500         24H_00:00       per10y5
# UER              out        400         24H_00:00       per10y5
# UER              out        300         24H_00:00       per10y5
# UER              out        250         24H_00:00       per10y5
# UER              out        200         24H_00:00       per10y5
# UER              out        100         24H_00:00       per10y5
# VER              out       1000         24H_00:00       per10y5
# VER              out        975         24H_00:00       per10y5
# VER              out        925         24H_00:00       per10y5
# VER              out        850         24H_00:00       per10y5
# VER              out        700         24H_00:00       per10y5
# VER              out        600         24H_00:00       per10y5
# VER              out        500         24H_00:00       per10y5
# VER              out        400         24H_00:00       per10y5
# VER              out        300         24H_00:00       per10y5
# VER              out        250         24H_00:00       per10y5
# VER              out        200         24H_00:00       per10y5
# VER              out        100         24H_00:00       per10y5
# TEMP             out       1000         24H_00:00       per10y5
 TEMP             out        975         24H_00:00       per10y5
 TEMP             out        925         24H_00:00       per10y5
 TEMP             out        850         24H_00:00       per10y5
 TEMP             out        700         24H_00:00       per10y5
 TEMP             out        600         24H_00:00       per10y5
 TEMP             out        500         24H_00:00       per10y5
 TEMP             out        400         24H_00:00       per10y5
 TEMP             out        300         24H_00:00       per10y5
 TEMP             out        250         24H_00:00       per10y5
 TEMP             out        200         24H_00:00       per10y5
 TEMP             out        100         24H_00:00       per10y5
