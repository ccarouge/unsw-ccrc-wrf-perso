# NETCDF
module load hdf5/1.8.11-intel
module load netcdf/4.2.1-intel
# NCO
module load nco/4.3.2
# CDO
module load cdo/1.6.1
# Python
module load python/2.7.5
# Pyclimate
##export PYTHONPATH='/home/z3393242/bin/i11.1.080_PyClimate-1.2.2/lib/python2.7/'
export PYTHONPATH='/home/z3236814/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/bin/storms/i11.1.080_PyClimate-1.2.2/lib/python2.7/'
# R
module load R/2.15.3
