# NETCDF
module load hdf5/1.8.8-intel
module load netcdf/4.1.3-intel
# NCO
module load nco/4.0.5-intel
# CDO
module load cdo/1.5.4
# Python
module load python/2.7.2
# Pyclimate
##export PYTHONPATH='/home/z3393242/bin/i11.1.080_PyClimate-1.2.2/lib/python2.7/'
export PYTHONPATH='/home/z3393242/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/bin/storms/i11.1.080_PyClimate-1.2.2/lib/python2.7/'
# R
module load R/2.14.2
